package APIRateLimiter;

import java.util.Random;

// problems to consider:
// 1. what is the handle request take lots of time? then we may not be able to actually handle all the request in time
// 2. try to use a queue to handle it instead of a simple counter. producer - consumer on the queue, queue will handled by cluster

// what we do not know:
// 1. when will the next request come in? dense or not?
// 2. how long will each request take? what if it takes long time?

public class Limiter {
//    use a counter to record current count in this second

    private int count = 0;

    private int Limit = 3;

    private long currentMillis = System.currentTimeMillis();

    public boolean handleRequest(Request request, Handler handler) {
//        for each incoming request, decide whether to handle it or not

        long requestTime = System.currentTimeMillis();

        if ((requestTime - currentMillis) >= 1000) {
//            reset counter and handle request
            count = 1;
            currentMillis = requestTime;
            System.out.println("\nNew second, " + count + " handle at " + System.currentTimeMillis());
            handler.handle(request);
            return true;
        } else {
//            if it is in current second, check whether it is within the limit
            if (count >= Limit) {
                System.out.println("\nOver the limit, "+ count+"  reject");
                return false;
            } else {
                count += 1;
                System.out.println("\nWithin limit, "+ count+" handle at " + System.currentTimeMillis());
                handler.handle(request);
                return true;
            }
        }
    }

    public static void main(String[] args) {
//        double check whether it is thread safe
        Limiter limiter = new Limiter();
        limiter.setLimit(5);

        Handler handler = new Handler();

        int n = 1;

        while(n < 500){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Request request = new Request();
                        limiter.handleRequest(request,handler);
                        Thread.sleep((long) (new Random().nextFloat()* 2000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            n++;

        }

    }

    public int getLimit() {
        return Limit;
    }

    public void setLimit(int limit) {
        Limit = limit;
    }
}
