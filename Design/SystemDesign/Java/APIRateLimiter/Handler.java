package APIRateLimiter;

import java.util.Random;

public class Handler {

    public void handle(Request request) {
        // thread sleep a random time
        Random random = new Random();

        try {
            Thread.sleep((long) (random.nextFloat() * 100));
            System.out.println("\n Request handled and finished at " + System.currentTimeMillis());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
