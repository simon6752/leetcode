package HitCounter;

import java.util.*;

public class HitCounter2 {


    // SNAKE
    // Senario: hit with timestamp, and get hits. need to save hit timestamp.
    // order is important, but absolute value is not important
    // new solution: use rotation array to override old timestamp

     public static int size = 300;
     private int[] hits = new int[size];
     private int[] timestamp = new int[size];

    public HitCounter2(){
        //initialization
        for(int i = 0;i<300;i++){
            timestamp[i] = i;
        }
    }

    public void hit(int time){
        // save this hit time
        // save it to time% 300. if timestamp[time%300] < time, means time is over, need to update timestamp and hits
        int i = time % 300;
        if(timestamp[i] != time){
            // timestamp is too old, need to update
            timestamp[i] = time ;
            hits[i] = 1;
        }else{
            hits[i] += 1 ;
        }
    }

    // get hits at time
    public int getHits(int time){
        // return hits in last 5 mins
        // iterate over the timestamp
        int sum = 0 ;
        for(int i = 0;i<300;i++){
            if(timestamp[i]> time-300){
                sum += hits[i];
            }
        }
        return sum ;
    }


    public static void main(String[] args){
        // test
        System.out.println("start test");

        HitCounter2 counter = new HitCounter2();

        // hit at timestamp 1.
        counter.hit(1);

        // hit at timestamp 2.
        counter.hit(2);

        // hit at timestamp 3.
        counter.hit(3);

        // get hits at timestamp 4, should return 3.
        assert counter.getHits(4) == 3;

        // hit at timestamp 300.
        counter.hit(300);

        // get hits at timestamp 300, should return 4.
        assert counter.getHits(300)==4;

        // get hits at timestamp 301, should return 3.
        assert counter.getHits(301)==3;
    }

}
