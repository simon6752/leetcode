package HitCounter;

import java.util.LinkedList;
import java.util.Queue;

public class HitCounter{
    // SNAKE
    // Senario: hit with timestamp, and get hits. need to save hit timestamp.
    // order is important, but absolute value is not important
    // naive solution: save all timestamp in a queue, and delete order than 5 minutes

    // public static int size = 300;

    public Queue<Integer> hits;
    public HitCounter(){
        //initialization
        hits = new LinkedList<Integer>();
    }

    public void hit(int time){
        // save this hit time
        hits.offer(time);

    }

    // get hits at time
    public int getHits(int time){
        // return hits in last 5 mins
        // check all numbers in queue, if older than 5 minutes, remove
        while(!hits.isEmpty() && time - hits.peek().intValue() >= 300){
            hits.poll();
        }
        return hits.size() ;
    }


    public static void main(String[] args){
        // test
        System.out.println("start test");

        HitCounter counter = new HitCounter();

        // hit at timestamp 1.
        counter.hit(1);

        // hit at timestamp 2.
        counter.hit(2);

        // hit at timestamp 3.
        counter.hit(3);

        // get hits at timestamp 4, should return 3.
        assert counter.getHits(4) == 3;

        // hit at timestamp 300.
        counter.hit(300);

        // get hits at timestamp 300, should return 4.
        assert counter.getHits(300)==4;

        // get hits at timestamp 301, should return 3.
        assert counter.getHits(301)==3;
    }


}