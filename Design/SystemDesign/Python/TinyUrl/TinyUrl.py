# 535. Encode and Decode TinyURL
# DescriptionHintsSubmissionsDiscussSolution
# Note: This is a companion problem to the System Design problem: Design TinyURL.
# TinyURL is a URL shortening service where you enter a URL such as https://leetcode.com/problems/design-tinyurl and it returns a short URL such as http://tinyurl.com/4e9iAk.

# Design the encode and decode methods for the TinyURL service. There is no restriction on how your encode/decode algorithm should work. You just need to ensure that a URL can be encoded to a tiny URL and the tiny URL can be decoded to the original URL



class Codec:
    
    def __init__(self):
        self.counter = 0
        self.dicturltoid = {} # key:value "www.google.com" : 183829
        self.dictidtourl = {} # key:value 183829: "www.google.com"
        self.base = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    def encode(self, longUrl):
        """Encodes a URL to a shortened URL.
        
        :type longUrl: str
        :rtype: str
        """
        "first check whether it is in dictionary. if yes,  find the id and convert to base62 shorurl. if no, assign it an id, convert to base62 shorturl, save id to hashmap, return it"
        
        if longUrl in self.dicturltoid:
            return self.base10tobase62(self.dicturltoid[longUrl])
        else:
            self.counter += 1 
            self.dicturltoid[longUrl] = self.counter
            self.dictidtourl[self.counter] = longUrl
            return self.base10tobase62(self.counter)
        
    def decode(self, shortUrl):
        """Decodes a shortened URL to its original URL.
        
        :type shortUrl: str
        :rtype: str
        """
        "first convert shortUrl to id, then check whether in dict"
        id = int(self.base62tobase10(shortUrl))
        if id in self.dictidtourl:
            return self.dictidtourl[id]
        else:
            return ""
        
        
    def base10tobase62(self,id):
        
        result = ""
        
        while id > 0:
            result += self.base[id%62]
            id = id//62
        return result
        
        
    def base62tobase10(self,str):
        "convert str to base 10 integer, then get it from hash table"
        
        "say we have a2B4c, "
        
        result = 0
        
        for i in range(0,len(str)):
            
            if ord(str[i]) >= ord("0") and ord(str[i]) <= ord("9"):
                digit = ord(str[i]) - ord("0")
            elif ord(str[i]) >= ord("a") and ord(str[i]) <= ord("z"):
                digit = ord(str[i]) - ord("a") + 10
            elif ord(str[i]) >= ord("A") and ord(str[i]) <= ord("Z"):
                digit = ord(str[i]) -ord("A") + 36
                
            result = result*62 + digit
        
        return result
                
        

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(url))


url = "www.google.com"
codec =  Codec() 
assert codec.decode(codec.encode(url)) == url
