//204. Singleton
//        Singleton is a most widely used design pattern. If a class has and only has one instance at every moment, we call this design as singleton. For example, for class Mouse (not a animal mouse), we should design it in singleton.
//
//        You job is to implement a getInstance method for given class, return the same instance of this class every time you call this method.
//
//        Example
//        In Java:
//
//        A a = A.getInstance();
//        A b = A.getInstance();
//        a should equal to b.
//
//        Challenge
//        If we call getInstance concurrently, can you make sure your code could run correctly?

package OOD.Singleton;

class SingletonSolution {

    private static SingletonSolution singletonSolution = null;
    //    use object as a mutex
    private static final Object object = new Object();

    // private constructor
    private SingletonSolution(){

    }

    /**
     * @return: The same instance of this class every time
     */
    public static SingletonSolution getInstance() {
        // write your code here
        SingletonSolution result = singletonSolution;

        // line A
        if (result == null) {
            // line B
            synchronized (object) {

                result = singletonSolution;
                //"we need to check result is null for one more time since another thread may created new object after A before line B !! "
                if (result == null) {
                    singletonSolution = new SingletonSolution();
                    result = singletonSolution;
                }

            }
        }

        return result;
    }


};