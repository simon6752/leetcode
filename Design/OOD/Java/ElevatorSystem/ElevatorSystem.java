package OOD.ElevatorSystem;

import java.util.List;

public class ElevatorSystem {

    private List<Elevator> elevators;

    private ExternalRequestHandler externalRequestHandler = new ExternalRequestHandler();
    private InternalRequestHandler internalRequestHandler = new InternalRequestHandler();

    public ElevatorSystem(List<Elevator> elevators) {
        this.elevators = elevators;
    }

    public ElevatorSystem() {

    }

    // need scheduler, use strategy pattern
    public void HandleRequest( Request request){
        // first, find an appropriate elevator to handle this request
        // check each elevator status, capacity, running direction
        Elevator elevator = new Elevator(10,100);

        if(request instanceof ExternalRequest) {
           // request from admin: start, stop
            externalRequestHandler.HandleRequest(elevator,request);
        }else{
            internalRequestHandler.HandleRequest(elevator,request);
        }
    }
}

