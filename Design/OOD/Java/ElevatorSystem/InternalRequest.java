package OOD.ElevatorSystem;

public class InternalRequest  implements  Request{

    private  int floorDestination;

    public int getFloorDestination() {
        return floorDestination;
    }

    public void setFloorDestination(int floorDestination) {
        this.floorDestination = floorDestination;
    }

    public RunningDirection getDirection() {
        return direction;
    }

    public void setDirection(RunningDirection direction) {
        this.direction = direction;
    }

    private RunningDirection direction;

    InternalRequest(int floorDestination, RunningDirection direction){
        this.floorDestination = floorDestination;
        this.direction = direction;
    }
}
