package OOD.ElevatorSystem;

public class ExternalRequest implements Request {

    public ElevatorStatus getTargetElevatorStatus() {
        return targetElevatorStatus;
    }

    public void setTargetElevatorStatus(ElevatorStatus targetElevatorStatus) {
        this.targetElevatorStatus = targetElevatorStatus;
    }

    private ElevatorStatus targetElevatorStatus;

    public ExternalRequest(ElevatorStatus targetElevatorStatus) {
        this.targetElevatorStatus = targetElevatorStatus;
    }
}
