public class Board{

    // design it as Singleton

    private static Board instance ;

    private  static Object object;

    private static int size = 3 ;

    private static int[size][size] status = new int[size][size]();

    private Board(){
        // need to initialize board size

    }

    public static Board getInstance(){
        if(instance == null){
            synchronize(object){
                if(instance == null){
                    instance = new Board();
                }
            }
        }

        return instance ;
    }

}