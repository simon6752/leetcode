package OOD.BookingSystem;
import java.util.*;
public class BookingSystem {
    // make in Singleton 

    // question:
    // 1. is it web backend? UI let user select hotel and rooms? Yes, it is online booking system
    // 2. or the system dispatch room to user according to requirement

    // use lazy initialization, make it thread safe
    private static BookingSystem instance  ; 

    private static Object object = new Object();

    private BookingSystem(){


    }

    public static BookingSystem getInstance(){
        if(instance == null ){
            synchronized(object) {
                if(instance == null ){
                    instance = new BookingSystem();
                }
            }
        }

        return instance ;
    }

    private List<Hotel> hotels;

    public List<Hotel> getHotesl(){
        return hotels;
    }

    public void setHotels(List<Hotel> hotels){
        this.hotels = hotels;
    }

}



