package OOD.BookingSystem 


public class Room{

    private int ID;
    // which hotel it belongs to 
    private Hotel hotel ;

    private int Capacity;

    public bool Booked;

    public Room(int ID, int Capacity){
        this.ID = ID;
        this.Capacity = Capacity;
    }

    public Hotel getHotel(){
        return this.hotel;
    }

    public void setHotel(Hotel hotel ){
        this.hotel = hotel;
    }

}

