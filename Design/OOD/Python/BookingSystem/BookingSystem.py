# class
# booking system, booking, customer, room, hotel 

# 1. web UI + backend api???
# 2. no UI, system dispatch a room for customer according to the requirement

# a booking should contain, time, location, how many people, start and end date, room type
import datetime
from enum import Enum 

class RoomType(Enum):
    
    Single = 1
    Double = 2 
    Triple = 3 

class BookingSystem:
    
    # make it Singleton, feed parameter later
    __instance = None

    def __init__(self):
        pass  
        
    def setHotels(self,hotels):
        self.hotels = hotels

    @staticmethod
    def getInstance():
        if __instance is None:
            __instance= BookingSystem()
        return __instance

    def getListOfHotels(self):
        "return all hotels in the system so that customer can choose"
        return self.hotels 

    # to do

    def book(self,customer,hotel,checkin, checkout, roomtype):
        # assign a room for customer according to the hotel and date, room type chosen
        # if success, return a booking object, otherwise return None
        pass 


class Booking:
    
    def __init__(self, checkin, checkout, customers,rooms ,location = None ):
        # a customer may reserve one or multiple rooms for multiple room
        pass 


class Customer:

    def __init__(self,id, fname,lname,sex,dob,email):
        self.id = id
        self.fname = fname
        self.lname = lname 
        self.sex = sex 
        self.dob = dob       
        self.email = email 


class Hotel:
    
    # rooms is a list of rooms
    def __init__(self,rooms):
        self.rooms = rooms

class Room:
    
    # each room has its capacity and availability on each date
    # for availability, use hash table, key is date, value is a list of customers of that date
    def __init__(self,capacity):
        self.capacity = capacity
        self.availability = {}
        # initialize today's availability to no customer
        self.availability[datetime.date.today()] = []


# test case 

customer1 = Customer(1,"John","Wong","M", datetime.date(1988,1,1),"johnw1@gmail.com" )
rooms = []
room1 = Room(1)
room2 = Room(2)
room3 = Room(3)
rooms.append(room1)
rooms.append(room2)
rooms.append(room3)
hotel1 = Hotel( rooms )
hotels = [hotel1]
bookingsystem = BookingSystem() 
bookingsystem.setHotels(hotels)


# challenge: multi threading