# Problem:

Design an elevator system for a building

- ask questions about core objects
-  behaviors 

## follow SOLID principle
- Single responsibility principle
a class should have a single responsibility 

- Open close principle

open to inheritance, close to modification

-  Liskov substitution principle 

objects can be replaced by its subtypes 

-  Interface segregation  principle 

more client specific interfaces is better than one general purpose interface

-  Dependency inversion principle 

general depends on abstraction, not the opposite
