#  460. Find K Closest Elements
# Description
# Given a target number, a non-negative integer k and an integer array A sorted in ascending order, find the k closest numbers to target in A, sorted in ascending order by the difference between the number and target. Otherwise, sorted in ascending order by number if the difference is same.
#
# The value k is a non-negative integer and will always be smaller than the length of the sorted array.
# Length of the given array is positive and will not exceed 10^4
# Absolute value of elements in the array and x will not exceed 10^4
# Have you met this question in a real interview?
# Example
# Given A = [1, 2, 3], target = 2 and k = 3, return [2, 1, 3].
#
# Given A = [1, 4, 6, 8], target = 3 and k = 3, return [4, 1, 6].
#
# Challenge
# O(logn + k) time complexity.


class Solution:
	"""
	@param A: an integer array
	@param target: An integer
	@param k: An integer
	@return: an integer array
	"""
	
	def kClosestNumbers (self, A, target, k):
		# write your code here
		# k cloest, since it is ordered, we can first find closest use binary search (O(log(n))), then check left side and right side, use two pointers. So binary search + two pointers O(log(n)+k)
		
		if A is None or len (A) == 0 or k is None or target is None or k <= 0:
			return []
		
		if len (A) <= k:
			# we can not directly return A, we need to update k = len(A), only need to find len(A) cloest, also need to return result in the order that closest to target, not original order
			k = len (A)
		
		start = 0
		end = len (A) - 1
		mid = start + (end - start) // 2
		while start + 1 < end:
			mid = start + (end - start) // 2
			if A [mid] > target:
				end = mid
			elif A [mid] < target:
				start = mid
			else:
				break
			
			# either A[mid] == target, or start == end or start == end - 1
		
		if A [mid] == target:
			closest = mid
		elif start == end - 1:
			# closest in start and end
			if abs (target - A [start]) <= abs (target - A [end]):
				closest = start
			else:
				closest = end
		
		p1 = closest - 1
		p2 = closest + 1
		# two pointers
		
		if k == 1:
			return A [closest]
		
		count = 1
		result = [A [closest]]
		# we need to find total k
		while count < k:
			# find candidate
			if p1 >= 0 and p2 < len (A):
				# we have two candidate
				if target - A [p1] <= A [p2] - target:
					result.append (A [p1])
					p1 -= 1
				else:
					result.append (A [p2])
					p2 += 1
			elif p1 < 0:
				result.append (A [p2])
				p2 += 1
			else:
				result.append (A [p1])
				p1 -= 1
			
			count += 1
		
		return result


assert Solution().kClosestNumbers([1,2,3],2,3) == [2,1,3]

assert Solution().kClosestNumbers([1,4,6,8],3,3) == [4,1,6]