# 159. Find Minimum in Rotated Sorted Array
#  Description
#  Notes
#  Testcase
#  Judge
# Suppose a sorted array is rotated at some pivot unknown to you beforehand.
#
# (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
#
# Find the minimum element.
#
#  Notice
# You may assume no duplicate exists in the array.
#
# Have you met this question in a real interview?
# Example
# Given [4, 5, 6, 7, 0, 1, 2] return 0


class Solution:
    """
    @param nums: a rotated sorted array
    @return: the minimum number in the array
    """

    def findMin(self, nums):
        # write your code here
        "binary search: if larger/smaller, go right/left"

        start = 0
        end = len(nums) - 1

        flagstart = False
        flagend = False

        while start + 1 < end:
            mid = start + (end - start) // 2

            if nums[mid] > nums[start]:
                start = mid
                flagstart = True
            elif nums[mid] < nums[end]:
                end = mid
                flagend = True

        "if start or end not move at all, means either side distance to peek is 0 or 1, minium exists in 0, 1, len-1, len-2"
        if flagstart is False or flagend is False:
            return min(nums[0], nums[-1], nums[1], nums[-2])

        return nums[end]


assert Solution().findMin([4, 5, 6, 7, 0, 1, 2]) == 0
assert Solution().findMin([1, 2, 3]) == 1
