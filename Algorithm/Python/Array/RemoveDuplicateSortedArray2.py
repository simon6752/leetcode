# Remove Duplicates from Sorted Array II
# 描述
# Follow up for "Remove Duplicates": What if duplicates are allowed at most twice?
# For example, given sorted array A = [1,1,1,2,2,3], your function should return length = 5, and A is now [1,1,2,2,3]


class Solution:
    """
    @param: nums: An ineger array
    @return: An integer
    """

    def removeDuplicates(self, nums):
        # write your code here

        "if less than 2, special case"
        if len(nums) < 3:
            return len(nums)

        "start from 3nd one(index is 2)"
        tail = 2

        for head in range(2, len(nums)):

            if not (nums[head] == nums[tail - 2]):
                " if nums[head] == nums[tail-2] which means we already recorded 2 nums[head], need to skip"
                "we need to record this"
                nums[tail] = nums[head]
                tail = tail + 1

        return nums[0:tail]


assert Solution().removeDuplicates([-8, 0, 1, 2, 3]) == [-8, 0, 1, 2, 3]

assert Solution().removeDuplicates([1, 1, 1, 2, 2, 3]) == ([1, 1, 2, 2, 3])
