#  Description
#  Notes
#  Testcase
#  Judge
# Suppose a sorted array is rotated at some pivot unknown to you beforehand.
#
# (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
#
# Find the minimum element.
#
#  Notice
# The array may contain duplicates.
#
# Have you met this question in a real interview?
# Example
# Given [4,4,5,6,7,0,1,2] return 0.


class Solution:
    """
    @param nums: a rotated sorted array
    @return: the minimum number in the array
    """

    def findMin(self, nums):
        # write your code here
        "still use binary search, but we need to handle when there are duplicates"

        "target is the position where left is maxium, current is mininum"

        "need to ask what to return in this case"
        # if nums == None or len(nums) == 0:
        #     return -1

        if len(nums) == 1:
            return nums[0]

        "find position k where nums[k-1] >= nums[0] and nums[k] <= nums[-1]"

        start = 0
        end = len(nums) - 1
        "use nums[end] as target since minimum is in the same half as end"
        while start + 1 < end:
            mid = start + (end - start) // 2

            if nums[mid] < nums[end]:
                "in right half, left move end"
                end = mid
            elif nums[mid] > nums[end]:
                "in left half, right move start"
                start = mid
            else:
                "need to consider what if nums[mid] == nums[end], left start or right end is the same:  1 1 1 1 1 1 2  3  4  4 4 4 4 5  6  7 0 0  1 1 1 1 1 1 1, nums[mid] == 1, we canot decide whether it is in start or end, no idea how to move start or end. "
                "it is always safe to remove last one to find minimum, in worst case, it is O(n) and fake binary search"
                end = end - 1

        if nums[start] <= nums[end]:
            return nums[start]
        else:
            return nums[end]


assert Solution().findMin([1, 1, 1, 1, 1, -1, 1]) == -1
