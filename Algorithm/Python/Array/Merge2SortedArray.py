# Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.
#
# Note:
# You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2. The number of elements initialized in nums1 and nums2 are m and n respectively.

class Solution:
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead."""
        "use two pointers"

        p1 = m - 1
        p2 = n - 1
        p3 = m + n - 1

        while p2 >= 0 and p1 >= 0:
            if nums1[p1] > nums2[p2]:
                nums1[p3] = nums1[p1]
                p1 = p1 - 1
            else:
                nums1[p3] = nums2[p2]
                p2 = p2 - 1

            p3 = p3 - 1

        for i in range(0, p2 + 1):
            nums1[i] = nums2[i]

        return nums1


assert Solution().merge([1, 2, 3, 0, 0, 0], 3, [2, 3, 5], 3) == [1, 2, 2, 3, 3, 5]

# 64. Merge Sorted Array
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given two sorted integer arrays A and B, merge B into A as one sorted array.
#
#  Notice
# You may assume that A has enough space (size that is greater or equal to m + n) to hold additional elements from B. The number of elements initialized in A and B are m and n respectively.
#
# Have you met this question in a real interview?
# Example
# A = [1, 2, 3, empty, empty], B = [4, 5]
#
# After merge, A will be filled as [1, 2, 3, 4, 5]
#
# Tags
# Sorted Array Microsoft Bloomberg Array Facebook


class Solution2:
    """
    @param: A: sorted integer array A which has m elements, but size of A is m+n
    @param: m: An integer
    @param: B: sorted integer array B which has n elements
    @param: n: An integer
    @return: nothing
    """

    def mergeSortedArray(self, A, m, B, n):
        # write your code here
        "3 pointers, do it from end of array"

        if A is None or len(A) == 0:
            return B

        if B is None or len(B) == 0:
            return A

        p1 = m - 1
        p2 = n - 1

        p3 = m + n - 1

        "append B to A"

        while p3 >= 0:
            "p1 or p2 may reach to 0, need to handle"

            if p2 < 0:
                A[p3] = A[p1]
                p3 -= 1
                p1 -= 1
            elif p1 < 0:
                A[p3] = B[p2]
                p3 -= 1
                p2 -= 1
            elif A[p1] >= B[p2]:
                A[p3] = A[p1]
                p3 -= 1
                p1 -= 1
            elif A[p1] < B[p2]:
                A[p3] = B[p2]
                p3 -= 1
                p2 -= 1

        return A


