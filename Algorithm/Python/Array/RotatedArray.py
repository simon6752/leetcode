class Solution(object):

    def reverse(self, nums, start, end):
        temp = 0
        while start < end:
            temp = nums[start]
            nums[start] = nums[end]
            nums[end] = temp
            start = start + 1
            end = end - 1

    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        if nums == None or len(nums) <= 1:
            return
        m = len(nums)

        "in case of overwritten, exchange two positions that should have each other's value"
        "in order to exchange position "

        "save the last one in case of overwritten"

        "reverse, reverse, then reverse"

        k = k % m
        self.reverse(nums, m - k, m - 1)
        self.reverse(nums, 0, m - k - 1)
        self.reverse(nums, 0, m - 1)


nums = [1, 2]

Solution().rotate(nums, 1)

assert nums == [2, 1]
