#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Follow up for Search in Rotated Sorted Array:
#
# What if duplicates are allowed?
#
# Would this affect the run-time complexity? How and why?
#
# Write a function to determine if a given target is in the array.
#
# Have you met this question in a real interview?
# Example
# Given [1, 1, 0, 1, 1, 1] and target = 0, return true.
# Given [1, 1, 1, 1, 1, 1] and target = 0, return false.
#
# Tags
# Binary Search Array Sorted Array


class Solution:
    """
    @param A: an integer ratated sorted array and duplicates are allowed
    @param target: An integer
    @return: a boolean
    """

    def search(self, A, target):
        # write your code here
        "binary search, need to handle duplication, worst case is O(n)"

        if A == None or len(A) == 0:
            return False

        start = 0
        end = len(A) - 1

        if A[start] == target or A[end] == target:
            return True

        if target < A[start] and target > A[end]:
            "check wheter target is in this region, this will save us conditions in below"
            return False

        "now we can be sure that target may be at left side or right side"

        " need to compare A[mid], target, and A[end], A[start]"
        "if no duplicates, we can be sure that A[start] > Alast. If there is duplicate, A[start] may be equal to Alast"

        while start + 1 < end:
            mid = start + (end - start) // 2

            "since start or end maybe updated during while loop, we need to compare A[start]/A[end] with loop"
            if A[mid] == target or A[start] == target or A[end] == target:

                return True
            elif A[mid] > target:
                if target > A[start]:
                    "target at left side : A[mid] > A[target] > A[start] >= A[end]"
                    end = mid
                else:
                    "we not sure whether   target < A[mid] <=  A[end] <= A[start] or target <= A[end] <= A[start] <= A[mid] or A[end] < target < A[start] <= A[mid] "

                    if A[mid] < A[end]:
                        end = mid
                    elif A[mid] > A[start]:
                        start = mid
                    elif A[mid] == A[start] or A[mid] == A[end]:
                        if A[start] == A[end]:
                            "target < A[mid] = A[start] = A[end], do not know where to go. target is at right side. it is alwasy safe to move end left for one step. "
                            end = end - 1
                        else:
                            "target < A[mid]  = A[end] < A[start]    or target <= A[end] < A[start] = A[mid]"
                            if A[mid] == A[end]:
                                end = mid
                            elif A[mid] == A[start]:
                                start = mid

            else:
                if target < A[end]:
                    "target at right side: A[mid] <  target < A[end] <= A[start] "
                    start = mid
                else:
                    "not sure A[mid] <= A[end] <= A[start] < target  or A[end] <= A[start] <= A[mid] < target or A[mid] <= A[end] <= target < A[start]"
                    if A[mid] < A[end]:
                        "A[mid] < A[end] <= A[start] < target"
                        end = mid
                    elif A[mid] > A[start]:
                        "A[end] <= A[start] < A[mid] < target"
                        start = mid
                    elif A[mid] == A[start] or A[mid] == A[end]:
                        if A[start] == A[end]:
                            "A[mid] = A[start] = A[end] < target, we do not know where to go. target is at left side. In this case, it is alwasy safe to move start one step more"
                            start = start + 1
                        else:
                            " A[mid] = A[end] < A[start] < target or A[end] < A[start] = A[mid] < target"
                            if A[mid] == A[end]:
                                end = mid
                            elif A[mid] == A[start]:
                                start = mid

        "then target maybe in start or end"
        if A[start] == target or A[end] == target:
            return True

        return False


assert Solution().search([4, 5, 1, 2, 3], 1) == True
assert Solution().search([4, 5, 1, 2, 3], 0) == False
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4,
     4, 4, ], 0) == False
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4,
     4, 4, ], 1) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4,
     4, 4, ], 2) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4,
     4, 4, ], 3) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4,
     4, 4, ], 4) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4,
     4, 4, ], 5) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4,
     4, 4, ], 6) == False

assert Solution().search(
    [4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4], 0) == False
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4], 1) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4], 2) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4], 3) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4], 4) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4], 5) == True
assert Solution().search(
    [4, 4, 4, 4, 4, 4, 5, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4], 6) == False
assert Solution().search([1, 1, 0, 1, 1, 1], 0) == True
assert Solution().search([1, 1, 1, 1, 1, 1], 0) == False
