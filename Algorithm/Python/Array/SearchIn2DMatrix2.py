# Write an efficient algorithm that searches for a value in an m x n matrix, return the occurrence of it.
#
# This matrix has the following properties:
#
# Integers in each row are sorted from left to right.
# Integers in each column are sorted from up to bottom.
# No duplicate integers in each row or column.
# Have you met this question in a real interview? Yes
# Example
# Consider the following matrix:
#
# [
#   [1, 3, 5, 7],
#   [2, 4, 7, 8],
#   [3, 5, 9, 10]
# ]
# Given target = 3, return 2.
#
# Challenge
# O(m+n) time and O(1) extra space

class Solution:
    """
    @param: matrix: A list of lists of integers
    @param: target: An integer you want to search in matrix
    @return: An integer indicate the total occurrence of target in the given matrix
    """

    def searchMatrix(self, matrix, target):
        # write your code here
        "find the edge from lower left to upper right. the line or this edge are either slightly larger or smaller than target. "

        "if current number is equal to target, move upper right(since this in undetermined. if current larger(smaller), go up(right)"

        if matrix == None or len(matrix) == 0 or len(matrix[0]) == 0:
            return 0

        count = 0

        "row number"
        m = len(matrix)
        "column number"
        n = len(matrix[0])

        "start from left lower corner"
        posi = m - 1
        posj = 0

        while posi >= 0 and posi < m and posj >= 0 and posj < n:
            if matrix[posi][posj] == target:
                count = count + 1
                "move up right"
                posi = posi - 1
                posj = posj + 1
            elif matrix[posi][posj] > target:
                "move up"
                posi = posi - 1
            else:
                "move right"
                posj = posj + 1

        return count


assert Solution().searchMatrix([[1, 3, 5, 7], [2, 4, 7, 8], [3, 5, 9, 10]], 3) == 2
