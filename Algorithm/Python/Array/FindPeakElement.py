# There is an integer array which has the following features:
#
# The numbers in adjacent positions are different.
# A[0] < A[1] && A[A.length - 2] > A[A.length - 1].
# We define a position P is a peak if:
#
# A[P] > A[P-1] && A[P] > A[P+1]
# Find a peak element in this array. Return the index of the peak.
#
#  Notice
# It's guaranteed the array has at least one peak.
# The array may contain multiple peeks, find any of them.
# The array has at least 3 numbers in it.
# Have you met this question in a real interview?
# Example
# Given [1, 2, 1, 3, 4, 5, 7, 6]
#
# Return index 1 (which is number 2) or 6 (which is number 7)

class Solution:
    """
    @param: A: An integers array.
    @return: return any of peek positions.
    """

    def findPeak(self, A):
        # write your code here
        "binary search"

        if len(A) == 3:
            return A[1] > A[0] and A[1] > A[2]

        start = 0
        end = len(A) - 1

        while start + 1 < end:
            mid = start + (end - start) // 2
            "if mid is peak"
            if A[mid] > A[mid - 1] and A[mid] > A[mid + 1]:
                return mid

            "if mid is bottom"
            if A[mid] < A[mid - 1] and A[mid] < A[mid + 1]:
                end = mid

            "then mid is in the middle of mid + 1 and mid - 1"

            if A[mid] > A[mid - 1]:
                start = mid
            else:
                end = mid


result = Solution().findPeak([1, 2, 1, 3, 4, 5, 7, 6])
assert result == 1 or result == 6
