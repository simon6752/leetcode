# Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
#
# You may assume NO duplicates in the array.
#
# Have you met this question in a real interview?
# Example
# [1,3,5,6], 5 → 2
#
# [1,3,5,6], 2 → 1
#
# [1,3,5,6], 7 → 4
#
# [1,3,5,6], 0 → 0
#
# Challenge


class Solution:
    """
    @param A: an integer sorted array
    @param target: an integer to be inserted
    @return: An integer
    """

    def searchInsert(self, A, target):
        # write your code here

        "binary search, we need to find insert position when target is not found"

        if A == None or len(A) == 0:
            return 0

        start = 0
        end = len(A) - 1

        while start + 1 < end:
            mid = start + (end - start) // 2

            if A[mid] == target:
                return mid
            elif A[mid] > target:
                end = mid
            else:
                start = mid

        "not found yet, check if target is in start or end"

        if A[start] == target:
            return start

        if A[end] == target:
            return end

        "still not found in start and end, need to check order of A[start], A[end] and target"

        if target < A[start]:
            "insert before start"
            return start

        if target > A[end]:
            "insert at end"
            return end + 1

        return end


assert Solution().searchInsert([1, 3, 5, 6], 5) == 2
assert Solution().searchInsert([1, 3, 5, 6], 2) == 1
assert Solution().searchInsert([1, 3, 5, 6], 7) == 4
assert Solution().searchInsert([1, 3, 5, 6], 0) == 0
