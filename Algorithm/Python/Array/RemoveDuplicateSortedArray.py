# 描述
#
# Given a sorted array, remove the duplicates in place such that each element appear only once and return the new length.
# Do not allocate extra space for another array, you must do this in place with constant memory.
# For example, Given input array A = [1,1,2],
# Your function should return length = 2, and A is now [1,2].


class Solution(object):
    def RemoveDuplicateFromArray(self, nums):

        if len(nums) == 0:
            return (0, [])

        tail = 0

        for head in range(0, len(nums)):
            if head == 0:
                continue

            "not the same as previous one, new number"
            if nums[head] != nums[head - 1]:
                nums[tail + 1] = nums[head]
                "move tail"
                tail = tail + 1

        return (tail + 1, nums[0:tail + 1])


assert Solution().RemoveDuplicateFromArray([1, 1, 2]) == (2, [1, 2])
