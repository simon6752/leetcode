# 62. Search in Rotated Sorted Array
#  Description
#  Notes
#  Testcase
#  Judge
# Suppose a sorted array is rotated at some pivot unknown to you beforehand.
#
# (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
#
# You are given a target value to search. If found in the array return its index, otherwise return -1.
#
# You may assume no duplicate exists in the array.
#
# Have you met this question in a real interview?
# Example
# For [4, 5, 1, 2, 3] and target=1, return 2.
#
# For [4, 5, 1, 2, 3] and target=0, return -1.
#
# Challenge
# O(logN) time


class Solution:
    """
    @param A: an integer rotated sorted array
    @param target: an integer to be searched
    @return: an integer
    """

    def search(self, A, target):
        # write your code here

        "binary search, drop half of elements using the trend"

        if A == None or len(A) == 0:
            return -1

        start = 0
        end = len(A) - 1
        "we need to compare value of A[mid] with target, and A[0]"

        A0 = A[0]
        Alast = A[-1]

        while start + 1 < end:
            mid = start + (end - start) // 2
            if A[mid] == target:
                return mid
            elif A[mid] > target:
                "depends target is at left half or right half, we need to update end to the right of target, we drop the element after A[mid], or we update start to left of target and drop elements before A[mid]"
                if target >= A0:
                    "target at left half"
                    "A[mid] > target >= A0"
                    end = mid
                else:
                    "two possibilities here: A[mid] >= A0 or A[mid] < A0, left or right side"
                    if A[mid] >= A0:
                        start = mid
                    else:
                        end = mid
            else:
                "depends target is at left half or right half, we need to update end to the right of target, we drop the element after A[mid], or we update start to left of target and drop elements before A[mid]"
                if target >= A0:
                    " two possibility here: A[mid] >= A0 and A[mid] < A0"
                    if A[mid] >= A0:
                        start = mid
                    else:
                        end = mid
                else:
                    "A[mid] < target < A0"
                    start = mid

        "only two elements left, and target still not found yet"

        if A[start] == target:
            return start
        elif A[end] == target:
            return end

        "not found"
        return -1


assert Solution().search([4, 5, 1, 2, 3], 1) == 2
assert Solution().search([4, 5, 1, 2, 3], 0) == -1
