# Given a sorted array of n integers, find the starting and ending position of a given target value.
#
# If the target is not found in the array, return [-1, -1].
#
# Have you met this question in a real interview?
# Example
# Given [5, 7, 7, 8, 8, 10] and target value 8,
# return [3, 4].


class Solution:
    """
    @param A: an integer sorted array
    @param target: an integer to be inserted
    @return: a list of length 2, [index1, index2]
    """

    def searchRange(self, A, target):
        # write your code here

        "binary search, need to find first and last target element"

        return [self.searchFirstLast(A, target, 0, len(A) - 1, True),
                self.searchFirstLast(A, target, 0, len(A) - 1, False)]

    def searchFirstLast(self, A, target, start, end, findFirst):

        result = -1

        if A == None or len(A) == 0 or start > end:
            "not found"
            return result

        while start + 1 < end:
            mid = start + (end - start) // 2

            if A[mid] == target:
                "depends on find first or last target, we assign mid to end or start"
                if findFirst:
                    end = mid
                else:
                    start = mid

            elif A[mid] > target:
                end = mid
            else:
                start = mid

        "check whether start or end is our first or last target"
        if A[start] == target:
            if (findFirst) or (not findFirst and A[end] != target):
                result = start

        if A[end] == target:
            if (findFirst and A[start] != target) or (not findFirst):
                result = end

        return result


assert Solution().searchRange([5, 7, 7, 8, 8, 10], 8) == [3, 4]
