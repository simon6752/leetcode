# There are two sorted arrays nums1 and nums2 of size m and n respectively.
#
# Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
#
# Example 1:
# nums1 = [1, 3]
# nums2 = [2]
#
# The median is 2.0
# Example 2:
# nums1 = [1, 2]
# nums2 = [3, 4]
#
# The median is (2 + 3)/2 = 2.5

import sys


class Solution:
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """

        "find kth(k = (m+n)/2) in two sorted arrays, use binary search"
        "compare A[k/2-1] and B[k/2-1], then remove the smaller k/2 every time"

        length = len(nums1) + len(nums2)

        if length % 2 != 0:
            return self.findkthSortedArray(nums1, 0, nums2, 0, (length + 1) // 2)
        else:
            return (self.findkthSortedArray(nums1, 0, nums2, 0, (length) // 2 + 1) + self.findkthSortedArray(nums1, 0,
                                                                                                             nums2, 0, (
                                                                                                                 length) // 2)) / 2

    def findkthSortedArray(self, nums1, start1, nums2, start2, k):

        "no element to remove in nums1, kth in nums2"
        if start1 >= len(nums1):
            return nums2[start2 + k - 1]
        "no elements to remove in nums2, kth in nums1"
        if start2 >= len(nums2):
            return nums1[start1 + k - 1]

        if k == 1:
            "no need to move k//2 ==0 element or figure out which k//2 element to remove"
            return min(nums1[start1], nums2[start2])

        "find the minumum boundary between start + k//2 and len(num)" \
        "assume that we have added integer.max to end of the shorter array( len(num) - start < k//2)"
        key1 = nums1[start1 + k // 2 - 1] if start1 + k // 2 - 1 < len(nums1) else sys.maxsize
        key2 = nums2[start2 + k // 2 - 1] if start2 + k // 2 - 1 < len(nums2) else sys.maxsize

        if key1 > key2:
            "we can remove first k/2 element in nums2"
            return self.findkthSortedArray(nums1, start1, nums2, start2 + k // 2, k - k // 2)
        else:
            "if key1 = key2, we can give up either k//2 from num1 or num2, here we choose nums1"
            return self.findkthSortedArray(nums1, start1 + k // 2, nums2, start2, k - k // 2)


assert Solution().findMedianSortedArrays([1, 3], [2]) == 2
assert Solution().findMedianSortedArrays([1, 2], [1, 2]) == 1.5
