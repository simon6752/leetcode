# 1183. Single Element in a Sorted Array
# Given a sorted array consisting of only integers where every element appears twice except for one element which appears once. Find this single element that appears only once.

# Example 1:

# Input: [1,1,2,3,3,4,4,8,8]
# Output: 2
# Example 2:

# Input: [3,3,7,7,10,11,11]
# Output: 10
# Example
# Your solution should run in O(log n) time and O(1) space.

class Solution:
    """
    @param nums: a list of integers
    @return: return a integer
    """
    def singleNonDuplicate(self, nums):
        # write your code here
        # bit operation, XOR, but it is O(n)
        
        result = 0
        
        for i in range(0,len(nums)):
            result ^= nums[i]
            
        return result

# my solution use binary search 

class Solution2:
    """
    @param nums: a list of integers
    @return: return a integer
    """
    def singleNonDuplicate(self, nums):
        # write your code here
        # bit operation, XOR, but it is O(n)
        # since it is sorted, we can try to use binary search 
        
        # do binary search on odd and even index, the difference should be the index 
        # 0 0 1 1 2 2 3  3  4  4 5 5 .. (k-1) (k-1) k (k+1) (k+1) .   (n-1 )  (n-1)  total 2*n - 1 
        # 0 1 2 3 4 5 6  7  8  9 10 11 ...2*(k-1) 2*(k-1)+1  2*k 2*k+1 ... 2n-3, 2n-2
        
        if len(nums) == 1:
            return nums[0]
            
        n = (len(nums)+1)//2 
        
        start = 0 
        end = n-1 
        
        while start < end:
            mid = start + (end-start)//2 
            
            if nums[mid*2] == nums[mid*2 + 1]:
                # the extra element is in the right side 
                start = mid + 1 
            elif nums[mid*2] < nums[mid*2 + 1]:
                # extra element is in the left side 
                end = mid 
            
        return nums[2*end]