# 692. Sliding Window Unique Elements Sum
# Description
# Given an array and a window size that is sliding along the array, find the sum of the count of unique elements in each window.
#
# Have you met this question in a real interview?
# Example
# Given a array nums = [1, 2, 1, 3, 3] and k = 3
#
# First window [1, 2, 1], only 2 is unique, count is 1.
# Second window [2, 1, 3], all elements unique, count is 3.
# Third window [1, 3, 3], only 1 is unique, count is 1.
# sum of count = 1 + 3 + 1 = 5
#
# Return 5

class Solution:
	"""
	@param nums: the given array
	@param k: the window size
	@return: the sum of the count of unique elements in each window
	"""
	
	def slidingWindowUniqueElementsSum (self, nums, k):
		# write your code here
		
		# naive solution not good
		# need to keep track of key count in a hash table, a current unique key:count table
		
		if nums is None or k < 0:
			return 0
		
		start = 0
		end = min (len (nums), k)
		
		currentCount = {}
		currentUnique = set ()
		
		# use two pointers, start and end
		
		for i in range (start, end):
			if nums [i] not in currentCount.keys ():
				currentCount [nums [i]] = 1
				currentUnique.add (nums [i])
			else:
				currentCount [nums [i]] += 1
				if nums [i] in currentUnique:
					currentUnique.remove (nums [i])
		
		# calculate count
		count = len (currentUnique)
		res = count
		
		start += 1
		end += 1
		while end <= len (nums):
			
			# keep moving
			# double check current end-1(newly added), and start - 1 (newly removed)
			if nums [end - 1] not in currentCount.keys ():
				currentCount [nums [end - 1]] = 1
				currentUnique.add (nums [end - 1])
			else:
				currentCount [nums [end - 1]] += 1
				if nums [end - 1] in currentUnique:
					currentUnique.remove (nums [end - 1])
			
			currentCount [nums [start - 1]] -= 1
			if currentCount [nums [start - 1]] == 1:
				# count to 1, become unique
				currentUnique.add (nums [start - 1])
			elif currentCount [nums [start - 1]] == 0:
				# count to 0 now, remove from unique, also remove from count
				del currentCount [nums [start - 1]]
				if nums [start - 1] in currentUnique:
					currentUnique.remove (nums [start - 1])
			
			count = len (currentUnique)
			
			res += count
			
			start += 1
			end += 1
		
		return res


assert Solution().slidingWindowUniqueElementsSum([1,2,1,3,3],3) == 5
