# 627. Longest Palindrome
# Description
# Given a string which consists of lowercase or uppercase letters, find the length of the longest palindromes that can be built with those letters.

# This is case sensitive, for example "Aa" is not considered a palindrome here.

# Assume the length of given string will not exceed 1010.

# Have you met this question in a real interview?  
# Example
# Given s = "abccccdd" return 7

# One longest palindrome that can be built is "dccaccd", whose length is 7.

class Solution:
    """
    @param s: a string which consists of lowercase or uppercase letters
    @return: the length of the longest palindromes that can be built
    """
    def longestPalindrome(self, s):
        # write your code here
        # 
        
        if s is None: 
            return 0 
            
        # count frequency 
        count = {}
        for i in range(0,len(s)):
            if s[i] not in count.keys():
                count[s[i]] = 1 
            else:
                count[s[i]] += 1 
        
        length = 0 
        oddExist = False 
        for key in count.keys():
            # check count 
            if count[key] % 2 != 0 :
                oddExist = True 
                length += count[key] -1 
            # if count is even, add it, if it is odd, add frequncy -1 
            else:
                length += count[key]
            
        if oddExist:
            # we can add one more if there is an odd count 
            length += 1 
            
        return length
