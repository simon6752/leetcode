# 1320. Contains Duplicate
# Description
# Given an array of integers, find if the array contains any duplicates. Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.

# Have you met this question in a real interview?  
# Example
# Given nums = [1,1], return ture.


class Solution:
    """
    @param nums: the given array
    @return: if any value appears at least twice in the array
    """
    def containsDuplicate(self, nums):
        # Write your code here
        
        # use hash set is enough
        
        if nums is None or len(nums) < 2 :
            return False 
            
        distinct = set()
        
        for i in range(0,len(nums)):
            if nums[i] not in distinct:
                distinct.add(nums[i])
            else:
                return True 
                
        return False