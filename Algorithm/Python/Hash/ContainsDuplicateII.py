# 1319. Contains Duplicate II
# Given an array of integers and an integer k, find out whether there are two distinct indices i and j in the array such that nums[i] = nums[j] and the absolute difference between i and j is at most k.

# Example
# Given nums = [1,2,1], k = 0, return false.


class Solution:
    """
    @param nums: the given array
    @param k: the given number
    @return:  whether there are two distinct indices i and j in the array such that nums[i] = nums[j] and the absolute difference between i and j is at most k
    """
    def containsNearbyDuplicate(self, nums, k):
        # Write your code here
        
        # naive solution, put all numbers into hash table, key is integer, value is array of indices 
        # then calculate distance 
        
        if nums is None or len(nums) < 2:
            return False 
            
        if k == 0 :
            return False 
            
        count = {}
        
        for i in range(0,len(nums)):
            if nums[i] in count.keys():
                # already in it, save it and calculate differences 
                if i - count[nums[i]][-1] <= k :
                    return True 
                else:
                    # not yet, add it to hash table 
                    count[nums[i]].append(i)
            else:
                # not in it, add it 
                count[nums[i]] = [i]    
            
        return False 