
# 134. LRU Cache
# Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and set.

# get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
# set(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.


# LRUCache(2)
# set(2, 1)
# set(1, 1)
# get(2)
# set(4, 1)
# get(1)
# get(2)

 


class LRUCache:
    
    # we need to sort object by its visit time, also make it unique 
    # 
    
    """
    @param: capacity: An integer
    """
    def __init__(self, capacity):
        # do intialization if necessary
        self.capacity = capacity
        # use hash table to keep inserted values 
        self.hm = {}
        
        # also need to keep an order use visited time
        # every visited item should be the latest visted. every time we delete item when exeeds maximum capacity we delete to least recent visited. 
        # need to sort
        # delete from head, newly visted to end, use double linked list 
        # from head to tail, time stamp increases. every time an item is visited, it becomes the latest visted with largest time stamp, move it to tail. Every time size is full, remove least recently accessed item, which is near the head.
        # initialize an empty linked List 
        self.head = DoubleListNode(-1,-1)
        self.tail = DoubleListNode(-1,-1)
        self.head.next = self.tail 
        self.tail.prev = self.head 

    """
    @param: key: An integer
    @return: An integer
    """
    def get(self, key):
        # write your code here
        
        # see if key is in cache, if yes, return, also put in to tail 
        # if not, we get it somewhere else? database ? or return -1 here 
        
        if key in self.hm:
            # get it from cache 
            node = self.hm[key]
            
            # disconnect from list 
            node.prev.next = node.next 
            node.next.prev = node.prev
            
            # put node to tail 
            self.moveToTail(node)
            
            return node.value
        else:
            return -1 

    """
    @param: key: An integer
    @param: value: An integer
    @return: nothing
    """
    def set(self, key, value):
        # write your code here
        
        if key in self.hm:
            # set value 
            self.hm[key].value = value  
            # also need to disconnect and move to tail
            self.get(key)
            return 
        else:
            # not in it, lets insert 
            # before insert, check size
            # remove least recently used item, which is near the head
            if len(self.hm) == self.capacity:
                temp = self.head.next
                self.head.next = self.head.next.next
                self.head.next.prev = self.head  
                del self.hm[temp.key]
            
            node = DoubleListNode(key,value )
            self.hm[key] = node 
            # need to put this node into right position of the double linked list 
            # move it to tail, which is most recent position
            self.moveToTail(node)
            
            return 
            
    
    def moveToTail(self,node):
        
        self.tail.prev.next = node 
        node.prev = self.tail.prev 
        node.next = self.tail 
        self.tail.prev= node 
        
class DoubleListNode:
    
    def __init__(self,key,value):
        self.key = key 
        self.value = value 
        self.next = None 
        self.prev = None 

# LRUCache(2)
# set(2, 1)
# set(1, 1)
# get(2)
# set(4, 1)
# get(1)
# get(2)
# Expected
# [1,-1,1]

cache = LRUCache(2)
cache.set(2, 1)
cache.set(1, 1)
assert cache.get(2) == 1
cache.set(4, 1)
assert cache.get(1) == -1 
assert cache.get(2) == 1
# Expected
# [1,-1,1]

cache = LRUCache(105)
cache.set(33, 219)
assert cache.get(39) == -1
cache.set(96, 56)
assert cache.get(129) == -1
assert cache.get(115) == -1
assert cache.get(112) == -1
cache.set(3, 280)
assert cache.get(40) == -1
cache.set(85, 193)
cache.set(10, 10)
cache.set(100, 136)
cache.set(12, 66)
cache.set(81, 261)
cache.set(33, 58)
assert cache.get(3) == 280
cache.set(121, 308)
cache.set(129, 263)
assert cache.get(105) == -1
cache.set(104, 38)
cache.set(65, 85)
cache.set(3, 141)
cache.set(29, 30)
cache.set(80, 191)
cache.set(52, 191)
cache.set(8, 300)
assert cache.get(136) == -1
cache.set(48, 261)
cache.set(3, 193)
cache.set(133, 193)
cache.set(60, 183)
cache.set(128, 148)
cache.set(52, 176)
assert cache.get(48) == 261
cache.set(48, 119)
cache.set(10, 241)
assert cache.get(124) == -1
cache.set(130, 127)
assert cache.get(61) == -1
cache.set(124, 27)
assert cache.get(94) == -1
cache.set(29, 304)
cache.set(102, 314)
assert cache.get(110) == -1
cache.set(23, 49)
cache.set(134, 12)
cache.set(55, 90)
assert cache.get(14) == -1
assert cache.get(104) == 38
cache.set(77, 165)
cache.set(60, 160)
assert cache.get(117) == -1
cache.set(58, 30)
assert cache.get(54) == -1
assert cache.get(136) == -1
assert cache.get(128) == 148
assert cache.get(131) == -1
cache.set(48, 114)
assert cache.get(136) == -1
cache.set(46, 51)
cache.set(129, 291)
cache.set(96, 207)
assert cache.get(131) == -1
cache.set(89, 153)
cache.set(120, 154)
assert cache.get(111) == -1
assert cache.get(47) == -1
assert cache.get(5) == -1
cache.set(114, 157)
cache.set(57, 82)
cache.set(113, 106)
cache.set(74, 208)
assert cache.get(56) == -1
assert cache.get(59) == -1
assert cache.get(100) == 136
assert cache.get(132) == -1
cache.set(127, 202)
assert cache.get(75) == -1
cache.set(102, 147)
assert cache.get(37) == -1
cache.set(53, 79)
cache.set(119, 220)
assert cache.get(47) == -1
assert cache.get(101) == -1
assert cache.get(89) == 153
assert cache.get(20) == -1
assert cache.get(93) == -1
assert cache.get(7) == -1
cache.set(48, 109)
cache.set(71, 146)
assert cache.get(43) == -1
assert cache.get(122) == -1
cache.set(3, 160)
assert cache.get(17) == -1
cache.set(80, 22)
cache.set(80, 272)
assert cache.get(75) == -1
assert cache.get(117) == -1
cache.set(76, 204)
cache.set(74, 141)
cache.set(107, 93)
cache.set(34, 280)
cache.set(31, 94)
assert cache.get(132) == -1
cache.set(71, 258)
assert cache.get(61) == -1
assert cache.get(60) == 160
cache.set(69, 272)
assert cache.get(46) == 51
cache.set(42, 264)
cache.set(87, 126)
cache.set(107, 236)
cache.set(131, 218)
assert cache.get(79) == -1
cache.set(41, 71)
cache.set(94, 111)
cache.set(19, 124)
cache.set(52, 70)
assert cache.get(131) == 218
assert cache.get(103) == -1
assert cache.get(81) == 261
assert cache.get(126) == -1
cache.set(61, 279)
cache.set(37, 100)
assert cache.get(95) == -1
assert cache.get(54) == -1
cache.set(59, 136)
cache.set(101, 219)
cache.set(15, 248)
cache.set(37, 91)
cache.set(11, 174)
cache.set(99, 65)
cache.set(105, 249)
assert cache.get(85) == 193
cache.set(108, 287)
cache.set(96, 4)
assert cache.get(70) == -1
assert cache.get(24) == -1
cache.set(52, 206)
cache.set(59, 306)
cache.set(18, 296)
cache.set(79, 95)
cache.set(50, 131)
cache.set(3, 161)
cache.set(2, 229)
cache.set(39, 183)
cache.set(90, 225)
cache.set(75, 23)
cache.set(136, 280)
assert cache.get(119) == 220
cache.set(81, 272)
assert cache.get(106) == -1
assert cache.get(106) == -1
assert cache.get(70) == -1
cache.set(73, 60)
cache.set(19, 250)
cache.set(82, 291)
cache.set(117, 53)
cache.set(16, 176)
assert cache.get(40) == -1
cache.set(7, 70)
cache.set(135, 212)
assert cache.get(59) == 306
cache.set(81, 201)
cache.set(75, 305)
assert cache.get(101) == 219
cache.set(8, 250)
assert cache.get(38) == -1
cache.set(28, 220)
assert cache.get(21) == -1
cache.set(105, 266)
assert cache.get(105) == 266
assert cache.get(85) == 193
assert cache.get(55) == 90
assert cache.get(6) == -1
cache.set(78, 83)
assert cache.get(126) == -1
assert cache.get(102) == 147
assert cache.get(66) == -1
cache.set(61, 42)
cache.set(127, 35)
cache.set(117, 105)
assert cache.get(128) == 148
assert cache.get(102) == 147
assert cache.get(50) == 131
cache.set(24, 133)
cache.set(40, 178)
cache.set(78, 157)
cache.set(71, 22)
assert cache.get(25) == -1
assert cache.get(82) == 291
assert cache.get(129) == 291
cache.set(126, 12)
assert cache.get(45) == -1
assert cache.get(40) == 178
assert cache.get(86) == -1
assert cache.get(100) == 136
cache.set(30, 110)
assert cache.get(49) == -1
cache.set(47, 185)
cache.set(123, 101)
assert cache.get(102) == 147
assert cache.get(5) == -1
cache.set(40, 267)
cache.set(48, 155)
assert cache.get(108) == 287
assert cache.get(45) == -1
cache.set(14, 182)
cache.set(20, 117)
cache.set(43, 124)
assert cache.get(38) == -1
cache.set(77, 158)
assert cache.get(111) == -1
assert cache.get(39) == 183
cache.set(69, 126)
cache.set(113, 199)
cache.set(21, 216)
assert cache.get(11) == 174
cache.set(117, 207)
assert cache.get(30) == 110
cache.set(97, 84)
assert cache.get(109) == -1
cache.set(99, 218)
assert cache.get(109) == -1
cache.set(113, 1)
assert cache.get(62) == -1
cache.set(49, 89)
cache.set(53, 311)
assert cache.get(126) == 12
cache.set(32, 153)
cache.set(14, 296)
assert cache.get(22) == -1
cache.set(14, 225)
assert cache.get(49) == 89
assert cache.get(75) == 305
cache.set(61, 241)
assert cache.get(7) == 70
assert cache.get(6) == -1
assert cache.get(31) == 94
cache.set(75, 15)
assert cache.get(115) == -1
cache.set(84, 181)
cache.set(125, 111)
cache.set(105, 94)
cache.set(48, 294)
assert cache.get(106) == -1
assert cache.get(61) == 241
cache.set(53, 190)
assert cache.get(16) == 176
cache.set(12, 252)
assert cache.get(28) == 220
cache.set(111, 122)
assert cache.get(122) == -1
cache.set(10, 21)
assert cache.get(59) == 306
assert cache.get(72) == -1
assert cache.get(39) == 183
assert cache.get(6) == -1
assert cache.get(126) == 12
cache.set(131, 177)
cache.set(105, 253)
assert cache.get(26) == -1
cache.set(43, 311)
assert cache.get(79) == 95
cache.set(91, 32)
cache.set(7, 141)
assert cache.get(38) == -1
assert cache.get(13) == -1
cache.set(79, 135)
assert cache.get(43) == 311
assert cache.get(94) == 111
cache.set(80, 182)
assert cache.get(53) == 190
cache.set(120, 309)
cache.set(3, 109)
assert cache.get(97) == 84
cache.set(9, 128)
cache.set(114, 121)
assert cache.get(56) == -1
assert cache.get(56) == -1
cache.set(124, 86)
cache.set(34, 145)
assert cache.get(131) == 177
assert cache.get(78) == 157
cache.set(86, 21)
assert cache.get(98) == -1
cache.set(115, 164)
cache.set(47, 225)
assert cache.get(95) == -1
cache.set(89, 55)
cache.set(26, 134)
cache.set(8, 15)
assert cache.get(11) == 174
cache.set(84, 276)
cache.set(81, 67)
assert cache.get(46) == 51
assert cache.get(39) == 183
assert cache.get(92) == -1
assert cache.get(96) == 4
cache.set(89, 51)
cache.set(136, 240)
assert cache.get(45) == -1
assert cache.get(27) == -1
cache.set(24, 209)
cache.set(82, 145)
assert cache.get(10) == 21
cache.set(104, 225)
cache.set(120, 203)
cache.set(121, 108)
cache.set(11, 47)
assert cache.get(89) == 51
cache.set(80, 66)
assert cache.get(16) == 176
cache.set(95, 101)
assert cache.get(49) == 89
assert cache.get(1) == -1
cache.set(77, 184)
assert cache.get(27) == -1
cache.set(74, 313)
cache.set(14, 118)
assert cache.get(16) == 176
assert cache.get(74) == 313
cache.set(88, 251)
assert cache.get(124) == 86
cache.set(58, 101)
cache.set(42, 81)
assert cache.get(2) == 229
cache.set(133, 101)
assert cache.get(16) == 176
cache.set(1, 254)
cache.set(25, 167)
cache.set(53, 56)
cache.set(73, 198)
assert cache.get(48) == 294
assert cache.get(30) == 110
assert cache.get(95) == 101
cache.set(90, 102)
cache.set(92, 56)
cache.set(2, 130)
cache.set(52, 11)
assert cache.get(9) == 128
assert cache.get(23) == 49
cache.set(53, 275)
cache.set(23, 258)
assert cache.get(57) == 82
cache.set(136, 183)
cache.set(75, 265)
assert cache.get(85) == 193
cache.set(68, 274)
cache.set(15, 255)
assert cache.get(85) == 193
cache.set(33, 314)
cache.set(101, 223)
cache.set(39, 248)
cache.set(18, 261)
cache.set(37, 160)
assert cache.get(112) == -1
assert cache.get(65) == 85
cache.set(31, 240)
cache.set(40, 295)
cache.set(99, 231)
assert cache.get(123) == 101
cache.set(34, 43)
assert cache.get(87) == 126
assert cache.get(80) == 66
cache.set(47, 279)
cache.set(89, 299)
assert cache.get(72) == -1
cache.set(26, 277)
cache.set(92, 13)
cache.set(46, 92)
cache.set(67, 163)
cache.set(85, 184)
assert cache.get(38) == -1
cache.set(35, 65)
assert cache.get(70) == -1
assert cache.get(81) == 67
cache.set(40, 65)
assert cache.get(80) == 66
cache.set(80, 23)
cache.set(76, 258)
assert cache.get(69) == 126
assert cache.get(133) == 101
cache.set(123, 196)
cache.set(119, 212)
cache.set(13, 150)
cache.set(22, 52)
cache.set(20, 105)
cache.set(61, 233)
assert cache.get(97) == 84
cache.set(128, 307)
assert cache.get(85) == 184
assert cache.get(80) == 23
assert cache.get(73) == 198
assert cache.get(30) == 110
cache.set(46, 44)
assert cache.get(95) == 101
cache.set(121, 211)
cache.set(48, 307)
assert cache.get(2) == 130
cache.set(27, 166)
assert cache.get(50) == 131
cache.set(75, 41)
cache.set(101, 105)
assert cache.get(2) == 130
cache.set(110, 121)
cache.set(32, 88)
cache.set(75, 84)
cache.set(30, 165)
cache.set(41, 142)
cache.set(128, 102)
cache.set(105, 90)
cache.set(86, 68)
cache.set(13, 292)
cache.set(83, 63)
cache.set(5, 239)
assert cache.get(5) == 239
cache.set(68, 204)
assert cache.get(127) == 35
cache.set(42, 137)
assert cache.get(93) == -1
cache.set(90, 258)
cache.set(40, 275)
cache.set(7, 96)
assert cache.get(108) == 287
cache.set(104, 91)
assert cache.get(63) == -1
assert cache.get(31) == 240
cache.set(31, 89)
assert cache.get(74) == 313
assert cache.get(81) == 67
cache.set(126, 148)
assert cache.get(107) == -1
cache.set(13, 28)
cache.set(21, 139)
assert cache.get(114) == 121
assert cache.get(5) == 239
assert cache.get(89) == 299
assert cache.get(133) == 101
assert cache.get(20) == 105
cache.set(96, 135)
cache.set(86, 100)
cache.set(83, 75)
assert cache.get(14) == 118
cache.set(26, 195)
assert cache.get(37) == 160
cache.set(1, 287)
assert cache.get(79) == 135
assert cache.get(15) == 255
assert cache.get(6) == -1
cache.set(68, 11)
assert cache.get(52) == 11
cache.set(124, 80)
cache.set(123, 277)
cache.set(99, 281)
assert cache.get(133) == 101
assert cache.get(90) == 258
assert cache.get(45) == -1
assert cache.get(127) == 35
cache.set(9, 68)
cache.set(123, 6)
cache.set(124, 251)
cache.set(130, 191)
cache.set(23, 174)
cache.set(69, 295)
assert cache.get(32) == 88
assert cache.get(37) == 160
cache.set(1, 64)
cache.set(48, 116)
assert cache.get(68) == 11
cache.set(117, 173)
cache.set(16, 89)
assert cache.get(84) == 276
cache.set(28, 234)
assert cache.get(129) == 291
assert cache.get(89) == 299
assert cache.get(55) == 90
assert cache.get(83) == 75
cache.set(99, 264)
assert cache.get(129) == 291
assert cache.get(84) == 276
assert cache.get(14) == 118
cache.set(26, 274)
assert cache.get(109) == -1
assert cache.get(110) == 121
cache.set(96, 120)
cache.set(128, 207)
assert cache.get(12) == 252
cache.set(99, 233)
cache.set(20, 305)
cache.set(26, 24)
cache.set(102, 32)
assert cache.get(82) == 145
cache.set(16, 30)
cache.set(5, 244)
assert cache.get(130) == 191
cache.set(109, 36)
cache.set(134, 162)
cache.set(13, 165)
cache.set(45, 235)
cache.set(112, 80)
assert cache.get(6) == -1
cache.set(34, 98)
cache.set(64, 250)
cache.set(18, 237)
cache.set(72, 21)
cache.set(42, 105)
cache.set(57, 108)
cache.set(28, 229)
assert cache.get(83) == 75
cache.set(1, 34)
cache.set(93, 151)
cache.set(132, 94)
cache.set(18, 24)
cache.set(57, 68)
cache.set(42, 137)
assert cache.get(35) == 65
assert cache.get(80) == 23
cache.set(10, 288)
assert cache.get(21) == 139
assert cache.get(115) == 164
assert cache.get(131) == 177
assert cache.get(30) == 165
assert cache.get(43) == 311
cache.set(97, 262)
cache.set(55, 146)
cache.set(81, 112)
cache.set(2, 212)
cache.set(5, 312)
cache.set(82, 107)
cache.set(14, 151)
assert cache.get(77) == 184
cache.set(60, 42)
cache.set(90, 309)
assert cache.get(90) == 309
cache.set(131, 220)
assert cache.get(86) == 100
cache.set(106, 85)
cache.set(85, 254)
assert cache.get(14) == 151
cache.set(66, 262)
cache.set(88, 243)
assert cache.get(3) == -1
cache.set(50, 301)
cache.set(118, 91)
assert cache.get(25) == 167
assert cache.get(105) == 90
assert cache.get(100) == -1
assert cache.get(89) == 299
cache.set(111, 152)
cache.set(65, 24)
cache.set(41, 264)
assert cache.get(117) == 173
assert cache.get(117) == 173
cache.set(80, 45)
assert cache.get(38) == -1
cache.set(11, 151)
cache.set(126, 203)
cache.set(128, 59)
cache.set(6, 129)
assert cache.get(91) == -1
cache.set(118, 2)
cache.set(50, 164)
assert cache.get(74) == 313
assert cache.get(80) == 45
cache.set(48, 308)
cache.set(109, 82)
cache.set(3, 48)
cache.set(123, 10)
cache.set(59, 249)
cache.set(128, 64)
cache.set(41, 287)
cache.set(52, 278)
cache.set(98, 151)
assert cache.get(12) == 252
assert cache.get(25) == 167
cache.set(18, 254)
cache.set(24, 40)
assert cache.get(119) == 212
cache.set(66, 44)
cache.set(61, 19)
cache.set(80, 132)
cache.set(62, 111)
assert cache.get(80) == 132
cache.set(57, 188)
assert cache.get(132) == 94
assert cache.get(42) == 137
cache.set(18, 314)
assert cache.get(48) == 308
cache.set(86, 138)
assert cache.get(8) == -1
cache.set(27, 88)
cache.set(96, 178)
cache.set(17, 104)
cache.set(112, 86)
assert cache.get(25) == 167
cache.set(129, 119)
cache.set(93, 44)
assert cache.get(115) == 164
cache.set(33, 36)
cache.set(85, 190)
assert cache.get(10) == 288
cache.set(52, 182)
cache.set(76, 182)
assert cache.get(109) == 82
assert cache.get(118) == 2
cache.set(82, 301)
cache.set(26, 158)
assert cache.get(71) == -1
cache.set(108, 309)
cache.set(58, 132)
cache.set(13, 299)
cache.set(117, 183)
assert cache.get(115) == 164
assert cache.get(89) == 299
assert cache.get(42) == 137
cache.set(11, 285)
cache.set(30, 144)
assert cache.get(69) == 295
cache.set(31, 53)
assert cache.get(21) == 139
cache.set(96, 162)
cache.set(4, 227)
cache.set(77, 120)
cache.set(128, 136)
assert cache.get(92) == -1
cache.set(119, 208)
cache.set(87, 61)
cache.set(9, 40)
cache.set(48, 273)
assert cache.get(95) == 101
assert cache.get(35) == 65
cache.set(62, 267)
cache.set(88, 161)
assert cache.get(59) == 249
assert cache.get(85) == 190
cache.set(131, 53)
cache.set(114, 98)
cache.set(90, 257)
cache.set(108, 46)
assert cache.get(54) == -1
cache.set(128, 223)
cache.set(114, 168)
cache.set(89, 203)
assert cache.get(100) == -1
assert cache.get(116) == -1
assert cache.get(14) == 151
cache.set(61, 104)
cache.set(44, 161)
cache.set(60, 132)
cache.set(21, 310)
assert cache.get(89) == 203
cache.set(109, 237)
assert cache.get(105) == 90
assert cache.get(32) == 88
cache.set(78, 101)
cache.set(14, 71)
cache.set(100, 47)
cache.set(102, 33)
cache.set(44, 29)
assert cache.get(85) == 190
assert cache.get(37) == 160
cache.set(68, 175)
cache.set(116, 182)
cache.set(42, 47)
assert cache.get(9) == 40
cache.set(64, 37)
cache.set(23, 32)
cache.set(11, 124)
cache.set(130, 189)
assert cache.get(65) == 24
cache.set(33, 219)
cache.set(79, 253)
assert cache.get(80) == 132
assert cache.get(16) == 30
cache.set(38, 18)
cache.set(35, 67)
assert cache.get(107) == -1
assert cache.get(88) == 161
cache.set(37, 13)
cache.set(71, 188)
assert cache.get(35) == 67
cache.set(58, 268)
cache.set(18, 260)
cache.set(73, 23)
cache.set(28, 102)
assert cache.get(129) == 119
assert cache.get(88) == 161
assert cache.get(65) == 24
assert cache.get(80) == 132
cache.set(119, 146)
assert cache.get(113) == -1
assert cache.get(62) == 267
cache.set(123, 138)
cache.set(18, 1)
cache.set(26, 208)
assert cache.get(107) == -1
assert cache.get(107) == -1
cache.set(76, 132)
cache.set(121, 191)
assert cache.get(4) == 227
assert cache.get(8) == -1
assert cache.get(117) == 183
cache.set(11, 118)
assert cache.get(43) == 311
assert cache.get(69) == 295
assert cache.get(136) == -1
cache.set(66, 298)
assert cache.get(25) == 167
assert cache.get(71) == 188
assert cache.get(100) == 47
cache.set(26, 141)
cache.set(53, 256)
cache.set(111, 205)
cache.set(126, 106)
assert cache.get(43) == 311
cache.set(14, 39)
cache.set(44, 41)
cache.set(23, 230)
assert cache.get(131) == 5

