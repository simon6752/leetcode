# 916. Palindrome Permutation
# Given a string, determine if a permutation of the string could form a palindrome.

# Example
# Given s = "code", return False.
# Given s = "aab", return True.
# Given s = "carerac", return True.

class Solution:
    """
    @param s: the given string
    @return: if a permutation of the string could form a palindrome
    """
    def canPermutePalindrome(self, s):
        # write your code here
        
        # the same as longest palindrome, if length of longest palindrome is full length, then yes 
        # hash table 
        # if only one odd frequency, then yes 
        
        if s is None:
            return True 
            
        count = {}
        
        for i in range(0,len(s)):
            if s[i] not in count.keys():
                count[s[i]] = 1 
            else:
                count[s[i]] += 1 
        
        countodd = 0 
        for key in count.keys():
            
            if count[key]% 2 != 0:
                countodd += 1 
        
        return countodd <= 1