# 	1. 3 x 3 board, each cell contains random letters. Start from any position, you can go 8 directions, but cannot go back to visited cells. Given a dictionary, find all words in this dictionary


#       B  C G 
#       F A  D 
#       Z Y A       

# For example, we can get BCAFZYZDG, CAF, etc. Given dictionary {"DAF", "AWZ", "DCFYA", ….}, find all words in this dictionary 

# 1. Consider board as a undirected graph, do DFS search 

class Solution:

    # board is 2 dimensional list
    def findWords(self, board, dictionary, n):
        
        if board is None:
            return None 
        
        result = set()
        for i in range(0, n):
        
            for j in range(0, n):
                str = ""
                visited =  {}
                self.dfs(i, j, str, result, n, board, visited,dictionary)
            
        return result 
    
    def dfs(self, i, j, str, result, n, board, visited,dictionary):
        if i < 0 or i >= n or j < 0 or j >= n:
            # cross boundary, return 
            return 
        if i * n + j in visited:
            # already visited
            return 
        visited[i * n + j] = 1
        str += board[i][j]
        if str in dictionary and str not in result:
            result.add(str)
        
        # check all 8 directions
        self.dfs(i + 1, j, str, result, n, board, visited,dictionary)
        self.dfs(i, j + 1, str, result, n, board, visited,dictionary)
        self.dfs(i-1, j, str, result, n, board, visited,dictionary)
        self.dfs(i, j-1, str, result, n, board, visited,dictionary)
        self.dfs(i + 1, j + 1, str, result, n, board, visited,dictionary)
        self.dfs(i + 1, j-1, str, result, n, board, visited,dictionary)
        self.dfs(i-1, j + 1, str, result, n, board, visited,dictionary)
        self.dfs(i-1, j-1, str, result, n, board, visited,dictionary)