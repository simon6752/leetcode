# kayak
# A man, a plan, a canal, Panama!

class Solution:
    
    def palindrom(self, str):
        
        # validate palindrome 
        if str is None or len(str) < 2:
            return True 
        
        # two pointers, time O(n), skip invalid char during iteration
        
        p1 = 0 
        p2 = len(str) -1 
        
        while p1 <= p2:
            
            # valid char between a-z and A-Z
            if str[p1].lower() > ord("z") or str[p1].lower() < ord("a"):
                p1 +=1 
                continue 
            
            if str[p2].lower() > ord("z") or str[p2].lower() < ord("a"):
                p2 -=1
                continue 
            
            if str[p1].lower() != str[p2].lower():
                return False
            else:
                p1 += 1 
                p2 -= 1
                
        
        return True 
            

assert Solution().palindrom("kayak") is True 
assert Solution().palindrom("A man, a plan, a canal, Panama!") is True


# =========================================================

# momanddad in the kayak => ["mom", "dad", "kayak"]


# naive solution O(n^2)
# palindrome, i = 0, j = len(str) -1 ,
# will do a conversion first, make it a valid string, remove spaces, to lowercase

class Solution2:
    
    def findAllPalindromes(self,str):
        
        if str is None:
            return []
        
        newstr = self.convert(str)
        
        # momanddadinthekayak, if we find a palindrome, start = i, end = j, we can expand our search from start = i-1 and end = j+1, reuse previous palindrome result, optimize 
        # challenge, center outwards, optimize to O(n) 
        
        # palindrome: kayak, abba
        # center, it might be a char position, it might be a space
        # char position, center 1 to -2
        # space position, center can be 0 to -2

        
        for centerpos in range(1, len(newstr)-1):
            # 1st round 
            # 
            pass 
        
        for centerpos in range(0,len(newstr)-1):
            # 2nd round
            pass 
        
        
        
    
    def convert(self,str):
        
        newstr = ""
        
        for i in range(0,len(str)):
            
            if ord(str[i].lower()) >= ord("a") and ord(str[i].lower()) <= ord("z"):
                newstr += str[i].lower()
                
        
        return newstr
        