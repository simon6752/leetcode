# 680. Split String
# Give a string, you can choose to split the string after one character or two adjacent characters, and make the string to be composed of only one character or two characters. Output all possible results.

# Example
# Given the string "123"
# return [["1","2","3"],["12","3"],["1","23"]]


class Solution:
    """
    @param: : a string to be split
    @return: all possible split string array
    """

    def splitString(self, s):
        # write your code here
        
        if s is None or len(s) == 0:
            return [[]]
            
        
        if len(s) == 1:
            return [[s]]
            
        "like Fibonacci"
        
        list1 = self.splitString(s[:-1])
        list2 = self.splitString(s[:-2])
        
        result = []
        
        for i in range(0,len(list1)):
            if list1[i] is not None:
                temp = list(list1[i])
                temp.append(s[-1])
                result.append(temp)
        
        for i in range(0,len(list2)):
            if list2[i] is not None:
                temp = list(list2[i])
                temp.append(s[-2:])
                result.append(temp)
                
        return result

assert Solution().splitString("123") == [["1","2","3"],["12","3"],["1","23"]]