# 420. Count and Say
# The count-and-say sequence is the sequence of integers beginning as follows:
#
# 1, 11, 21, 1211, 111221, ...
#
# 1 is read off as "one 1" or 11.
#
# 11 is read off as "two 1s" or 21.
#
# 21 is read off as "one 2, then one 1" or 1211.
#
# Given an integer n, generate the nth sequence.
#
# Example
# Given n = 5, return "111221".

class Solution:
    """
    @param n: the nth
    @return: the nth sequence
    """
    def countAndSay(self, n):
        # write your code here
        # 1, 11,21,1211,111221,... (count + digit) repeat
        # 0, 1, 11,21, 1211,...
        # count number until reach a different one

        numstr = "1"

        for i in range(0,n-1):
            "only need to run n-1 times"
            numstr = self.countAndSayHelepr(numstr)

        return numstr


    def countAndSayHelepr(self,numstr):
        "return count and say of current numstr "

        result = ""
        currentCount = 1
        currentNum = numstr[0]

        for i in range(1,len(numstr)):
            if numstr[i] == currentNum:
                "count + 1"
                currentCount += 1

            else:

                result += str(currentCount) + currentNum

                currentCount = 1
                currentNum = numstr[i]

        "add lastone's count and say"
        result += str(currentCount) + currentNum

        return result


