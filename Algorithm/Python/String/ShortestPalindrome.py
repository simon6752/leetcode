# 678. Shortest Palindrome
# Description
# Given a string S, you are allowed to convert it to a palindrome by adding characters in front of it. Find and return the shortest palindrome you can find by performing this transformation.

# Have you met this question in a real interview?  
# Example
# Given "aacecaaa", return "aaacecaaa".

# Given "abcd", return "dcbabcd".


class Solution:
    """
    @param str: String
    @return: String
    """
    def convertPalindrome(self, str):
        # Write your code here
        # start from mid char/space, move center left, record the 1st successful reach 1st char 
        # time: O(n)
        
        if str is None or len(str) < 2 :
            return str 
        
        # we need to remove space, invalid char, number, convert to lower. save it for now
        str = str.lower()
        mincount = 0 
        minStr = ""
        
        # for char 
        for startcenterpos in range((len(str)+1)//2 -1,-1,-1): 
            # len = 2, pos = 0 ; len = 3, pos = 1 
        
            start = startcenterpos
            end = startcenterpos
            
            while start >= 0 and end <= len(str) -1  :
                if str[start] == str[end]:
                    start -= 1 
                    end += 1 
                else:
                    break 
                
            if start == -1:
                # 1st to reach start = 0
                # how many letters we need to add 
                mincount = len(str) - 2*startcenterpos - 1
                minStr = str[2*startcenterpos+1:][::-1] + str 
                # break and only keep 1st 
                break
        
        # for space 
        # startcenterpos is the space to add, before index i 
        for startcenterpos in range(len(str)//2,0,-1):
            # len = 2, pos = 1, len = 3, pos = 1 
            
            start = startcenterpos -1 
            end = startcenterpos
            
            while start >= 0 and end <= len(str) - 1 :
                if str[start] == str[end]:
                    start -= 1 
                    end += 1 
                else:
                    break 
            
            if start == -1 :
                # 1st to reach start == 0 
                # how many letters we need to add 
                if len(str) - 2 * startcenterpos  < mincount:
                    # need to update mincount 
                    minStr = str[2*startcenterpos:][::-1]    + str 
                    
                # break since we only keep the first one reach the start index 
                break 
        
        return minStr 

# public class Solution {

#     public String shortestPalindrome(String s) {

#         int j = 0;

#         for (int i = s.length() - 1; i >= 0; i--) {//找到第一个使他不回文的位置

#            if (s.charAt(i) == s.charAt(j)) { 

#                j += 1; 

#            }

#         }

#         if (j == s.length()) {  //本身是回文

#             return s; 

#         }

#         String suffix = s.substring(j); // 后缀不能够匹配的字符串

#         String prefix = new StringBuilder(suffix).reverse().toString(); // 前面补充prefix让他和suffix回文匹配

#         String mid = shortestPalindrome(s.substring(0, j)); //递归调用找 [0,j]要最少可以补充多少个字符让他回文

#         String ans = prefix + mid  + suffix;

#         return  ans;

#     }

# }