# 891. Valid Palindrome II
# Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.

# Example
# Given s = "aba" return true
# Given s = "abca" return true // delete c

class Solution:
    """
    @param s: a string
    @return: nothing
    """
    def validPalindrome(self, s):
        # Write your code here
        
        # record skip times. if already skiped once, not possible 
        
        if s is None or len(s) <= 2 :
            return True 
            
        skip = 0 
        s = s.lower()
        start = 0 
        end = len(s) -1 
        
        while start <= end:
            
            if s[start] == s[end]:
                # good, move on 
                start += 1 
                end -= 1 
            else:
                # not good, 
                if skip == 1:
                    # already skipped
                    return False 
                else:
                    skip = 1 
                    if s[start+1] == s[end]:
                        start += 1 
                    elif s[start] == s[end-1]:
                        end -= 1 
                    else:
                        # no way to skip once and continue to be palindrom 
                        
                        return False 
                        
        return True 
        
