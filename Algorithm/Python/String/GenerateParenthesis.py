# 427. Generate Parentheses
# Description
# Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
#
# Have you met this question in a real interview?
# Example
# Given n = 3, a solution set is:
#
# "((()))", "(()())", "(())()", "()(())", "()()()"

class Solution:
    """
    @param n: n pairs
    @return: All combinations of well-formed parentheses
    """
    def generateParenthesis(self, n):
        # write your code here
        "use DFS, recursive"

        result = []
        stack = []
        parenthesesstr = ""
        self.generateParenthesisHelper(0,0, parenthesesstr,result,n)

        return result

    def generateParenthesisHelper(self, numleft,numright,currentStr,result,n):

        "we need to cut/stop during dfs. when do we stop?"

        if numleft == n and numright == n:
            "valid, return"
            result.append(currentStr)
            return

        if numleft > n or numright > n or numright > numleft:
            "invalid, return"
            return

        "what to add? depends on numleft and numright relation"

        if numright < numleft:
            "we can add a right, else we can add left and right"

            self.generateParenthesisHelper(numleft,numright+1,currentStr+")",result,n)

        "add a left"
        self.generateParenthesisHelper(numleft+1,numright,currentStr+"(",result,n)

