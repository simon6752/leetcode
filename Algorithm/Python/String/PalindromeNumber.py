# 491. Palindrome Number
# Description
# Check a positive number is a palindrome or not.

# A palindrome number is that if you reverse the whole number you will get exactly the same number.

# It's guaranteed the input number is a 32-bit integer, but after reversion, the number may exceed the 32-bit integer.

# Have you met this question in a real interview?  
# Example
# 11, 121, 1, 12321 are palindrome numbers.

# 23, 32, 1232 are not palindrome numbers.

class Solution:
    """
    @param num: a positive number
    @return: true if it's a palindrome or false
    """
    def isPalindrome(self, num):
        # write your code here
        "two pointers"
        num = str(num)
        
        if num is None or len(num) == 0:
            return True
            
        i = 0
        j = len(num)  - 1
        
        while j>=i:
            if num[i] != num[j]:
                return False
            
            i +=1
            j -=1
            
        return True



