# 640. One Edit Distance
# Given two strings S and T, determine if they are both one edit distance apart.

# Example
# Given s = "aDb", t = "adb"
# return true

class Solution:
    """
    @param s: a string
    @param t: a string
    @return: true if they are both one edit distance apart or false
    """
    def isOneEditDistance(self, s, t):
        # write your code here
        "first check length"
        
        if s is None:
            return t is None or len(t)<=1
        
        if t is None:
            return s is None or len(s) <= 1
        
        "now s and t are not None"
        
        lens = len(s)
        lent = len(t)
        
        if abs(lens-lent) > 1:
            return False 
        
        count = 0 
        if lens == lent:
            for i in range(0,lens):
                if s[i] != t[i]:
                    count += 1 
                    
            return count == 1 
        
        "then they must only differ in one letter "
        
        s1 = s 
        s2 = t
        if lens > lent:
            s1 = t  
            s2 = s 
            
        "s1 is always the shorter one "
        
        count = 0 
        m = 0 
        n = 0 
        for i in range(0,len(s1)):
            if s1[m] != s2[n]:
                n += 1 
                count += 1 
                if count > 1:
                    return False 
            else:
                "both move"
                m+= 1 
                n += 1 
            
        "only 1 is different, no more no less "
        return True
        
        
        
        
        
        