# 837. Palindromic Substrings
# Given a string, your task is to count how many palindromic substrings in this string.

# The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.

# Example
# Input: "abc"
# Output: 3
# Explanation: Three palindromic strings: "a", "b", "c".

class Solution:
    """
    @param str: s string
    @return: return an integer, denote the number of the palindromic substrings
    """
    def countPalindromicSubstrings(self, str):
        # write your code here
        
        if str is None or len(str) == 0:
            return []
            
        # remove spaces and invalid char 
        newstr = self.convert(str)
        
        # palindrome, center can be a char position and can be a space  
        count = 0 
        
        for i in range(0,len(newstr)):
            # char position from 0 to n-1 
            
            start = i
            end = i 
            while start >=0 and end <= len(newstr)-1:
                if newstr[start]==newstr[end]:
                    count += 1 
                    start -= 1 
                    end +=1 
                else:
                    break 
        
        for i in range(0,len(newstr)-1):
            # space position from 0 to n-2 
            
            start = i 
            end = i +  1  
            while start >=0 and end <= len(newstr) - 1 :
                if newstr[start] == newstr[end]:
                    count += 1 
                    start -= 1 
                    end += 1 
                else:
                    break 
        
        return count 
        
        
    def convert(self,str):
        
        newstr = ""
        
        for i in range(0,len(str)):
            ch = str[i].lower()
            if ord(ch) >= ord("a") and ord(ch) <= ord("z"):
                newstr += ch
                
        return newstr