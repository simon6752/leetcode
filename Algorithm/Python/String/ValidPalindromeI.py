# 415. Valid Palindrome
# Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

# Example
# "A man, a plan, a canal: Panama" is a palindrome.

# "race a car" is not a palindrome.

# Challenge
# O(n) time without extra memory.


class Solution:
    """
    @param s: A string
    @return: Whether the string is a valid palindrome
    """
    def isPalindrome(self, s):
        # write your code here
        "two pointers. need to get alphanumeric char and ignore cases"
        
        
        if s is None or len(s) <= 1:
            return True
            
        p = 0
        q = len(s) -1
        
        while p < q:
            
            if not s[p].isalnum():
                p += 1 
                continue
            if not  s[q].isalnum():
                q -= 1 
                continue
            
            if not(s[p].lower() == s[q].lower()):
                return False
            else:
                p += 1 
                q -= 1 
                
        
        return True