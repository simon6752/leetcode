# 664. Counting Bits
# Given a non negative integer number num. For every numbers i in the range 0 ≤ i ≤ num calculate the number of 1's in their binary representation and return them as an array.

# Example
# Given num = 5 you should return [0,1,1,2,1,2].

# Challenge
# It is very easy to come up with a solution with run time O(n*sizeof(integer)). But can you do it in linear time O(n) /possibly in a single pass?
# Space complexity should be O(n).
# Can you do it like a boss? Do it without using any builtin function like __builtin_popcount in c++ or in any other language.


class Solution:
    """
    @param num: a non negative integer number
    @return: an array represent the number of 1's in their binary
    """
    def countBits(self, num):
        # write your code here

        # if n is even, it will have the same as n//2, otherwise it will be 1s of (n-1)//2 then + 1
        
        if num == 0:
            return [num]
        
        result = [0]* (num + 1)
        
        result[1] = 1
        
        for i in range(2,num+1):
            if i%2 == 0:
                "even"
                result[i] = result[i//2]
            else:
                "odd"
                result[i] = result[(i-1)//2] + 1 
                
        return result