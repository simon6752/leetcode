# 987. Binary Number with Alternating Bits
# Given a positive integer, check whether it has alternating bits: namely, if two adjacent bits will always have different values.

# Example
# Example 1:

# Input: 5
# Output: True
# Explanation:
# The binary representation of 5 is: 101
# Example 2:

# Input: 7
# Output: False
# Explanation:
# The binary representation of 7 is: 111.
# Example 3:

# Input: 11
# Output: False
# Explanation:
# The binary representation of 11 is: 1011.
# Example 4:

# Input: 10
# Output: True
# Explanation:
# The binary representation of 10 is: 1010.


class Solution:
    """
    @param n: a postive Integer
    @return: if two adjacent bits will always have different values
    """
    def hasAlternatingBits(self, n):
        # Write your code here
        
        # 101010101  or 101010,  
        # 101010101  101010100 => 10101010 
        # 2^n = 0b1000000   (2^n) & (2^n -1) == 0
        
        # n + ((n-1)//2) + 1 
        
        if n % 2 == 1:
            "odd "
            m = n + ((n-1)//2) + 1 
            " m should be 2 ^ k"
            return m & (m-1) == 0
        else:
            m = n + n//2 + 1
            return m & (m-1) == 0