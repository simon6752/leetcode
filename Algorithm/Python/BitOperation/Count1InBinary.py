# 365. Count 1 in Binary
# Count how many 1 in binary representation of a 32-bit integer.
#
# Example
# Given 32, return 1
#
# Given 5, return 2
#
# Given 1023, return 9
#
# Challenge
# If the integer is n bits with m 1 bits. Can you do it in O(m) time?

class Solution:
    """
    @param: num: An integer
    @return: An integer
    """
    def countOnes(self, num):
        # write your code here

        # 5 0b101, we got 2
        # 7 0b111, we got 3
        "how many 1's in n, depends on how many 1's in n-1"
        " if n -1 is even, then n has 1 more 1,   it will have 1s of (n-1)//2 + 1 or  1s of (n-1) + 1 "
        "if n is even, it will have 1s of (n-1)//2"
        "but this is log(n)"

        "let's consider this way:  10 - 1 =  01   11 -1  = 10    100 -1  = 011, num & (num-1) = ? each time we do this, it will keep higher digits while eleminate last 1 "

        "how about negatives?"
        "-x is 1 with 31 digits of ~(x-1)"

        count = 0

        if num < 0 :

            num1 = -num - 1
        else:
            num1 = num

        while num1 != 0 :
            num1 = num1 & (num1 - 1)
            count += 1

        if num < 0 :
            count = 1 + (31- count )

        return count

