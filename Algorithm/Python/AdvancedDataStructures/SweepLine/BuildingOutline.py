#  131. The Skyline Problem
# Description
# Given N buildings in a x-axis，each building is a rectangle and can be represented by a triple (start, end, height)，where start is the start position on x-axis, end is the end position on x-axis and height is the height of the building. Buildings may overlap if you see them from far away，find the outline of them。
#
# An outline can be represented by a triple, (start, end, height), where start is the start position on x-axis of the outline, end is the end position on x-axis and height is the height of the outline.
#
# Building Outline
#
# Please merge the adjacent outlines if they have the same height and make sure different outlines cant overlap on x-axis.
#
# Have you met this question in a real interview?
# Example
# Given 3 buildings：
#
# [
#   [1, 3, 3],
#   [2, 4, 4],
#   [5, 6, 1]
# ]
# The outlines are：
#
# [
#   [1, 2, 3],
#   [2, 4, 4],
#   [5, 6, 1]
# ]


# my solution version 1, time limit exceeded

from heapq import heappush, heappop, heapify


class Solution:
	"""
	@param buildings: A list of lists of integers
	@return: Find the outline of those buildings
	"""
	
	def buildingOutline (self, buildings):
		# write your code here
		
		# building has start and end, we need hight at each position, not the number of flights, so we need a heap to save all height at current position and the maximum of them is the height at current position
		
		# heap put in (start,height), (end,-height)
		
		if buildings is None or len (buildings) == 0:
			return []
		
		heap = []
		
		for i in range (0, len (buildings)):
			heappush (heap, (buildings [i] [0], buildings [i] [2]))
			heappush (heap, (buildings [i] [1], -buildings [i] [2]))
		
		# save current building heights into temp heap, we save - height into it so this minimum heap will alwasy return - MaxHeight
		
		heapheights = []
		
		# how to delete a specific value from heap?? use list remove() to remove specific value from list, then heapq.heapify()!
		
		result = []
		currentHeight = -1
		currentstart = -1
		
		while len (heap) != 0:
			
			node = heappop (heap )
			
			if node [1] > 0:
				# it is start, we need to consider its height
				# compare its value to currentHeight
				
				# it is either higher or lower, add it to heapheights
				heappush (heapheights, -node [1])
			
			else:
				# node[1] < 0, we met an end
				heapheights.remove (node [1])
				heapify (heapheights)
			
			if len (heapheights) != 0:
				# peek only, use first element
				currenthighest = -heapheights[0]
			else:
				currenthighest = 0
			if (currentHeight != -1 and currenthighest != currentHeight) or currentHeight == -1:
				# we have a new hightest value, so we have a new segment to add
				# we do not add 0 height into result
				if currentstart != -1 and currentHeight != 0 :
					result.append ([currentstart, node [0], currentHeight])
				
				# update current start
				currentstart = node [0]
				# update currenthighest
				currentHeight = currenthighest
		
		return result


assert Solution().buildingOutline([[1,3,3],[2,4,4],[5,6,1]]) == [[1,2,3],[2,4,4],[5,6,1]]

assert Solution().buildingOutline([[1,100,20],[2,99,19],[3,98,18]]) == [[1,100,20]]

# TODO hash heap