#  391. Number of Airplanes in the Sky
# Description
# Given an interval list which are flying and landing time of the flight. How many airplanes are on the sky at most?
#
# If landing and flying happens at the same time, we consider landing should happen at first.
#
# Have you met this question in a real interview?
# Example
# For interval list
#
# [
#   (1,10),
#   (2,3),
#   (5,8),
#   (4,7)
# ]
# Return 3

class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""


# my solution v1, naive solution


class Solution:
	"""
	@param airplanes: An interval array
	@return: Count of airplanes are in the sky.
	"""
	
	def countOfAirplanes (self, airplanes):
		# write your code here
		
		# analysis: swipe line
		# a line swipe from min to maximum of time. when there is no start/end point on line, number of airplanes do not change. so we only consider those start and end time
		
		# for interval (1,10), it is [1,10), only time 1 - 10 is in sky
		
		# notice, there might be multiple take off and land in the same time, like time 10, there is 5 landing and 3 take off
		# we may want to sort all start and end time, but need to handle duplicate and take off and landing
		
		if airplanes is None or len (airplanes) == 0:
			return 0
		
		time = []
		# may need 2 hash table to save landing and take off count for each start/end time
		landcount = {}
		takeoffcount = {}
		
		for i in range (0, len (airplanes)):
			if airplanes [i].start not in takeoffcount.keys ():
				takeoffcount [airplanes [i].start] = 1
			else:
				takeoffcount [airplanes [i].start] += 1
			
			if airplanes [i].end not in landcount.keys ():
				landcount [airplanes [i].end] = 1
			else:
				landcount [airplanes [i].end] += 1
			
			# add all start and end
			time.append (airplanes [i].start)
			time.append (airplanes [i].end)
		
		time = sorted (time)
		# now there might be duplicates
		
		currentinsky = 0
		maxinsky = 0
		
		for i in range (0, len (time)):
			
			if i != 0 and time [i] == time [i - 1]:
				# duplicates
				continue
			
			takeoff = takeoffcount [time [i]] if time [i] in takeoffcount.keys () else 0
			land = landcount [time [i]] if time [i] in landcount.keys () else 0
			currentinsky = currentinsky + takeoff - land
			maxinsky = max (maxinsky, currentinsky)
		
		return maxinsky

assert Solution().countOfAirplanes([Interval(1,10),Interval(2,3),Interval(5,8),Interval(4,7)]) == 3

# solution 2 from jiu zhang. when start/end time is added to time points, it is compared by +1(start)/-1(end). start will add one to sky, end will decrease one to sky

def sorter (x, y):
	if x [0] != y [0]:
		return x [0] - y [0]
	return x [1] - y [1]


class Solution2:
	# @param airplanes, a list of Interval
	# @return an integer
	def countOfAirplanes (self, airplanes):
		timepoints = []
		for airplane in airplanes:
			timepoints.append ((airplane.start, 1))
			timepoints.append ((airplane.end, -1))
		
		timepoints = sorted (timepoints, cmp = sorter)
		
		sum, most = 0, 0
		for t, delta in timepoints:
			sum += delta
			most = max (most, sum)
		
		return most