# 471. Top K Frequent Words
# Given a list of words and an integer k, return the top k frequent words in the list.
#
# Example
# Given
#
# [
#     "yes", "lint", "code",
#     "yes", "code", "baby",
#     "you", "baby", "chrome",
#     "safari", "lint", "code",
#     "body", "lint", "code"
# ]
# for k = 3, return ["code", "lint", "baby"].
#
# for k = 4, return ["code", "lint", "baby", "yes"],
#
# Challenge
# Do it in O(nlogk) time and O(n) extra space.


from heapq import heappush, heappop

class Solution:
    """
    @param words: an array of string
    @param k: An integer
    @return: an array of string
    """
    def topKFrequentWords(self, words, k):
        # write your code here
        "use hash table for freqency count, then put into min heap,size k. "
        "why keep size k? why not put all n nodes into it? since that will make the heap operation time O(log(n)), total would be O(n log(n)). but we need O(n log(k))."
        "we only keep the larget k in heap. when insert into heap, once size > k, poll one(smallest, order would be > k+1, safe to poll). poll all k elements in min heap and get our result"

        "calculate frequency"

        if words is None:
            return []

        if k >= len(words):
            return words

        freq = {}

        for i in range(len(words)-1,-1,-1):
            if words[i] not in freq.keys():
                freq[words[i]] = 1
            else:
                freq[words[i]] += 1

        minheap = []

        i = 0
        for key in freq.keys():
            "we need to push (freq[key],key]) to heap"

            "push one "

            heappush(minheap,(freq[key],key))

            if i>= k:
                "poll minimum from heap"
                heappop(minheap)

            i += 1

        "now we have top k words in heap, poll out and get result"

        result = []

        for i in range(0,len(minheap)):
            result.insert(0,heappop(minheap)[1])

        return result



# input
# ["yes","lint","code","yes","code","baby","you","baby","chrome","safari","lint","code","body","lint","code"]
# 3
# Output
# ["code","lint","body"]
# Expected
# ["code","lint","baby"]

assert Solution().topKFrequentWords(["yes","lint","code","yes","code","baby","you","baby","chrome","safari","lint","code","body","lint","code"],3) == ["code","lint","yes"]