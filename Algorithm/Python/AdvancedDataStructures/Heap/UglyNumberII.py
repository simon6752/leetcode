# 4. Ugly Number II
# Description
# Ugly number is a number that only have factors 2, 3 and 5.
#
# Design an algorithm to find the nth ugly number. The first 10 ugly numbers are 1, 2, 3, 4, 5, 6, 8, 9, 10, 12...
#
# Note that 1 is typically treated as an ugly number.
#
# Have you met this question in a real interview?
# Example
# If n=9, return 10.
#
# Challenge
# O(n log n) or O(n) time.


# O(n) time, O(n) space
class Solution:
    """
    @param n: An integer
    @return: the nth prime number as description.
    """
    def nthUglyNumber(self, n):
        # write your code here
        # new ugly number is multiply 2 or 3 or 5 from one of existing ugly number, also need to choose smallest one
        # use 3 pointers 
        
        
        if n is None or n == 0 :
            return 1 
            
        res = [1,2,3]
        
        if n < 3:
            return res[n-1]
        
        # the index of next, for 2, we already have 2 in res, that is 2*res[0] = 2*1, so p2 is 1     
        p2 = 1
        p3 = 1
        p5 = 0 
        
        k = 3 
        
        while k < n :
            k += 1 
            
            # check minimum that is larger than res[-1]
            if res[p2]*2 > res[-1]:
                next = res[p2] * 2 
            if res[p3]*3 > res[-1]:
                next = min(next,res[p3]*3)
            if res[p5]*5 > res[-1]:
                next = min(next,res[p5]*5)

            res.append(next)
            
            if next == res[p2]*2:
                p2+=1 
                
            if next == res[p3] *3 :
                p3 +=1 
            
            if next == res[p5] * 5:
                p5 += 1 
                
        return res[-1]
        
            
# from JiuZhang, use heap
class Solution2:
    """
	@param {int} n an integer.
    @return {int} the nth prime number as description.
    """
    def nthUglyNumber(self, n):
        import heapq
        if n <= 1:
            return n

        n -= 1
        key = [2, 3, 5]
        h = []
        for i in range(3):
            heapq.heappush(h, (key[i], i))

        value = key[0]
        while n > 0:
            value, level = heapq.heappop(h)
            while level < 3:
                new_value = key[level] * value
                heapq.heappush(h, (new_value, level))
                level += 1
            n -= 1
        return value       

# TODO use heap