# TODO 364. Trapping Rain Water II
# Description
# Given n x m non-negative integers representing an elevation map 2d where the area of each cell is 1 x 1, compute how much water it is able to trap after raining.
#
#
#
# Have you met this question in a real interview?
# Example
# Given 5*4 matrix
#
# [12,13,0,12]
# [13,4,13,12]
# [13,8,10,12]
# [12,13,12,12]
# [13,13,13,13]