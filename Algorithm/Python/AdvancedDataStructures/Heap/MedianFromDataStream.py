# 81. Find Median from Data Stream
# Description
# Numbers keep coming, return the median of numbers at every time a new number added.

# Have you met this question in a real interview?  
# Clarification
# What's the definition of Median?

# Median is the number that in the middle of a sorted array. If there are n numbers in a sorted array A, the median is A[(n - 1) / 2]. For example, if A=[1,2,3], median is 2. If A=[1,19], median is 1.
# Example
# For numbers coming list: [1, 2, 3, 4, 5], return [1, 1, 2, 2, 3].

# For numbers coming list: [4, 5, 1, 3, 2, 6, 0], return [4, 4, 4, 3, 3, 3, 3].

# For numbers coming list: [2, 20, 100], return [2, 2, 20].

# Challenge
# Total run time in O(nlogn).

# DifficultyHard
# Total Accepted10367
# Total Submitted33787
# Accepted Rate30%
#  Show Tags
# HeapData StreamPriority QueueLintCode Copyright
#  Company
# Google


from heapq import heappush, heappop 
class Solution:
    """
    @param nums: A list of integers
    @return: the median of numbers
    """
    def medianII(self, nums):
        # write your code here
        # since we want to always keep (n+1)//2 th element, for n = 2k + 1, there must be k smaller, and k larger. 
        # we need to keep k + 1, that is (n+1)//2. there will always be (n-1)//2 smaller, and (n-1)//2 larger 
        # every time a new element come in, we need to make sure which side it went to. 
        # left max heap, and right min heap. it is fast to compare new element with left max and right min to determine where it went to. we need to first compare it with median element, then left max and right min. also need to keep a record of left heap and right heap size 
        
        if nums is None or len(nums) == 0 :
            return [] 
            
        # heap operation time is log(n). so total time is n log(n)
        
        # save current median 
        result = [nums[0]]
        
        leftheap = []
        rightheap = []
        
        # when new number come in, we need to find right position for it 
        
        for i in range(1,len(nums)):
            # every time we need to decide which way to go 
            left = min(nums[i],result[i-1])
            right = max(nums[i],result[i-1])
            
            if len(leftheap) > 0:
                leftmax = heappop(leftheap)[1]
                # compare this value with left 
                if leftmax > left:
                    leftmax, left = left, leftmax 

                # put back 
                heappush(leftheap,(-leftmax,leftmax))
            
            if len(rightheap) > 0:
                rightmin = heappop(rightheap)[1]
                if rightmin < right:
                    rightmin, right = right, rightmin 
                    
                heappush(rightheap,(rightmin,rightmin))
            
            # now we have leftheap < left < right < rightheap 
            # then according to length of left and right heap to determine which way to go 
            if len(leftheap) < len(rightheap):
                # push left to left heap 
                heappush(leftheap,(-left,left))
                result.append(right)
            
            else:
                # len(leftheap) == len(rightheap)
                # push right to right heap 
                heappush(rightheap,(right,right))
                result.append(left )
            
        return result 
                

class Solution2:
    """
    @param nums: A list of integers
    @return: the median of numbers
    """
    def medianII(self, nums):
        # write your code here
        # since we want to always keep (n+1)//2 th element, for n = 2k + 1, there must be k smaller, and k larger. 
        # we need to keep k + 1, that is (n+1)//2. there will always be (n-1)//2 smaller, and (n-1)//2 larger 
        # every time a new element come in, we need to make sure which side it went to. 
        # left max heap, and right min heap. it is fast to compare new element with left max and right min to determine where it went to. we need to first compare it with median element, then left max and right min. also need to keep a record of left heap and right heap size 
        
        if nums is None or len(nums) == 0 :
            return [] 
            
        # heap operation time is log(n). so total time is n log(n)
        
        # save current median 
        result = [nums[0]]
        
        leftheap = [(-nums[0],nums[0])]
        rightheap = []
        
        # when new number come in, we need to find right position for it 
        # let's keep left max heap equal or one element more than right heap. max of left heap is the median 
        
        for i in range(1,len(nums)):
            # every time we need to decide which way to go 
            # nums[i] where to go 
            
            if len(leftheap) == len(rightheap):
                # need to compare max left heap, nums[i], min right heap and put them in right position 
                if nums[i] >= rightheap[0][1]:
                    heappush(leftheap,(-rightheap[0][1],rightheap[0][1]))
                    heappop(rightheap)
                    heappush(rightheap,(nums[i],nums[i]))
                else:
                    # push to left max heap 
                    heappush(leftheap,(-nums[i],nums[i]))
            else:
                # else left heap has one element more 
                # need to compare nums[i] with leftheap[0][1] and put the max to rightheap 
                if nums[i] >= leftheap[0][1]:
                    heappush(rightheap,(nums[i],nums[i]))
                else:
                    temp = heappop(leftheap)[1]
                    heappush(leftheap,(-nums[i],nums[i]))
                    heappush(rightheap,(temp,temp))
            
            # no matter what, left max is median 
            
            result.append(leftheap[0][1])
                
        return result                   

#     Input
# [4,5,1,3,2,6,0]
# Output
# [4,4,1,1,2,2,2]
# Expected
# [4,4,4,3,3,3,3]

assert Solution2().medianII([4,5,1,3,2,6,0]) == [4,4,4,3,3,3,3]
