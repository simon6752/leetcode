# 612. K Closest Points
# Given some points and a point origin in two dimensional space, find k points out of the some points which are nearest to origin.
# Return these points sorted by distance, if they are same with distance, sorted by x-axis, otherwise sorted by y-axis.

# Example
# Given points = [[4,6],[4,7],[4,4],[2,5],[1,1]], origin = [0, 0], k = 3
# return [[1,1],[2,5],[4,4]]

class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b

"""
Definition for a point.
class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b
    
"""
from heapq import heappush, heappop
class Solution:
    """
    @param points: a list of points
    @param origin: a point
    @param k: An integer
    @return: the k closest points
    """
    def kClosest(self, points, origin, k):
        # write your code here
        "naive solution: calculate all distances then sort, O(n log(n)), n is number of points.  "
        "if we have n points, nx distinct x values in [xa,xb], ny distinct y values in [ya,yb]"
        "think about it, if we have millions of code, most of the points are far away and we do not even need to think about it. so, sort x and y , then find minimum 3 points by compare x and y "
        
        "x ^ 2 + y ^ 2 < (x+y)^2 "
        if points is None:
            return None 
            
        if len(points) <= k:
            return points
            
        "say if (x1,y1) is closer compare to (x2,y2), then x1 < x2 or y1 < y2. so we consider smaller x or y or x + y  first "
        "since origin may change, we consider sort ( abs(x-x0), abs(y-y0)) instead of (x,y)"
        
        "3 list, sort by abs(x-x0), abs(y-y0), abs(x-x0 + y-y0)"
        
        "use min heap "
        
        heap = []
        
        for i in range(0,len(points)):
            
            heappush(heap, ((points[i].x - origin.x)**2 + (points[i].y - origin.y)**2, points[i].x,points[i].y))
            
        result = []
        
        for i in range(0,k):
            temp = heappop(heap)
            result.append(Point(temp[1],temp[2]))
            
        return result
        
        
        