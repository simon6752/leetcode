# 591. Connecting Graph III
# Given n nodes in a graph labeled from 1 to n. There is no edges in the graph at beginning.
#
# You need to support the following method:
#
# connect(a, b), an edge to connect node a and node b
# query(), Returns the number of connected component in the graph
# Example
# 5 // n = 5
# query() return 5
# connect(1, 2)
# query() return 4
# connect(2, 4)
# query() return 3
# connect(1, 4)
# query() return 3



class ConnectingGraph3:
    """
    @param: n: An integer
    """

    def __init__(self, n):
        # do intialization if necessary
        self.unionfind = UnionFind(n)

    """
    @param: a: An integer
    @param: b: An integer
    @return: nothing
    """

    def connect(self, a, b):
        # write your code here
        self.unionfind.Union(a - 1, b - 1)

    """
    @return: An integer
    """

    def query(self):
        # write your code here
        return self.unionfind.count


"define union find"


class UnionFind:
    def __init__(self, n):
        self.father = []
        "initialize union find to n self pointed nodes, total component is n"
        self.count = n

        for i in range(0, n):
            self.father.append(i)

    def Find(self, x):
        "find father for x, use path compression"

        if self.father[x] == x:
            return self.father[x]
        else:
            "update x's father to be root father"
            self.father[x] = self.Find(self.father[x])
            return self.father[x]

    def Union(self, a, b):
        roota = self.Find(a)
        rootb = self.Find(b)

        if roota != rootb:
            "join by assign a'f ther to b"
            self.father[rootb] = roota
            "decrease the count of component"
            self.count -= 1


result = []
graph = ConnectingGraph3(12)
graph.connect(3, 9)
graph.connect(10, 9)
graph.connect(5, 7)
result.append(graph.query())
result.append(graph.query())
result.append(graph.query())
graph.connect(3, 2)
graph.connect(10, 11)
result.append(graph.query())
graph.connect(12, 8)
graph.connect(10, 3)
graph.connect(10, 12)
result.append(graph.query())
graph.connect(10, 5)
result.append(graph.query())
result.append(graph.query())
result.append(graph.query())
result.append(graph.query())
result.append(graph.query())
graph.connect(10, 8)
graph.connect(12, 2)
result.append(graph.query())
result.append(graph.query())
graph.connect(7, 6)
# Output =[9,9,9,7,4,3,3,3,3,3,3,3]
Expected = [9, 9, 9, 7, 5, 4, 4, 4, 4, 4, 4, 4]

assert result == Expected
