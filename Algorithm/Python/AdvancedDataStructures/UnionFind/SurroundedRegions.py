# 477. Surrounded Regions
# Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.
#
# A region is captured by flipping all 'O''s into 'X''s in that surrounded region.
#
# Example
# X X X X
# X O O X
# X X O X
# X O X X
# After capture all regions surrounded by 'X', the board should be:
#
# X X X X
# X X X X
# X X X X
# X O X X

# we have N rows, M cols
# x is col direction, change in row
# y is row direction, change in col
# (x,y) to index x* M + y
#     ------> y
#     |
#     |
#     |
#     |
#     x
# index to (x,y) => (i//M,i%M)
class Solution:
    """
    @param: board: board a 2D board containing 'X' and 'O'
    @return: nothing
    """

    def surroundedRegions(self, board):
        # write your code here
        "first, we create union find, connected Os should be in the same group."
        "(i,j) => i*n+j"
        "for those O on edge, should be a different group: father -1"
        "all group except edge should flip"

        if board == None or len(board) == 0 or len(board[0]) == 0:
            return board
        # N rows, M cols
        N = len(board)
        M = len(board[0])
        self.unionfind = UnionFind(N * M, N, M, board)
        "find the same group for each node"
        for i in range(1, N - 1):
            for j in range(1, M - 1):

                if board[i][j] == "O":
                    if board[i - 1][j] == "O":
                        self.joinHelper(board, N, M, i, j, i - 1, j)
                    if board[i + 1][j] == "O":
                        self.joinHelper(board, N, M, i, j, i + 1, j)
                    if board[i][j - 1] == "O":
                        self.joinHelper(board, N, M, i, j, i, j - 1)
                    if board[i][j + 1] == "O":
                        self.joinHelper(board, N, M, i, j, i, j + 1)

        "flip"

        for i in range(1, N - 1):
            line = ""
            line += board[i][0]
            for j in range(1, M - 1):
                if board[i][j] == "O" and self.unionfind.find(i * M + j) != -1:
                    "for those O not at or with boundary O"
                    line +="X"
                else:
                    line += board[i][j]
            line  += board[i][-1]

            board[i] = line


    def joinHelper(self, board, N, M, x1, y1, x2, y2):

        "if (x1,y1) or (x2,y2) one of them is in boundary, join the non bondary one to bondary"
        self.unionfind.union(x1 * M + y1, x2 * M + y2)


class UnionFind:
    def __init__(self, n, N, M, board):
        "N row, M col"
        self.father = [-1] * n
        "save bord dimension N"
        self.dimx = N
        self.dimy = M
        for i in range(0, n):
            if not (self.atBoundary(i) and board[i // M][i % M] == "O"):
                "except boundary Os, all node was initialized to point to itself"
                self.father[i] = i

    def atBoundary(self, i):
        x = i // self.dimy
        y = i % self.dimy
        return x == 0 or y == 0 or x == self.dimx - 1 or y == self.dimy - 1

    def union(self, p, q):
        "union set p and q in to set p"

        rootp = self.find(p)
        rootq = self.find(q)

        if rootp != rootq:
            if rootq != -1:
                self.father[rootq] = rootp
            else:
                if rootp != -1:
                    self.father[rootp] = rootq

    def find(self, x):
        "return root of x. use path compression"

        if self.father[x] == -1:
            "reached boundary, do not go further"
            return -1

        if self.father[x] == x:
            return x

        self.father[x] = self.find(self.father[x])

        return self.father[x]


# Input
# ["XXXX","XOOX","XXOX","XOXX"]
# Output
# ["XXXX","XOOX","XXOX","XOXX"]
# Expected
# ["XXXX","XXXX","XXXX","XOXX"]

Input = ["XXXX", "XOOX", "XXOX", "XOXX"]

Expected = ["XXXX", "XXXX", "XXXX", "XOXX"]

Solution().surroundedRegions(Input) 
assert Input == Expected


# TODO we can also do it in DFS