#  433. Number of Islands
# Given a boolean 2D matrix, 0 is represented as the sea, 1 is represented as the island. If two 1 is adjacent, we consider them in the same island. We only consider up/down/left/right adjacent.
#
# Find the number of islands.
#
# Example
# Given graph:
#
# [
#   [1, 1, 0, 0, 0],
#   [0, 1, 0, 0, 1],
#   [0, 0, 0, 1, 1],
#   [0, 0, 0, 0, 0],
#   [0, 0, 0, 0, 1]
# ]
# return 3.




class Solution:
	"""
	@param grid: a boolean 2D matrix
	@return: an integer
	"""
	
	def numIslands (self, grid):
		# write your code here
		
		# use union find, union all islands
		
		# index (i,j ) to i*n + j, total n^2
		
		if grid is None or len (grid) == 0:
			return 0
		
		n = len (grid)
		m = len (grid [0])
		# grid is n row x m col
		
		union = UnionFind (n * m)
		
		zerocount = 0
		
		for i in range (0, n):
			
			for j in range (0, m):
				# index
				# we only connect to right and lower , upper and left ones,
				
				if i != n - 1 and grid [i] [j] == 1 and grid [i + 1] [j] == 1:
					# lower is 1
					union.union (i * m + j, (i + 1) * m + j)
				
				if j != m - 1 and grid [i] [j] == 1 and grid [i] [j + 1] == 1:
					# right is 1
					union.union (i * m + j, i * m + j + 1)
				
				if i != 0 and grid [i] [j] == 1 and grid [i - 1] [j] == 1:
					# upper is 1
					union.union (i * m + j, (i - 1) * m + j)
				
				if j != 0 and grid [i] [j] == 1 and grid [i] [j - 1] == 1:
					# left is 1
					union.union (i * m + j, i * m + j - 1)
				
				if grid [i] [j] == 0:
					zerocount += 1
				
				# count which one of union is it self,
		
		for i in range (0, n):
			for j in range (0, m):
				union.find (i * m + j)
		
		count = union.count - zerocount
		
		return count
	
	# use union find


class UnionFind:
	def __init__ (self, n):
		
		self.father = []
		
		self.count = n
		
		for i in range (0, n):
			# initialize each one's father to its own
			
			self.father.append (i)
	
	def union (self, a, b):
		
		fathera = self.find (a)
		fatherb = self.find (b)
		
		if fathera == fatherb:
			# belong to the same group
			return
		else:
			self.father [a] = fatherb
			# reduce one
			self.count -= 1
	
	def find (self, x):
		# find father of x
		
		if self.father [x] == x:
			# root
			return x
		
		# path compression
		self.father [x] = self.find (self.father [x])
		
		return self.father [x]
	
	def query (self):
		return self.count
	
assert Solution().numIslands([[1,1,0,0,0],[0,1,0,0,1],[0,0,0,1,1],[0,0,0,0,0],[0,0,0,0,1]])  == 3

assert Solution().numIslands([[1,1,1,1,1,1],[1,0,0,0,0,1],[1,0,1,1,0,1],[1,0,0,0,0,1],[1,1,1,1,1,1]]) == 2


# java version


#   DFS

class Solution2:
	"""
	@param grid: a boolean 2D matrix
	@return: an integer
	"""
	
	def numIslands (self, grid):
		# write your code here
		# use dfs this time
		
		if grid is None or len (grid) == 0 or len (grid [0]) == 0:
			return 0
		
		n = len (grid)
		m = len (grid [0])
		
		count = 0
		visited = set ()
		
		for i in range (n):
			
			for j in range (m):
				
				if grid [i] [j] == 1 and i * m + j not in visited:
					self.dfs (i, j, visited, grid, m, n)
					count += 1
		
		return count
	
	def dfs (self, i, j, visited, grid, m, n):
		
		# valid i, j
		
		if i < 0 or i >= n or j < 0 or j >= m:
			return
		
		if grid [i] [j] == 0 or i * m + j in visited:
			return
		
		visited.add (i * m + j)
		
		self.dfs (i + 1, j, visited, grid, m, n)
		self.dfs (i - 1, j, visited, grid, m, n)
		self.dfs (i, j + 1, visited, grid, m, n)
		self.dfs (i, j - 1, visited, grid, m, n)

# TODO BFS
