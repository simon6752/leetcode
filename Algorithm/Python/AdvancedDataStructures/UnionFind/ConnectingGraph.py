# [LintCode] 589 Connecting Graph 解题报告
# Description
# Given n nodes in a graph labeled from 1 to n. There is no edges in the graph at beginning.
#
# You need to support the following method:
# 1. connect(a, b), add an edge to connect node a and node b.
# 2. query(a, b), check if two nodes are connected
#
#
# Example
# 5 // n = 5
# query(1, 2) return false
# connect(1, 2)
# query(1, 3) return false
# connect(2, 4)
# query(1, 4) return true

class GraphNode:
    def __init__(self, val):
        self.val = val


class GraphEdge:
    def __init__(self, start, end):
        self.start = start

        self.end = end


class UnionFind:
    def __init__(self, n):
        self.father = []

        for i in range(0, n):
            "each element's father is itself: father[i] = i"
            self.father.append(i)

    def Find(self, x):
        "use path compression"

        if self.father[x] == x:
            "this is the root, find, return"
            return x

        "path compression"
        self.father[x] = self.Find(self.father[x])

        return self.father[x]

    def Union(self, x1, x2):

        root1 = self.Find(x1)
        root2 = self.Find(x2)

        if root1 == root2:
            "belong to the same group, no need to union"
            return
        else:
            "assign root2 to root1's father"
            self.father[root1] = root2


class Graph:
    # def __init__(self,vertices,edges):
    #     self.vertices = vertices
    #     self.edges = edges

    def __init__(self, n):
        self.nodes = []

        for i in range(0, n):
            self.nodes.append(GraphNode(i))

        self.edges = []


class ConnectingGraph1:
    """
    @param: n: An integer
    """

    "create a initial graph with union find. father of each node is itself"
    "when a new edge is added, union the nodes"
    "for query, return find result for Union-Find of these two nodes"

    def __init__(self, n):
        # do intialization if necessary

        self.unionfind = UnionFind(n)

    """
    @param: a: An integer
    @param: b: An integer
    @return: nothing
    """

    def connect(self, a, b):
        # write your code here

        self.unionfind.Union(a, b)

    """
    @return: An integer
    """

    def query(self, a, b):
        # write your code here

        return self.unionfind.Find(a) == self.unionfind.Find(b)


# 5 // n = 5
# query(1, 2) return false
# connect(1, 2)
# query(1, 3) return false
# connect(2, 4)
# query(1, 4) return true

graph = ConnectingGraph1(5)

assert graph.query(1, 2) == False

graph.connect(1, 2)

assert graph.query(1, 3) == False

graph.connect(2, 4)

assert graph.query(1, 4) == True
