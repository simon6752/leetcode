# 411. Gray Code
# Description
# The gray code is a binary numeral system where two successive values differ in only one bit.
#
# Given a non-negative integer n representing the total number of bits in the code, find the sequence of gray code. A gray code sequence must begin with 0 and with cover all 2n integers.
#
# For a given n, a gray code sequence is not uniquely defined.
#
# [0,2,3,1] is also a valid gray code sequence according to the above definition.
#
# Have you met this question in a real interview?
# Example
# Given n = 2, return [0,1,3,2]. Its gray code sequence is:
#
# 00 - 0
# 01 - 1
# 11 - 3
# 10 - 2
# Challenge
# O(2n) time.

class Solution:
    """
    @param n: a number
    @return: Gray code
    """
    def grayCode(self, n):
        # write your code here
        "00    01    11 10      110 111 101 100   1100 1101 1111 1110 1010 1011 1001 1000  11000  "

        "from n -> n +1, add 1 to highest digit and then reverse repeat previous result"
        result = []
        self.grayCodeHelper(result,n)

        return result

    def grayCodeHelper(self,result,n):
        "get it from n-1"

        if n == 0:
            result.append(0)
            return

        "get result from n - 1"

        self.grayCodeHelper(result,n-1)
        "add 1 to head of each digit and then reverse repeat"

        for i in range(len(result)-1,-1,-1):
            result.append(result[i] + (1 << (n-1)))

# how to get to this?
# how to reduce from O(2^(n+1)) to O(2^n) ?
