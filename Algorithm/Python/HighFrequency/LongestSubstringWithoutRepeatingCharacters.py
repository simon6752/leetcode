# 384. Longest Substring Without Repeating Characters
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a string, find the length of the longest substring without repeating characters.
#
# Have you met this question in a real interview?
# Example
# For example, the longest substring without repeating letters for "abcabcbb" is "abc", which the length is 3.
#
# For "bbbbb" the longest substring is "b", with the length of 1.
#
# Challenge
# O(n) time
#
# Tags
# Two Pointers String Hash Table Amazon Bloomberg Yelp Adobe


class Solution:
    """
    @param s: a string
    @return: an integer
    """

    def lengthOfLongestSubstring(self, s):
        # write your code here
        "two pointers + hashtable"
        "two pointer, start and end, add end letter to hashtable if not exist in hashtable. if exist, means repeating character appears, need to move start character"
        "hashtable keeps latest position of each letter between start and end"
        "start point to next position of last repeat letter"

        if s is None or len(s) == 0:
            return 0

        if len(s) == 1:
            return 1

        dict = {s[0]: 0}

        start = 0
        end = 1
        maxl = 1
        while end < len(s):

            if s[end] in dict.keys():
                "repeating character appears, need to remove"
                maxl = max(maxl, end - start)

                "start need to move to last of this character position"
                "but do not go back too much, so we need max"
                start = max(start, dict[s[end]] + 1)
                "also we need to update current end position in dict"
                dict[s[end]] = end
            else:
                maxl = max(maxl, end - start + 1)
                "put it in hashtable"
                dict[s[end]] = end

            end = end + 1

        return maxl


assert Solution().lengthOfLongestSubstring("aaaa") == 1
assert Solution().lengthOfLongestSubstring("aab") == 2
assert Solution().lengthOfLongestSubstring("an++--viaj") == 5
assert Solution().lengthOfLongestSubstring("gehmbfqmozbpripibusbezagafqtypz") == 9
