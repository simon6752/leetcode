# 488. Happy Number
# Write an algorithm to determine if a number is happy.

# A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.

# Example
# 19 is a happy number

# 1^2 + 9^2 = 82
# 8^2 + 2^2 = 68
# 6^2 + 8^2 = 100
# 1^2 + 0^2 + 0^2 = 1

class Solution:
    """
    @param n: An integer
    @return: true if this is a happy number or false
    """
    def isHappy(self, n):
        # write your code here
        
        "we keep calculate its digits square sum, until it is 1 or there is cycle. if it is 1, return True, otherwise False"
        
        ss = self.SquareSum(n)
        "save all square sum to detect cycle"
        listss = []
        
        while ss != 1 :
            
            ss = self.SquareSum(ss)
            
            "need to find out when to stop and return False"
            " if there is cycle, return False"
            
            if ss in listss:
                return False
            else:
                listss.append(ss)
        
        return True
        
    
    def SquareSum(self,n):
        
        sum = 0
        
        reminder = n % 10
        
        while n != 0:
            sum += reminder*reminder
            n = n//10
            reminder = n% 10
            
        # print(sum)
            
        return sum