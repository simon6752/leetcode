# 12. Min Stack
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Implement a stack with min() function, which will return the smallest number in the stack.
#
# It should support push, pop and min operation all in O(1) cost.
#
#  Notice
# min operation will never be called if there is no number in the stack.
#
# Have you met this question in a real interview?
# Example
# push(1)
# pop()   // return 1
# push(2)
# push(3)
# min()   // return 2
# push(1)
# min()   // return 1
# Tags
# Zenefits Stack Uber Google Amazon Bloomberg Snapchat


# analysis: for push and pop, use a stack. for min, we need to stack to keep track of current min. so every push, we need to compare the inserted element with current min and update stack 2 if necessary. when pop, pop from stack 1, also pop current min from stack 2

class MinStack:

    def __init__(self):
        # do intialization if necessary
        "use two stack"

        self.stack1 = []
        self.stack2 = []

    """
    @param: number: An integer
    @return: nothing
    """

    def push(self, number):
        # write your code here
        self.stack1.append(number)
        if len(self.stack2) > 0:
            self.stack2.append(min(number, self.stack2[-1]))
        else:
            self.stack2.append(number)

    """
    @return: An integer
    """

    def pop(self):
        # write your code here

        self.stack2.pop()
        return self.stack1.pop()

    """
    @return: An integer
    """

    def min(self):
        # write your code here
        return self.stack2[-1]


a = MinStack()
a.push(1)
assert a.pop() == 1
a.push(2)
a.push(3)
assert a.min() == 2
a.push(1)
assert a.min() == 1
