# 165. Merge Two Sorted Lists
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Merge two sorted (ascending) linked lists and return it as a new sorted list. The new sorted list should be made by splicing together the nodes of the two lists and sorted in ascending order.
#
# Have you met this question in a real interview?
# Example
# Given 1->3->8->11->15->null, 2->null , return 1->2->3->8->11->15->null.
#
# Tags
# Amazon LinkedIn Microsoft Apple Linked List

"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param l1: ListNode l1 is the head of the linked list
    @param l2: ListNode l2 is the head of the linked list
    @return: ListNode head of linked list
    """

    def mergeTwoLists(self, l1, l2):
        # write your code here
        "two pointers"

        if l1 is None:
            return l2

        if l2 is None:
            return l1

        p1 = l1
        p2 = l2

        "new header is smaller one"
        "make a dummy node"
        dummy = ListNode(0)
        n1 = dummy

        while p1 is not None and p2 is not None:
            if p1.val < p2.val:
                "n1 point to smaller one"
                n1.next = p1
                p1 = p1.next
            else:
                n1.next = p2
                p2 = p2.next

            "move n1"
            n1 = n1.next

        "check if p1 and p2 if one of them is not finished yet"
        if p1 is not None:
            n1.next = p1
        elif p2 is not None:
            n1.next = p2

        return dummy.next
