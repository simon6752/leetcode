# 655. Add Strings
# Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.

# Example
# Given num1 = "123", num2 = "45"
# return "168"

class Solution:
    """
    @param num1: a non-negative integers
    @param num2: a non-negative integers
    @return: return sum of num1 and num2
    """
    def addStrings(self, num1, num2):
        # write your code here

        if num1 is None:
            return num2 
            
        if num2 is None:
            return num1 
            
        # two pointers 
        # return string 
        
        # avoid overflow 
        
        p1 = 0 
        p2 = 0 
        digit1 = 0 
        digit2 = 0 
        # record if digit1 + digit2 > 10 
        digit3 = 0 
        total = "" 
        
        while p1 < len(num1) or p2 < len(num2):
            
            digit2 =0 
            digit1 = 0 
            if p1 >= len(num1):
                digit2 = ord(num2[len(num2)-1-p2]) - ord("0")
                p2 += 1 
                
            elif p2 >= len(num2):
                digit1 = ord(num1[len(num1)-1-p1]) - ord("0")
                p1 += 1 
            else:
                digit1 = ord(num1[len(num1)-1-p1]) - ord("0")
                digit2 = ord(num2[len(num2)-1-p2]) - ord("0")
                p1 += 1 
                p2 += 1 

            total = str((digit1+digit2+ digit3)% 10) + total 
            digit3 = (digit1+ digit2+digit3)// 10 
        
        # double check if digit3 is not 0 
        if digit3 !=0:
            total = str(digit3) + total
            
        return total 
            
                
            
                
            
            
                
assert Solution().addStrings("123","45") == "168"
                
            