
# 46. Majority Element
# Given an array of integers, the majority number is the number that occurs more than half of the size of the array. Find it.
#
# Example
# Given [1, 1, 1, 1, 2, 2, 2], return 1
#
# Challenge
# O(n) time and O(1) extra space

class Solution:
    """
    @param: nums: a list of integers
    @return: find a  majority number
    """
    def majorityNumber(self, nums):
        # write your code here
        "if we use hash table to save the count, then it is not O(1) extra space solution"
        "use majority number to replace following number, and anyhow in the rest of the numbers, majority element is still the majority number"


        count = 1
        major = nums[0]

        current = 0

        for i in range(1,len(nums)-1):
            "compare"

            if nums[i] == major:
                count += 1
            else:
                "distroy one major element and current element"
                count -= 1

                if count <= 0:
                    "we already used up current major element, lets set major element as next element"
                    count = 1
                    major = nums[i+1]
                    i += 1

        return major


