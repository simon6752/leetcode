# 911. Maximum Size Subarray Sum Equals k
# Description
# Given an array nums and a target value k, find the maximum length of a subarray that sums to k. If there isn't one, return 0 instead.

# The sum of the entire nums array is guaranteed to fit within the 32-bit signed integer range.

# Have you met this question in a real interview?  
# Example
# Given nums = [1, -1, 5, -2, 3], k = 3, return 4.

# Explanation:
# because the subarray [1, -1, 5, -2] sums to 3 and is the longest.
# Given nums = [-2, -1, 2, 1], k = 1, return 2.

# Explanation:
# because the subarray [-1, 2] sums to 1 and is the longest.
# Challenge
# Can you do it in O(n) time?


class Solution:
    """
    @param nums: an array
    @param k: a target value
    @return: the maximum length of a subarray that sums to k
    """
    def maxSubArrayLen(self, nums, k):
        # Write your code here


        # use hash table for fast look up 
        
        # naive solution: O(n^2), check start i, end j, 0<= i < j <= n-1, s[i:j+1] sum is k. find longest length of subarray 
        # 3 hashset. 1 save all elements, 1 saves all sum from left, 1 saves all sum from right 
        # check from left to i, check if sum(nums) - k - sum(nums[0:i+1]) exists in right sum hashset. if yes, whether i < j 
        # why we use hash table? since it will save out lookup time from O(n) to O(1)
        
        if nums is None or len(nums) == 0 :
            return 0 
        
        totalsum = sum(nums)
        
        hashright = {}
        # saves sum from j to n-1, value is a list of index that kind of j 
        
        rightsum = 0 
        for j in range(len(nums), -1, -1 ):
            # since right list may contain nothing, so j could be  len(nums)
            if j != len(nums):
                rightsum += nums[j]
            if rightsum not in hashright.keys():
                hashright[rightsum] = [j]
            else:
                hashright[rightsum].append(j)
                
        # now we check from left to right 
        
        leftsum = 0 
        longest = 0 
        for i in range(-1,len(nums)):
            # since left list could contain nothing, so i can be -1 
            if i != -1:
                leftsum += nums[i]
            if totalsum  - k - leftsum in hashright.keys():
                # so we have that [i+1,j] is sum of k. 
                rightlist = hashright[totalsum -k - leftsum]
                for m in range(0,len(rightlist)):
                    j = rightlist[m]
                    if j-1 >= i+1 :
                        # which means there is at least 1 element in this array 
                        longest = max(longest, j-1 - (i+1) + 1 )
                        
        return longest
            
            
assert Solution().maxSubArrayLen([1,-1,5,-2,3] ,3 ) == 4