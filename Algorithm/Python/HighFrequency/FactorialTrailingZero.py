# 1347. Factorial Trailing Zeroes
# Description
# Given an integer n, return the number of trailing zeroes in n!.

# Your solution should be in logarithmic time complexity.

class Solution:
    """
    @param n: a integer
    @return: return a integer
    """
    def trailingZeroes(self, n):
        # write your code here


        # n !. how many zeros depend on how many 10 = 5 * 2. for n, number of 2 is always more than 5, so, we need to find out 
        # how many 5 have in (1,2,3,...,n-1,n)
        # 1 5 10 15 25 30 35 40 45 50 55,...
        # 1 2 3 4 5 6 7 8 9 10 11...
        # we have n/5+ n/5^2+...+ n/5^k where 5^k <= n < 5^(k+1)
        
        if n <= 0 or n is None:
            return 0 
            
        m = n//5
        total = 0 
        while m != 0:
            total += m
            m = m//5 
            
        return total 
