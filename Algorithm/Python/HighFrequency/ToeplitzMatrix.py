# 1042. Toeplitz Matrix
# A matrix is Toeplitz if every diagonal from top-left to bottom-right has the same element.
#
# Now given an M x N matrix, return True if and only if the matrix is Toeplitz.
#
# Example
# Example 1:
#
# Input: matrix = [[1,2,3,4],[5,1,2,3],[9,5,1,2]]
# Output: True
# Explanation:
# 1234
# 5123
# 9512
#
# In the above grid, the diagonals are "[9]", "[5, 5]", "[1, 1, 1]", "[2, 2, 2]", "[3, 3]", "[4]", and in each diagonal all elements are the same, so the answer is True.
#
#
# Example 2:
#
# Input: matrix = [[1,2],[2,2]]
# Output: False
# Explanation:
# The diagonal "[1, 2]" has different elements.

class Solution:
	"""
	@param matrix: the given matrix
	@return: True if and only if the matrix is Toeplitz
	"""
	def isToeplitzMatrix(self, matrix):
		# Write your code here
		
		# pay attention to start and end point
		
		if matrix is None or len (matrix) == 0 or len (matrix [0]) == 0:
			return True
		
		n = len (matrix)
		m = len (matrix [0])
		
		# start point from (0,m-1) to (0,0) then to (n-1,0)
		
		for j in range (m - 1, -1, -1):
			startx = 0
			starty = j
			val = matrix [0] [j]
			
			while startx < n and starty < m:
				# stop when x or y reached boundary
				if matrix [startx] [starty] != val:
					# values on the same line must be the same
					return False
				
				# update
				startx += 1
				starty += 1
			
			# start point from (0,0) to (n-1,0)
		for i in range (0, n):
			startx = i
			starty = 0
			
			val = matrix [startx] [starty]
			
			while startx < n and starty < m:
				# stop while when x or y reached boundary
				if matrix [startx] [starty] != val:
					# values on the same line must be the same
					return False
				
				# update
				startx += 1
				starty += 1
			
			
			# return True
		
		return True
