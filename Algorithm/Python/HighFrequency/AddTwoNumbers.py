# 167. Add Two Numbers
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# You have two numbers represented by a linked list, where each node contains a single digit. The digits are stored in reverse order, such that the 1's digit is at the head of the list. Write a function that adds the two numbers and returns the sum as a linked list.
#
# Have you met this question in a real interview?
# Example
# Given 7->1->6 + 5->9->2. That is, 617 + 295.
#
# Return 2->1->9. That is 912.
#
# Given 3->1->5 and 5->9->2, return 8->0->8.
#
# Tags
# High Precision Cracking The Coding Interview Linked List Microsoft Amazon Bloomberg Airbnb Adobe

"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param l1: the first list
    @param l2: the second list
    @return: the sum list of l1 and l2
    """
    def addLists(self, l1, l2):
        # write your code here

        "special case"
        if l1 is None:
            return l2
        if l2 is None:
            return l1

        "need a special digit to record 0/1"

        "if one is longer than the other, continue with that one"

        dummy = ListNode(0)
        result = dummy

        p1 = l1
        p2 = l2
        digit = 0

        while p1 is not None and p2 is not None:

            val = (p1.val + p2.val + digit)
            digit = 1 if val >= 10 else 0
            nodeval = val - 10 if val >=10 else val
            node = ListNode(nodeval)
            result.next = node
            "go next"
            p1= p1.next
            p2 = p2.next
            result = result.next

        "if one is longer than the other"

        p = None
        if p1 is not None:
            p = p1

        if p2 is not None:
            p = p2

        if p is not None:
            while p is not None:
                val = p.val + digit
                digit = 1 if val >= 10 else 0
                nodeval = val - 10 if val >=10 else val
                node = ListNode(nodeval)
                result.next = node
                "go next"
                result = result.next
                p=p.next

        "if finally we have digit = 1"

        if digit == 1:
            node = ListNode(1)
            result.next = node

        return dummy.next

