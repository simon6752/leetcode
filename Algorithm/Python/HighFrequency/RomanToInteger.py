# 419. Roman to Integer
# Given a roman numeral, convert it to an integer.

# The answer is guaranteed to be within the range from 1 to 3999.

# Example
# IV -> 4

# XII -> 12

# XXI -> 21

# XCIX -> 99

class Solution:
    """
    @param s: Roman representation
    @return: an integer
    """
    def romanToInt(self, s):
        # write your code here

        # need to save 
        
        values = {"I":1,"V":5,"X":10,"L":50,"C":100,"D":500,"M":1000}
        
        # if left < right, left * -1, total + 
        
        if s is None:
            return 0 
            
        total = 0 
        
        for i in range(0,len(s)):
            
            if i!= len(s) -1 and values[s[i]] < values[s[i+1]]:
                total -= values[s[i]]
            else:
                total += values[s[i]]
                
        return total 