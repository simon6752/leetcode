# 41. Maximum Subarray
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given an array of integers, find a contiguous subarray which has the largest sum.
#
#  Notice
# The subarray should contain at least one number.
#
# Have you met this question in a real interview?
# Example
# Given the array [−2,2,−3,4,−1,2,1,−5,3], the contiguous subarray [4,−1,2,1] has the largest sum = 6.
#
# Challenge
# Can you do it in time complexity O(n)?
#
# Tags
# LinkedIn Subarray Greedy LintCode Copyright Microsoft Bloomberg Array Enumeration

"my solution not work"
import sys


class MySolution:
    """
    @param nums: A list of integers
    @return: A integer indicate the sum of max subarray
    """

    def maxSubArray(self, nums):
        # write your code here
        "naive two pointers solutions is O(n^2)"

        sum = []

        sumi = 0
        for i in range(0, len(nums)):
            sumi += nums[i]
            sum.append(sumi)

        "find the index of minimum and maximum"

        maxi = sum[0]
        mini = sum[0]
        minindex = 0
        maxindex = 0
        "we need to find maximum continous difference. so it can only be one of: 1. (local min before max, max), 2. (min, local max after min)"

        for i in range(1, len(sum)):
            "first find the glocal maximum and minimum location"
            if sum[i] < mini:
                mini = sum[i]
                minindex = i

            if sum[i] > maxi:
                maxi = sum[i]
                maxindex = i

        minindexb4max = 0
        localmin = sum[0]
        for i in range(maxindex - 1, -1, -1):
            if sum[i] < localmin:
                minindexb4max = i
                localmin = sum[i]

        maxindexaftermin = -1
        localmax = sum[-1]

        for i in range(minindex + 1, len(sum)):
            if sum[i] > localmax:
                maxindexaftermin = i
                localmax = sum[i]

        if minindex < maxindex:
            return sum[maxindex] - sum[minindex]
        else:
            sum1 = sum[maxindex] - sum[minindexb4max] if minindexb4max != 0 else sum[maxindex]
            sum2 = sum[maxindexaftermin] - sum[minindex] if maxindexaftermin != -1 else sum[minindex]
            return max(sum1, sum2)


class Solution:
    """
    @param nums: A list of integers
    @return: A integer indicate the sum of max subarray
    """

    def maxSubArray(self, nums):
        # write your code here
        "we need to find max of (current sum - previous min sum)"

        "maximum subarray sum, which is current sum - previous min sum"
        maxsum = -sys.maxsize
        "record minimum sum of all previous position"
        minsum = 0
        sum = 0
        for i in range(0, len(nums)):
            sum += nums[i]
            maxsum = max(maxsum, sum - minsum)
            minsum = min(minsum, sum)

        return maxsum


assert Solution().maxSubArray([-2, 2, -3, 4, -1, 2, 1, -5, 3]) == 6
assert Solution().maxSubArray([-1, -2, -3, -100, -1, -50]) == -1
