#
# 172. Remove Element
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given an array and a value, remove all occurrences of that value in place and return the new length.
#
# The order of elements can be changed, and the elements after the new length don't matter.
#
# Have you met this question in a real interview?
# Example
# Given an array [0,4,4,0,0,2,4,4], value=4
#
# return 4 and front four elements of the array is [0,0,0,2]
#
# Tags
# Two Pointers Array

class Solution:
    """
    @param: A: A list of integers
    @param: elem: An integer
    @return: The new length after remove
    """

    def removeElement(self, A, elem):
        # write your code here
        "two pointers. one from end, one from start. fill current 1st target value with current non-target value by end pointer"

        if A is None or len(A) == 0:
            return 0

        start = 0
        end = len(A) - 1

        while start <= end:

            if A[start] != elem:
                start += 1
                continue

            if A[end] == elem:
                end -= 1
                continue

            "now we get start is  elem and end is not elem"
            A[start] = A[end]
            start += 1
            end -= 1

        return start


assert Solution().removeElement([0, 4, 4, 0, 0, 2, 4, 4], 4) == 4
