#  418. Integer to Roman
# Description
# Given an integer, convert it to a roman numeral.
#
# The number is guaranteed to be within the range from 1 to 3999.
#
# Have you met this question in a real interview?
# Clarification
# What is Roman Numeral?
#
# https://en.wikipedia.org/wiki/Roman_numerals
# https://zh.wikipedia.org/wiki/罗马数字
# http://baike.baidu.com/view/42061.htm
# Example
# 4 -> IV
#
# 12 -> XII
#
# 21 -> XXI
#
# 99 -> XCIX
#
# more examples at: http://literacy.kent.edu/Minigrants/Cinci/romanchart.htm
#
#
# 418. Integer to Roman
# Given an integer, convert it to a roman numeral.
#
# The number is guaranteed to be within the range from 1 to 3999.
#
# Example
# 4 -> IV
#
# 12 -> XII
#
# 21 -> XXI
#
# 99 -> XCIX
#
# more examples at: http://literacy.kent.edu/Minigrants/Cinci/romanchart.htm

class Solution:
	"""
	@param n: The integer
	@return: Roman representation
	"""
	
	def intToRoman (self, n):
		# write your code here
		
		num = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
		# for integer, we start from large to small
		
		
		num1 = ["I", "X", "C", "M"]
		num5 = ["V", "L", "D"]
		
		# from large to small
		result = ""
		val = 0
		res = n
		for i in range (len (num1) - 1, -1, -1):
			val = res // num [num1 [i]]
			res = res % num [num1 [i]]
			
			# check if val is 0, 1-4,5, or 6-8, 9
			# I, II, III,IV, V, VI, VII,VIII,XI X
			if val > 0 and val < 4:
				# 1, 2,3
				for j in range (val):
					result += num1 [i]
			elif val == 4:
				result += num1 [i]
				result += num5 [i]
			elif val == 5:
				result += num5 [i]
			elif val > 5 and val < 9:
				# 6, 7 , 8
				result += num5 [i]
				for j in range (val - 5):
					result += num1 [i]
			elif val == 9:
				result += num1 [i]
				result += num1 [i + 1]
			elif val == 0:
				pass
		
		return result

# test
assert  Solution().intToRoman(99) == "XCIX"


# complexity

# time O(n), space O(n)