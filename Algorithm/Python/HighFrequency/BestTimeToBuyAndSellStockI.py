# 149. Best Time to Buy and Sell Stock
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Say you have an array for which the ith element is the price of a given stock on day i.
#
# If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.
#
# Have you met this question in a real interview?
# Example
# Given array [3,2,3,1,2], return 1.
#
# Tags
# Amazon Greedy Bloomberg Uber Microsoft Array Facebook Enumeration
import sys


class Solution:
    """
    @param prices: Given an integer array
    @return: Maximum profit
    """

    def maxProfit(self, prices):
        # write your code here
        "this is the same as maximum subarray. we do it from end of list"
        "record previous maximum price, use  previous maximum price minus current price, see if we get maximum profit"

        if prices is None or len(prices) <= 1:
            return 0

        maxprofit = - sys.maxsize
        premax = prices[-1]

        "need to handle boundary cases"

        for i in range(len(prices) - 2, -1, -1):
            maxprofit = max(maxprofit, premax - prices[i])
            premax = max(premax, prices[i])

        "we can do nothing if maxprofit is less than 0"
        return max(maxprofit, 0)
