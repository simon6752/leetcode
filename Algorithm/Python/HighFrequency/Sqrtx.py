# 141. Sqrt(x)
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Implement int sqrt(int x).
#
# Compute and return the square root of x.
#
# Have you met this question in a real interview?
# Example
# sqrt(3) = 1
#
# sqrt(4) = 2
#
# sqrt(5) = 2
#
# sqrt(10) = 3
#
# Challenge
# O(log(x))
#
# Tags
# Binary Search Mathematics Apple Bloomberg Facebook


class Solution:
    """
    @param x: An integer
    @return: The sqrt of x
    """

    def sqrt(self, x):
        # write your code here
        "use binary search"

        if x == 0:
            return 0

        start = 1
        end = x

        while start + 1 < end:
            mid = start + (end - start) // 2

            if mid * mid == x:
                return mid
            elif mid * mid > x:
                end = mid
            else:
                start = mid

        if end * end < x:
            return end
        else:
            "return the smaller one"
            return start


