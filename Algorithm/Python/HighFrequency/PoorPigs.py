# 1228. Poor Pigs
# Description
# There are 1000 buckets, one and only one of them contains poison, the rest are filled with water. They all look the same. If a pig drinks that poison it will die within 15 minutes. What is the minimum amount of pigs you need to figure out which bucket contains the poison within one hour.

# Answer this question, and write an algorithm for the follow-up general case.

# Have you met this question in a real interview?  
# Challenge
# If there are n buckets and a pig drinking poison will die within m minutes, how many pigs (x) you need to figure out the "poison" bucket within p minutes? There is exact one bucket with poison.


from math import log,floor,ceil
class Solution:
    """
    @param buckets: an integer
    @param minutesToDie: an integer
    @param minutesToTest: an integer
    @return: how many pigs you need to figure out the "poison" bucket within p minutes 
    """
    def poorPigs(self, buckets, minutesToDie, minutesToTest):
        # Write your code here
        # for this question, think about it that how we can reuse pig. 
        # a pig can drink multiple buckets water at one time, and can die at some time 
        # total times p//m 
        
        # n = p //m 
        n = minutesToTest//minutesToDie
        
        # corner case, n = 1 and n = 0 
        if n == 1 or n == 0:
            # need the same as buckets
            return 0
        
        # total buckets, lets say 1000. 
        # lets say if we have two pigs, 3 time chance :
        # 0 1 2  3 
        # 4 5 6  7 
        # 8 9 10 11 
        # 12 13 14 15 
        # 1st time, let pig 1 drink 0 4 8 12, 2 drink 0 1 2 3 
        # 2nd time, let pig 1 drink 1 5 9 13, 2 drink 4 5 6 7
        # 3rd time, let pig 1 drink 2 6 10 14, 2 drinks 8 9 10 11 
        # 4rd time, let pig 1 drink 3 7 11 15, 2 drinks 12 13 14 15 
        # if the poison is at bucket 6, then pig 1 dies at 3rd time and pig 2 dies at 2nd time.
        # so pig number is dimension. we can have (3+1) ^(pig number ) = total buckets 
        # pig needed is log (total buckets)/log(time ), and we need at least one more when it is not exactly integer 
        # if we get 4.2, we need 5. if we get 5, we only need 5 , use ceil.
        
        return ceil(log(buckets)/(log(n + 1 ))) 