# #   604 Window Sum
# Description
# Given an array of n integer, and a moving window(size k), move the window at each iteration from the start of the array, find the sum of the element inside the window at each moving.
#
#
# Example
# For array [1,2,7,8,5], moving window size k = 3.
# 1 + 2 + 7 = 10
# 2 + 7 + 8 = 17
# 7 + 8 + 5 = 20
# return [10,17,20]


class Solution:
	
	
	def winSum(self,nums,k):
		
		if nums is None or k < 0 :
			return [0]
		
		if k >= len(nums):
			# return k size of [sum(nums)]
			return [sum(nums)]
		
		
		temp = sum(nums[0:k])
		
		result = [temp]
		
		for i in range(k,len(nums)):
			
			result.append(result[-1]+nums[i]-nums[i-k])
		
		return result
	

assert Solution().winSum([1,2,7,8,5],3) == [10,17,20]