# * use hashtable ( dictonary for python) to save time
#
# Given an array of integers, return indices of the two numbers such that they add up to a specific target.
#
# You may assume that each input would have exactly one solution, and you may not use the same element twice.
#
# Example:
#
# Given
# nums = [2, 7, 11, 15], target = 9,
#
# Because
# nums[0] + nums[1] = 2 + 7 = 9,
# return [0, 1].


class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        # put data in hash table (dictionary). exactly one solution
        # corner case:
        # what if 6 =  3 +  3

        dict1 = {}
        for i in range(0, len(nums)):
            if (nums[i] in dict1.keys() and nums[i] * 2 == target):
                #                 already put target/2 in dictionary, so return dict1[nums[i]] and current i
                return sorted([i, dict1[nums[i]]])
            dict1[nums[i]] = i

        for i in dict1.keys():
            if target - i in dict1.keys():
                return sorted([dict1[i], dict1[target - i]])


assert Solution().twoSum([2, 7, 11, 15], 9) == [0, 1]
assert Solution().twoSum([0,4,3,0], 0) == [0, 3]


class Solution2:
    """
    @param numbers: An array of Integer
    @param target: target = numbers[index1] + numbers[index2]
    @return: [index1 + 1, index2 + 1] (index1 < index2)
    """

    def twoSum(self, numbers, target):
        # write your code here

        "use hash table(dictionary)"

        dict = {}
        for i in range(0, len(numbers)):
            "how about duplicates???  what if there are multiple pairs that satisfy the requirement?"
            "check if there is duplicate? if yes, check whether it is half of target"
            if numbers[i] in dict.keys():
                if 2 * numbers[i] == target:
                    return [min(i, dict[numbers[i]]), max(i, dict[numbers[i]])]
            else:
                "no duplicate yet, put it in hashtable"
                dict[numbers[i]] = i

        for i in range(0, len(numbers)):
            if numbers[i] in dict.keys() and target - numbers[i] in dict.keys():
                return [min(dict[numbers[i]], dict[target - numbers[i]]),
                        max(dict[numbers[i]], dict[target - numbers[i]])]

        return [-1, -1]

assert Solution2().twoSum([2, 7, 11, 15], 9) == [0, 1]
assert Solution2().twoSum([0,4,3,0], 0) == [0, 3]