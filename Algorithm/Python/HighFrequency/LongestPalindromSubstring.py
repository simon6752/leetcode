class Solution:
    """
    @param s: input string
    @return: the longest palindromic substring
    """

    def longestPalindrome(self, s):
        # write your code here
        "naive solution, two pointers"

        if s is None or len(s) == 0:
            return ""

        if len(s) == 1:
            return s

        maxp = 0
        subs = ""
        for p in range(0, len(s)):
            curp = 1
            for q in range(1, min(p + 1, len(s) - p)):
                if s[p + q] == s[p - q]:
                    curp += 2
                    if curp > maxp:
                        "need to update our current longest palindrom substring"
                        subs = s[p - q: p + q + 1]
                else:
                    break

            maxp = max(maxp, curp)

        subs2 = ""
        "we also need to consider where palindrom is even number"
        for p in range(1, len(s)):
            curp = 0
            for q in range(1, min(p, len(s) - p) + 1):
                if s[p - q] == s[p + q - 1]:
                    "palindrom for now"
                    curp += 2
                    if curp > maxp:
                        subs2 = s[p - q: p + q]
                else:
                    break
            maxp = max(maxp, curp)

        return subs if len(subs) > len(subs2) else subs2


assert Solution().longestPalindrome("a") == "a"
assert Solution().longestPalindrome("aa") == "aa"
assert Solution().longestPalindrome("ccc") == "ccc"


class Solution2:
    """
    @param s: input string
    @return: the longest palindromic substring
    """

    def longestPalindrome(self, s):
        # write your code here
        "naive solution, two pointers"

        if s is None or len(s) == 0:
            return ""

        if len(s) == 1:
            return s
        "first count ith positon, then count ith space"

        "position"
        lps = 1
        lpss = s[0]

        for i in range(0, len(s)):
            lpsi = 1
            for j in range(1, min(i + 1, len(s) - i)):
                if s[i + j] == s[i - j]:
                    lpsi += 2
                else:
                    break

            if lpsi > lps:
                lps = lpsi
                lpss = s[i - (lpsi - 1) // 2:i + (lpsi - 1) // 2 + 1]

        lpss2 = ""
        lps = 0
        for i in range(0, len(s) - 1):
            lpsi = 0
            for j in range(1, min(i + 2, len(s) - i)):
                " 0  1  2  .... k ( space).... n-1 "
                if s[i + 1 - j] == s[i + j]:
                    lpsi += 2
                else:
                    break
            if lpsi > lps:
                lps = lpsi
                lpss2 = s[i + 1 - lpsi // 2: i + lpsi // 2 + 1]

        return lpss if len(lpss) > len(lpss2) else lpss2


assert Solution2().longestPalindrome("bb") == "bb"
assert Solution2().longestPalindrome("a") == "a"
assert Solution2().longestPalindrome("aaa") == "aaa"

# or use dynamic programming O(n^2)


class Solution3:
    """
    @param s: input string
    @return: the longest palindromic substring
    """
    def longestPalindrome(self, s):
        # write your code here
        
        "use dynamic programming"
        
        "f[i][j], length of palindromic substring start i, end j"
        "or longest palindromic substring???"
        
        "go with 1st"
        
        if s is None:
            return ""
        if len(s) <= 1:
            return s
            
        lps = []
        
        "initialization with empty list"
        for i in range(0,len(s)):
            lps.append([])
            
        maxlps = 1
        lpss = s[0]
        "decomposition"
        for i in range(len(s)-1,-1,-1):
            "start i from large to small because small depends on large, so calculate large first"
            for j in range(0,len(s)):
                "end position could be i -> len(s) -1"
                if j < i:
                    "start must be less than end"
                    lps[i].append(0)
                elif j == i:
                    "there is always palindrom when i == j"
                    lps[i].append(1)
                else:
                    if s[i] == s[j]:
                        "start letter equals to end letter"
                        if (i+1 < j and lps[i+1][j-1] !=0) or (j == i+1):
                            "either there is enough string between i and j and it is palindrom, or there is no string between i and j"
                            lps[i].append(lps[i+1][j-1]+2)
                            if lps[i][j] > maxlps:
                                maxlps = lps[i][j]
                                lpss = s[i:j+1]
                        else:
                            "internal substring is not palindrom"
                            lps[i].append(0)
                            
                    else:
                        "no way"
                        lps[i].append(0)
        
        return lpss
            
        

assert Solution3().longestPalindrome("bb") == "bb"
