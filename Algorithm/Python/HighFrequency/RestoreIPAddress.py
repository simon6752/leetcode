#  426. Restore IP Addresses
# Description
# Given a string containing only digits, restore it by returning all possible valid IP address combinations.
#
# Have you met this question in a real interview?
# Example
# Given "25525511135", return
#
# [
#   "255.255.11.135",
#   "255.255.111.35"
# ]
# Order does not matter.


class Solution:
	"""
	@param s: the IP string
	@return: All possible valid IP addresses
	"""
	
	def restoreIpAddresses (self, s):
		# write your code here
		# backtracking
		
		if s is None or len (s) == 0:
			return []
		
		result = []
		
		self.restoreHelper (s, 0, result, s, [])
		
		return result
	
	def restoreHelper (self, s, level, result, currentStr, currentCombination):
		
		if level == 4 and currentStr == "":
			# reached level 4, and we already spend all strings, add it to result
			result.append (currentCombination [0] + "." + currentCombination [1] + "." + currentCombination [2] + "." +
			               currentCombination [3])
			return
		
		# check first 3 letter of currentStr
		# recursion
		# need to check length of current string, 0, 1,2,3,+
		# need to check case like "01","00","010"
		if len (currentStr) > 0:
			self.restoreHelper (s, level + 1, result, currentStr [1:], currentCombination + [currentStr [0]])
			if len (currentStr) > 1 and currentStr [0] != "0":
				self.restoreHelper (s, level + 1, result, currentStr [2:], currentCombination + [currentStr [0:2]])
				if len (currentStr) > 2 and self.toDigit (currentStr [0:3]) <= 255:
					self.restoreHelper (s, level + 1, result, currentStr [3:], currentCombination + [currentStr [0:3]])
	
	def toDigit (self, s):
		# return "255" to 255
		if s is None or s == "":
			return 0
		
		#
		result = 0
		
		for i in range (0, len (s)):
			result = result * 10 + ord (s [i]) - ord ("0")
		
		return result
	
assert  sorted(Solution().restoreIpAddresses("172162541"))  == sorted(["17.216.25.41","17.216.254.1","172.16.25.41","172.16.254.1","172.162.5.41","172.162.54.1"])