#  59. 3Sum Closest
# Description
# Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers.
#
# You may assume that each input would have exactly one solution.
#
# Have you met this question in a real interview?
# Example
# For example, given array S = [-1 2 1 -4], and target = 1. The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
#
# Challenge
# O(n^2) time, O(1) extra space


class Solution:
	"""
	@param numbers: Give an array numbers of n integer
	@param target: An integer
	@return: return the sum of the three integers, the sum closest target.
	"""
	
	def threeSumClosest (self, numbers, target):
		# write your code here
		# compare to 3 sum, we just need to continue add until we added all a + b + c < target when we find one
		
		if numbers is None or len (numbers) < 3:
			return None
		
		numbers = sorted (numbers)
		
		result = numbers [0] + numbers [1] + numbers [2]
		closest = abs (target - result)
		
		# fix 1st one
		i = 0
		while i < len (numbers) - 2:
			# 1st one from 0 to n-3
			pmin = i + 1
			pmax = len (numbers) - 1
			# min pos is i+1, max pos is n - 1
			# for target  - numbers[i], we need to think about (i+1, i+2), (n-2,n-1), (i+1,n-1)
			# need to do early stop( (i+1,i+2),(n-2,n-1)), need to skip duplicates)
			# if target - numbers[i] > numbers[-2] + numbers[-1]:
			#     # cloest would be number[-2] and numbers[-1]
			#     if abs(numbers[i] + numbers[-2] + numbers[-1] - target ) < closest:
			#         closest =  abs(numbers[i] + numbers[-2] + numbers[-1] - target )
			#         result = numbers[i] + numbers[-1] + numbers[-2]
			# elif target - numbers[i] > numbers[i+1] + numbers[i+2]:
			#     # closest would be i, i+1, i+2
			#     if abs(numbers[i] + numbers[i+1] + numbers[i+2] - target ) < closest:
			#         closest = abs(numbers[i] + numbers[i+1] + numbers[i+2] - target)
			#         result = numbers[i] + numbers[i+1] + numbers[i+2]
			# else:
			# if not larger or smaller
			while pmin < pmax:
				# need to find out closest
				if abs (numbers [i] + numbers [pmin] + numbers [pmax] - target) < closest:
					closest = abs (numbers [i] + numbers [pmin] + numbers [pmax] - target)
					result = numbers [i] + numbers [pmin] + numbers [pmax]
				
				if target - numbers [i] < numbers [pmin] + numbers [pmax]:
					# move pmax left
					pmax -= 1
				else:
					# move pmin right
					pmin += 1
			
			i += 1
		
		return result

assert Solution().threeSumClosest([1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,0,0,-2,2,-5,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99,1,2,5,6,7,3,5,8,-33,-5,-72,12,-34,100,99], 25) == 25

# copied and modified from 3 sum, need to skip duplicates

class Solution2:
	"""
	@param numbers: Give an array numbers of n integer
	@param target: An integer
	@return: return the sum of the three integers, the sum closest target.
	"""
	
	def threeSumClosest (self, numbers, target):
		# write your code here
		"""
		"use the result of two sum, avoid duplicate"
		"no"
		"sort to avoid duplicates"

		"iterate over the list, find -S[i] in the rest of the list(use two pointers)"
		"""
		
		nums = sorted (numbers)
		
		result = nums [0] + nums [1] + nums [2]
		temp = 0
		currentClosest = abs (target - nums [0] - nums [1] - nums [2])
		for i in range (0, len (nums) - 2):
			"initialize two pointers"
			pMin = i + 1
			pMax = len (nums) - 1
			
			"if current nums[i] is equal to nums[i-1], minimum number is the same, needs to skip"
			if i > 0 and nums [i] == nums [i - 1]:
				continue
			
			while pMin < pMax:
				temp = nums [pMin] + nums [pMax]
				if abs (target - nums [i] - temp) < currentClosest:
					currentClosest = abs (target - nums [i] - temp)
					result = nums [i] + nums [pMin] + nums [pMax]
				
				if temp > target - nums [i]:
					pMax = pMax - 1
				else:
					pMin = pMin + 1
				
				"need to skip the same value"
				while pMax > pMin and pMax < len (nums) - 1 and nums [pMax] == nums [pMax + 1]:
					pMax = pMax - 1
				
				while pMin < pMax and pMin > i and nums [pMin] == nums [pMin - 1]:
					pMin = pMin + 1
		
		return result
