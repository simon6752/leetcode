#  994. Contiguous Array
# Description
# Given a binary array, find the maximum length of a contiguous subarray with equal number of 0 and 1.
#
# The length of the given binary array will not exceed 50,000.
#
# Have you met this question in a real interview?
# Example
# Example 1:
#
# Input: [0,1]
# Output: 2
# Explanation: [0, 1] is the longest contiguous subarray with equal number of 0 and 1.
# Example 2:
#
# Input: [0,1,0]
# Output: 2
# Explanation: [0, 1] (or [1, 0]) is a longest contiguous subarray with equal number of 0 and 1.


class Solution:
	"""
	@param nums: a binary array
	@return: the maximum length of a contiguous subarray
	"""
	
	def findMaxLength (self, nums):
		# Write your code here
		# similar to maximum subarray size equals size k. we know subarray has equal 0s and 1s, we need to find start and end of the maximum length of array. once we iterate left end, we need to look for specific number of difference of 0s and 1s in right end, so we use hash table for quick look up to optimize O(n) to O(1)
		
		# naive solution is O(n^2). with hash table, we can optimize it to O(n)
		
		if nums is None or len (nums) == 0:
			return 0
		
		totaldiff = self.diff1and0 (nums, 0, len (nums) - 1)
		
		# need a hash table to save, from right to left, key is the difference of 1s and 0s, value is list of indecies for look up
		
		rightdifftable = {}
		totalrightdiff = 0
		for j in range (len (nums), -1, -1):
			# j is the position of last position, so it could be len(nums), which means it is an empty array
			if j != len (nums):
				if nums [j] == 1:
					totalrightdiff += 1
				else:
					totalrightdiff -= 1
			
			if totalrightdiff in rightdifftable.keys ():
				# already there, just need to append
				rightdifftable [totalrightdiff].append (j)
			else:
				# otherwise we need to initialize a list
				rightdifftable [totalrightdiff] = [j]
		
		# now we start from left side, check one by one
		
		totalleftdiff = 0
		longest = 0
		for i in range (-1, len (nums)):
			# i is the left end letter position, it could be -1, which means it is empty array
			if i != -1:
				if nums [i] == 1:
					totalleftdiff += 1
				else:
					totalleftdiff -= 1
				
				# now we need to look up the value: totaldiff - 0 - totalleftdiff in rightdifftable. Find that list, and check all larger than i index j, then find longest j - i
			
			if totaldiff - totalleftdiff in rightdifftable.keys ():
				rightlist = rightdifftable [totaldiff - totalleftdiff]
				
				for m in range (0, len (rightlist)):
					# check each rightlist[m]
					
					if rightlist [m] - 1 >= i + 1:
						# subarray start is i+1, end is rightlist[m] - 1
						longest = max (longest, rightlist [m] - 1 - (i + 1) + 1)
		
		return longest
	
	def diff1and0 (self, nums, start, end):
		
		if nums is None or len (nums) == 0:
			return 0
		
		totaldiff = 0  # record difference of total 1s - total 0s
		
		for i in range (start, end + 1):
			if nums [i] == 1:
				totaldiff += 1
			else:
				totaldiff -= 1
		
		return totaldiff