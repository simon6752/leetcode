# 1363. ZigZag Conversion
# The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

# P   A   H   N
# A P L S I I G
# Y   I   R
# And then read line by line: "PAHNAPLSIIGYIR"
# Write the code that will take a string and make this conversion given a number of rows:

# String convert(String s, int numRows);
# convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".

# Example
# Given s = "PAYPALISHIRING", numRows = 3, return "PAHNAPLSIIGYIR".

class Solution:
    """
    @param s: the given string
    @param numRows: the number of rows
    @return: the string read line by line
    """
    def convert(self, s, numRows):
        # Write your code here

    # 0   4   4*i              4*(i+1)
    # 1 3 ... 4*i + 1 4*i + 3 
    # 2       4*i + 2 
    
    # 2* row - 2 
    # (2*r -2 )* i                                       (2*r -2 ) *(i + 1 )
    # (2*r -2 ) * i + 1       (2*r -2 ) * i + 2*r - 3
    # ...
    # (2*r -2 ) * i  + r -1 
    
        if numRows == 1:
            return s 
            
        k = 2 * numRows  - 2 
        result = ""
        
        for j in range(0,numRows):
            for i in range(0, len(s)//k + 2 ):
                if (k*i + j ) < len(s):
                    result += s[k*i + j]
                if (k*i + 2 * numRows -2 - j) < len(s):
                    if j != 0 and j != numRows -1 :
                        "not first and last row"
                        result += s[k*i + 2*numRows -2 -j]
                        
        return result 

string1 = "Apalindromeisaword,phrase,number,orothersequenceofunitsthatcanbereadthesamewayineitherdirection,withgeneralallowancesforadjustmentstopunctuationandworddividers."

assert Solution().convert(string1,5) == "Aore,sohraerwraancaiprmods,roreeftaeesmniie,ieawnrdetntnndv.adew,anereqcustbaeeitdcntnlocojmsuuoddislniaprubohunntcndhwyhrtohealefuttpaiwrdrishmteiataeiglssotoe"

