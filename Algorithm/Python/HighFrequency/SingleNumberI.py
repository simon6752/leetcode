# 82. Single Number
# Given 2*n + 1 numbers, every numbers occurs twice except one, find it.

# Example
# Given [1,2,2,1,3,4,3], return 4

# Challenge
# One-pass, constant extra space.

class Solution:
    """
    @param A: An integer array
    @return: An integer
    """

    def singleNumber(self, A):
        # write your code here
        "bit operation"

        m = 0
        for i in range(0, len(A)):
            m = m ^ A[i]

        return m


assert Solution().singleNumber([1, 1, 2, 2, 3, 4, 4]) == 3
