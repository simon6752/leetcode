# 643. Longest Absolute File Path
# Suppose we abstract our file system by a string in the following manner:

# The string "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext" represents:

# dir
#     subdir1
#     subdir2
#         file.ext
# The directory dir contains an empty sub-directory subdir1 and a sub-directory subdir2 containing a file file.ext.

# The string

# "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"
# represents:

# dir
#     subdir1
#         file1.ext
#         subsubdir1
#     subdir2
#         subsubdir2
#             file2.ext
# The directory dir contains two sub-directories subdir1 and subdir2. subdir1 contains a file file1.ext and an empty second-level sub-directory subsubdir1. subdir2 contains a second-level sub-directory subsubdir2 containing a file file2.ext.

# We are interested in finding the longest (number of characters) absolute path to a file within our file system. For example, in the second example above, the longest absolute path is "dir/subdir2/subsubdir2/file2.ext", and its length is 32 (not including the double quotes).

# Given a string representing the file system in the above format, return the length of the longest absolute path to file in the abstracted file system. If there is no file in the system, return 0.

# Example
# Give input = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext" return 20

# not pass
class Solution:
    """
    @param input: an abstract file system
    @return: return the length of the longest absolute path to file
    """
    def lengthLongestPath(self, input):
        # write your code here
        # map string to tree, calculate maximum depth 
        
        # calculate maximum string in tree, DFS, pre-order, use stack 
        # if we know pre-order and level of each node, find the maximum depth(includ node length) in a tree  
        
        if input is None or len(input) == 0 :
            return 0 
        
        inputSplit = input.split("\n")
        
        stack = [] 
        maxlength = 0 
        # need a hashtable to save all previous level and index/length at current position
        levelIndexInfo = {}
        levelIndexInfo[0]= 0
        levelLengthInfo = {}
        levelLengthInfo[0]= len(inputSplit[0])+1 
        
        # level by number of "\t" . leaf is .ext
        
        self.dfs(maxlength,inputSplit,0,0,0,levelIndexInfo,levelLengthInfo)
        
        return maxlength
        
    def dfs(self,maxlength,input,index,level,currentlength,levelIndexInfo,levelLengthInfo):
        # do a pre-order dfs on input
        
        if len(input[index]) > 4 and input[index][-4:] == ".ext":
            # it is a file, need to compare and return 
            # +1 is for the "\n"
            maxlength = max(maxlength,currentlength + len(input[index])+1)
            return 
        else:
            # folder, go deeper, check if next one is one more level. if yes, go deeper 
            # number of "\t" is level. start from 0 
            
            currentLevel = len(input[index].split("\t")) -1
            levelIndexInfo[currentLevel]= index 
        
        
# TODO