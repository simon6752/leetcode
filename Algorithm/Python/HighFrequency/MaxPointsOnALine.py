# 186. Max Points on a Line
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
#
# Have you met this question in a real interview?
# Example
# Given 4 points: (1,2), (3,6), (0,0), (1,3).
#
# The maximum number is 3.
#
# Tags
# LinkedIn Twitter Apple Hash Table Mathematics


"""
Definition for a point.
class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b
"""
import math

"""
Definition for a point.
class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b
"""

"my own solution is not complete yet"


class Solution:
    """
    @param points: an array of point
    @return: An integer
    """

    def maxPoints(self, points):
        # write your code here
        "naive solution: for any i and j point, find k and b. then find points with the same (k,b)"

        "but naive solution is C(n,2) = n*(n-1)/2, O(n^2)"

        "fine, let's do it with hash table"

        if points is None or len(points) == 0:
            return 0

        if len(points) <= 2:
            return len(points)

        dict = {}
        "save infinity slope point"
        dict2 = {}
        "save duplicate points"
        dict3 = {}
        curMax = 0
        for i in range(0, len(points)):

            for j in range(0, len(points)):

                if j != i:
                    "need to handle duplicate points(x1 == x2 && y1 == y2) and infinity slope(x1 == x2)"
                    if points[i].x == points[j].x and points[i].y == points[j].y:
                        "duplicate points"
                        if (points[i].x, points[i].y) in dict3.keys():
                            dict3[(points[i].x, points[i].y)] = dict3[(points[i].x, points[i].y)] + 1
                        else:
                            dict3[(points[i].x, points[i].y)] = 1

                    elif points[i].x == points[j].x and points[i].y != points[j].y:
                        "if k is inf, we will use a seperate hashtable, key is x"
                        if points[i].x in dict2:
                            dict2[points[i].x] = dict2[points[i].x] + 1
                        else:
                            dict2[points[i].x] = 1
                        curMax = max(curMax, dict2[points[i].x])
                    else:
                        k = (points[i].y - points[j].y) / (points[i].x - points[j].x)
                        b = (points[i].y * points[j].x - points[i].x * points[j].y) / (points[j].x - points[i].x)

                        "Is this (k,b) in dict? if yes, inc, if not, put in"
                        if (k, b) in dict.keys():
                            dict[(k, b)] = dict[(k, b)] + 1
                        else:
                            dict[(k, b)] = 1
                        curMax = max(curMax, dict[(k, b)])

        "1. if we have m points in a line, the combination of (k,b) is m(m-1)/2"
        "2. we need to take care of k is 0 and inf"

        "decompose curMax to m(m-1)"

        "let find sqrt(curMax), then + 1"

        result = int(math.sqrt(curMax)) + 1

        return result


class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b


list1 = []
list1.append(Point(1, 2))
list1.append(Point(3, 6))
list1.append(Point(0, 0))
list1.append(Point(1, 3))
assert Solution().maxPoints(list1) == 3

list2 = []
list2.append(Point(0, 0))
list2.append(Point(1, 1))
list2.append(Point(1, -1))
assert Solution().maxPoints(list2) == 2

#  [[0,-12],[5,2],[2,5],[0,-5],[1,5],[2,-2],[5,-4],[3,4],[-2,4],[-1,4],[0,-5],[0,-8],[-2,-1],[0,-11],[0,-9]]  6

list3 = [[0, -12], [5, 2], [2, 5], [0, -5], [1, 5], [2, -2], [5, -4], [3, 4], [-2, 4], [-1, 4], [0, -5], [0, -8],
         [-2, -1], [0, -11], [0, -9]]
list4 = []
for i in range(0, len(list3)):
    list4.append(Point(list3[i][0], list3[i][1]))

assert Solution().maxPoints(list4) == 6

list5 = [[-54, -297], [-36, -222], [3, -2], [30, 53], [-5, 1], [-36, -222], [0, 2], [1, 3], [6, -47], [0, 4], [2, 3],
         [5, 0], [48, 128], [24, 28], [0, -5], [48, 128], [-12, -122], [-54, -297], [-42, -247], [-5, 0], [2, 4],
         [0, 0], [54, 153], [-30, -197], [4, 5], [4, 3], [-42, -247], [6, -47], [-60, -322], [-4, -2], [-18, -147],
         [6, -47], [60, 178], [30, 53], [-5, 3], [-42, -247], [2, -2], [12, -22], [24, 28], [0, -72], [3, -4],
         [-60, -322], [48, 128], [0, -72], [-5, 3], [5, 5], [-24, -172], [-48, -272], [36, 78], [-3, 3]]
list6 = []
for i in range(0, len(list5)):
    list6.append(Point(list5[i][0], list5[i][1]))


# assert Solution().maxPoints(list6) == 30


class Solution2:
    # @param {int[]} points an array of point
    # @return {int} an integer
    def maxPoints(self, points):
        # Write your code here
        len_points = len(points)
        if len_points <= 1:
            return len_points
        max_count = 0
        for index1 in range(0, len_points):
            p1 = points[index1]
            gradients = {}
            infinite_count = 0
            duplicate_count = 0
            for index2 in range(index1, len_points):
                p2 = points[index2]
                dx = p2.x - p1.x
                dy = p2.y - p1.y
                if 0 == dx and 0 == dy:
                    duplicate_count += 1
                if 0 == dx:
                    infinite_count += 1
                else:
                    g = float(dy) / dx
                    gradients[g] = (gradients[g] + 1 if g in gradients.keys() else 1)
            if infinite_count > max_count:
                max_count = infinite_count
            for k, v in gradients.items():
                v += duplicate_count
                if v > max_count:
                    max_count = v
        return max_count


list5 = [[-54, -297], [-36, -222], [3, -2], [30, 53], [-5, 1], [-36, -222], [0, 2], [1, 3], [6, -47], [0, 4], [2, 3],
         [5, 0], [48, 128], [24, 28], [0, -5], [48, 128], [-12, -122], [-54, -297], [-42, -247], [-5, 0], [2, 4],
         [0, 0], [54, 153], [-30, -197], [4, 5], [4, 3], [-42, -247], [6, -47], [-60, -322], [-4, -2], [-18, -147],
         [6, -47], [60, 178], [30, 53], [-5, 3], [-42, -247], [2, -2], [12, -22], [24, 28], [0, -72], [3, -4],
         [-60, -322], [48, 128], [0, -72], [-5, 3], [5, 5], [-24, -172], [-48, -272], [36, 78], [-3, 3]]
list6 = []
for i in range(0, len(list5)):
    list6.append(Point(list5[i][0], list5[i][1]))

assert Solution2().maxPoints(list6) == 30

"refined my own version "


class Solution3:
    """
    @param points: an array of point
    @return: An integer
    """

    def maxPoints(self, points):
        # write your code here
        "use hash table, and need to handle duplicate points and infinity slope"

        "special case"

        if points is None:
            return 0

        if len(points) <= 2:
            return len(points)

        n = len(points)
        cntMax = 0
        for i in range(0, n):
            "initialize slope hash table"
            slope = {}
            cntDuplicate = 0
            cntInf = 0
            for j in range(i, n):
                "consider start point at i, and end point after it"

                dx = points[j].x - points[i].x
                dy = points[j].y - points[i].y

                if dx == 0 and dy == 0:
                    cntDuplicate += 1

                if dx == 0:
                    cntInf += 1

                if dx != 0:
                    slp = float(dy) / dx
                    "update slope hash table count"
                    slope[slp] = slope[slp] + 1 if slp in slope.keys() else 1

            cntMax = max(cntMax, cntInf)
            for k in slope.keys():
                slope[k] += cntDuplicate
                cntMax = max(cntMax, slope[k])

        return cntMax
