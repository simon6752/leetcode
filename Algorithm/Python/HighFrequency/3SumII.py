# 831. 3Sum II
# Given n, find the number of solutions for all x, y, z that conforms to x^2+y^2+z^2 = n.(x, y, z are non-negative integers)

# Example
# Given n = 0, return 1.

# Explanation:
# When x = 0, y = 0, z = 0, the equation holds.
# Given n = 1, return 1.

# Explanation:
# When one of them is 1 and the remaining two are 0, there is a total of 1 solution.


class Solution:
    """
    @param n: an integer
    @return: the number of solutions
    """
    def threeSum2(self, n):
        # Write your code here
        "first, find sqrt(n), assume 0<=x<=y<=z<= sqrt(n)"
        
        if n == 0 or n == 1:
            return 1 
            
        count = 0
        
        root = self.sqrt(n) + 1 
        
        for z in range(root,-1,-1):
            for y in range(z,-1,-1):
                if z * z + y*y <= n:
                    root2 = self.sqrt(n -z*z - y*y)
                    if root2*root2  + y*y + z*z == n and root2 <= y:
                        # print("x " + str(root2) + " y " + str(y) + " z " + str(z))
                        count += 1 
                        
        return count
        
    def sqrt(self,n):
        
        if n == 0 or n == 1:
            return n 
            
        "use binary search to find sqrt(n)"
        
        start = 0
        
        end = n 
        
        while start + 1 < end:
            
            mid = start + (end-start)//2 
            
            if mid*mid == n:
                return mid 
            
            elif mid*mid > n:
                end = mid
            else:
                start = mid
                
        
        "return smaller of start, end"
        
        return start 