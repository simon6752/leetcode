# # #    58. 4Sum
# Description
# Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target?
#
# Find all unique quadruplets in the array which gives the sum of target.
#
# Elements in a quadruplet (a,b,c,d) must be in non-descending order. (ie, a ≤ b ≤ c ≤ d)
# The solution set must not contain duplicate quadruplets.
#
# Have you met this question in a real interview?
# Example
# Given array S = {1 0 -1 0 -2 2}, and target = 0. A solution set is:
#
# (-1, 0, 0, 1)
# (-2, -1, 1, 2)
# (-2, 0, 0, 2)

class Solution:
	"""
	@param numbers: Give an array
	@param target: An integer
	@return: Find all unique quadruplets in the array which gives the sum of zero
	"""
	
	def fourSum (self, numbers, target):
		# write your code here
		# sort it first, O(n log(n))
		# we have x1 <= x2 <= ...... <= xk <= xl <= pos <= xm <= xn <=..... <= xs <= xt
		# left side (x1+x2,xk+xl), right side (xm+xn, xs + xt )
		# left side + right side (x1 + x2 + xm + xn , xk+xl + xs + xt ), if target is not in this region, we can skip
		# if target in this region, we need to find two in left, two in right. choose target2  , find target-target2  in left side, and target2 in right side , use hash table.
		# so we know x1 + x2 + xm + xn <= target <= xk + xl + xs + xt
		# xm + xn <= target2 <= xs + xt
		# x1 + x2 <= target - target2 <= xk + xl  =>    target -(xk + xl) <= target2 <= target -(x1 + x2 )
		#   max(xm+xn, target -(xk + xl))        <= target2  <= min(xs+xt, target -(x1+x2))
		# when we look for target2 in right side (xm + xn, xs + xt), this is 2 sum problem
		# also we know that it is sorted
		# we can assume that when we choose pos, xm must be used. so we look for target2 - xm in right side
		# too complicated!
		
		# start from naive solution
		# sort it. then we have i and j fixed, O(n^2), then we only need to find two numbers with sum of target - numbers[i] - numbers[j]. then we can use two pointers
		# for 3 sum, two pointers, O(n^2). so 4 sum, add one outer loop, O(n^3)
		# we can optimize based on O(n^3). save all sum of two numbers to hash map for repid look up. time complexity is O(n^2) , O(n log n ),
		
		if numbers is None or len (numbers) < 4:
			return []
		
		numbers = sorted (numbers)
		
		# do it O(n^3)
		result = []
		
		for i in range (0, len (numbers) - 3):
			
			if i != 0 and numbers [i] == numbers [i - 1]:
				# skip duplicates
				i += 1
				continue
			
			for j in range (i + 1, len (numbers) - 2):
				
				# looking for target - numbers[i] - numbers[j] in numbers[j+1:], use two pointers
				
				if j != i + 1 and numbers [j] == numbers [j - 1]:
					# duplicates
					j += 1
					continue
				
				start = j + 1
				end = len (numbers) - 1
				
				while start < end:
					
					if start != j+1  and numbers [start] == numbers [start - 1]:
						# duplicates
						start += 1
						continue
					
					if end != len (numbers) - 1 and numbers [end] == numbers [end + 1]:
						# duplicates
						end -= 1
						continue
					
					if numbers [start] + numbers [end] == target - numbers [i] - numbers [j]:
						result.append ([numbers [i], numbers [j], numbers [start], numbers [end]])
						start += 1
						end -= 1
					elif numbers [start] + numbers [end] > target - numbers [i] - numbers [j]:
						# end left move
						end -= 1
					else:
						start += 1
		
		return result
	
assert Solution().fourSum([1,0,-1,-1,-1,-1,0,1,1,1,2],2) == [[-1,0,1,2],[-1,1,1,1],[0,0,1,1]]


class Solution2:
	"""
	@param numbers: Give an array
	@param target: An integer
	@return: Find all unique quadruplets in the array which gives the sum of zero
	"""
	
	def fourSum (self, numbers, target):
		# write your code here
		# sort it first, O(n log(n))
		# we have x1 <= x2 <= ...... <= xk <= xl <= pos <= xm <= xn <=..... <= xs <= xt
		# left side (x1+x2,xk+xl), right side (xm+xn, xs + xt )
		# left side + right side (x1 + x2 + xm + xn , xk+xl + xs + xt ), if target is not in this region, we can skip
		# if target in this region, we need to find two in left, two in right. choose target2  , find target-target2  in left side, and target2 in right side , use hash table.
		# so we know x1 + x2 + xm + xn <= target <= xk + xl + xs + xt
		# xm + xn <= target2 <= xs + xt
		# x1 + x2 <= target - target2 <= xk + xl  =>    target -(xk + xl) <= target2 <= target -(x1 + x2 )
		#   max(xm+xn, target -(xk + xl))        <= target2  <= min(xs+xt, target -(x1+x2))
		# when we look for target2 in right side (xm + xn, xs + xt), this is 2 sum problem
		# also we know that it is sorted
		# we can assume that when we choose pos, xm must be used. so we look for target2 - xm in right side
		# too complicated!
		
		# start from naive solution
		# sort it. then we have i and j fixed, O(n^2), then we only need to find two numbers with sum of target - numbers[i] - numbers[j]. then we can use two pointers
		# for 3 sum, two pointers, O(n^2). so 4 sum, add one outer loop, O(n^3)
		# we can optimize based on O(n^3). save all sum of two numbers to hash map for repid look up. time complexity is O(n^2) , O(n log n ),
		
		if numbers is None or len (numbers) < 4:
			return []
		
		numbers = sorted (numbers)
		
		# do it O(n^3)
		result = []
		
		for i in range (0, len (numbers) - 3):
			
			if i != 0 and numbers [i] == numbers [i - 1]:
				# skip duplicates
				i += 1
				continue
			
			for j in range (i + 1, len (numbers) - 2):
				
				# looking for target - numbers[i] - numbers[j] in numbers[j+1:], use two pointers
				
				if j != i + 1 and numbers [j] == numbers [j - 1]:
					# duplicates
					j += 1
					continue
				
				start = j + 1
				end = len (numbers) - 1
				
				while start < end:
					
					if numbers [start] + numbers [end] == target - numbers [i] - numbers [j]:
						result.append ([numbers [i], numbers [j], numbers [start], numbers [end]])
						start += 1
						end -= 1
						
						while start < end and numbers [start] == numbers [start - 1]:
							# duplicates
							start += 1
						
						while start < end and numbers [end] == numbers [end + 1]:
							# duplicates
							end -= 1
					
					elif numbers [start] + numbers [end] > target - numbers [i] - numbers [j]:
						# end left move
						end -= 1
					else:
						start += 1
		
		return result
	
	
	
	
# TODO hash map

# TODO recursively convert k sum to k-1 sum
