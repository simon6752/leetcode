# 1324. Count Primes
# Description
# Count the number of prime numbers less than a non-negative number, n.

import math
class Solution:
    """
    @param n: a integer
    @return: return a integer
    """
    def countPrimes(self, n):
        # write your code here

        "we just cannot determine if each 1 -> n-1 is prime or not then count! "
        "we need to remove all numbers that are not prime "

        "1, 2, 3, 4, 5,6 ,7, 8 , 9 , 10, 11, 12, 13, 14, 15, 16, ..."
        "1st round, remove factor of 2, left  2, 3, 5, 7, 9 , 11, 13, 15, 17,19, 21,23,25,27 ..."
        "2nd round, remove facotor of 3, left 2,3, 5, 7, 11, 13, 17,19,23,25,29, ..."
        "3rd round, remove factor of 5(why not 4? we only use current left min), left 2,3,5,11,13,17,19,23,29,31,37..."

        "until we remove factor of n//2"
        "so it is O(n^2)??"
        "use memorization search, memorize and use previous result"

        if n <= 2 :
            return 0

        if n == 3:
            return 1

        if n == 4 :
            "2,3"
            return 2

        "record whether i is prime or not"
        prime = [1]* n
        prime[0] = 0
        prime[1] = 0

        "use memorization to avoid unnecessary calculation"
        "every compound in [1,n) must be some a*b, a is in [2,n//2+1), b is in [2,n//i + 1), we can also assume a is prime. we can also assume a < sqrt(n) since one of a,b must be smaller than sqrt(n)"
        for i in range(2,int(math.sqrt(n))+1):
            if prime[i] == 1:
                for j in range(i,n//i+1 ):
                    "mark i*j to 0 "
                    if i*j < n:
                        prime[i*j] = 0

        "count 1s in prime "

        return sum(prime)
