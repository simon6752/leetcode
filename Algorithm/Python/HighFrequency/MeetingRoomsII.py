#  919. Meeting Rooms II
# Description
# Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei), find the minimum number of conference rooms required.
#
# Have you met this question in a real interview?
# Example
# Given intervals = [(0,30),(5,10),(15,20)], return 2.


"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""


class Solution:
	"""
	@param intervals: an array of meeting time intervals
	@return: the minimum number of conference rooms required
	"""
	
	def minMeetingRooms (self, intervals):
		# Write your code here
		
		# only need to find out maximum rooms at the same time, use swipe line to solve it
		
		if intervals is None or len (intervals) == 0:
			return 0
		
		startend = []
		mark = []
		
		for i in range (0, len (intervals)):
			startend.append (intervals [i].start)
			mark.append (1)
			startend.append (intervals [i].end)
			mark.append (-1)
		
		# sort startend while update the same for mark
		sortedmark = [x for _, x in sorted (zip (startend, mark))]
		sortedstartend = sorted (startend)
		
		# if start time equal to end time, like (0,30), (30,40) at 30, we would first start (30,40), then end (0,30) at 30
		
		maxrooms = 0
		currentRoomNeeded = 0
		
		for i in range (0, len (sortedmark)):
			currentRoomNeeded += sortedmark [i]
			maxrooms = max (maxrooms, currentRoomNeeded)
		
		return maxrooms


# complexity

# time, O(n log(n)), space, O(n)


