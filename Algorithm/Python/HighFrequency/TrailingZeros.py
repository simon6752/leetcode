# 2. Trailing Zeros
# Write an algorithm which computes the number of trailing zeros in n factorial.

# Example
# 11! = 39916800, so the out should be 2

# Challenge
# O(log N) time


class Solution:
    """
    @param: n: An integer
    @return: An integer, denote the number of trailing zeros in n!
    """
    def trailingZeros(self, n):
        # write your code here, try to do it without arithmetic operators.
        
        # n ! = 2^a1 * 3^a2 * 5^a3 * ... number of 0s = min(a1,a3)
        # we can simply assume that there is always more 2 than 5. 
        # so find out the number of 5 factor from 1-> n , n itself 
        # 5, 2*5, 3^5, 4^5, 5^5, ...., k*5^m, ....,  
        
        # find maximum 5^k <= n 
        # no arithmetic operators, so use bit operators 
        
        m = 0 
        s = 1
        while s*5 <= n:
            m += 1
            s= s * 5
            
        # now we get 5 ^ m <= n 
        # 5, 10, 15, 25, 30, 35, 40, 45, 50, 55,...
        # number of 5 is n/5 + n/25 + n/... + n/5^m 
        
        result = 0
        p5 = 1
        for i in range(1,m+1):
            p5= p5*5
            result += n//p5
        
        return result 