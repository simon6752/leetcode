#  150. Best Time to Buy and Sell Stock II
# Say you have an array for which the ith element is the price of a given stock on day i.
#
# Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times). However, you may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
#
# Example
# Given an example [2,1,2,0,1], return 2

# my solution v1, not work, too complicated to handle peak, valley, duplicates and plateau
class Solution:
	"""
	@param prices: Given an integer array
	@return: Maximum profit
	"""
	
	def maxProfit (self, prices):
		# write your code here
		# since we can do it multiple times, just be greedy, but do not sell it too early
		
		# buy low and sell high. find all peaks and valleys
		
		
		profit = 0
		
		valley = -1
		peak = -1
		
		for i in range (0, len (prices)):
			# need to skip duplicates, handle plateau
			
			if i != 0 and prices [i] == prices [i - 1]:
				continue
			
			if ((i != 0 and prices [i] < prices [i - 1]) or i == 0) and (
					i == len (prices) - 1 or (i != len (prices) - 1 and prices [i] < prices [i + 1])):
				# find a valley
				
				valley = i
			
			if (i == 0 or (i != 0 and prices [i] > prices [i - 1])) and (
					i == len (prices) - 1 or (i != len (prices) - 1 and prices [i] > prices [i + 1])):
				peak = i
				
				# find a peak, need to calculate valley
				
				if valley != -1:
					profit += prices [peak] - prices [valley]
					# reset valley
					valley = -1
		
		return profit


class Solution2:
	"""
	@param prices: Given an integer array
	@return: Maximum profit
	"""
	
	def maxProfit (self, prices):
		# write your code here
		# use greedy, we do not care when to buy or sell. we can earn any price diff
		
		total = 0
		
		for i in range (0, len (prices)):
			
			if i != 0 and prices [i] > prices [i - 1]:
				total += prices [i] - prices [i - 1]
		
		return total