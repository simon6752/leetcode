# 98. Sort List
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Sort a linked list in O(n log n) time using constant space complexity.
#
# Have you met this question in a real interview?
# Example
# Given 1->3->2->null, sort it to 1->2->3->null.
#
# Challenge
# Solve it by merge sort & quick sort separately.
#
# Tags
# Linked List


"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param head: The head of linked list.
    @return: You should return the head of the sorted linked list, using constant space complexity.
    """

    def sortList(self, head):
        # write your code here
        "do it merge sort this time"

        "divide and conquer"

        return self.mergeHelper(head)

    def mergeHelper(self, head):
        if head is None or head.next is None:
            return head

        "use two fast slow pointers to find mid element, then merge left and right seperately, then merge two sorted list use two pointers"
        split = self.findMid(head)
        left = split[0]
        right = split[1]

        leftsort = self.mergeHelper(left)
        rightsort = self.mergeHelper(right)

        return self.mergeSortedList(leftsort, rightsort)

    def findMid(self, head):
        "find mid element in a linked list, and split it into left and right"

        if head is None:
            return [None, None]

        if head.next is None:
            return [head, None]
        "use fast slow pointers"

        p1 = head
        p2 = head
        p1pre = p1

        while p1 is not None and p2 is not None:
            "both pointers are not None"

            p2 = p2.next
            if p2 is None:
                break
            p2 = p2.next
            p1pre = p1
            p1 = p1.next

        p1pre.next = None
        return [head, p1]

    def mergeSortedList(self, head1, head2):

        "merge two sorted link list"
        "two pointers"

        if head1 is None:
            return head2

        if head2 is None:
            return head1

        p1 = head1
        p2 = head2

        dummy = ListNode(0)
        head = dummy

        while p1 is not None and p2 is not None:
            if p1.val < p2.val:
                head.next = p1
                p1 = p1.next
            else:
                head.next = p2
                p2 = p2.next

            head = head.next

        "check whether p1 or p2 is not None"

        if p1 is not None:
            head.next = p1
        elif p2 is not None:
            head.next = p2

        return dummy.next


list1 = ListNode(1)
list1.next = ListNode(-1)

list2 = ListNode(-1)
list2.next = ListNode(1)
assert Solution().sortList(list1).equals(list2)
