# 413. Reverse Integer
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Reverse digits of an integer. Returns 0 when the reversed integer overflows (signed 32-bit integer).
#
# Have you met this question in a real interview?
# Example
# Given x = 123, return 321
#
# Given x = -123, return -321
#
# Tags
# Bloomberg Integer Apple


class Solution:
    """
    @param n: the integer to be reversed
    @return: the reversed integer
    """

    def reverseInteger(self, n):
        # write your code here
        "reverse integer"

        "add digits to a list"

        digit = []
        x = n if n > 0 else -n
        while abs(x) >= 10:
            digit.append(x % 10)
            x = x // 10
        digit.append(x)

        result = digit[0]
        for i in range(1, len(digit)):
            result = result * 10 + digit[i]

        if result > 2 ** 31 - 1 or result < -2 ** 31:
            return 0

        return result if n > 0 else -result


assert Solution().reverseInteger(-123) == -321
assert Solution().reverseInteger(1534236469) == 0
