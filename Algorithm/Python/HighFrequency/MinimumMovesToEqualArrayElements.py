# 1231. Minimum Moves to Equal Array Elements
# Given a non-empty integer array of size n, find the minimum number of moves required to make all array elements equal, where a move is incrementing n - 1 elements by 1.
#
# Example
# Input:
# [1,2,3]
#
# Output:
# 3
#
# Explanation:
# Only three moves are needed (remember each move increments two elements):
#
# [1,2,3]  =>  [2,3,3]  =>  [3,4,3]  =>  [4,4,4]

# my 1st version, time limit exceeded

class Solution:
	"""
	@param nums: an array
	@return: the minimum number of moves required to make all array elements equal
	"""
	
	def minMoves (self, nums):
		# Write your code here
		
		# 1st sort it . or not sort it
		# lets say [a1 a2 ... an] a1 <= a2 <= a3 ... <= an
		
		# if they are all increased to m. total sum is a1 + ... + an. every time we added n-1 elements
		# total added  (m*n - (a1 + ... + an ) )/(n-1), m is the smallest integer. but we need to consider what if like a1 is much smaller than all the elements?? so we need to think about the element difference
		#  m = ( k * (n-1) - (a1+ ...+ an))/n, where k is smallest integer
		# consider smallest and largest
		
		# lets say, we have a1 <= a2 <= a3 <= a4 <= a5 ....<= a_(n-1) <= an
		# 1st time, we add (an-a1) (n-1) to a1 -> a_(n-1) to match a1 to an
		# become      an  an-a1 + a2  an-a1 + a3 ....    a_(n-1) + an - a1    an
		# we reorder it, got 2 an,=> an an an -a1 + a2, an-a1 + a3 .....   a_(n-1)+ an- a1
		# then we do it again, recursively, match the rest
		
		
		if nums is None or len (nums) < 2:
			return 0
		
		return self.minMovesHelper (nums)
	
	def minMovesHelper (self, nums):
		
		nums = sorted (nums)
		if nums [0] == nums [-1]:
			# equal now
			return 0
		
		count = 0
		
		count = nums [-1] - nums [0]
		
		for i in range (0, len (nums) - 1):
			nums [i] += count
		
		return count + self.minMovesHelper (nums)


class Solution2:
	"""
	@param nums: an array
	@return: the minimum number of moves required to make all array elements equal
	"""
	
	def minMoves (self, nums):
		# Write your code here
		
		# 1st sort it . or not sort it
		# lets say [a1 a2 ... an] a1 <= a2 <= a3 ... <= an
		
		# if they are all increased to m. total sum is a1 + ... + an. every time we added n-1 elements
		# total added  (m*n - (a1 + ... + an ) )/(n-1), m is the smallest integer. but we need to consider what if like a1 is much smaller than all the elements?? so we need to think about the element difference
		#  m = ( k * (n-1) - (a1+ ...+ an))/n, where k is smallest integer
		# consider smallest and largest
		
		# lets say, we have a1 <= a2 <= a3 <= a4 <= a5 ....<= a_(n-1) <= an
		# 1st time, we add (an-a1) (n-1) to a1 -> a_(n-1) to match a1 to an
		# become      an  an-a1 + a2  an-a1 + a3 ....    a_(n-1) + an - a1    an
		# we reorder it, got 2 an,=> an an an -a1 + a2, an-a1 + a3 .....   a_(n-1)+ an- a1
		# then we do it again, recursively, match the rest
		
		# optimize, we do not need to really add to every elements
		# 1st round, 1st element is added an-a1, from 2-> n-1, all added an - a1
		# then sort, or we do not need to actually sort it, just move our index
		
		
		if nums is None or len (nums) < 2:
			return 0
		nums = sorted (nums)
		
		return self.minMovesHelper (nums, 0)
	
	def minMovesHelper (self, nums, level):
		
		if nums [0] == nums [len (nums) - 1 - level]:
			# equal now
			return 0
		
		count = nums [len (nums) - 1 - level] - nums [0]
		
		return count + self.minMovesHelper (nums, level + 1)





