# TODO 30. Insert Interval
# Given a non-overlapping interval list which is sorted by start point.
#
# Insert a new interval into it, make sure the list is still in order and non-overlapping (merge intervals if necessary).
#
# Example
# Insert (2, 5) into [(1,2), (5,9)], we get [(1,9)].
#
# Insert (3, 4) into [(1,2), (5,9)], we get [(1,2), (3,4), (5,9)].


"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""
from Algorithm.Python.Sort.MergeIntervals import Interval


"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""


# class Solution:
# 	"""
# 	@param intervals: Sorted interval list.
# 	@param newInterval: new interval.
# 	@return: A new interval list.
# 	"""
#
# 	def insert (self, intervals, newInterval):
# 		# write your code here
#
# 		# search insert position from start to end
#
# 		if intervals is None or len (intervals) == 0:
# 			return [newInterval]
#
# 		if newInterval is None or newInterval.end < newInterval.start:
# 			return intervals
#
# 		startFound = False
# 		endFound = False
#
# 		result = []
#
# 		i = 0
# 		interval = Interval (newInterval.start, newInterval.end)
# 		while i < len (intervals):
#
# 			if startFound and endFound:
# 				result.append (intervals [i])
#
# 			elif not startFound:
# 				if newInterval.start <= intervals [i].end:
# 					interval.start = min (newInterval.start, intervals [i].start)
# 					result.append (interval)
# 					startFound = True
# 				else:
# 					result.append (intervals [i])
#
# 			else:
#
# 				if newInterval.end < intervals [i].start:
# 					interval.end = newInterval.end
# 					result.append (intervals [i])
# 					endFound = True
# 				elif newInterval.end >= intervals [i].start and newInterval.end <= intervals [i].end:
# 					interval.end = max (newInterval.end, intervals [i].end)
# 					endFound = True
# 				else:
# 					result.append (intervals [i])
#
# 			i += 1
#
# 		# if i == len(intervals) but start/end not found ??
# 		if not startFound:
# 			result.append (newInterval)
# 			startFound = True
# 		elif not endFound:
# 			interval.end = max (newInterval.end, intervals [i - 1].end)
# 			endFound = True
#
# 		return result


interval1 = Interval(5,7)
intervals = []
result = [interval1]

assert Solution().insert(intervals,interval1) == result

intervals = [Interval(1,5)]
interval1 = Interval(2,3)
result = intervals

# assert Solution().insert(intervals,interval1) == result

intervals = [Interval(1,5)]
interval1 = Interval(0,0)
result = [Interval(0,0),Interval(1,5)]
# assert Solution().insert(intervals,interval1) == result


# [(1,5),(7,8),(10,13)]
# (6,6)
#
# [(1,5),(6,6),(7,8),(10,13)]

intervals = [Interval(1,5),Interval(7,8),Interval(10,10)]
interval1 = Interval(6,6)
result =  [Interval(1,5), Interval(6,6),Interval(7,8),Interval(10,10)]
assert Solution().insert(intervals,interval1) == result







