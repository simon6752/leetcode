#  1032. Letter Case Permutation
# Given a string S, we can transform every letter individually to be lowercase or uppercase to create another string. Return a list of all possible strings we could create.
#
# Example
# Input: S = "a1b2"
# Output: ["a1b2", "a1B2", "A1b2", "A1B2"]
#
# Input: S = "3z4"
# Output: ["3z4", "3Z4"]
#
# Input: S = "12345"
# Output: ["12345"]


class Solution:
	"""
	@param S: a string
	@return: return a list of strings
	"""
	
	def letterCasePermutation (self, S):
		# write your code here
		
		# need dfs, recursion
		
		if S is None:
			return []
		
		result = []
		
		self.LCPHelper (S, 0, result, "")
		return result
	
	def LCPHelper (self, S, level, result, prefix):
		
		if level == len (S):
			# reached end
			result.append (prefix)
			return
		
		if not S [level].isalpha ():
			self.LCPHelper (S, level + 1, result, prefix + S [level])
		else:
			self.LCPHelper (S, level + 1, result, prefix + S [level].upper ())
			self.LCPHelper (S, level + 1, result, prefix + S [level].lower ())