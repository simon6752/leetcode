#   1350. Excel Sheet Column Title
# Description
# Given a positive integer, return its corresponding column title as appear in an Excel sheet.
#
# Have you met this question in a real interview?
# Example
# 1 -> A
# 2 -> B
# 3 -> C
# ...
# 26 -> Z
# 27 -> AA
# 28 -> AB


class Solution:
	"""
	@param n: a integer
	@return: return a string
	"""
	
	def convertToTitle (self, n):
		# write your code here
		
		# actually it is 26 digit
		
		if n is None or n <= 0:
			return ""
		
		result = ""
		
		while n != 0:
			result = chr (n % 26 + ord ("A") - 1) + result
			n = n // 26
		
		return result