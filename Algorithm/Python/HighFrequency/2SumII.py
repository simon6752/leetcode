# #  Two Sum II
# Question
# Given an array of integers, find how many pairs in the array such that their sum is bigger than a specific target number. Please return the number of pairs.
#
# Example
# Given numbers = [2, 7, 11, 15], target = 24. Return 1. (11 + 15 is the only pair)
# Challenge
# Do it in O(1) extra space and O(nlogn) time.

# unlike the two sum I, use hash table, we need to still use two pointers

# 167. Two Sum II - Input array is sorted
# DescriptionHintsSubmissionsDiscussSolution
# Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.
#
# The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2.
#
# Note:
#
# Your returned answers (both index1 and index2) are not zero-based.
# You may assume that each input would have exactly one solution and you may not use the same element twice.
# Example:
#
# Input: numbers = [2,7,11,15], target = 9
# Output: [1,2]
# Explanation: The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.

class Solution:
	def twoSum (self, numbers, target):
		"""
		:type numbers: List[int]
		:type target: int
		:rtype: List[int]
		"""
		
		# use two pointer instead of hash map
		
		if numbers is None or len (numbers) < 2:
			return None
		
		# binary search
		
		start = 0
		end = len (numbers) - 1
		
		while start < end:
			
			if numbers [start] + numbers [end] == target:
				return [start + 1, end + 1]
			elif numbers [start] + numbers [end] > target:
				# right move left
				end -= 1
			else:
				start += 1
		
		return []