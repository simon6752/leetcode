# TODO 151. Best Time to Buy and Sell Stock III
# Description
# Say you have an array for which the ith element is the price of a given stock on day i.
#
# Design an algorithm to find the maximum profit. You may complete at most two transactions.
#
# You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
#
# Have you met this question in a real interview?
# Example
# Given an example [4,4,6,1,1,4,2,5], return 6.