# 53. Reverse Words in a String
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given an input string, reverse the string word by word.
#
# For example,
# Given s = "the sky is blue",
# return "blue is sky the".
#
# Have you met this question in a real interview?
# Clarification
# What constitutes a word?
# A sequence of non-space characters constitutes a word.
# Could the input string contain leading or trailing spaces?
# Yes. However, your reversed string should not contain leading or trailing spaces.
# How about multiple spaces between two words?
# Reduce them to a single space in the reversed string.
# Example
# Tags
# String Yelp Snapchat Apple Bloomberg Microsoft


class Solution:
    """
    @param: s: A string
    @return: A string
    """

    def reverseWords(self, s):
        # write your code here

        "naive solution"
        "if space, just ignore. if new letter, start a new string"

        if s is None or len(s) == 0:
            return s

        rev = []

        laststart = 0
        lastend = 0

        for i in range(0, len(s) + 1):
            if i == len(s) or s[i] == " ":
                if i > 0 and s[i - 1] != " ":
                    lastend = i
                    "finished a word, lets put it in rev"
                    rev.append(s[laststart:lastend])
            else:
                if i > 0 and s[i - 1] == " ":
                    laststart = i

        result = ""
        for i in range(len(rev) - 1, -1, -1):
            result = result + rev[i] + " "

        return result[0:-1]


assert Solution().reverseWords("world") == "world"
