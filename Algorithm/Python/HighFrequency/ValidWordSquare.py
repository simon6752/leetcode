# 888. Valid Word Square
# Given a sequence of words, check whether it forms a valid word square.

# A sequence of words forms a valid word square if the k^th row and column read the exact same string, where 0 ≤ k < max(numRows, numColumns).

# Example
# Given
# [
#   "abcd",
#   "bnrt",
#   "crmy",
#   "dtye"
# ]
# return true

# Explanation:
# The first row and first column both read "abcd".
# The second row and second column both read "bnrt".
# The third row and third column both read "crmy".
# The fourth row and fourth column both read "dtye".

# Therefore, it is a valid word square.
# Given
# [
#   "abcd",
#   "bnrt",
#   "crm",
#   "dt"
# ]
# return true

# Explanation:
# The first row and first column both read "abcd".
# The second row and second column both read "bnrt".
# The third row and third column both read "crm".
# The fourth row and fourth column both read "dt".

# Therefore, it is a valid word square.
# Given
# [
#   "ball",
#   "area",
#   "read",
#   "lady"
# ]
# return false

# Explanation:
# The third row reads "read" while the third column reads "lead".

# Therefore, it is NOT a valid word square.


class Solution:
    """
    @param words: a list of string
    @return: a boolean
    """
    def validWordSquare(self, words):
        # Write your code here
        
        # start from 0 -> n -1, check (i,i) right to (i,n-1), then down to (n-1,i)
        
        if words is None or len(words) < 2 :
            return True 
            
        n = len(words)
        m = len(words[0])
        
        if n != m :
            return False 
            
        for i in range(0,n):
            
            for j in range(i,n):
                if words[i][j] != words[j][i]:
                    return False 
                    
        return True 

# complexity

# O(n^2)