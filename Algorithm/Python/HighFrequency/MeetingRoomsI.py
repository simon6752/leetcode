#  920. Meeting Rooms
# Description
# Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei), determine if a person could attend all meetings.
#
# Have you met this question in a real interview?
# Example
# Given intervals = [[0,30],[5,10],[15,20]], return false.


"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""


class Solution:
	"""
	@param intervals: an array of meeting time intervals
	@return: if a person could attend all meetings
	"""
	
	def canAttendMeetings (self, intervals):
		# Write your code here
		# must be non overlapping intervals
		# sort it, heap
		
		if intervals is None or len (intervals) == 0:
			return True
		
		# use start as key to sort
		newintervals = sorted (intervals, key = lambda interval: interval.start)
		
		for i in range (1, len (newintervals)):
			
			if newintervals [i].start < newintervals [i - 1].end:
				return False
		
		return True
