#  945. Task Scheduler
# Description
# Given a char array representing tasks CPU need to do. It contains capital letters A to Z where different letters represent different tasks.Tasks could be done without original order. Each task could be done in one interval. For each interval, CPU could finish one task or just be idle.
#
# However, there is a non-negative cooling interval n that means between two same tasks, there must be at least n intervals that CPU are doing different tasks or just be idle.
#
# You need to return the least number of intervals the CPU will take to finish all the given tasks.
#
# The number of tasks is in the range [1, 10000].
# The integer n is in the range [0, 100].
# Have you met this question in a real interview?
# Example
# Given tasks = ['A','A','A','B','B','B'], n = 2, return 8.
#
# Explanation:
# A -> B -> idle -> A -> B -> idle -> A -> B.


# use heap

from heapq import heappush, heappop


class Solution:
	"""
	@param tasks: the given char array representing tasks CPU need to do
	@param n: the non-negative cooling interval
	@return: the least number of intervals the CPU will take to finish all the given tasks
	"""
	
	def leastInterval (self, tasks, n):
		# write your code here
		
		# first, do a frequency count
		
		if tasks is None or len (tasks) == 0:
			return 0
		
		count = {}
		maxc = 0
		for i in range (0, len (tasks)):
			if tasks [i] in count.keys ():
				count [tasks [i]] += 1
			else:
				count [tasks [i]] = 1
			
			maxc = max (maxc, count [tasks [i]])
		
		m = len (count)
		
		# minimum time cannot be less than longest task
		minimumt = maxc
		
		# not think about task, think about position
		# we have n buckets
		
		# can we just simply sort the frequency? use most frequency and least frequency for partner
		
		# 5  ------ -----
		# 4  ------- ----
		# 3  -------- ---
		# 2  --------- --
		# 1  ---------- -
		
		# lets say we have n threads
		# each has its own queue
		#   time    :   0   1   2    3   4    5    6    7    8    9
		#  cpu 1 :      A   A   A
		#  cpu 2 :      B   B   B
		#  ....
		#  cpu k :
		#  ....
		#  cpu n :
		# we want minimum time
		# we only need to simulate with specific algorithm to make time short
		
		# cases to consider: total kind of tasks, lets say m
		# assign task use frequency descending order from cpu 1 -> n , then descending order n->1, then descending order n -> 1, then descending order n -> 1
		
		# put current longest task to the most available cpu
		# sometimes we may put several short task together, sometime we may break 1 long task into several small pieces, all depends on difference between current cpu availability and current busiest cpu availability
		
		# use n queues, we return maximum length of all n queues, and try to make this maximum number as small as possible
		
		# strategy: sort frequency descending, start from longest task, put into current shortest queue
		
		# input n, we have n+1 queue(cpu)
		
		queue = []
		for i in range (0, n + 1):
			# now we have n+1 queue
			queue.append ([])
		
		queue2 = []
		
		# use priority queue, then we can always get current maximum count, and current shortest queue
		
		for key in count.keys ():
			heappush (queue2, (-count [key], key))
		
		# we also need to maintain a priority queue to save current queue length
		queue3 = []
		for i in range (0, n + 1):
			heappush (queue3, (0, i))
		
		# now we heappop from queue2, maximum element come out first
		while len (queue2) > 0:
			currentMax = heappop (queue2)
			currentMin = heappop (queue3)
			for i in range (0, -currentMax [0]):
				queue [currentMin [1]].append (currentMax [1])
			
			# now queue length has changed, need to update queue3
			heappush (queue3, (currentMin [0] - currentMax [0], currentMin [1]))
		
		# get max length of queue
		
		maxl = 0
		for i in range (0, len (queue)):
			maxl = max (maxl, len (queue [i]))
		
		total = 0
		
		for i in range (0, len (queue)):
			if len (queue [i]) == maxl:
				total += maxl
			else:
				total += maxl - 1
		
		return total



