#  995. Best Time to Buy and Sell Stock with Cooldown
# Say you have an array for which the ith element is the price of a given stock on day i.
#
# Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times) with the following restrictions:
#
# You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
# After you sell your stock, you cannot buy stock on next day. (ie, cooldown 1 day)
# Example
# prices = [1, 2, 3, 0, 2]
# maxProfit = 3
# transactions = [buy, sell, cooldown, buy, sell]


# analysis

# every day, we can either buy or not buy if no stock, sell or hold if already has stock
# 2 status everyday, 2^n choice, use dynamic programmming to optimize it
# since profit for each state at current day depends on the other state, we need 2 list to record the profit of each state
# pay attention to initialization and transfer function

class Solution:
	"""
	@param prices: a list of integers
	@return: return a integer
	"""
	
	def maxProfit (self, prices):
		# write your code here
		
		# dynamic programming
		# since there is cooldown, it changed our transfer function a little bit
		
		if prices is None or len (prices) < 2:
			return 0
		
		sell = []
		own = []
		
		# initialization for 1st two days
		sell.append (0)
		own.append (-prices [0])
		# sell 1 is either sell[0], or own at 0 and sell at 1
		sell.append (max (sell [0], own [0] + prices [1]))
		# own 1 is either own[0], or buy at 1
		own.append (max (own [0], -prices [1]))
		
		maxprofit = 0
		
		for i in range (2, len (prices)):
			# sell is decided by own yesterday
			sell.append (max (sell [i - 1], own [i - 1] + prices [i]))
			own.append (max (own [i - 1], sell [i - 2] - prices [i]))
		
		maxprofit = sell [-1]
		
		return maxprofit

assert Solution().maxProfit([2,6,8,7,8,7,9,4,1,2,4,5,8]) == 15

assert Solution().maxProfit([8,3,6,2,8,8,8,4,2,0,7,2,9,4,9]) == 18


