#  1000. Best Time to Buy and Sell Stock with Transaction Fee
# Description
# Your are given an array of integers prices, for which the i-th element is the price of a given stock on day i; and a non-negative integer fee representing a transaction fee.
#
# You may complete as many transactions as you like, but you need to pay the transaction fee for each transaction. You may not buy more than 1 share of a stock at a time (ie. you must sell the stock share before you buy again.)
#
# Return the maximum profit you can make.
#
# 0 < prices.length <= 50000.
# 0 < prices[i] < 50000.
# 0 <= fee < 50000.
#
# Have you met this question in a real interview?
# Example
# Input: prices = [1, 3, 2, 8, 4, 9], fee = 2
# Output: 8
# Explanation: The maximum profit can be achieved by:
# Buying at prices[0] = 1
# Selling at prices[3] = 8
# Buying at prices[4] = 4
# Selling at prices[5] = 9
# The total profit is ((8 - 1) - 2) + ((9 - 4) - 2) = 8.

# my solution 1, not pass
# class Solution:
# 	"""
# 	@param prices: a list of integers
# 	@param fee: a integer
# 	@return: return a integer
# 	"""
#
# 	def maxProfit (self, prices, fee):
# 		# write your code here
# 		# instead of use one array, we use two. add transaction fee to it to form the buy array. sell array is original array
#
#
# 		if prices is None or len (prices) < 2:
# 			return 0
#
# 		buyprices = []
# 		sellprices = prices
# 		for i in range (0, len (prices)):
# 			buyprices.append (prices [i] + fee)
#
# 		# [1,3,2,8,4,9] => [3,5,4,10,6,11]
# 		# then use greedy ? yes, find valley and peek
# 		# valley: s[i-1] b[i] s[i+1], s means sell, b means buy. notice that they are from different arrays
# 		# peek: b[i-1]  s[i] b[i+1]
# 		# then use greedy, buy at valley and sell at peek
# 		# need a list of valleys index, and an array of peek index
#
# 		valleys = []
# 		peeks = []
#
# 		for i in range (0, len (prices)):
# 			if (i != 0 and i != len (prices) - 1 and buyprices [i] <= prices [i + 1] and buyprices [i] <= prices [
# 					i - 1]) or (i == 0 and buyprices [i] <= prices [i + 1]) or (
# 					i == len (prices) - 1 and buyprices [i] <= prices [i - 1]):
# 				valleys.append (i)
# 			if (i != 0 and i != len (prices) - 1 and prices [i] >= buyprices [i - 1] and prices [i] >= buyprices [
# 					i + 1]) or (i == 0 and prices [i] >= buyprices [i + 1]) or (
# 					i == len (prices) - 1 and prices [i] >= buyprices [i - 1]):
# 				peeks.append (i)
#
# 		totalprofit = 0
#
# 		# use two pointers to find it
#
# 		p1 = 0
# 		p2 = 0
# 		#   if we sort the indecies of valleys and peeks together, there might be multiple adjacent peeks or valleys. we need to find lowest in adjacent valleys and highest in adjacent peeks
#
# 		while p1 < len (valleys) and p2 < len (peeks):
#
# 			if valleys [p1] < peeks [p2]:
# 				totalprofit += prices [peeks [p2]] - buyprices [valleys [p1]]
# 				# then we move p1 until it is larger than p2
# 				while p1 < len (valleys) and valleys [p1] < peeks [p2]:
# 					p1 += 1
#
# 			else:
# 				# move peek until it is larger than p1
# 				while p2 < len (peeks) and peeks [p2] <= valleys [p1]:
# 					p2 += 1
#
# 		return totalprofit

# assert Solution().maxProfit([1,4,6,2,8,3,10,14], 3) == 13


class Solution:
	"""
	@param prices: a list of integers
	@param fee: a integer
	@return: return a integer
	"""
	
	def maxProfit (self, prices, fee):
		# write your code here
		# naive solution: every day, we have two choices, buy/sell/hold, either buy/pass or sell/hold, depends on previous day's stock status. So if we use DFS, we have 2^n choices. Find maximum of it.
		
		# optimization: dynamic programming to remove duplicate calculation. today's choice depends on yesterday. yesterday 1. have stock  2. no stock, so today: 1. sell or hold 2. buy or pass. we need two list to save previous maximum profit based on choice. so we have sell and hold list
		# for day i, sell[i] = max(sell[i-1](yesterday has stock, not sell, hold, or yesterday no stock, pass ), prices[i]  + hold[i-1] -fee (yesterday has stock, sell it, pay fee ) )
		# for day i, hold[i] = max(hold[i-1](yesterday has stock, not sell, hold, or yesterday no stock , pass), sell[i-1] -prices[i]  (yesterday no stock, buy today))
		
		# we assume that when we buy, we only pay prices. when sell, we get current prices and pay fee. In this way, we only pay fee once for each transaction(buy, then sell)
		
		if prices is None or len (prices) < 2:
			return 0
		
		if fee is None or fee < 0:
			# invalid
			return 0
		
		sell = []
		hold = []
		
		for i in range (0, len (prices)):
			if i == 0:
				sell.append (0)
				hold.append (-prices [0])
			else:
				sell.append (max (sell [i - 1], prices [i] + hold [i - 1] - fee))
				hold.append (max (hold [i - 1], sell [i - 1] - prices [i]))
		
		return sell [-1]
