#

class Solution:
    """
    @param A: An array of Integer
    @return: an integer
    """

    def longestIncreasingContinuousSubsequence(self, A):
        # write your code here
        "naive solution is enough since the subsequence is continous!!!"

        if A == None or len(A) == 0:
            return 0
        if len(A) == 1:
            return 1
        maxlics = 1
        length = 1
        for i in range(1, len(A)):
            if A[i] > A[i - 1]:
                length = length + 1
            else:
                length = 1
            maxlics = max(maxlics, length)

        "do it from the end"

        length = 1

        for i in range(1, len(A)):
            if A[i] < A[i - 1]:
                length = length + 1
            else:
                length = 1
            maxlics = max(maxlics, length)

        return maxlics


assert Solution().longestIncreasingContinuousSubsequence([5, 4, 2, 1, 3]) == 4
assert Solution().longestIncreasingContinuousSubsequence([5, 1, 2, 3, 4]) == 4
