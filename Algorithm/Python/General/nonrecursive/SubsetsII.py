# 18. Subsets II
# Given a list of numbers that may has duplicate numbers, return all possible subsets
# 
# Example
# If S = [1,2,2], a solution is:
# 
# [
#     [2],
#     [1],
#     [1,2,2],
#     [2,2],
#     [1,2],
#     []
# ]
# Challenge
# Can you do it in both recursively and iteratively?


# iterative solution

class Solution:
    """
    @param nums: A set of numbers.
    @return: A list of lists. All valid subsets.
    """
    def subsetsWithDup(self, nums):
        # write your code here

        "without duplicates, we get 2^n result"
        "first sort it"
        "for each new element, we can add it to the end of all previous results to double it"
        "for duplicate, we can only add a few cases"
        " let's say we have k 2s, for kth 2, we can only add to the result that already had k-1 2s since k-1 s will cover "

        if nums == None or len(nums) == 0:
            return [[]]

        newnums = sorted(nums)

        result = [[],[newnums[0]]]

        countduplicate = 1
        for i in range(1,len(newnums)):
            "try to add result due to newnums[i]"

            if newnums[i] == newnums[i-1]:
                countduplicate += 1
            else:
                "else reset it back to 1"
                countduplicate = 1

            for j in range(0,len(result)):

                if countduplicate > 1:
                    "we got duplicates, we only add result that already had k-1 2s"
                    if len(result[j]) >= countduplicate -1  and result[j][-(countduplicate-1)] == newnums[i]:
                        "we need to add to end of this one, else we skip it"
                        temp = list(result[j])
                        temp.append(newnums[i])
                        result.append(temp)
                    else:
                        pass

                else:
                    "append it to result[j]  "
                    temp = list(result[j])
                    temp.append(newnums[i])
                    result.append(temp)

        return result



