#  15. Permutations
# Description
# Given a list of numbers, return all possible permutations.
#
# You can assume that there is no duplicate numbers in the list.
#
# Have you met this question in a real interview?
# Example
# For nums = [1,2,3], the permutations are:
#
# [
#   [1,2,3],
#   [1,3,2],
#   [2,1,3],
#   [2,3,1],
#   [3,1,2],
#   [3,2,1]
# ]
# Challenge
# Do it without recursion.

class Solution:
	"""
	@param: nums: A list of integers.
	@return: A list of permutations.
	"""
	
	def permute (self, nums):
		# write your code here
		
		# non recursive
		# total number is n!
		
		if nums is None or len (nums) == 0:
			return [[]]
		
		result = [[nums [0]]]
		
		for i in range (1, len (nums)):
			# for i, we just need to insert nums[i] into every position of every permutation of i-1
			n = len (result)
			newresult = []
			for j in range (0, n):
				
				temp1 = list (result [j])
				# we have len(result[j]) +1 choices to insert nums[i]
				m = len (result [j])
				for k in range (0, m + 1):
					temp1 = list (result [j])
					temp1.insert (k, nums [i])
					newresult.append (temp1)
			
			result = newresult
		
		return result


assert Solution().permute([0,1]) == [[0,1],[1,0]]


