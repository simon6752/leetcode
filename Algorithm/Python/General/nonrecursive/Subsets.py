# Given a set of distinct integers, return all possible subsets.
#
#  Notice
# Elements in a subset must be in non-descending order.
# The solution set must not contain duplicate subsets.
# Have you met this question in a real interview?
# Example
# If S = [1,2,3], a solution is:
#
# [
#   [3],
#   [1],
#   [2],
#   [1,2,3],
#   [1,3],
#   [2,3],
#   [1,2],
#   []
# ]
# Challenge
# Can you do it in both recursively and iteratively?

class Solution:
    """
    @param: nums: A set of numbers
    @return: A list of lists
    """

    def subsets(self, nums):
        # write your code here

        "iterative"
        if nums == None:
            return [[]]

        nums = sorted(nums)

        results = [[]]

        for i in range(0, len(nums)):
            m = len(results)
            for j in range(0, m):
                "add current one to end of results"
                "use [:] to create new list use all elements in existing list"
                # results.append(results[j][:])
                "or we can use new = list(old) to create new list"
                results.append(list(results[j]))
                results[j].append(nums[i])

        return results


"we need to compare all elements in list are the same, ignore order"
assert sorted(Solution().subsets([0])) == [[], [0]]
# assert (Solution().subsets([0,1])) == [[], [0],[1],[0,1]]
