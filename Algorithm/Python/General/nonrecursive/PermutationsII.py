# # TODO 16. Permutations II
# Description
# Given a list of numbers with duplicate number in it. Find all unique permutations.
#
# Have you met this question in a real interview?
# Example
# For numbers [1,2,2] the unique permutations are:
#
# [
#   [1,2,2],
#   [2,1,2],
#   [2,2,1]
# ]
# Challenge
# Using recursion to do it is acceptable. If you can do it without recursion, that would be great!