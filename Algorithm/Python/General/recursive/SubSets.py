# Given a set of distinct integers, return all possible subsets.
#
#  Notice
# Elements in a subset must be in non-descending order.
# The solution set must not contain duplicate subsets.
# Have you met this question in a real interview?
# Example
# If S = [1,2,3], a solution is:
#
# [
#   [3],
#   [1],
#   [2],
#   [1,2,3],
#   [1,3],
#   [2,3],
#   [1,2],
#   []
# ]
# Challenge
# Can you do it in both recursively and iteratively?

class Solution:
    """
    @param: nums: A set of numbers
    @return: A list of lists
    """

    def subsets(self, nums):
        # write your code here

        "recursive"

        nums = sorted(nums)

        return self.subsetHelper(nums, 0, len(nums))

    def subsetHelper(self, nums, start, end):

        if nums == None or len(nums) == 0 or start + 1 > end:
            return [[]]

        result = []

        result1 = self.subsetHelper(nums, start + 1, end)

        for i in range(0, len(result1)):
            result.append(result1[i])
            result.append([nums[start]] + (result1[i]))

        return result

        "iterative"
        return result


assert Solution().subsets([0]) == [[], [0]]
