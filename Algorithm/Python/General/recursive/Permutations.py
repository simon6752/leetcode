# Given n and k, return the k-th permutation sequence.
#
#  Notice
# n will be between 1 and 9 inclusive.

class Solution:
    """
    @param n: n
    @param k: the k th permutation
    @return: return the k-th permutation
    """

    def getPermutation(self, n, k):
        # write your code here

        "permutation of n element is: 1 -> n + permutation of rest "

        list1 = [str(i) for i in range(1, n + 1)]

        return self.permutationFromList(list1)[k - 1]

    def permutationFromList(self, numset):

        if numset == None or len(numset) == 0:
            return []

        if len(numset) == 1:
            return numset

        set2 = list(numset)

        result = []
        result2 = []

        for i in range(0, len(numset)):
            set2 = list(numset)
            del set2[i]
            result2 = self.permutationFromList(set2)
            for j in range(0, len(result2)):
                result.append(numset[i] + (result2[j]))

        return result


assert Solution().getPermutation(3, 2) == "132"
