# find kth largest element in an array in O(n) time and O(1) extra space

# . Kth Largest Element
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Find K-th largest element in an array.
#
#  Notice
# You can swap elements in the array
#
# Have you met this question in a real interview?
# Example
# In array [9,3,2,4,8], the 3rd largest element is 4.
#
# In array [1,2,3,4,5], the 1st largest element is 5, 2nd largest element is 4, 3rd largest element is 3 and etc.
#
# Challenge
# O(n) time, O(1) extra memory.
#
# Tags
# Sort Quick Sort

class Solution:
    # @param k & A a integer and an array
    # @return ans a integer
    def kthLargestElement(self, k, A):

        "use quick select, choose kth as pivot, it is O(n) time"

        "choose last as pivot, after partition 1 time, we will put A[pivot] at its right location. if k > pivot, find kth in right side of A[pivot] else left side."

        self.kthlargestHelper(k, A, 0, len(A) - 1)

        return A[-k]

    def kthlargestHelper(self, k, A, start, end):

        pivot = A[end]

        low = start
        high = end - 1

        while high >= low:

            if A[high] > pivot:
                high = high - 1
            elif A[low] < pivot:
                low = low + 1
            else:
                "A[high] < pivot and A[low] > pivot, swap"
                temp = A[high]
                A[high] = A[low]
                A[low] = temp

        "high < low, need to swap pivot to right position"
        "swap high+1 and pivot"
        temp = A[high + 1]
        A[high + 1] = A[end]
        A[end] = temp

        "now we have: start ->   ..(smaller than high + 1 element)..  high + 1 ( this one is in correct position) -> ..(larger than high + 1 element).. end "
        if end - high  > k:
            "end - (high + 1) == k -1"
            "in right part"

            self.kthlargestHelper(k, A, high + 2, end)
        elif end - high  == k:
            return
        else:
            "in left part"
            self.kthlargestHelper(k - (end -high), A, start, high)


assert Solution().kthLargestElement(3,[9,3,2,4,8]) == 4
assert Solution().kthLargestElement(10,[1,2,3,4,5,6,8,9,10,7]) == 1
assert Solution().kthLargestElement(8,[1,10,3,8,5,7,2,9,6]) == 2


class Solution2:
    def kthLargestElement(self, k, A):
        n = len(A)
        k -= 1

        def partitionHelper(s, e):
            p, q = s + 1, e
            while p <= q:
                if (A[p] > A[s]):
                    p += 1
                else:
                    A[p], A[q] = A[q], A[p]
                    q -= 1

            A[s], A[q] = A[q], A[s]

            m = q
            if m == k:
                return A[m]
            elif m < k:
                return partitionHelper(m + 1, e)
            else:
                return partitionHelper(s, m - 1)

        return partitionHelper(0, n - 1)

