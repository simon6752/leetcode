#   1206. Next Greater Element I
# Description
# You are given two arrays (without duplicates) nums1 and nums2 where nums1’s elements are subset of nums2. Find all the next greater numbers for nums1's elements in the corresponding places of nums2.
#
# The Next Greater Number of a number x in nums1 is the first greater number to its right in nums2. If it does not exist, output -1 for this number.
#
# 1.All elements in nums1 and nums2 are unique.
# 2.The length of both nums1 and nums2 would not exceed 1000.
#
# Have you met this question in a real interview?
# Example
# Example 1:
#
# Input: nums1 = [4,1,2], nums2 = [1,3,4,2].
# Output: [-1,3,-1]
# Explanation:
#     For number 4 in the first array, you cannot find the next greater number for it in the second array, so output -1.
#     For number 1 in the first array, the next greater number for it in the second array is 3.
#     For number 2 in the first array, there is no next greater number for it in the second array, so output -1.
# Example 2:
#
# Input: nums1 = [2,4], nums2 = [1,2,3,4].
# Output: [3,-1]
# Explanation:
#     For number 2 in the first array, the next greater number for it in the second array is 3.
#     For number 4 in the first array, there is no next greater number for it in the second array, so output -1.


# my solution v1, not right. next greater element may not be peak, or may be next next peak, or...
class Solution:
	"""
	@param nums1: an array
	@param nums2: an array
	@return:  find all the next greater numbers for nums1's elements in the corresponding places of nums2
	"""
	
	def nextGreaterElement (self, nums1, nums2):
		# Write your code here
		
		# next greater element, find all peaks, go from end to start, all the positions between two peaks belong to the later one
		
		# no, maybe there are multiple peaks in the right, and the peak is smaller than current value
		# so next greater is the 1st one greater than current value
		
		
		
		if nums1 is None:
			return []
		
		if nums2 is None:
			return []
		
		res = []
		# initialize it to -1
		
		for i in range (0, len (nums2) - 1):
			res.append (-1)
		
		currentPeak = len (nums2) - 1
		
		for i in range (len (nums2) - 1, -1, 0):
			
			# find peak element
			
			if (i != 0 and i != len (nums2) - 1 and nums2 [i] > nums2 [i - 1] and nums2 [i] > nums2 [i + 1]) or (
					i == 0 and nums2 [i] > nums2 [i + 1]) or (i == len (nums2) - 1 and nums2 [i] > nums2 [i - 1]):
				currentPeak = i
			
			# in some cases, next greater element not exist
			
			res [i] = currentPeak


class Solution2:
	"""
	@param nums1: an array
	@param nums2: an array
	@return:  find all the next greater numbers for nums1's elements in the corresponding places of nums2
	"""
	
	def nextGreaterElement (self, nums1, nums2):
		# Write your code here
		
		# 1  3  7 5  2  1  2 3  4 8  9 4
		
		# when trend goes down, push to a stack for future comparison. when trend goes up, peek and compare
		
		if nums2 is None or len (nums2) == 0 or nums1 is None or len (nums1) == 0:
			return []
		
		stack = []
		
		# save key, and pos in a hash table
		
		pos = {}
		
		# save next greater element for each pos to a list
		nge = []
		
		for i in range (0, len (nums2)):
			nge.append (-1)
			pos [nums2 [i]] = i
		
		for i in range (0, len (nums2)):
			
			if i == 0:
				stack.append (i)
			else:
				if nums2 [i] > nums2 [i - 1]:
					# increasing
					nge [i - 1] = i
					# peek and see
					
					while len (stack) > 0 and nums2 [stack [-1]] < nums2 [i]:
						# found 1st greater element
						nge [stack [-1]] = i
						stack.pop ()
					
					# then do not forget to append i since now nums2[i] is the smallest compare to stack, and next greater element of nums2[i] is unknown
					stack.append (i)
				
				else:
					# decreasing
					stack.append (i)
		
		# check nums1
		
		res = []
		
		for i in range (0, len (nums1)):
			if nge [pos [nums1 [i]]] == -1:
				res.append (-1)
			else:
				res.append (nums2 [nge [pos [nums1 [i]]]])
		
		return res


assert Solution2().nextGreaterElement([4,1,2],[1,3,4,2]) == [-1,3,-1]
