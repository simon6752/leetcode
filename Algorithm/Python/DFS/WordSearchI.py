#  123. Word Search
# Description
# Given a 2D board and a word, find if the word exists in the grid.
#
# The word can be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once.
#
# Have you met this question in a real interview?
# Example
# Given board =
#
# [
#   "ABCE",
#   "SFCS",
#   "ADEE"
# ]
# word = "ABCCED", -> returns true,
# word = "SEE", -> returns true,
# word = "ABCB", -> returns false.

# my solution 1, use dfs, time limit exceeded for some cases
class Solution:
	"""
	@param board: A list of lists of character
	@param word: A string
	@return: A boolean
	"""
	
	def exist (self, board, word):
		# write your code here
		# graph, dfs, back tracking. need to record visited position, current level
		
		
		if word is None or len (word) == 0:
			return True
		
		if board is None or len (board) == 0 or len (board [0]) == 0:
			return False
		
		# n row, m col
		n = len (board)
		m = len (board [0])
		for i in range (0, n):
			for j in range (0, m):
				
				level = 0
				currentStr = ""
				index = i * m + j
				# record visited index to avoid duplicate
				visited = set ()
				if self.dfs (level, board, word, currentStr, index, visited, m, n):
					return True
		
		return False
	
	def dfs (self, level, board, word, currentStr, index, visited, m, n):
		
		if level > len (word):
			# too long, should stop
			return False
		
		# check current state
		if index in visited:
			# already visited
			return False
		
		i = index // m
		j = index % m
		
		if i < 0 or i > n - 1 or j < 0 or j > m - 1:
			# cross the border
			return False
		
		# if current letter is not the same as the right position of
		if board [i] [j] != word [level]:
			# do not need to continue
			return False
		
		if currentStr + board [i] [j] == word:
			# now whole word found
			return True
		
		# recursively search in all four directions
		
		# we need to use seperate visited and currentStr for each further exploration since they are seperate path
		
		visited1 = set (visited)
		visited1.add (index)
		visited2 = set (visited)
		visited2.add (index)
		visited3 = set (visited)
		visited3.add (index)
		visited4 = set (visited)
		visited4.add (index)
		
		if self.dfs (level + 1, board, word, currentStr + board [i] [j], (i + 1) * m + j, visited1, m, n) or self.dfs (
						level + 1, board, word, currentStr + board [i] [j], (i - 1) * m + j, visited2, m,
						n) or self.dfs (level + 1, board, word, currentStr + board [i] [j], i * m + j + 1, visited3, m,
		                                n) or self.dfs (level + 1, board, word, currentStr + board [i] [j],
		                                                i * m + j - 1, visited4, m, n):
			return True
		
		return False


assert Solution().exist(["ABCE","SFES","ADEE"],"ABCESEEEFS") == True

#  ABCE
#  SFES
#  ADEE

# this test case might failed because to too many choices. we need to early return
# assert  Solution().exist(["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaab"],"baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa") == True

# my solution 2, optimized visited matrix, still not pass some test cases due to time limit exceeded
class Solution2:
	"""
	@param board: A list of lists of character
	@param word: A string
	@return: A boolean
	"""
	
	def exist (self, board, word):
		# write your code here
		# graph, dfs, back tracking. need to record visited position, current level
		
		
		if word is None or len (word) == 0:
			return True
		
		if board is None or len (board) == 0 or len (board [0]) == 0:
			return False
		
		# n row, m col
		n = len (board)
		m = len (board [0])
		# use board size list to save visited
		
		visited = []
		
		for i in range (0, n):
			visited.append ([])
			for j in range (0, m):
				visited [i].append (False)
		
		for i in range (0, n):
			for j in range (0, m):
				
				level = 0
				currentStr = ""
				index = i * m + j
				# record visited index to avoid duplicate
				if self.dfs (level, board, word, currentStr, index, visited, m, n):
					return True
		
		return False
	
	def dfs (self, level, board, word, currentStr, index, visited, m, n):
		
		if level > len (word):
			# too long, should stop
			return False
		
		i = index // m
		j = index % m
		
		if i < 0 or i > n - 1 or j < 0 or j > m - 1:
			# cross the border
			return False
		
		# check current state
		if visited [i] [j]:
			# already visited
			return False
		
		# if current letter is not the same as the right position of
		if board [i] [j] != word [level]:
			# do not need to continue
			return False
		
		if currentStr + board [i] [j] == word:
			# now whole word found
			return True
		
		# recursively search in all four directions
		
		# we need to use seperate visited and currentStr for each further exploration since they are seperate path
		
		visited [i] [j] = True
		if self.dfs (level + 1, board, word, currentStr + board [i] [j], (i + 1) * m + j, visited, m, n) or self.dfs (
						level + 1, board, word, currentStr + board [i] [j], (i - 1) * m + j, visited, m,
						n) or self.dfs (level + 1, board, word, currentStr + board [i] [j], i * m + j + 1, visited, m,
		                                n) or self.dfs (level + 1, board, word, currentStr + board [i] [j],
		                                                i * m + j - 1, visited, m, n):
			return True
		
		visited [i] [j] = False
		return False


assert Solution2().exist(["ABCE","SFCS","ADEE"],"ABCB") == False
assert  Solution2().exist(["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaab"],"baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa") == True

# jiuzhang
class Solution3:
    # @param board, a list of lists of 1 length string
    # @param word, a string
    # @return a boolean
    def exist(self, board, word):
        # write your code here
        # Boundary Condition
        if word == []:
            return True
        m = len(board)
        if m == 0:
            return False
        n = len(board[0])
        if n == 0:
            return False
        # Visited Matrix
        visited = [[False for j in range(n)] for i in range(m)]
        # DFS
        for i in range(m):
            for j in range(n):
                if self.exist2(board, word, visited, i, j):
                    return True
        return False

    def exist2(self, board, word, visited, row, col):
        if word == '':
            return True
        m, n = len(board), len(board[0])
        if row < 0 or row >= m or col < 0 or col >= n:
            return False
        if board[row][col] == word[0] and not visited[row][col]:
            visited[row][col] = True
            # row - 1, col
            if self.exist2(board, word[1:], visited, row - 1, col) or self.exist2(board, word[1:], visited, row, col - 1) or self.exist2(board, word[1:], visited, row + 1, col) or self.exist2(board, word[1:], visited, row, col + 1):
                return True
            else:
                visited[row][col] = False
        return False

assert  Solution3().exist(["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaab"],"baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa") == True

# my solution v3, changed dfs search order: up, left, lower, right
class Solution4:
	"""
	@param board: A list of lists of character
	@param word: A string
	@return: A boolean
	"""
	
	def exist (self, board, word):
		# write your code here
		# graph, dfs, back tracking. need to record visited position, current level
		
		
		if word is None or len (word) == 0:
			return True
		
		if board is None or len (board) == 0 or len (board [0]) == 0:
			return False
		
		# n row, m col
		n = len (board)
		m = len (board [0])
		# use board size list to save visited
		
		visited = []
		
		for i in range (0, n):
			visited.append ([])
			for j in range (0, m):
				visited [i].append (False)
		
		for i in range (0, n):
			for j in range (0, m):
				
				level = 0
				currentStr = ""
				index = i * m + j
				# record visited index to avoid duplicate
				if self.dfs (level, board, word, currentStr, index, visited, m, n):
					return True
		
		return False
	
	def dfs (self, level, board, word, currentStr, index, visited, m, n):
		
		if level > len (word):
			# too long, should stop
			return False
		
		i = index // m
		j = index % m
		
		if i < 0 or i > n - 1 or j < 0 or j > m - 1:
			# cross the border
			return False
		
		# check current state
		if visited [i] [j]:
			# already visited
			return False
		
		# if current letter is not the same as the right position of
		if board [i] [j] != word [level]:
			# do not need to continue
			return False
		
		if currentStr + board [i] [j] == word:
			# now whole word found
			return True
		
		# recursively search in all four directions
		
		# we need to use seperate visited and currentStr for each further exploration since they are seperate path
		
		visited [i] [j] = True
		# it is very important that the explore pattern follows up, left, lower, right so the dfs search will not end in redundant search when there are lots of identical letters !
		if self.dfs (level + 1, board, word, currentStr + board [i] [j], (i - 1) * m + j, visited, m, n) or self.dfs (
						level + 1, board, word, currentStr + board [i] [j], i * m + j - 1, visited, m, n) or self.dfs (
						level + 1, board, word, currentStr + board [i] [j], (i + 1) * m + j, visited, m,
						n) or self.dfs (level + 1, board, word, currentStr + board [i] [j], i * m + j + 1, visited, m,
		                                n):
			return True
		
		visited [i] [j] = False
		return False







