#  515. Paint House
# Description
# There are a row of n houses, each house can be painted with one of the three colors: red, blue or green. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.
#
# The cost of painting each house with a certain color is represented by a n x 3 cost matrix. For example, costs[0][0] is the cost of painting house 0 with color red; costs[1][2] is the cost of painting house 1 with color green, and so on... Find the minimum cost to paint all houses.
#
# All costs are positive integers.
#
# Have you met this question in a real interview?
# Example
# Given costs = [[14,2,11],[11,14,5],[14,3,10]] return 10
#
# house 0 is blue, house 1 is green, house 2 is blue, 2 + 5 + 3 = 10



# dynamic programming


class Solution:
	"""
	@param costs: n x 3 cost matrix
	@return: An integer, the minimum cost to paint all houses
	"""
	
	def minCost (self, costs):
		# write your code here
		# check
		# 14  2   11
		# 11 14   5
		# 14 3   10
		
		# 1st row, min cost to paint it in specific color is 14  2 11
		# 2nd row, min cost to paint it in specific color is the cost of that color for 2 add minimum cost to paint among those with different color of 1, so it is 11+2= 13, 14 + 11 = 25, 5 + 2 = 7
		# 3rd row, the same,  14 + 7 = 21,  3 + 7 = 10, 10 + 13 = 23
		# total is 10
		
		# so f[i][j] = min(f[i-1][k]) + costs[i][j], k is one of RGB but differnt in j. j = 0, then 1, 2. j = 1, then 0,2. j =2 then 0, 1
		
		if costs is None or len (costs) == 0:
			return 0
		
		n = len (costs)
		
		mincost = [[costs [0] [0], costs [0] [1], costs [0] [2]]]
		
		for i in range (1, n):
			row = []
			row.append (costs [i] [0] + min (mincost [i - 1] [1], mincost [i - 1] [2]))
			row.append (costs [i] [1] + min (mincost [i - 1] [0], mincost [i - 1] [2]))
			row.append (costs [i] [2] + min (mincost [i - 1] [0], mincost [i - 1] [1]))
			
			mincost.append (row)
		
		# chech minimum of last row
		
		minc = min (min (mincost [-1] [0], mincost [-1] [1]), mincost [-1] [2])
		
		return minc
	
# test case


# complexity

# time, O(n), space, O(n)