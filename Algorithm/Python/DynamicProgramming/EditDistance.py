# 119. Edit Distance
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given two words word1 and word2, find the minimum number of steps required to convert word1 to word2. (each operation is counted as 1 step.)
#
# You have the following 3 operations permitted on a word:
#
# Insert a character
# Delete a character
# Replace a character
# Have you met this question in a real interview?
# Example
# Given word1 = "mart" and word2 = "karma", return 3.
#
# Tags
# Dynamic Programming String

class Solution:
    """
    @param word1: A string
    @param word2: A string
    @return: The minimum number of steps.
    """

    def minDistance(self, word1, word2):
        # write your code here

        "each step of operation is a transformation from substring1 to substring2, we need many steps to achieve goal."

        "substring1 and substring2, they can first reach: 1. if len(substring1) == len(substring2), already find ways to convert [0:-1] of substring1 to [0:-1] of substring2, then we only need to replace last char; 2. len(substring1) +1  = len(substring2), already find ways to convert [0:-1] of substring1 to substring2, we only need to insert last one of substring 1; 3. len(substring) == len(substring2) +1. if we already found way to convert [0:-1] of substring1 to substring2, we can remove last one of substring1"

        " we need to consider len(substring1) and len(substring2). only it is in 0, +1, -1, we can do something. "

        m = len(word1)
        n = len(word2)

        if m == 0:
            return n
        if n == 0:
            return m

        "initialization"
        "takes 0 step from empty to empty since they are equal"
        mat = [[0]]

        for i in range(1, m + 1):
            mat.append([i])

        for i in range(1, n + 1):
            mat[0].append(i)

        "decomposition"
        for i in range(1, m + 1):

            for j in range(1, n + 1):
                mat[i].append(0)
                "check last char of string1 and string2, equal?"
                if word1[i - 1] == word2[j - 1]:
                    "either from left then add last char, or from up remove one char, or from diagonal directly"
                    mat[i][j] = min(min(mat[i - 1][j] + 1, mat[i][j - 1] + 1), mat[i - 1][j - 1])
                else:
                    "either from left then add last char, or from up remove one char, or from diagonal then replace last char"
                    mat[i][j] = min(min(mat[i - 1][j] + 1, mat[i][j - 1] + 1), mat[i - 1][j - 1] + 1)

        return mat[-1][-1]


assert Solution().minDistance("sea", "ate") == 3
