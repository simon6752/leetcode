# 79. Longest Common Substring
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given two strings, find the longest common substring.
#
# Return the length of it.
#
#  Notice
# The characters in substring should occur continuously in original string. This is different with subsequence.
#
# Have you met this question in a real interview?
# Example
# Given A = "ABCD", B = "CBCE", return 2.
#
# Challenge
# O(n x m) time and memory.
#
# Tags
# LintCode Copyright String


class Solution:
    """
    @param A: A string
    @param B: A string
    @return: the length of the longest common substring.
    """

    def longestCommonSubstring(self, A, B):
        # write your code here
        "ABCD  CBCE LCSS[i][j] and LCSS[i-1][j-1]. if A[i] == B[j], LCSS[i][j] = LCSS[i-1][j-1] + 1, else it is 0. return max of whole LCSS"

        if A is None or B is None or len(A) == 0 or len(B) == 0:
            return 0

        lcss = [[0]]

        for i in range(1, len(A) + 1):
            lcss.append([0])

        for j in range(1, len(B) + 1):
            lcss[0].append(0)

        maxlcss = 0

        for i in range(1, len(A) + 1):
            for j in range(1, len(B) + 1):
                if A[i - 1] == B[j - 1]:
                    "the same, rely on lcss[i-1][j-1]"
                    "use append, do not use lcss[i][j] = since lcss[i][j] not exist yet"
                    lcss[i].append(lcss[i - 1][j - 1] + 1)
                else:
                    "not the same, restart count"
                    lcss[i].append(0)

                maxlcss = max(maxlcss, lcss[i][j])

        return maxlcss


assert Solution().longestCommonSubstring("www.lintcode.com code", "www.ninechapter.com code") == 9
