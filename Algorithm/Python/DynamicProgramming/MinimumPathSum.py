class Solution:
    """
    @param grid: a list of lists of integers
    @return: An integer, minimizes the sum of all numbers along its path
    """

    def minPathSum(self, grid):
        # write your code here
        "minimum path sum to current grid is mininum path sum to upper one or left one add current value"

        if grid is None or len(grid) == 0:
            return 0

        m = len(grid)
        n = len(grid[0])

        "initialization"

        result = []

        result.append([grid[0][0]])

        for i in range(1, m):
            result.append([result[i - 1][0] + grid[i][0]])

        for j in range(1, n):
            result[0].append(result[0][j - 1] + grid[0][j])

        "decomposition"

        for i in range(1, m):
            for j in range(1, n):
                "element [i][j] is not there yet, need to append"
                result[i].append(min(result[i - 1][j], result[i][j - 1]) + grid[i][j])

        return result[-1][-1]


grid = [[1, 2], [1, 1]]

print(Solution().minPathSum(grid))
