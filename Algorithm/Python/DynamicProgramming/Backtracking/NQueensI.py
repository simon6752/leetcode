# 33. N-Queens
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# The n-queens puzzle is the problem of placing n queens on an n×n chessboard such that no two queens attack each other.
#
# Given an integer n, return all distinct solutions to the n-queens puzzle.
#
# Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a queen and an empty space respectively.
#
# Have you met this question in a real interview?
# Example
# There exist two distinct solutions to the 4-queens puzzle:
#
# [
#   // Solution 1
#   [".Q..",
#    "...Q",
#    "Q...",
#    "..Q."
#   ],
#   // Solution 2
#   ["..Q.",
#    "Q...",
#    "...Q",
#    ".Q.."
#   ]
# ]
# Challenge
# Can you do it without recursion?
#
# Tags
# Recursion Depth First Search

"my solution not work"
class Solution:
    """
    @param: n: The number of queens
    @return: All distinct solutions
    """

    def solveNQueens(self, n):
        # write your code here
        "first version, recursion, DFS search"

        result = []
        curColRowDiff = [0] * n
        curColChoice = [0] * n
        curColRowSum = [0]*n*2

        "queen with the same i, or j, or i1-j1= i2-j2 (0 to +/- (n-1) or i1+j1 = i2+j2"

        self.NQueensHelper(n, result, 0, curColRowDiff, curColChoice,[],curColRowSum)

        return result

    def NQueensHelper(self, n, result, level, curColRowDiff, curColChoice,tempresult,curColRowSum):

        if level == n:
            result.append(tempresult)
            return

        "add solution for current level recursively"
        "each queen must have unique i, j, and i - j"
        "so for each new row, we only need to choose a unique col - row, so the col # will be (col - row ) + col"

        if level == 0:
            tempresult = []
        for i in range(0, n):
            if curColRowDiff[i] == 0:
                "available"
                curColRowDiff[i] = 1
                "Q's position is (col-row) + col, (i + level) % n here"
                m = (i+ level) % n
                "make sure current col is available"
                if curColChoice[m] == 0:
                    "current col available"
                    curColChoice[m] = 1
                    row = "." * max((m),0) + "Q" + "." * max((n-m-1),0)
                    "calculate and check i+j to see if it is available"
                    sum = (level + m)
                    if curColRowSum[sum] == 0:
                        curColRowSum[sum] = 1
                        tempresult.append(row)
                        self.NQueensHelper(n, result, level + 1, curColRowDiff,curColChoice, tempresult,curColRowSum)
                        "reset it back"
                        curColChoice[m] = 0
                        curColRowDiff[i] = 0
                        curColRowSum[sum] = 0

assert Solution().solveNQueens(2) == []
assert Solution().solveNQueens(3) == []
assert Solution().solveNQueens(4) == [["..Q.","Q...","...Q",".Q.."],[".Q..","...Q","Q...","..Q."]]

# TODO