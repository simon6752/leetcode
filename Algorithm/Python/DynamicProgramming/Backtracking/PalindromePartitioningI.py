# 136. Palindrome Partitioning
# Description
# Given a string s, partition s such that every substring of the partition is a palindrome.

# Return all possible palindrome partitioning of s.

# Have you met this question in a real interview?
# Example
# Given s = "aab", return:

# [
#   ["aa","b"],
#   ["a","a","b"]
# ]


# my solution 1, wrong
class Solution:
	"""
	@param: s: A string
	@return: A list of lists of string
	"""
	
	def partition (self, s):
		# write your code here
		
		# s, start with each char or space, if left part is palindrome, reuse it
		
		# s = "aaabb"
		# [ a a a b b ] [ aa a b b ] [ a aa b b ] [a a a bb] [aa a bb] [a aa bb] [ aaa b b ] [ aaa bb]
		# s = "abca"
		# [a b c a ]
		
		# 1st find all subsets of combination of partition
		
		# lets say we already have palindrome of s[1:], we decide to add s[0] to it. either we have s[0] + s[1:] or add s[0] to 1st palindrome of s[1:]
		
		if s is None or len (s) <= 1:
			return [[s]]
		
		# use linear scan and central expansion to find all possible start/end for palinrome
		# then do partition using this list!
		
		pos = {}
		for i in range (0, len (s) - 1):
			# center pos for space,after i
			for j in range (0, min (i + 1, len (s) - i - 1)):
				if s [i - j] == s [i + 1 + j]:
					# add this start/end to pos
					if i - j in pos.keys ():
						pos [i - j].append (i + 1 - j)
					else:
						pos [i - j] = [i + 1 - j]
		
		for i in range (0, len (s)):
			# center pos for char
			for j in range (0, min (i + 1, len (s) - i)):
				if s [i + j] == s [i - j]:
					# add this start/end to pos
					if i + j not in pos.keys ():
						pos [i + j] = [i - j]
					else:
						pos [i + j].append (i - j)
		
		# now we get all possible end position for start position
		
		result = [[]]
		self.partitionHelper (result, s, 0, pos)
		return result
	
	def partitionHelper (self, result, s, start, pos):
		# update result recursively use pos table
		# end index is current position for specific start in pos. say pos = {0:[3,5,7],1:[2,4,8]}. for 1, endindex can be 0,1,2
		
		if start >= len (s):
			# need to stop
			return
		
		for i in range (0, len (pos [start])):
			for j in range (0, len (result)):
				result [j].append (s [start:i + 1])
				self.partitionHelper (result, s, pos [start] [i] + 1, pos)


class Solution2:
	"""
	@param: s: A string
	@return: A list of lists of string
	"""
	
	def partition (self, s):
		# write your code here
		# analyze: 1. palindrome is symmetric to a center, either a char or a space
		# 2. partition, reuse previous result. we need to first find a palindrome, then partition rest of the string
		# time complexity:
		
		if s is None or len (s) == 0:
			return []
		
		result = []
		self.partitionHelper (result, s, [])
		return result
	
	def partitionHelper (self, result, s, stringlist):
		# stringlist is previous palindromes before s
		
		if s is None:
			return
		
		if len (s) == 0:
			result.append (stringlist)
			return
		
		for i in range (0, len (s)):
			# find 1st palindrome
			if self.isPalindrome (s [0:i + 1]):
				# continue partition rest of s
				self.partitionHelper (result, s [i + 1:], stringlist + [s [0:i + 1]])
	
	def isPalindrome (self, s):
		# return if s is palindrome
		
		if s is None or len (s) < 2:
			return True
		
		for i in range (0, len (s) // 2):
			# 2: 0 1, 0
			# 3: 0 1 2 , 0
			if s [i] != s [len (s) - 1 - i]:
				return False
		
		return True

