# 169. Tower of Hanoi
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Tower of Hanoi problem, is a well-known problem. On the A, B, C three pillars, there are n disks of different sizes (radii 1-n), they are stacked in a start on A, your goal is to a minimum number of legal steps to move all the plates move from A to C tower tower.
# Each step in the rules of the game are as follows:
# 1. Each step is only allowed to move a plate (from the top of one pillars to the top of another pillars)
# 2. The process of moving, you must ensure that a large dish is not at the top of the small plates (small can be placed on top of a large, below the maximum plate size can not have any other dish)
#
# Diagram:
# hanoi
#
# Have you met this question in a real interview?
# Example
# Given n = 3
#
# return ["from A to C","from A to B","from C to B","from A to C","from B to A","from B to C","from A to C"]
#
# Tags
# Backtracking

class Solution:
    """
    @param n: the number of disks
    @return: the order of moves
    """

    def towerOfHanoi(self, n):
        # write your code here
        "lets say we have 1->n size disk in 1-> 3 tower"

        "define move"

        "think reversely"

        "n and n-1"
        "assume we can move from 1-> n-1 to a stack, n in another tower"
        "then n to destination, then 1-> n-1 to destination"

        # totalStep = 1 + 2* self.towerOfHanoi(n-1)
        "need to output 1-> n -1 from A to B, then n from A to C, then 1-> n-1 from B to C"

        result = []
        self.towerOfHanoiHelper(n, "A", "C", "B", result)
        return result

    def towerOfHanoiHelper(self, n, start, end, aux, result):

        if n == 1:
            result.append("from " + start + " to " + end)
            return

        else:
            self.towerOfHanoiHelper(n - 1, start, aux, end, result)
            self.towerOfHanoiHelper(1, start, end, aux, result)
            self.towerOfHanoiHelper(n - 1, aux, end, start, result)
            return
