# 77. Longest Common Subsequence
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given two strings, find the longest common subsequence (LCS).
#
# Your code should return the length of LCS.
#
# Have you met this question in a real interview?
# Clarification
# What's the definition of Longest Common Subsequence?
#
# https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
# http://baike.baidu.com/view/2020307.htm
# Example
# For "ABCD" and "EDCA", the LCS is "A" (or "D", "C"), return 1.
#
# For "ABCD" and "EACB", the LCS is "AC", return 2.
#
# Tags
# Dynamic Programming LintCode Copyright Longest Common Subsequence

class Solution:
    """
    @param A: A string
    @param B: A string
    @return: The length of longest common subsequence of A and B
    """

    def longestCommonSubsequence(self, A, B):
        # write your code here
        if A is None or B is None or len(A) == 0 or len(B) == 0:
            return 0

        "use dynamic programming"
        "f[i][j] is the LCS of A[0:i] and B[0:j]"
        "f[i][j] = max(1 + f[i-1][j-1], f[i-1][j],f[i][j-1]) if A[i] == B[j], else max(f[i-1][j], f[i][j-1])"

        m = len(A)
        n = len(B)

        lcs = []

        for i in range(0, m):
            if B[0] == A[i]:
                lcs.append([1])
            else:
                lcs.append([0])

        for j in range(1, n):
            if B[j] == A[0]:
                lcs[0].append(1)
            else:
                lcs[0].append(0)

        for i in range(1, m):
            for j in range(1, n):
                if A[i] == B[j]:
                    "we may use lcs[i-1][j-1], but we cannot ignore lcs[i-1][j] and lcs[i][j-1]"
                    "or probably not. lets try to find some test cases"
                    lcs[i].append(max(max(1 + lcs[i - 1][j - 1], lcs[i-1][j]),lcs[i][j-1]))
                else:
                    lcs[i].append(max(lcs[i - 1][j], lcs[i][j - 1]))

        return lcs[-1][-1]


# aaaaaaab
# acb
#
#           a    a     c   b
#      a    1    1    1     1
#      a    1    2    2     2
#      a    1    2    2     2
#      a    1    2    2     2
#      a    1    2    2     2
#      a    1    2    2     2
#      a    1    2    2     2
#      b    1    2    2     3
