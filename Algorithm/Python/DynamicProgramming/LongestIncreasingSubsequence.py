# 76. Longest Increasing Subsequence
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a sequence of integers, find the longest increasing subsequence (LIS).
#
# You code should return the length of the LIS.
#
# Have you met this question in a real interview?
# Clarification
# What's the definition of longest increasing subsequence?
#
# The longest increasing subsequence problem is to find a subsequence of a given sequence in which the subsequence's elements are in sorted order, lowest to highest, and in which the subsequence is as long as possible. This subsequence is not necessarily contiguous, or unique.
#
# https://en.wikipedia.org/wiki/Longest_increasing_subsequence
#
# Example
# For [5, 4, 1, 2, 3], the LIS is [1, 2, 3], return 3
# For [4, 2, 4, 5, 3, 7], the LIS is [2, 4, 5, 7], return 4
#
# Challenge
# Time complexity O(n^2) or O(nlogn)
#
# Tags
# Binary Search LintCode Copyright Dynamic Programming Microsoft


class Solution:
    """
    @param nums: An integer array
    @return: The length of LIS (longest increasing subsequence)
    """

    def longestIncreasingSubsequence(self, nums):
        # write your code here
        "naive solution: f[i][j] is a LIS start at i and end with j. f[i][j] = 1 + f[i][the position of first number smaller than jth number]. but this is O(n^3)"

        "a1, a2, a3, a4, a5,...., aj,..."
        "aj, find maximum LIS of all elements before aj"
        "  a[j] = 1 + max(LCS of a[1] -> a[j-1])(all nums[k] < nums[j]), but this is O(n^2) "

        if nums is None or len(nums) == 0:
            return 0

        if len(nums) == 1:
            return 1

        "minimum[i] represent LIS ends with nums[i]"
        minimum = [1]
        for i in range(1, len(nums)):
            minimum.append(0)

        max2 = 0
        for i in range(1, len(nums)):
            maxi = 0
            for j in range(0, i):
                if nums[j] < nums[i]:
                    maxi = max(maxi, minimum[j])

            minimum[i] = 1 + maxi
            max2 = max(max2, minimum[i])

        "the LIS may not end with last one, so return max of all"

        return max2


assert Solution().longestIncreasingSubsequence(
    [88, 4, 24, 82, 86, 1, 56, 74, 71, 9, 8, 18, 26, 53, 77, 87, 60, 27, 69, 17, 76, 23, 67, 14, 98, 13, 10, 83, 20, 43,
     39, 29, 92, 31, 0, 30, 90, 70, 37, 59]) == 10
