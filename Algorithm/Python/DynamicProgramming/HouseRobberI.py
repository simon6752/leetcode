# 392. House Robber
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.
#
# Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.
#
# Have you met this question in a real interview?
# Example
# Given [3, 8, 4], return 8.
#
# Challenge
# O(n) time and O(1) memory.
#
# Tags
# Airbnb LinkedIn Dynamic Programming

class Solution:
    """
    @param A: An array of non-negative integers
    @return: The maximum amount of money you can rob tonight
    """

    def houseRobber(self, A):
        # write your code here
        "dynamic programming: f[i] is if we rob ends at i, maximum we can get"

        "every home, you can choose do or not, 2^n choices. but we have to limit it to avoid adjacent houses"

        if A is None or len(A) == 0:
            return 0

        money = [0]

        for i in range(0, len(A)):
            money.append(0)
        money[1] = A[0]

        for i in range(2, len(A) + 1):
            money[i] = max(A[i - 1] + money[i - 2], money[i - 1])

        return money[-1]
