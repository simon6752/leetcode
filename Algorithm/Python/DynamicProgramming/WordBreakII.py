# TODO  582. Word Break II
# Description
# Given a string s and a dictionary of words dict, add spaces in s to construct a sentence where each word is a valid dictionary word.
#
# Return all such possible sentences.
#
# Have you met this question in a real interview?
# Example
# Gieve s = lintcode,
# dict = ["de", "ding", "co", "code", "lint"].
#
# A solution is ["lint code", "lint co de"].

# my solution 1, no dfs, no memorization, dynamic programming but time limit exceeded
class Solution:
	"""
	@param: s: A string
	@param: wordDict: A set of words.
	@return: All possible sentences.
	"""
	
	def wordBreak (self, s, wordDict):
		# write your code here
		
		# use dynamic programming
		
		# f[i] is the list of combinations end with position i, where i is from 0 to n
		
		if s is None or len (s) == 0:
			return []
		
		if wordDict is None or len (wordDict) == 0:
			return []
		
		f = []
		
		n = len (s)
		for i in range (0, n):
			f.append ([])
		
		for i in range (0, n):
			
			# check j from 0 to i, if s[j:i+1] is in wordDict, append all s[j:i+1] + f[j]
			
			for j in range (0, i + 1):
				
				if s [j:i + 1] in wordDict:
					if j != 0 and len (f [j - 1]) != 0:
						# there is combination for f[j-1]
						for k in range (0, len (f [j - 1])):
							f [i].append (f [j - 1] [k] + " " + s [j:i + 1])
					elif j == 0:
						# there is no combination for f[j-1]
						f [i].append (s [j:i + 1])
					else:
						# j!= 0 and len(f[j-1] ) == 0
						# do nothing
						pass
		
		return f [-1]
	
assert Solution().wordBreak("lintcode",["de", "ding", "co", "code", "lint"]) == ["lint code", "lint co de"]

# Input
# "ab"
# ["b","a"]
# Output
# []
# Expected
# ["a b"]

assert Solution().wordBreak("ab",["b","a"]) == ["a b"]

assert Solution().wordBreak("aaab",["b","aa"]) == []

# time limit exceeded

# time and space analysis

# time:

assert Solution().wordBreak("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" ,["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]) == []