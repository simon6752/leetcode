class Solution:
    """
    @param triangle: a list of lists of integers
    @return: An integer, minimum path sum
    """

    def minimumTotal(self, triangle):
        # write your code here

        "dynamic programming"

        m = len(triangle)

        sum = []
        "initialization"
        if m == 1:
            return triangle[0][0]

        sum.append([triangle[0][0]])
        for i in range(1, m):
            sum.append([triangle[i][0] + sum[i - 1][0]])

        "decomposition"
        for i in range(1, m):
            for j in range(1, i + 1):
                if j == i:
                    sum[i].append(sum[i - 1][j - 1] + triangle[i][j])
                else:
                    sum[i].append(min(sum[i - 1][j - 1], sum[i - 1][j]) + triangle[i][j])

        "find minimum in last row"

        minpath = sum[-1][0]

        for i in range(0, m):
            minpath = min(minpath, sum[-1][i])

        return minpath


print(Solution().minimumTotal([[1], [2, 3]]))
