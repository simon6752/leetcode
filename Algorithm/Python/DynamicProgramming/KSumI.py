# TODO 89. k Sum
# Description
# Given n distinct positive integers, integer k (k <= n) and a number target.
#
# Find k numbers where sum is target. Calculate how many solutions there are?
#
# Have you met this question in a real interview?
# Example
# Given [1,2,3,4], k = 2, target = 5.
#
# There are 2 solutions: [1,4] and [2,3].
#
# Return 2.