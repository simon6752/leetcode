#  90. k Sum II
# Description
# Given n unique integers, number k (1<=k<=n) and target.
#
# Find all possible k integers where their sum is target.
#
# Have you met this question in a real interview?
# Example
# Given [1,2,3,4], k = 2, target = 5. Return:
#
# [
#   [1,4],
#   [2,3]
# ]

class Solution:
	"""
	@param: A: an integer array
	@param: k: a postive integer <= length(A)
	@param: targer: an integer
	@return: A list of lists of integer
	"""
	
	def kSumII (self, A, k, target):
		# write your code here
		
		# find all, use dfs and backtracking
		
		if A is None or len (A) < k:
			return []
		
		A = sorted (A)
		
		# k sum is, we find 1st in A[:i+1], find rest k-1 in A[i+1:]
		
		result = []
		
		self.kSumIIHelper (A, k, target, result, [])
		return result
	
	def kSumIIHelper (self, A, k, target, result, currentList):
		
		if len (A) < k:
			return
		
		# if k == len (A):
		# 	if sum (A) == target:
		# 		currentList += A
		# 		result.append (currentList)
		#
		# 	return
		
		if k == 0:
			if target == 0:
				# we already find all elements
				result.append (currentList)
			return
		
		# now we have more element to explore
		# we can do early stop actually. if sum(A[0:k]) > target or sum(A[-k:]) > target
		#
		
		for i in range (0, len (A) - k + 1):
			# 1st element could be 0 to len(A) -k
			# find rest k-1 in rest
			self.kSumIIHelper (A [i + 1:], k - 1, target - A [i], result, currentList + [A [i]])
			
			
assert Solution().kSumII([1,2,3,4],2,5) == [[1,4],[2,3]]

