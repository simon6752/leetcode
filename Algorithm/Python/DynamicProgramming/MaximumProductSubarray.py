# 191. Maximum Product Subarray
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Find the contiguous subarray within an array (containing at least one number) which has the largest product.
#
# Have you met this question in a real interview?
# Example
# For example, given the array [2,3,-2,4], the contiguous subarray [2,3] has the largest product = 6.
#
# Tags
# Subarray LinkedIn Dynamic Programming

class Solution:
    """
    @param nums: An array of integers
    @return: An integer
    """

    def maxProduct(self, nums):
        # write your code here
        "state, subarray end with current value, maximum product. it depends on previous element's maximum product and whether it is negative. use a number to record and compare with maximum product of current element, then we get global maximum"

        "so it is dynamic programming"

        " maximum(i) = 1. max(, maximum(i-1) * num[i]) if num[i-1] is positive; 2. nums[i]*minimum(i-1) if nums[i-1] is negative"

        "wait, maybe we can have even numbers of nagatives in final subarray, so we need to record positive maximum and negative maximum?"

        "no numbers between (-1,1), need even number of negatives"
        "we need to keep and compare two list: max list and min list. min list is for negative product"

        maxlist = []
        minlist = []

        for i in range(0, len(nums)):
            "initialization???"
            maxlist.append(nums[i])
            minlist.append(nums[i])

        curmax = nums[0]

        for i in range(1, len(nums)):
            "four cases for two consequent number: ++, -+, +-, --"
            if nums[i] > 0:
                "-+ vs ++. if -+, we have to restart our subarray with nums[i]"
                maxlist[i] = max(nums[i], maxlist[i - 1] * nums[i])
                "-+ vs ++"
                minlist[i] = min(nums[i], minlist[i - 1] * nums[i])
            else:
                "nums[i] < 0"
                "-- vs +-"
                maxlist[i] = max(nums[i], minlist[i - 1] * nums[i])
                "+- vs --"
                minlist[i] = min(nums[i], maxlist[i - 1] * nums[i])
            print( "max list: %s" , maxlist)
            print( "min list: %s" , minlist)
            if maxlist[i] > curmax:
                curmax = maxlist[i]

        return curmax

assert Solution().maxProduct([2,3,-2,4]) == 6
assert Solution().maxProduct([-1, 99]) == 99

# class Solution2:
#
#     """
#     @param nums: An array of integers
#     @return: An integer
#     """
#
#     def maxProduct(self, nums):
#         # write your code here
#         "state, subarray end with current value, maximum product. it depends on previous element's maximum product and whether it is negative. use a number to record and compare with maximum product of current element, then we get global maximum"
#
#         "so it is dynamic programming"
#
#         " maximum(i) = 1. max(, maximum(i-1) * num[i]) if num[i-1] is positive; 2. nums[i]*minimum(i-1) if nums[i-1] is negative"
#
#         "wait, maybe we can have even numbers of nagatives in final subarray, so we need to record positive maximum and negative maximum?"
#
#         "no numbers between (-1,1), need even number of negatives"
#         "we need to keep and compare two list: max list and min list. min list is for negative product"
#
#         maxlist = []
#         minlist = []
#
#         for i in range(0, len(nums)):
#             "initialization???"
#             maxlist.append(nums[i])
#             minlist.append(nums[i])
#
#         curmax = nums[0]
#
#         for i in range(1, len(nums)):
#             "four cases for two consequent number: ++, -+, +-, --"
#             if nums[i] > 0:
#                 "-+ vs ++. if -+, we have to restart our subarray with nums[i]"
#                 maxlist[i] =  maxlist[i - 1] * nums[i]
#                 "-+ vs ++"
#                 minlist[i] =    minlist[i - 1] * nums[i]
#             else:
#                 "nums[i] < 0"
#                 "-- vs +-"
#                 maxlist[i] =    minlist[i - 1] * nums[i]
#                 "+- vs --"
#                 minlist[i] =    maxlist[i - 1] * nums[i]
#
#             print( "max list: %s" , maxlist)
#             print( "min list: %s" , minlist)
#
#             if maxlist[i] > curmax:
#                 curmax = maxlist[i]
#
#         return curmax
#
# # assert Solution2().maxProduct([2,3,-2,4]) == 6
# assert Solution2().maxProduct([-1, 99]) == 99