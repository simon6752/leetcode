#  516. Paint House II
# There are a row of n houses, each house can be painted with one of the k colors. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.
#
# The cost of painting each house with a certain color is represented by a n x k cost matrix. For example, costs[0][0] is the cost of painting house 0 with color 0; costs[1][2] is the cost of painting house 1 with color 2, and so on... Find the minimum cost to paint all houses.
#
# Example
# Given n = 3, k = 3, costs = [[14,2,11],[11,14,5],[14,3,10]] return 10
#
# house 0 is color 2, house 1 is color 3, house 2 is color 2, 2 + 5 + 3 = 10
#
# Challenge
# Could you solve it in O(nk)?

import sys


class Solution:
	"""
	@param costs: n x k cost matrix
	@return: an integer, the minimum cost to paint all houses
	"""
	
	def minCostII (self, costs):
		# write your code here
		
		# the same to to paint house I, color change from 3 to k
		
		# dynamic programming
		
		# time, O(nk), space, O(nk)
		
		if costs is None or len (costs) == 0 or len (costs [0]) == 0:
			return 0
		
		mincost = []
		
		n = len (costs)
		k = len (costs [0])
		
		# we need to remember the last two least cost in previous row in mincost, since we need to decide which previous minimum to use.
		
		small1index = 0
		small1value = sys.maxsize
		small2index = 0
		small2value = sys.maxsize
		
		# initialization
		
		row = []
		for i in range (k):
			row.append (costs [0] [i])
			if costs [0] [i] < small1value:
				# update
				small1index = i
				small1value = costs [0] [i]
		
		for i in range (k):
			
			if costs [0] [i] > small1value:
				# costs[0][i] is not smallest
				if costs [0] [i] < small2value:
					small2index = i
					small2value = costs [0] [i]
			elif costs [0] [i] == small1value and i != small1index:
				if costs [0] [i] < small2value:
					small2index = i
					small2value = costs [0] [i]
		
		mincost.append (row)
		
		# decomposition
		for i in range (1, n):
			row = []
			for j in range (0, k):
				# assume we paint house i with color j, then minimum cost of this case would be costs[i][j]  add min(mincost[i-1][k]), where k != j
				if j != small1index:
					row.append (costs [i] [j] + small1value)
				else:
					row.append (costs [i] [j] + small2value)
			
			mincost.append (row)
			
			# another round to update small1index, small1value, small2index,small2value
			
			# initialize again
			small1index = 0
			small1value = sys.maxsize
			small2index = 0
			small2value = sys.maxsize
			
			for j in range (0, k):
				if mincost [i] [j] < small1value:
					small1value = mincost [i] [j]
					small1index = j
				
				# smallest and second smallest, use two round to calculate
			for j in range (0, k):
				if mincost [i] [j] > small1value:
					if mincost [i] [j] < small2value:
						small2value = mincost [i] [j]
						small2index = j
				elif mincost [i] [j] == small1value and j != small1index:
					# is a different index, with the same value as smallest
					small2index = j
					small2value = mincost [i] [j]
		
		return small1value


assert Solution().minCostII([[14,2,11],[11,14,5],[14,3,10]]) == 10

assert Solution().minCostII([[1,7,15],[4,19,8],[6,9,20],[14,10,7],[6,11,4],[8,5,8],[13,10,9],[1,5,15],[7,18,18],[2,17,13],[14,12,6],[13,8,17],[15,2,4],[14,10,2],[5,13,5],[11,18,4],[3,5,8],[13,8,18],[7,5,3],[17,3,11],[3,4,5],[19,3,10],[5,12,15],[12,2,9],[10,10,1],[15,1,8],[15,8,8],[17,2,2],[3,11,20],[11,20,8],[5,15,8],[6,2,14],[14,2,10],[16,11,14],[6,6,6],[10,15,3],[4,11,14],[8,12,11],[12,3,11],[16,19,14],[2,9,17],[2,18,17],[2,17,13],[13,7,13],[3,11,9],[2,4,4],[15,7,4],[16,8,20],[11,4,5],[11,10,1],[2,12,1],[10,18,1],[11,13,4],[9,7,14],[3,16,4],[9,9,4],[15,7,8],[3,7,14],[5,13,9],[12,17,7],[9,5,1],[16,13,20],[17,7,1],[8,17,1],[19,7,6],[16,4,18],[10,20,18],[6,18,13],[19,11,20],[14,6,3],[14,9,13],[1,9,14],[15,18,17],[8,6,4],[8,13,11],[18,5,9],[5,19,3],[7,19,6],[10,11,14],[5,10,19],[19,16,10],[16,12,17],[14,3,13],[18,11,12],[13,14,10],[2,10,20],[16,4,5],[12,13,8],[17,11,8],[12,15,17],[2,1,2],[17,17,12],[4,13,11],[10,8,6],[11,20,6],[14,15,19],[20,4,2],[4,18,17],[8,3,12],[5,3,9]]) == 627
