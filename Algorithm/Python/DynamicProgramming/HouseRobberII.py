# 534. House Robber II
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# After robbing those houses on that street, the thief has found himself a new place for his thievery so that he will not get too much attention. This time, all houses at this place are arranged in a circle. That means the first house is the neighbor of the last one. Meanwhile, the security system for these houses remain the same as for those in the previous street.
#
# Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.
#
#  Notice
# This is an extension of House Robber.
#
# Have you met this question in a real interview?
# Example
# nums = [3,6,4], return 6
#
# Tags
# Dynamic Programming Microsoft


class Solution2:
    """
    @param nums: An array of non-negative integers.
    @return: The maximum amount of money you can rob tonight
    """

    def houseRobber2(self, nums):
        # write your code here
        "let's handle 0 and n-1: both not and pick one"

        if nums is None or len(nums) == 0:
            return 0

        if len(nums) == 1:
            return nums[0]

        if len(nums) == 2:
            return max(nums[0], nums[1])

        if len(nums) == 3:
            "can only pick up 1 and do it"
            return max(max(nums[0], nums[1]), nums[2])

        n = len(nums)
        # 0   1  2 3 ... n-3  n-2 n-1
        # 1   0  ? ?      ?    ?     0
        # 0   ?  ?  ?     ?    ?     ?
        # 1 means do, 0 means no, ? means does not matter
        # so our case is actually max of [2:n-1] + nums[0], [1:n]

        return max(nums[0] + self.HouseRobber1Helper(nums[2: n - 1]), self.HouseRobber1Helper(nums[1:n]))

    def HouseRobber1Helper(self, nums):

        if nums is None or len(nums) == 0:
            return 0

        if len(nums) == 1:
            return nums[0]

        backpack = [0, nums[0]]

        n = len(nums)

        for i in range(2, n + 1):
            backpack.append(max(backpack[i - 1], nums[i - 1] + backpack[i - 2]))

        return backpack[-1]


testa = [828, 125, 740, 724, 983, 321, 773, 678, 841, 842, 875, 377, 674, 144, 340, 467, 625, 916, 463, 922, 255, 662,
         692, 123, 778, 766, 254, 559, 480, 483, 904, 60, 305, 966, 872, 935, 626, 691, 832, 998, 508, 657, 215, 162,
         858, 179, 869, 674, 452, 158, 520, 138, 847, 452, 764, 995, 600, 568, 92, 496, 533, 404, 186, 345, 304, 420,
         181, 73, 547, 281, 374, 376, 454, 438, 553, 929, 140, 298, 451, 674, 91, 531, 685, 862, 446, 262, 477, 573,
         627, 624, 814, 103, 294, 388]

assert Solution2().houseRobber2(testa) == 29029

from Algorithm.Python.DynamicProgramming.HouseRobberI import Solution

print(testa[0] + Solution().houseRobber(testa[2:-1]))
print(Solution().houseRobber(testa[1:]))

print(testa[0] + Solution2().HouseRobber1Helper(testa[2:-1]))
print(Solution2().HouseRobber1Helper(testa[1:]))
