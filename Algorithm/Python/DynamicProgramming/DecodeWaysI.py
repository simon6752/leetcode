# 512. Decode Ways
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# A message containing letters from A-Z is being encoded to numbers using the following mapping:
#
# 'A' -> 1
# 'B' -> 2
# ...
# 'Z' -> 26
# Given an encoded message containing digits, determine the total number of ways to decode it.
#
# Have you met this question in a real interview?
# Example
# Given encoded message 12, it could be decoded as AB (1 2) or L (12).
# The number of ways decoding 12 is 2.
#
# Tags
# Dynamic Programming String Facebook Uber Microsoft
# Related Problems
# Hard Decode Ways II

class Solution:
    """
    @param s: a string,  encoded message
    @return: an integer, the number of ways decoding
    """

    def numDecodings(self, s):
        # write your code here
        "dynamic programming: 1218129573162349215. for a[i], it can be docode to 1 or two ways. if s[i-1] is 1 or 2, we can decode to 2 numbers,  a[i-2] + a[i-1]. if s[i-1] is not 1 or 2, only one way, the same as a[i-1]"

        "if previous digit is not 1 or 2, only 1 way. if previous digit is 1 or 2, current digit is 1-6, we have 2, otherwise still 1"

        if s is None or len(s) == 0:
            return 0

        " two digits: 1st in (0,1,2), 2nd in (0-6),(7-9)"

        set1 = set(['1', '2', '3', '4', '5', '6'])
        set2 = set(['7', '8', '9'])
        set4 = set(['0'])
        set3 = set(['1', '2'])

        "initialize"

        way = [1] * (len(s) + 1)
        if s[0] == "0":
            "if 1st is 0, no way to decode"
            way[1] = 0

        for i in range(1, len(s)):
            "set default,treat s[i] as one digit"
            way[i + 1] = way[i]
            if s[i] in set1:
                "(1-6) -> (1,2), we can do either do 11 or 1, then 1"
                if s[i - 1] in set3:
                    way[i + 1] = way[i - 1] + way[i]
            elif s[i] in set2:
                if s[i - 1] == '1':
                    "17, 18, 19 and 1 7, 1 8, 1 9"
                    way[i + 1] = way[i - 1] + way[i]
            else:
                "is 0"
                if s[i - 1] in set3:
                    "only treat as 10, 20"
                    way[i + 1] = way[i - 1]
                else:
                    way[i + 1] = 0

        return way[-1]
