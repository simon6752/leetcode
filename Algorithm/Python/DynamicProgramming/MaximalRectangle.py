# TODO 510. Maximal Rectangle
# Description
# Given a 2D boolean matrix filled with False and True, find the largest rectangle containing all True and return its area.
#
# Have you met this question in a real interview?
# Example
# Given a matrix:
#
# [
#   [1, 1, 0, 0, 1],
#   [0, 1, 0, 0, 1],
#   [0, 0, 1, 1, 1],
#   [0, 0, 1, 1, 1],
#   [0, 0, 0, 0, 1]
# ]
# return 6.


# similar to maximal square

class Solution:
	"""
	@param matrix: a boolean 2D matrix
	@return: an integer
	"""
	
	def maximalRectangle (self, matrix):
		# write your code here
		# similar to square. but for rectange, we need to record sizes in two dimension: rectx, recty
		
		
		if matrix is None or len (matrix) == 0:
			return 0
		
		n = len (matrix)
		m = len (matrix [0])
		# row n, col m
		
		if m == 0:
			return 0
		
		# maximal rectangle size in x and y direction of position(i,j)
		rectx = []
		recty = []
		
		# also need to update max rectangle area
		maxrect = 1
		for i in range (0, n):
			rectx.append ([])
			recty.append ([])
			for j in range (0, m):
				rectx [i].append (matrix [i] [j])
				recty [i].append (matrix [i] [j])
				maxrect = max (maxrect, matrix [i] [j])
		
		# decomposition
		
		for i in range (1, n):
			for j in range (1, m):
				
				if matrix [i] [j] == 1:
					# depends on (i-1,j),(i,j-1), (i- k,j-l)
					
					# find the intersection point in upper left side
					# rectangle formed by (i-1,j ): (i-1,j),(i-1,j-rectx[i-1][j]),(i-1-recty[i-1][j],j),
					# (i-1-recty[i-1][j],j-rectx[i-1][j])
					# rectangle formed by (i,j-1): (i,j-1),(i-recty[i][j-1],j-1 ),(i,j-1-rectx[i][j-1]),
					# (i-recty[i][j-1],j-1-rectx[i][j-1])
					
					# according to relative position of upper left corner (i-1,j) and (i,j-1), there are four cases we need to consider. each case we may have 1 or 3 more extra point to consider
					
					
					# 1st, let's consider one is covering the other one
					
					if (i - 1 - recty [i - 1] [j] - (i - recty [i - 1] [j])) * (
							j - rectx [i - 1] [j] - (j - 1 - rectx [i] [j - 1])) >= 0:
						crossx = max (i - 1 - recty [i - 1] [j], i - recty [i - 1] [j])
						crossy = max (j - rectx [i - 1] [j], j - 1 - rectx [i] [j - 1])
						deltax = min (rectx [crossx] [crossy],
						              abs (j - rectx [i - 1] [j] - (j - 1 - rectx [i] [j - 1])))
						deltay = min (recty [crossx] [crossy],
						              abs (i - 1 - recty [i - 1] [j] - (i - recty [i - 1] [j])))
						
						rectx [i] [j] = i - crossx + deltax
						recty [i] [j] = j - crossy + deltay
					
					maxrect = max (maxrect, rectx [i] [j] * recty [i] [j])
			
			return maxrect








