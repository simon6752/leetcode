# 118. Distinct Subsequences
# Difficulty Medium Accepted Rate 32%
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a string S and a string T, count the number of distinct subsequences of T in S.
#
# A subsequence of a string is a new string which is formed from the original string by deleting some (can be none) of the characters without disturbing the relative positions of the remaining characters. (ie, "ACE" is a subsequence of "ABCDE" while "AEC" is not).
#
# Have you met this question in a real interview?
# Example
# Given S = "rabbbit", T = "rabbit", return 3.
#
# Challenge
# Do it in O(n2) time and O(n) memory.
#
# O(n2) memory is also acceptable if you do not know how to optimize memory.
#
# Tags
# Dynamic Programming String


class Solution:
    """
    @param: : A string
    @param: : A string
    @return: Count the number of distinct subsequences
    """

    def numDistinct(self, S, T):
        # write your code here
        if S is None or len(S) == 0:
            return 0

        if len(T) == 0:
            return 1

        "dynamic programming, f[i][j] records S[0:i] with subsequence from T[0:j]"
        "we need a hashmap to save the letters in T and the position of those letters: rabbit, {r:[0],a:[1],b:[1,2],i:[3],t:[4]}"

        m = len(S)
        n = len(T)
        f = []
        "use append instead of [0]*n for initialization to avoid object assignment"

        if S[0] == T[0]:
            f.append([1])
        else:
            f.append([0])

        for i in range(1, m):
            if S[i] == T[0]:
                f.append([f[i - 1][0] + 1])
            else:
                f.append([f[i - 1][0]])

        for i in range(0, m):
            for j in range(1, n):
                f[i].append(0)

        for i in range(1, m):
            "j can be at most i or n-1"
            for j in range(1, min(i + 1, n)):
                if S[i] == T[j]:
                    "we can choose stop at current i position,or previous i-1 position"
                    f[i][j] = f[i - 1][j - 1] + f[i - 1][j]
                else:
                    f[i][j] = f[i - 1][j]

        return f[-1][-1]


assert Solution().numDistinct("aacaacca", "ca") == 5
assert Solution().numDistinct("ddd", "dd") == 3
