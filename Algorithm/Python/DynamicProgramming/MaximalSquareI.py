#  436. Maximal Square
# Description
# Given a 2D binary matrix filled with 0's and 1's, find the largest square containing all 1's and return its area.
#
# Have you met this question in a real interview?
# Example
# For example, given the following matrix:
#
# 1 0 1 0 0
# 1 0 1 1 1
# 1 1 1 1 1
# 1 0 0 1 0
# Return 4.


class Solution:
	"""
	@param matrix: a matrix of 0 and 1
	@return: an integer
	"""
	
	def maxSquare (self, matrix):
		# write your code here
		# Dynamic Programming
		
		# 1st f[i][j] maximal square relation to f[i-1][j-1]
		# f[i][j] to f[i-1][j-1] until f[i-min(i,j)][j-min(i,j)], all are possible square.
		# f[i][j] , assume  f[i-1][j-1] already has a square end with f[i-1][j-1], then we only need to care about two more edges
		# for a square, we need start point (i,j), and size k
		
		# for i, j, we only need to check diagonal position, f[i-1][j-1] to f[i-min(i-j)][j-min(i,j)]
		# for each point (i,j), we check use (i,j) as right lower corner, how far can we go?
		# at point (i,j), lets assume use it as right lower corner, we can go size k. then f[i-1][j] and f[i][j-1] must can go k-1. so f[i][j] = min(f[i-1][j],f[i][j-1]) + 1 , we also need to check the far most diagonal point (i - min(f[i-1][j],f[i][j-])  , j - min(f[i-1][j],f[i][j-1])  )
		
		if matrix is None or len (matrix) == 0:
			return 0
		
		if len (matrix [0]) == 0 or len (matrix [0]) == 0:
			return 0
		
		n = len (matrix)
		m = len (matrix [0])
		
		# row n, col m
		
		# initialization
		# DO NOT USE * for initialization
		square = []
		
		for i in range (0, n):
			square.append ([])
			for j in range (0, m):
				square [i].append (0)
		
		maxk = 0
		
		for i in range (0, n):
			square [i] [0] = matrix [i] [0]
			maxk = max (maxk, square [i] [0])
		
		for j in range (0, m):
			square [0] [j] = matrix [0] [j]
			maxk = max (maxk, square [0] [j])
		
		# decomposition
		for i in range (1, n):
			for j in range (1, m):
				
				if matrix [i] [j] == 1:
					
					minc = min (square [i - 1] [j], square [i] [j - 1])
					# check (i-minc,j-minc )
					
					if matrix [i - minc] [j - minc] == 1:
						# square can increase 1
						square [i] [j] = minc + 1
					else:
						# no, can not increase 1
						square [i] [j] = minc
					
					maxk = max (maxk, square [i] [j])
		
		return maxk * maxk


matrix = [[0,1,1,1,1,1,1,1,1,1],
 [1,0,1,1,1,1,1,1,1,1],
 [1,1,0,1,1,1,1,1,1,1],
 [1,1,1,0,1,1,1,1,1,1],
 [1,1,1,1,0,1,1,1,1,1],
 [1,1,1,1,1,0,1,1,1,1],
 [1,1,1,1,1,1,0,1,1,1],
 [1,1,1,1,1,1,1,0,1,1],
 [1,1,1,1,1,1,1,1,0,1],
 [1,1,1,1,1,1,1,1,1,0]]

assert Solution().maxSquare(matrix) == 25