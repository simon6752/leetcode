# 29. Interleaving String
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given three strings: s1, s2, s3, determine whether s3 is formed by the interleaving of s1 and s2.
#
# Have you met this question in a real interview?
# Example
# For s1 = "aabcc", s2 = "dbbca"
#
# When s3 = "aadbbcbcac", return true.
# When s3 = "aadbbbaccc", return false.
# Challenge
# O(n2) time or better
#
# Tags
# Dynamic Programming Longest Common Subsequence

class Solution1:
    """
    @param s1: A string
    @param s2: A string
    @param s3: A string
    @return: Determine whether s3 is formed by interleaving of s1 and s2
    """

    def isInterleave(self, s1, s2, s3):
        # write your code here

        if s1 is None and s2 is None and s3 is None:
            return True

        if len(s1) == 0 and len(s2) == 0 and len(s3) == 0:
            return True

        if len(s1) == 0:
            return s2 == s3

        if len(s2) == 0:
            return s1 == s3

        if s3 is None or len(s3) != len(s1) + len(s2):
            return False

        "two pointers or dynamic programming"
        "at every position of s3, it must be either from s1 current position i, or s2 current position j. if yes, move pointer. if not, return False"
        "two pointers does not work here since when s1[p1] == s2[p2] = s3[p3], p1 p2 which one should move?"

        p1 = 0
        p2 = 0

        for p3 in range(0, len(s3)):
            if p1 < len(s1) and s3[p3] == s1[p1]:
                p1 += 1
            elif p2 < len(s2) and s3[p3] == s2[p2]:
                p2 += 1
            else:
                return False

        return True


# assert   Solution1().isInterleave("sdfjas;dfjoisdufzjkndfasdkfja;sdfa;dfa;dfaskdjhfasdhjdfakhdgfkajdfasdjfgajksdfgaksdhfasdkbfjkdsfbajksdfhakjsdfbajkdfbakdjsfgaksdhgfjkdsghfkdsfgadsjfgkajsdgfkjasdfh","dfnakdjnfjkzghdufguweygfasjkdfgb2gf8asf7tgbgasjkdfgasodf7asdgfajksdfguayfgaogfsdkagfsdhfajksdvfbgkadsghfakdsfgasduyfgajsdkfgajkdghfaksdgfuyadgfasjkdvfjsdkvfakfgauyksgfajkefgjkdasgfdjksfgadjkghfajksdfgaskdjfgasjkdgfuyaegfasdjkfgajkdfygadjskfgjkadfg","sdfjas;dfjoisdfnakdjnfjkzghdufguwdufzjkeygfasjkdfgb2gf8asf7ndtgbgasjkdfgasodf7asdfgfajkasdksdfguayfgaogfsdkagfsfjadhfajksdvfbgkadsghfa;sdkdsfgasduyfgajsdkfgafajkdghfaksdgfuyadgfas;dfjkdvfjsdkvfakfgauyksa;dgfajkefgjkdasgfdjksffaskdjhfasdhjdfakhdgadjkghfajgfkajdfksdfgaskdjfgasjkdgfuasdjfgajksdfgaksdhfasdkbfjkdsfbajksdfyaegfasdjkfgajkdfygadjskfgjkadfghakjsdfbajkdfbakdjsfgaksdhgfjkdsghfkdsfgadsjfgkajsdgfkjasdfh")  == True


"use dynamic programming"


class Solution:
    """
    @param s1: A string
    @param s2: A string
    @param s3: A string
    @return: Determine whether s3 is formed by interleaving of s1 and s2
    """

    def isInterleave(self, s1, s2, s3):
        # write your code here

        if s1 is None and s2 is None and s3 is None:
            return True

        if len(s1) == 0 and len(s2) == 0 and len(s3) == 0:
            return True

        if len(s1) == 0:
            return s2 == s3

        if len(s2) == 0:
            return s1 == s3

        if s3 is None or len(s3) != len(s1) + len(s2):
            return False

        "two pointers or dynamic programming"
        "at every position of s3, it must be either from s1 current position i, or s2 current position j. if yes, move pointer. if not, return False"
        "two pointers does not work here since when s1[p1] == s2[p2] = s3[p3], p1 p2 which one should move?"
        "for any given position m at s3, it must be from some decomposition m = i + j, s1[0:i] and s2[0:j] is interleaving string of s3[0:m]. so we can use dynamic programming. "

        "initialization"
        "for empty s1 and empty s2, True"
        mat = [[True]]

        "for empty s2, we need to compare s1 with s3 one by one"
        for i in range(1, len(s1) + 1):
            mat.append([s1[i - 1] == s3[i - 1] and mat[i - 1][0] is True])

        "for empty s1, we need to compare s2 and s3 one by one"
        for j in range(1, (len(s2) + 1)):
            mat[0].append(s2[j - 1] == s3[j - 1] and mat[0][j - 1] is True)

        for i in range(1, len(s1) + 1):
            for j in range(1, len(s2) + 1):
                "current one either equal to s1 in i-1, or equal to s2 in j-1"
                if (s3[i + j - 1] == s1[i - 1] and mat[i - 1][j] is True) or (
                        s3[i + j - 1] == s2[j - 1] and mat[i][j - 1] is True):
                    mat[i].append(True)
                else:
                    mat[i].append(False)

        return mat[-1][-1]


assert Solution().isInterleave("aab", "a", "aaab") == True
