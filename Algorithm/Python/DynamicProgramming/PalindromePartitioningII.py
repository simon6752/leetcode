# 108. Palindrome Partitioning II
# Description
# Given a string s, cut s into some substrings such that every substring is a palindrome.
# 
# Return the minimum cuts needed for a palindrome partitioning of s.
# 
# Have you met this question in a real interview?  
# Example
# Given s = "aab",
# 
# Return 1 since the palindrome partitioning ["aa", "b"] could be produced using 1 cut.


# solution 1, time limit exceeded
class Solution:
	"""
	@param s: A string
	@return: An integer
	"""
	
	def minCut (self, s):
		# write your code here
		# we can do palindrome partition I and record cuts and find minimum
		# but we can optimize it since we do not need to really cut it
		
		# dynamic programming. f[i][j] and f[i+1][j-1]
		# if s[i] == s[j] and s[i:j+1] is Palindrome, f[i][j] = f[i+1][j-1], else f[i][j] = min(1+ f[i][j-1], 1+f[i+1][j-1])
		
		if s is None or len (s) < 2:
			return 0
		
		
		# initialization
		f = []
		for i in range (0, len (s)):
			f.append ([0] * len (s))
		
		# i could be 0 to len(s) -1 , j could be i to len(s) -1
		# when i ==0 ,f[i][j] = 0. when i == j , f[i][j] = 0
		
		for i in range (len (s) - 1, -1, -1):
			for j in range (i + 1, len (s)):
				if s [i] == s [j] and self.isPalindrome(s [i:j + 1]):
					# do not need to cut
					f [i] [j] = 0
				else:
					# you do not know where to cut is best, so we need to check all
					minval = f [i] [j - 1]
					for k in range (i, j - 1):
						# i -> k -> j, cut at k
						minval = min (minval, f [i] [k] + f [k + 1] [j])
					f [i] [j] = 1 + minval
		
		return f [0] [-1]
	
	def isPalindrome (self, s):
		
		if s is None or len (s) < 2:
			return True
		
		for i in range (0, len (s) // 2):
			if s [i] != s [len (s) - i - 1]:
				return False
		
		return True

assert Solution().minCut("ababbbabbaba") == 3


# need to optimize time and space
# time O(n^3), space O(n^2)

# since we are calculate isPalindrome O(n^2) times, why not save it in storage so we can use it?


# 2nd solution, not pass all cases
class Solution2:
	"""
	@param s: A string
	@return: An integer
	"""
	
	def minCut (self, s):
		# write your code here
		# we can do palindrome partition I and record cuts and find minimum
		# but we can optimize it since we do not need to really cut it
		
		# dynamic programming. f[i][j] and f[i+1][j-1]
		# if s[i] == s[j] and s[i:j+1] is Palindrome, f[i][j] = f[i+1][j-1], else f[i][j] = min(1+ f[i][j-1], 1+f[i+1][j-1])
		
		if s is None or len (s) < 2:
			return 0
		
		
		# initialization
		f = []
		for i in range (0, len (s)):
			f.append ([0] * len (s))
		
		isPalindrome = [[False for x in range (0, len (s))] for x in range (0, len (s))]
		
		# i could be 0 to len(s) -1 , j could be i to len(s) -1
		# when i ==0 ,f[i][j] = 0. when i == j , f[i][j] = 0
		
		for i in range (len (s) - 1, -1, -1):
			for j in range (i, len (s)):
				if s [i] == s [j] and (j - i <= 2 or isPalindrome [i + 1] [j - 1]):
					isPalindrome [i] [j] = True
					# do not need to cut or cut once
					f [i] [j] = 0
				else:
					# else either s[i] != s[j] or even s[i] == s[j], s[i:j+1] is not Palindrome
					# in this case we need to cut once, it could be any position between i and j
					f [i] [j] = 1 + min (f [i + 1] [j], f [i] [j - 1])
		
		return f [0] [-1]

assert Solution2().minCut("cabababcbc") == 3


# jiuzhang solution

class Solution3:
    # @param s, a string
    # @return an integer
    def minCut(self, s):
        n = len(s)
        f = []
        p = [[False for x in range(n)] for x in range(n)]
        # the worst case is cutting by each char
        for i in range(n+1):
            f.append(n - 1 - i) # the last one, f[n]=-1
        for i in reversed(range(n)):
            for j in range(i, n):
                if (s[i] == s[j] and (j - i < 2 or p[i + 1][j - 1])):
                    p[i][j] = True
                    f[i] = min(f[i], f[j + 1] + 1)
        return f[0]
    
# TODO  write own right version

