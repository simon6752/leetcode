#  164. Unique Binary Search Trees II
# Given n, generate all structurally unique BST's (binary search trees) that store values 1...n.
#
# Example
# Given n = 3, your program should return all 5 unique BST's shown below.
#
#    1         3     3      2      1
#     \       /     /      / \      \
#      3     2     1      1   3      2
#     /     /       \                 \
#    2     1         2                 3


class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""

# DFS

class Solution:
	# @paramn n: An integer
	# @return: A list of ro
	def generateTrees (self, n):
		# write your code here
		
		# use DFS or DP
		
		# DFS
		
		# BST property and in -order traverse
		
		# [1,2,3], use 1, 2, 3 as root, use rest as left and righ child, do this recursively
		if n == 0:
			return [None]
		
		result = []
		list1 = list (range (1, n + 1))
		for i in range (0, n):
			result += self.getTree (list1 [0:i], list1 [i + 1:], list1 [i])
		
		return result
	
	def getTree (self, left, right, rootval):
		# return a BST, use left list as left child , right list as right child, return a list of root node
		
		# need to set stop criteria here
		
		root = TreeNode (rootval)
		
		lefts = []
		if len (left) != 0:
			# tree every of left as root, and left side as left child, right side as right child
			for i in range (0, len (left)):
				lefts += self.getTree (left [0:i], left [i + 1:], left [i])
		
		else:
			lefts = [None]
		
		rights = []
		
		if len (right) != 0:
			for i in range (0, len (right)):
				rights += self.getTree (right [0:i], right [i + 1:], right [i])
		
		else:
			rights = [None]
		
		# for every combination in lefts, and rights, construct the root node
		
		result = []
		
		for i in range (0, len (lefts)):
			
			for j in range (0, len (rights)):
				root = TreeNode (rootval)
				root.left = lefts [i]
				root.right = rights [j]
				result.append (root)
		
		return result


# complexity

# since we searched all possibility of a binary search tree, it is 2^n


