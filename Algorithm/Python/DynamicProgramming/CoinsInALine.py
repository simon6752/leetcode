# 394. Coins in a Line
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# There are n coins in a line. Two players take turns to take one or two coins from right side until there are no more coins left. The player who take the last coin wins.
#
# Could you please decide the first play will win or lose?
#
# Have you met this question in a real interview?
# Example
# n = 1, return true.
#
# n = 2, return true.
#
# n = 3, return false.
#
# n = 4, return true.
#
# n = 5, return true.
#
# Challenge
# O(n) time and O(1) memory
#
# Tags
# Dynamic Programming Array Greedy Game Theory

class Solution:
    """
    @param n: An integer
    @return: A boolean which equals to true if the first player will win
    """

    def firstWillWin(self, n):
        # write your code here
        "a[i] could be +1 or -1"
        ""
        win1 = True
        win2 = True

        if n == 0:
            return False

        if n <= 2:
            return True

        for i in range(2, n):
            "if there is 1 or 2 coins left and it is the first one's turn, he will win"
            "for i coin, we can pick 1 or 2 coin, so make the other player with i-1 and i-2 coin as the first player "
            if win1 is True and win2 is True:
                win3 = False
            else:
                win3 = True

            win1 = win2
            win2 = win3

        return win3
