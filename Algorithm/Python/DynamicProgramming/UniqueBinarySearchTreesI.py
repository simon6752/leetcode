# 163. Unique Binary Search Trees
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given n, how many structurally unique BSTs (binary search trees) that store values 1...n?
#
# Have you met this question in a real interview?
# Example
# Given n = 3, there are a total of 5 unique BST's.
#
# 1           3    3       2      1
#  \         /    /       / \      \
#   3      2     1       1   3      2
#  /      /       \                  \
# 2     1          2                  3
# Tags
# Dynamic Programming Catalan Number Snapchat

class Solution:
    """
    @param n: An integer
    @return: An integer
    """

    def numTrees(self, n):
        # write your code here

        "in order is [1,2,3,...n], we need to find the location of left and right tree in this in order traversal to get a unique BST"

        "first find root, then left side and right side is the BST in order traversal. do it recursively"

        if n == 0 or n == 1:
            return 1

        ways = [0] * (n + 1)
        ways[1] = 1
        ways[0] = 1

        for i in range(2, n + 1):
            "assume the length of tree is i"
            for j in range(1, i + 1):
                "we pick jth as root, (1,...,j-1) as left tree,(j+1,...,i) as right tree"
                ways[i] = ways[i] + ways[j - 1] * ways[i - j]

        return ways[-1]
