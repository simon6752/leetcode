# 667. Longest Palindromic Subsequence
# Description
# Notes
# Testcase
# Judge
# Discuss
# Given a string s, find the longest palindromic subsequence's length in s. You may assume that the maximum length of s is 1000.
#
# Have you met this question in a real interview?
# Example
# Given s = "bbbab" return 4
# One possible longest palindromic subsequence is "bbbb".
#
# Tags
# Amazon Dynamic Programming Uber


class Solution:
    """
    @param s: the maximum length of s is 1000
    @return: the longest palindromic subsequence's length
    """

    def longestPalindromeSubseq(self, s):
        # write your code here
        " a[i][j] is the longest parlindrom subsequence start at i, ends with j"
        " for a[i][j], we need to find all positions that are the same with s[j]. a[i][j] = max(a[k+1][j-1])+2, where s[k] == s[j] && k >=i. if s[k] !=s[j], then max(a[i+1][j], a[i][j-1])"
        "every palindromic subsequence must be composed of lots of smaller palindromic subsequence"

        if s is None or len(s) == 0:
            return 0

        "initialization"
        a = []

        for i in range(0, len(s)):
            a.append([])

        "decomposition"
        "start i from lower left corner to upper right corner"
        for i in range(len(s) - 1, -1, -1):
            for j in range(0, len(s)):
                if j < i:
                    a[i].append(0)
                elif j == i:
                    a[i].append(1)
                else:
                    "j > i"
                    if s[j] == s[i]:
                        "we only need to check inner region"
                        a[i].append(a[i + 1][j - 1] + 2)
                    else:
                        "we need to check both end"
                        a[i].append(max(a[i + 1][j], a[i][j - 1]))

        "upper right corner is the anwser"
        return a[0][-1]
