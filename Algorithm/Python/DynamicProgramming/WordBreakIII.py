#  683. Word Break III
# Description
# Give a dictionary of words and a sentence with all whitespace removed, return the number of sentences you can form by inserting whitespaces to the sentence so that each word can be found in the dictionary.
#
# Ignore case
#
# Have you met this question in a real interview?
# Example
# Given a String CatMat
# Given a dictionary ["Cat", "Mat", "Ca", "tM", "at", "C", "Dog", "og", "Do"]
# return 3
#
# we can form 3 sentences, as follows:
# CatMat = Cat Mat
# CatMat = Ca tM at
# CatMat = C at Mat

# my solution 1 consider case

class Solution:
	"""
	@param: : A string
	@param: : A set of word
	@return: the number of possible sentences.
	"""
	
	def wordBreak3 (self, s, dict):
		# Write your code here
		
		# backtracking or DP
		
		# since there is duplicate calculation when use backtracking, we need dp to reduce duplicate calculation
		
		# also the result is number of sentences, not all the possible sentences, so DP
		
		# assume f[j] is the number of ways when word is s[0:j+1], so f[j] = sum of (f[i]), where i <=j and s[i:j+1] is in dictionary
		# we need a matrix to record whether s[i:j+1] is in dictionary
		
		# CatMat = C + ("atMat"), Ca + ("tMat"), Cat + ("Mat")
		
		if s is None or len (s) == 0:
			return 0
		
		if dict is None or len (dict) == 0:
			return 0
		
		# initialization
		
		f = []
		
		for i in range (0, len (s)):
			f.append (0)
		
		# decomposition
		
		for j in range (0, len (s)):
			# i is the from 0 to j
			for i in range (-1, j):
				
				if s [i + 1:j + 1] in dict:
					if i != -1:
						f [j] += f [i]
					else:
						f [j] += 1
		
		return f [-1]
	
assert Solution().wordBreak3("CatMat",["Cat", "Mat", "Ca", "tM", "at", "C", "Dog", "og", "Do"]) == 3

assert Solution().wordBreak3("Catmat",["Cat","mat","ca","tm","at","C","Dog","og","Do"]) == 3

# solution 2 ignore case
class Solution2:
	"""
	@param: : A string
	@param: : A set of word
	@return: the number of possible sentences.
	"""
	
	def wordBreak3 (self, s, dict):
		# Write your code here
		
		# backtracking or DP
		
		# since there is duplicate calculation when use backtracking, we need dp to reduce duplicate calculation
		
		# also the result is number of sentences, not all the possible sentences, so DP
		
		# assume f[j] is the number of ways when word is s[0:j+1], so f[j] = sum of (f[i]), where i <=j and s[i:j+1] is in dictionary
		# we need a matrix to record whether s[i:j+1] is in dictionary
		
		# CatMat = C + ("atMat"), Ca + ("tMat"), Cat + ("Mat")
		
		# need to update it to ignore case
		
		if s is None or len (s) == 0:
			return 0
		
		if dict is None or len (dict) == 0:
			return 0
		
		# convert dict to lower
		
		dict2 = set ()
		for key in dict:
			dict2.add (key.lower ())
		
		# initialization
		
		f = []
		
		for i in range (0, len (s)):
			f.append (0)
		
		# decomposition
		
		for j in range (0, len (s)):
			# i is the from 0 to j
			for i in range (-1, j):
				
				if s [i + 1:j + 1].lower () in dict2:
					if i != -1:
						f [j] += f [i]
					else:
						f [j] += 1
		
		return f [-1]