# 107. Word Break
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a string s and a dictionary of words dict, determine if s can be break into a space-separated sequence of one or more dictionary words.
#
# Have you met this question in a real interview?
# Example
# Given s = "lintcode", dict = ["lint", "code"].
#
# Return true because "lintcode" can be break as "lint code".
#
# Tags
# String Dynamic Programming Facebook Amazon Bloomberg Uber Yahoo Google Pocket Gems Coupang Square

class Solution:
    """
    @param: s: A string
    @param: dict: A dictionary of words dict
    @return: A boolean
    """

    def wordBreak(self, s, dict):
        # write your code here

        "use dynamic programming, O(n^2). a[i] = s[i] - s[m] is in dict and a[m] can be break "

        if s is None or len(s) == 0:
            return True

        if len(s) == 1:
            return s[0] in dict

        if len(dict) == 0:
            return False

        can = [False] * len(s)

        "get max length of word in dict"
        maxl = 0
        for j in dict:
            maxl = max(maxl, len(j))

        can[0] = s[0] in dict

        for i in range(1, len(s)):
            "count from i to i - maxl+1, is there any word match the word in dict"
            for j in range(0, min(maxl, i + 1)):
                "test i -> i - j, index is [i-j:i+1] or [0:i+1]"
                if s[i - j:i + 1] in dict and ((i - j) == 0 or (i - j > 0 and can[i - j - 1] is True)):
                    can[i] = True

        return can[-1]


assert Solution().wordBreak("aaab", ["aa", "ab"]) == True
assert Solution().wordBreak("bb", ["a", "b", "bbb", "bbbb"]) == True
