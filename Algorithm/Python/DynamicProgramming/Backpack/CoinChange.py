# 669. Coin Change
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# You are given coins of different denominations and a total amount of money amount. Write a function to compute the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination of the coins, return -1.
#
#  Notice
# You may assume that you have an infinite number of each kind of coin.
#
# Have you met this question in a real interview?
# Example
# Given coins = [1, 2, 5], amount = 11
# return 3 (11 = 5 + 5 + 1)
#
# Given coins = [2], amount = 3
# return -1.
#
# Tags
# Dynamic Programming Backpack

class Solution:
    """
    @param coins: a list of integer
    @param amount: a total amount of money amount
    @return: the fewest number of coins that you need to make up
    """

    def coinChange(self, coins, amount):
        # write your code here

        "amount/coints[i], then try every possibility"

        "f[n] = min(f[n-coins[j]])"

        ways = []
        "sort coint amount from small to large"

        coins = sorted(coins)
        "[5,1,2] to [1,2,5]"

        ways.append(0)

        for i in range(1, amount + 1):
            ways.append(-1)
            "try i minus coins[j], from largest coin"
            for j in range(len(coins) - 1, -1, -1):
                if i - coins[j] >= 0 and ways[i - coins[j]] >= 0:
                    if ways[i] > 0:
                        "choose current coin"
                        ways[i] = min(ways[i], ways[i - coins[j]] + 1)
                    else:
                        ways[i] = ways[i - coins[j]] + 1

        return ways[-1]


assert Solution().coinChange([5, 2, 1], 11) == 3
