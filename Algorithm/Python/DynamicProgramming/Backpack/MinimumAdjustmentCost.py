# 91. Minimum Adjustment Cost
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given an integer array, adjust each integers so that the difference of every adjacent integers are not greater than a given number target.
#
# If the array before adjustment is A, the array after adjustment is B, you should minimize the sum of |A[i]-B[i]|
#
#  Notice
# You can assume each number in the array is a positive integer and not greater than 100.
#
# Have you met this question in a real interview?
# Example
# Given [1,4,2,3] and target = 1, one of the solutions is [2,3,2,3], the adjustment cost is 2 and it's minimal.
#
# Return 2.
#
# Tags
# Dynamic Programming Backpack LintCode Copyright
import sys


# wrong, list index out of range
class Solution:
    """
    @param: A: An integer array
    @param: target: An integer
    @return: An integer
    """

    def MinAdjustmentCost(self, A, target):
        # write your code here

        "from start to end, adjust each integer. when adjust A[i], we can adjust A[i-1] + target to A[i-1] - target. so total 2*target + 1 choice, the cost we spend is A[i] - A[i-1] - target to A[i] - A[i-1] + target"

        "think it this way, every time we try to adjust A[i-1] to in range target of A[i], we bring up or down of A[i-1] and this will also affect all previous element. so it is multiplied by i-1"

        "dynamic programming, the matrix size is len(A) + 1 * (2 * target + 1)"

        " do we need to remmeber current value after adjustment??"

        if A is None or len(A) == 0 or target <= 0:
            return 0

        mat = []

        n = len(A)

        "initialization"
        for i in range(0, n + 1):
            mat.append([0])
            for j in range(0, n + 1):
                mat[i].append(0)

        "consider i, j. The last element is A[i], adjustment to A[i-1] is A[i] + j, cost is A[i-1] - (A[i] + j)"

        " or we consider i, j, current array length is i, current last adjust position is j. No!"

        for i in range(1, n + 1):

            for j in range(1, i + 1):
                "this step, adjust cost is (j)*abs(abs(A[j] - A[j+1]) - target)"
                "we need to calculate cost carefully"

                "A[j] to A[j+1} +/- target, minimum cost of A[j] move to this range"

                mat[i][j] = mat[i][j - 1] + j * (min(A[j] - (A[j + 1] + target), A[j] - (A[j + 1] - target)))

                " in this way, we fixed last element A[i] actually. we may need to move whole array back to balance a little bit"

        "we need to find minimum cost, it is minimum of last row"

        minx = mat[-1][-1]

        for i in range(1, n + 1):
            minx = min(minx, mat[-1][i])

        return minx


assert Solution().MinAdjustmentCost([1, 4, 2, 3], 1) == 2


# /*
#      * SOLUTION 4：
#      * DP
#      * */
#     /**
#      * @param A: An integer array.
#      * @param target: An integer.
#      */

class Solution2:
    def MinAdjustmentCost(self,A, target):
        # // write your code here
        if (A == None or len(A) == 0):
            return 0

            # // D[i][v]: 把index = i的值修改为v，所需要的最小花费
        D = [  ]

        for i in range(0,len(A)):
            D.append([0])
            for j in range(0,100):
                D[i].append(0)

        size = len(A)

        for i in range(0, size):
            for j in range(1, 101):
                D[i][j] = sys.maxsize
                if (i == 0):
                    # // The first element.
                    D[i][j] = abs(j - A[i])
                else:
                    for k in range(1, 101):
                        # // 不符合条件
                        if (abs(j - k) > target):
                            pass

                        dif = abs(j - A[i]) + D[i - 1][k]
                        D[i][j] = min(D[i][j], dif)

        ret = sys.maxsize
        for i in range(1, 101):
            ret = min(ret, D[size - 1][i])

        return ret

assert Solution2().MinAdjustmentCost([1, 4, 2, 3], 1) == 2

# TODO need to understand and fix