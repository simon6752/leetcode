# 700. Cutting a Rod
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a rod of length n inches and an array of prices that contains prices of all pieces of size smaller than n. Determine the maximum value obtainable by cutting up the rod and selling the pieces. For example, if length of the rod is 8 and the values of different pieces are given as following, then the maximum obtainable value is 22 (by cutting in two pieces of lengths 2 and 6)
#
# Have you met this question in a real interview?
# Example
# length   | 1   2   3   4   5   6   7   8
# --------------------------------------------
# price    | 1   5   8   9  10  17  17  20
# Given price = [1, 5, 8, 9, 10, 17, 17, 20], n = 8
# Return 22 // by cutting in two pieces of lengths 2 and 6
#
# length   | 1   2   3   4   5   6   7   8
# --------------------------------------------
# price    | 3   5   8   9  10  17  17  20
# Given price = [3, 5, 8, 9, 10, 17, 17, 20], n = 8
# Return 24 // by cutting in eight pieces of length 1
#
# Tags
# Dynamic Programming Backpack


class Solution:
    """
    @param prices: the prices
    @param n: the length of rod
    @return: the max value
    """
    def cutting(self, prices, n):
        # Write your code here
        "backpacking, backpack size is n, different item(size) has different value"

        cut = [0]

        for i in range(1 , n +1):
            # cut[0].append(0)
            "for each length, default value is keep it as a whole"
            cut.append(prices[ i -1])

        for i in range(1 , n +1):
            for j in range(1 , i +1):
                "cut length is j, j is from 1 to i, rest length is i-j"
                "at each step, we can choose to cut or not"
                cut[i] = max(cut[i] ,cut[ i -j ]+ prices[ j -1])

        return cut[-1]