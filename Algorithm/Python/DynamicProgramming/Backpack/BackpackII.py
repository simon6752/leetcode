# 125. Backpack II
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given n items with size Ai and value Vi, and a backpack with size m. What's the maximum value can you put into the backpack?
#
#  Notice
# You cannot divide item into small pieces and the total size of items you choose should smaller or equal to m.
#
# Have you met this question in a real interview?
# Example
# Given 4 items with size [2, 3, 5, 7] and value [1, 5, 2, 4], and a backpack with size 10. The maximum value is 9.
#
# Challenge
# O(n x m) memory is acceptable, can you do it in O(m) memory?
#
# Tags
# Dynamic Programming Backpack LintCode Copyright

class Solution:
    """
    @param m: An integer m denotes the size of a backpack
    @param A: Given n items with size A[i]
    @param V: Given n items with value V[i]
    @return: The maximum value
    """

    def backPackII(self, m, A, V):
        # write your code here
        "in backpack I, value is the weight itself. Now value is given. Restriction on weight"

        "\sum w[i]x[i] < m, need to maximize \sum v[i]x[i]"

        if A is None or len(A) == 0:
            return 0

        "if pack size m is larger than all size of items, take all"
        if m > sum(A):
            return sum(V)

        "need a (m+1)*(n+1) matrix"

        n = len(A)

        pack = [[0]]

        for i in range(1, m + 1):
            pack.append([0])

        for i in range(0, m + 1):
            for j in range(1, n + 1):
                pack[i].append(0)

        "decomposition"

        for i in range(1, n + 1):
            for j in range(1, m + 1):
                "if we can put current item into backpack"
                if j >= A[i - 1]:
                    pack[j][i] = (max(pack[j][i - 1], pack[j - A[i - 1]][i - 1] + V[i - 1]))
                else:
                    pack[j][i] = (pack[j][i - 1])

        "return maximum"
        return pack[-1][-1]


assert Solution().backPackII(100,[77,22,29,50,99],[92,22,87,46,90]) == 133
assert Solution().backPackII(1000,[3,68,24,80,76,9,24,2,46,75,56,41,95,46,23,34,64,76,6,48,25,73,87,67,58,7,93,66,55,75,38,27,53,6,100,36,26,17,53,88,21,9,34,90,32,47,4,6,57,50,30,25,41,24,12,74,92,17,32,96,35,76,52,93,64,55,1,70,26,35,2,97,82,22,41,37,63,28,90,13,18,55,28,58,59,74,71,32,71,66,4,5,48,52,70,62,28,36,39,48] ,[38,16,29,47,22,25,17,49,15,15,75,11,56,99,51,92,59,37,13,98,61,50,32,17,44,79,41,53,45,29,62,64,2,23,31,45,57,68,57,26,51,26,86,83,94,20,98,24,91,89,1,63,21,46,74,56,64,72,58,8,74,24,27,35,94,49,65,21,16,25,1,45,63,4,37,25,39,68,49,11,31,95,5,79,20,21,52,50,8,19,67,21,24,89,28,88,38,96,64,84]) == 2558



class Solution2:
    """
    @param m: An integer m denotes the size of a backpack
    @param A: Given n items with size A[i]
    @param V: Given n items with value V[i]
    @return: The maximum value
    """
    def backPackII(self, m, A, V):
        # write your code here
        f = [0 for i in range(m+1)]
        n = len(A)
        for i in range(n):
            for j in range(m, A[i]-1, -1):
                f[j] = max(f[j] , f[j-A[i]] + V[i])
        return f[m]

assert Solution2().backPackII(100,[77,22,29,50,99],[92,22,87,46,90]) == 133
assert Solution2().backPackII(1000,[3,68,24,80,76,9,24,2,46,75,56,41,95,46,23,34,64,76,6,48,25,73,87,67,58,7,93,66,55,75,38,27,53,6,100,36,26,17,53,88,21,9,34,90,32,47,4,6,57,50,30,25,41,24,12,74,92,17,32,96,35,76,52,93,64,55,1,70,26,35,2,97,82,22,41,37,63,28,90,13,18,55,28,58,59,74,71,32,71,66,4,5,48,52,70,62,28,36,39,48] ,[38,16,29,47,22,25,17,49,15,15,75,11,56,99,51,92,59,37,13,98,61,50,32,17,44,79,41,53,45,29,62,64,2,23,31,45,57,68,57,26,51,26,86,83,94,20,98,24,91,89,1,63,21,46,74,56,64,72,58,8,74,24,27,35,94,49,65,21,16,25,1,45,63,4,37,25,39,68,49,11,31,95,5,79,20,21,52,50,8,19,67,21,24,89,28,88,38,96,64,84]) == 2558
