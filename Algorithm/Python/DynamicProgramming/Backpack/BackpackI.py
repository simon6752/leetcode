# 92. Backpack
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given n items with size Ai, an integer m denotes the size of a backpack. How full you can fill this backpack?
#
#  Notice
# You can not divide any item into small pieces.
#
# Have you met this question in a real interview?
# Example
# If we have 4 items with size [2, 3, 5, 7], the backpack size is 11, we can select [2, 3, 5], so that the max size we can fill this backpack is 10. If the backpack size is 12. we can select [2, 3, 7] so that we can fulfill the backpack.
#
# You function should return the max size we can fill in the given backpack.
#
# Challenge
# O(n x m) time and O(m) memory.
#
# O(n x m) memory is also acceptable if you do not know how to optimize memory.
#
# Tags
# Dynamic Programming Backpack LintCode Copyright


class Solution:
    """
    @param m: An integer m denotes the size of a backpack
    @param A: Given n items with size A[i]
    @return: The maximum size
    """

    def backPack(self, m, A):
        # write your code here
        if A is None or len(A) == 0 or m == 0:
            return 0
        "sorted it asc"
        A = sorted(A)
        n = len(A)

        "add short cut when backpack is larger than total item size"
        if m >= sum(A):
            return sum(A)

        "we need to maximize backpack with total weight"
        "assume largest can be put is A[j]"

        "initialization"
        backpack = []

        for i in range(0, m + 1):
            backpack.append([0])
        for i in range(0,n+1):
            backpack[0].append(0)

        "backpack[i][j] means backpack size i, item A[0:j], best choice if our largest item is A[j-1]"
        "decomposition"
        for i in range(1, m + 1):
            "test item with largest size"
            maxrow = 0
            for j in range(1, n + 1):
                "j from 1:n : A[0] to A[n-1]"
                if i >= A[j - 1]:
                    "backpack larger than largest item, either pick up A[j], backpack size change to i - A[j-1], maximum item size is A[j-1], or we choose current row max"
                    maxrow = max(maxrow, backpack[i][j-1 ])
                    backpack[i].append(max(maxrow, backpack[i - A[j-1 ]][j-1 ] + A[j-1 ]))
                else:
                    "current maximum A[j-1] is larger than backpack size, we choose backpack[i][j-1]"
                    backpack[i].append(backpack[i][j-1])

        "we should return the max of last row since the maximum backpack solution may not have the largest size item"

        maxbackpack = 0
        for i in range(1, m + 1):
            maxbackpack = max(maxbackpack, backpack[i][-1])

        return maxbackpack

assert Solution().backPack(100, [16,16,3,5,16,16,16,16,16,16,16,16,16,16,16,16,16]) == 99
"need to optimize memory from O(m*n) to O(m) memory for this test case"
# assert Solution().backPack(80000,[81,112,609,341,164,601,97,709,944,828,627,730,460,523,643,901,602,508,401,442,738,443,555,471,97,644,184,964,418,492,920,897,99,711,916,178,189,202,72,692,86,716,588,297,512,605,209,100,107,938,246,251,921,767,825,133,465,224,807,455,179,436,201,842,325,694,132,891,973,107,284,203,272,538,137,248,329,234,175,108,745,708,453,101,823,937,639,485,524,660,873,367,153,191,756,162,50,267,166,996,552,675,383,615,985,339,868,393,178,932]) == 52741

"will backpack solution exeeds the size of backpack? let's test"
assert Solution().backPack(9000, [988,417,92,268,313,293,530,134,311,918,355,826,94,580,793,731,320,101,612,410,640,393,278,660,842,543,130,793,407,797,176,685,521,776,473,756,597,376,615,547,310,579,177,450,842,677,640,687,515,178,583,271,161,401,595,354,868,773,74,178,626,192,747,716,148,499,654,584,886,127,171,121,563,222,802,818,546,230,50,470,134,689,276,948,261,794,680,674,444,580,313,125,473,940,888,899,75,243,792,568,173,872,376,513,719,302,96,369,163,314,61,58,181,262,85,432,695,728,759,969,305,826,345,388,79,953,838,648,692,240,645,675,352,257,711,536,272,779,99,332,909,175,711,561,170,390,814,820,383,690,460,664,395,831,420,876,413,824,605,812,581,343,518,213,236,957,273,707,561,922,681,985,282,493,299,261,382,560,263,361,936,746,224,509,578,149,993,989,730,354,587,662,184,924,70,422,344,794,459,277,286,572,970,947,665,287,675,239,208,596,907,685,714,544,90,706,305,538,558,182,798,924,211,245,613,416,63,514,830,641,421,858,238,968,77,612,864,524,988,302,332,402,751,421,992,94,836,953,445,600,197,894,195,630,792,762,844,993,80,186,471,725,265,132,939,509,653,347,837,877,601,941,418,784,677,351,673,734,790,465,965,600,267,478,868,927,825,697,400,116,708,520,792,129,448,464,336,570,888,100,352,300,717,65,999,673,595,376,480,782,929,288,810,400,546,78,991,566,494,904,461,110,741,757,743,351,570,713,615,589,838,433,970,895,767,862,314,601,205,316,831,912,453,701,919,778,843,891,794,612,287,403,358,812,336,455,114,758,116,456,467,895,108,273,887,561,586,841,858,548,352,216,425,537,990,305,568,138,985,274,436,483,749,693,817,789,325,667,481,723,725,742,420,851,666,849,893,169,911,320,696,241,108,633,877,343,898,690,986,721,819,160,238,780,185,143,473,208,506,829,747,640,407,734,84,724,873,492,798,839,161,141,696,571,688,866,111,795,651,983,664,748,875,814,710,556,219,72,462,948,89,114,497,176,68,409,826,181,118,781,394,123,644,149,665,739,381,912,256,693,752,810,697,50,739,315,219,930,411,453,362,845,609,485,461,920,290,815,283,778,635,787,154,987,404,60,414,462,919,864,356,124,169,133,586,242,302,428,68,517,576,300,432,203,228,932,657,916,997,852,197,625,157,603,475,937,215,710,234,645,281,569,645,912,390,361,853,661,981,290,303,303,342,215,716,136,293,579,555,197,299,969,619,279,696,820,374,748,521,788,482,538,896,600,574,189,318,857,125,752,190,104,987,617,531,929,602,423,242,328,461,780,409,474,903,675,388,625,80,230,500,653,546,990,138,576,888,481,402,487,948,226,441,431,145,765,222,933,276,182,769,228,91,935,728,385,543,466,415,574,227,319,572,752,759,239,724,956,709,861,512,157,294,950,265,741,373,498,767,1000,957,168,149,668,811,573,658,971,711,662,352,473,404,868,750,793,614,532,853,620,918,283,301,547,221,486,164,664,275,287,360,79,680,59,461,874,283,438,512,358,378,559,378,681,841,479,454,513,419,310]) == 9000

"optimize memory from n x m to m"
class Solution2:
    """
    @param m: An integer m denotes the size of a backpack
    @param A: Given n items with size A[i]
    @return: The maximum size
    """

    def backPack(self, m, A):
        # write your code here
        if A is None or len(A) == 0 or m == 0:
            return 0
        "sorted it asc"
        A = sorted(A)
        n = len(A)

        "add short cut when backpack is larger than total item size"
        if m >= sum(A):
            return sum(A)

        "we need to maximize backpack with total weight"
        "assume largest can be put is A[j]"

        "initialization"
        backpack = []

        for i in range(0, m + 1):
            backpack.append(0)

        "backpack[i][j] means backpack size j, item A[0:i]"
        "decomposition"
        for i in range(1, n + 1):
            "test item with largest size first, current values for all < j is still for last i-1!"
            for j in range(m, A[i-1]-1, -1):
                "j from m:A[i-1] : A[0] to A[n-1]"
                "backpack larger than largest item, either pick up A[j], backpack size change to i - A[j-1], maximum item size is A[j-1], or we choose current row max"
                backpack[j] = max(backpack[j], backpack[j - A[i - 1]] + A[i - 1])

        return backpack[-1]

assert Solution2().backPack(10, [3,4,8,5]) == 9
assert Solution2().backPack(100, [16,16,3,5,16,16,16,16,16,16,16,16,16,16,16,16,16]) == 99
"need to optimize memory from O(m*n) to O(m) memory for this test case"
assert Solution2().backPack(80000,[81,112,609,341,164,601,97,709,944,828,627,730,460,523,643,901,602,508,401,442,738,443,555,471,97,644,184,964,418,492,920,897,99,711,916,178,189,202,72,692,86,716,588,297,512,605,209,100,107,938,246,251,921,767,825,133,465,224,807,455,179,436,201,842,325,694,132,891,973,107,284,203,272,538,137,248,329,234,175,108,745,708,453,101,823,937,639,485,524,660,873,367,153,191,756,162,50,267,166,996,552,675,383,615,985,339,868,393,178,932]) == 52741


class Solution3:
    """
    @param m: An integer m denotes the size of a backpack
    @param A: Given n items with size A[i]
    @return: The maximum size
    """

    def backPack(self, m, A):
        # write your code here
        if A is None or len(A) == 0 or m == 0:
            return 0

        "add short cut when backpack is larger than total item size"
        if m >= sum(A):
            return sum(A)

        "no need to sorted it asc"
        n = len(A)

        maxi = [0]

        for i in range(1, m + 1):
            maxi.append(0)

        for j in range(0, n):
            for i in range(m, A[j] - 1, -1):
                "test for each pack size i with A[0:j]"
                "test from m -> A[j] -1 so all previous pack size,values are still for j-1"
                maxi[i] = max(maxi[i], maxi[i - A[j]] + A[j])
                if maxi[i] == m:
                    "if we already can find a way to fill the backpack,return m"
                    return m

        return maxi[-1]