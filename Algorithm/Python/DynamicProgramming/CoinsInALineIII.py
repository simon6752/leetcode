#  396. Coins in a Line III
# Description
# There are n coins in a line. Two players take turns to take a coin from one of the ends of the line until there are no more coins left. The player with the larger amount of money wins.
#
# Could you please decide the first player will win or lose?
#
# Have you met this question in a real interview?
# Example
# Given array A = [3,2,2], return true.
#
# Given array A = [1,2,4], return true.
#
# Given array A = [1,20,4], return false.
#
# Challenge
# Follow Up Question:
#
# If n is even. Is there any hacky algorithm that can decide whether first player will win or lose in O(1) memory and O(n) time?

class Solution:
	"""
	@param values: a vector of integers
	@return: a boolean which equals to true if the first player will win
	"""
	
	def firstWillWin (self, values):
		# write your code here
		
		# lets say, at i, step, we already have i-1 value taken, values[k:k+n-i+1], we need to decide to take left or right end
		# either take values[k], or values[k+n-i], then
		# f[i][j] means when facing A[i:j+1], the maximum 1st player can get
		# t[i][j] is total value A[i:j+1]
		# f[i][j] =   max(t[i][j]-f[i+1][j], t[i][j]-f[i][j-1])
		
		
		if values is None or len (values) < 3:
			return True
		
		n = len (values)
		
		# n rows, n cols
		
		t = []
		f = []
		# initialization
		
		for i in range (n):
			t.append ([])
			f.append ([])
			for j in range (n):
				t [i].append (0)
				f [i].append (0)
		
		for i in range (n - 1, -1, -1):
			for j in range (i, n):
				# for j >= i only
				if j == i:
					t [i] [j] = values [i]
					f [i] [j] = values [i]
				else:
					
					t [i] [j] = t [i] [j - 1] + values [j]
		
		# since f[i][j] depends on left one and lower one, we need to go up and go right
		# which means i decrease, j increases
		
		for i in range (n - 2, -1, -1):
			
			for j in range (i + 1, n):
				
				# sol 2
				
				f [i] [j] = max (t [i + 1] [j] - f [i + 1] [j] + values [i], t [i] [j - 1] - f [i] [j - 1] + values [j])
				
				# # sol 1
				#
				# if t [i] [j] - f [i + 1] [j] > t [i] [j] - f [i] [j - 1]:
				# 	# going down has larger value, choose left one
				# 	f [i] [j] = t [i] [j] - f [i + 1] [j] + values [i]
				# else:
				# 	# going left has larger value, choose right one
				# 	f [i] [j] = t [i] [j] - f [i] [j - 1] + values [j]
		
		# we should check f[0][n-1] and t[0][n-1] - f[0][n-1]
		
		return f [0] [n - 1] > t [0] [n - 1] - f [0] [n - 1]


assert Solution().firstWillWin([1,20,4]) is False