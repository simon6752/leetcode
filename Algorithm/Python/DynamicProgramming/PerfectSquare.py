# 513. Perfect Squares
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a positive integer n, find the least number of perfect square numbers (for example, 1, 4, 9, 16, ...) which sum to n.
#
# Have you met this question in a real interview?
# Example
# Given n = 12, return 3 because 12 = 4 + 4 + 4
# Given n = 13, return 2 because 13 = 4 + 9
#
# Tags
# Mathematics Dynamic Programming Google
import sys


class Solution:
    """
    @param n: a positive integer
    @return: An integer
    """

    def numSquares(self, n):
        # write your code here

        """
        "for n, first find nearest square, that is sqrt(n) n = sqrt(n)*sqrt(n) + sqrt(n-sqrt(n)*sqrt(n)) -..."
        " for 1 2  3  4  5  6  7  8 9   10 11 12 13 14 15 16 17 ,  we got 1 1 1 2 2 2 2 2 3 3 3 3 3 3 3 4 4 "
        "square: 1, 4, 9 , 16 ,25, 36, 49,...9 - 1 = 4 + 4, 16 -1 = 9 + 4 + 1 + 1, 25 -1 = 16 + 4 + 4,..."
        1   = 1                     1
        2   = 1 + 1                 2
        3   = 1 + 1 + 1             3
        4   = 2 ^2                  1
        5   = 2^2 + 1               2
        6   = 2^2 + 1 + 1           3
        7   = 2^2 + 1 + 1 + 1       4
        8   = 2^2 + 2^2             2
        9   = 3 ^ 3                 1
        10  = 3^3 + 1               2
        11  = 3^3 + 1 + 1           3
        12  = 3^3 + 1 + 1 + 1       4 ?? no  2^2 + 2^2 + 2^2   3
        13  = 3^3 + 2^2             2"

        "ok, find it. n = either largest Sqrt(n)^Sqrt(n) + numSquares(n- Sqrt(n)*Sqrt(n)), or 2nd largest Sqrt(m), n = Sqrt(m) * Sqrt(m) + numSquares(n-Sqrt(m)*Sqrt(m))"

        """

        if n == 0 or n == 1:
            return 1

        if n == 2:
            return 2
        if n == 3:
            return 3

        result = [0, 1, 2, 3]
        square = [0, 1, 1, 1]
        for i in range(4, n + 1):
            result.append(0)
            if (square[i - 1] + 1) * (square[i - 1] + 1) == i:
                square.append(square[i - 1] + 1)
            else:
                square.append(square[i - 1])

        for i in range(4, n + 1):
            root = square[i]
            "root is largest one we need to consider"
            "we are sure that result[i] must have :  result[i] = a^2 + b ^2 + c^2 + ... a, b, c must be less than root"
            "only thing we are sure is result[i] = a^2 + result[i-a*a], a < root of i"
            "double check from root -> 1, which one has min result we need"
            minx = sys.maxsize
            for j in range(root, 0, -1):
                minx = min(minx, 1 + result[i - j * j])

            result[i] = minx

        "use dynamic programming to remove redundant calculation in recursion"

        return result[-1]

assert Solution().numSquares(1684 ) == 2
