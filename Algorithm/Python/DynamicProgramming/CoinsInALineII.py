# 395. Coins in a Line II
# Description
# There are n coins with different value in a line. Two players take turns to take one or two coins from left side until there are no more coins left. The player who take the coins with the most value wins.
#
# Could you please decide the first player will win or lose?
#
# Have you met this question in a real interview?
# Example
# Given values array A = [1,2,2], return true.
#
# Given A = [1,2,4], return false.


# solution 1, DP, time limit exceeded


class Solution:
	"""
	@param values: a vector of integers
	@return: a boolean which equals to true if the first player will win
	"""
	
	def firstWillWin (self, values):
		# write your code here
		
		# dynamic programming
		
		# since first player has the priority, he can choose a better strategy for him
		
		if values is None or len (values) < 2:
			return True
		
		total = sum (values)
		
		# player one either take 1st, or 1st and 2nd
		
		value1 = self.maxVal (values [1:])
		value2 = self.maxVal (values [2:])
		if total - value1 > value1 or total - value2 > value2:
			# if either case, palyer 1 can win
			return True
		else:
			return False
	
	def maxVal (self, values):
		# the maximum value the 1st palyer can get
		
		# the 1st player can take 1 or 2, so he choose maximum value in one of these two cases
		
		if len (values) < 2:
			# take all
			return sum (values)
		
		else:
			total = sum (values)
			value1 = self.maxVal (values [1:])
			value2 = self.maxVal (values [2:])
			return max (total - value1, total - value2)
		
# compexity

# recursion is used, might be 2 ^n, need to reuse previous result, reduce duplicate calculation


class Solution2:
	"""
	@param values: a vector of integers
	@return: a boolean which equals to true if the first player will win
	"""
	
	def firstWillWin (self, values):
		# write your code here
		
		# dynamic programming
		
		# since first player has the priority, he can choose a better strategy for him
		
		if values is None or len (values) < 2:
			return True
		
		total = sum (values)
		
		# player one either take 1st, or 1st and 2nd
		
		maxval = []
		
		for i in range (0, len (values)):
			maxval.append (0)
		
		currentsum = 0
		for i in range (len (values) - 1, -1, -1):
			# from end to start, every time, we calculate maximum value palyer 1 can get from this point
			currentsum += values [i]
			if i == len (values) - 1:
				maxval [i] = values [-1]
			elif i == len (values) - 2:
				maxval [i] = values [-1] + values [-2]
			else:
				# player one has the right to take 1 or 2 from index i
				maxval [i] = max (currentsum - maxval [i + 1], currentsum - maxval [i + 2])
		
		value1 = maxval [0]
		if value1 > total - value1:
			# if either case, player 1 can win
			return True
		else:
			return False



# complexity
# time O(n), space, O(n)