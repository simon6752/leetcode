#   517. Ugly Number
# Description
# Write a program to check whether a given number is an ugly number`.
#
# Ugly numbers are positive numbers whose prime factors only include 2, 3, 5. For example, 6, 8 are ugly while 14 is not ugly since it includes another prime factor 7.
#
# Note that 1 is typically treated as an ugly number.
#
# Have you met this question in a real interview?
# Example
# Given num = 8 return true
# Given num = 14 return false


class Solution:
	"""
	@param num: An integer
	@return: true if num is an ugly number or false
	"""
	
	def isUgly (self, num):
		# write your code here
		# check all 2, 3, 5 and see anything left
		
		if num is None or num <= 0:
			return False
		
		if num == 1:
			return True
		
		# check 2
		
		while num % 2 == 0:
			num = num // 2
		
		while num % 3 == 0:
			num = num // 3
		
		while num % 5 == 0:
			num = num // 5
		
		if num == 1:
			return True
		else:
			return False