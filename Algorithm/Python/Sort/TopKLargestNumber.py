# 544. Top k Largest Numbers
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given an integer array, find the top k largest numbers in it.
#
# Have you met this question in a real interview?
# Example
# Given [3,10,1000,-99,4,100] and k = 3.
# Return [1000, 100, 10].
#
# Tags
# Heap Priority Queue


# 1. use quick select 2. use max heap 3. bubble sort k times (O(nk)) 4. sort then return (O(n log(n))


class Solution:
    """
    @param nums: an integer array
    @param k: An integer
    @return: the top k largest numbers in array
    """

    def topk(self, nums, k):
        # write your code here
        "use quick select for array, instead of return only kth, return top k"

        if nums is None or len(nums) == 0:
            return []

        # if len(nums) <= k:
        #     return nums

        "every time we pick up a pivot, sort it, return the sorted position and its value"
        "keep quick select until position k is found"
        self.quickselectk(nums, k, 0, len(nums) - 1)

        return sorted(nums[len(nums) - k:len(nums)], reverse=True)

    def quickselectk(self, nums, k, start, end):

        m = self.quickselectHelper(nums, start, end)

        "keep select left part and right part"

        if m == len(nums)- k:
            "this means that len(nums)-k to len(nums) -1 are all larger than nums[len(nums)-k], we already found all largest k elements, return"
            return
        else:
            if start < m - 1:
                self.quickselectk(nums, k, start, m - 1)
            if m + 1 < end:
                self.quickselectk(nums, k, m + 1, end)

    def quickselectHelper(self, nums, start, end):

        pivot = end

        low = start
        high = end -1
        while low <= high:
            if nums[low] <= nums[pivot]:
                low += 1
            elif nums[high] > nums[pivot]:
                high -= 1
            else:
                "we need to exchange them"
                temp = nums[low]
                nums[low] = nums[high]
                nums[high] = temp

        "exchange value position high + 1  with pivot"
        temp = nums[high + 1]
        nums[high + 1] = nums[pivot]
        nums[pivot] = temp

        return high + 1


assert Solution().topk([9, 3, 2, 4, 8], 3) == [9, 8, 4]
assert Solution().topk([1,2,3,4,5,6,8,9,10,7],10) == [10,9,8,7,6,5,4,3,2,1]

from queue import PriorityQueue


class Solution2:
    """
    @param nums: an integer array
    @param k: An integer
    @return: the top k largest numbers in array
    """

    def topk(self, nums, k):
        # write your code here
        "use max heap/priority queue, push all items, then pop q times"

        q = PriorityQueue()

        for i in range(0, len(nums)):
            "(priority, value)"
            q.put((-nums[i], nums[i]))

        result = []
        for i in range(0, k):
            result.append(q.get()[1])

        return result
