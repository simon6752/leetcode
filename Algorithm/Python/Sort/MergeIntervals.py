# 156. Merge Intervals
# Description
# Given a collection of intervals, merge all overlapping intervals.

# Have you met this question in a real interview?  
# Example
# Given intervals => merged intervals:

# [                     [
#   (1, 3),               (1, 6),
#   (2, 6),      =>       (8, 10),
#   (8, 10),              (15, 18)
#   (15, 18)            ]
# ]
# Challenge
# O(n log n) time and O(1) extra space.


# solution1 failed because no > < between Interval is defined

class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""

class Solution:
    """
    @param intervals: interval list.
    @return: A new interval list.
    """
    def merge(self, intervals):
        # write your code here
        # 1st, merge sort by start of interval, O(n log(n)) time and  extra space, then merge two adjacent intervals
        
        # sort it, then merge two adjacent intervals 
        
        if intervals is None or len(intervals) == 0:
            return intervals 
            
        # sort, O(n log(n))
        newintervals = sorted(intervals, key=lambda interval:interval.start)
        
        # we need 2 pointers, 1st point to head of interval that can be merge to, also end of merged region, 2nd point to intervals waiting to be merged 
        
        p1 = 0 
        p2 = 1
 
        while p2 < len(newintervals):
            int1 = newintervals[p1]
            int2 = newintervals[p2]
            
            if int1.end < int2.start:
                # int1 not intersect with int2 
                # move forward 
                newintervals[p1+1] = newintervals[p2]
                p1 = p1 + 1
                p2 = p2 + 1

            elif int1.end > int2.end:
                # int1 cover int2, ignore int2 
                p2 = p2 + 1 
            
            else:
                # intersect , merge 
                int1.end = int2.end 
                p2 = p2 + 1 
                
        return newintervals[0:p1+1]
                
                
                
                
                

