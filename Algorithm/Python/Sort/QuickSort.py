# let's say we have a list [2, 5, 9,7,1,4,8,10,6,11,3,12]





class Solution:

    def quicksort(self,nums):

        "first pick, then parition, do it recursively"

        if nums == None or len(nums)<=1:
            return nums

        self.quicksortHelper(nums,0,len(nums)-1)

        return nums

    def quicksortHelper(self,nums,low,high):

        if high <= low:
            "do nothing"
            return
        pos = self.partition(nums,low,high)
        "after partition, pos is the pivot position, left part < nums[pos], right part > nums[pos]"

        "pos is the sorted pivot position, we need to recursively to it for the rest part(left part, and right part)"

        self.quicksortHelper(nums,low,pos-1)
        self.quicksortHelper(nums,pos+1,high)

    def partition(self,nums,low,high):

        "pick a pivot"
        pivot = high

        i = low
        j = high -1

        while( j >= i):

            if nums[j] > nums[pivot]:
                j= j - 1
            elif nums[i] < nums[pivot]:
                i = i +1
            else:
                " nums[j] < nums[pivot] and nums[i] > nums[pivot], exchange"
                temp = nums[j]
                nums[j] = nums[i]
                nums[i] = temp
                "move j one step"

        "when while loops ends,  j < i, j +1 is the first large than pivot, should exchange with pivot "

        "exchange nums[pivot] with  nums[j+1] "

        temp = nums[pivot]
        nums[pivot] = nums[j+1]
        nums[j+1] = temp
        "return the position of pivot"
        return j+1

assert Solution().quicksort( [2, 5, 9,7,1,4,8,10,6,11,3,12]) == [ 1,2,3,4,5,6,7,8,9,10,11,12]
assert Solution().quicksort( [1,3,5,7,9,8,6,4,2]) == [ 1,2,3,4,5,6,7,8,9]





