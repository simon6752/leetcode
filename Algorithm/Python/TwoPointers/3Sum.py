
# Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
#
# Note: The solution set must not contain duplicate triplets.
#
# For example, given array S = [-1, 0, 1, 2, -1, -4],
#
# A solution set is:
# [
#   [-1, 0, 1],
#   [-1, -1, 2]
# ]
#
#
# use TwoSum (no)
# need to avoid duplicates
# avoid index out of range

class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        "use the result of two sum, avoid duplicate"
        "no"
        "sort to avoid duplicates"

        "iterate over the list, find -S[i] in the rest of the list(use two pointers)"

        nums =sorted(nums)

        result = []
        temp = 0
        for i in range(0 ,len(nums ) -2):
            "initialize two pointers"
            pMin = i+ 1
            pMax = len(nums) - 1

            "if current nums[i] is equal to nums[i-1], minimum number is the same, needs to skip"
            if i > 0 and nums[i] == nums[i - 1]:
                continue

            while pMin < pMax:
                temp = nums[pMin] + nums[pMax]
                if temp == - nums[i]:
                    result.append([nums[i], nums[pMin], nums[pMax]])
                    "both pointers move"
                    pMax = pMax - 1
                    pMin = pMin + 1
                    "need to skip the same value"
                    while pMax > pMin and pMax < len(nums) - 1 and nums[pMax] == nums[pMax + 1]:
                        pMax = pMax - 1

                    while pMin < pMax and pMin > i and nums[pMin] == nums[pMin - 1]:
                        pMin = pMin + 1
                elif temp > -nums[i]:
                    pMax = pMax - 1
                else:
                    pMin = pMin + 1

        return result


# test cases
assert Solution().threeSum([-1, 0, 1, 2, -1, -4]) == [[-1, -1, 2], [-1, 0, 1]]
assert Solution().threeSum([1, 1, 1]) == []
assert Solution().threeSum([-2, 0, 0, 2, 2]) == [[-2, 0, 2]]
assert Solution().threeSum([-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6]) == [[-4, -2, 6], [-4, 0, 4], [-4, 1, 3],
                                                                                  [-4, 2, 2], [-2, -2, 4], [-2, 0, 2]]