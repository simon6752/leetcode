#
# 918. 3Sum Smaller
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given an array of n integers nums and a target, find the number of index triplets i, j, k with 0 <= i < j < k < n that satisfy the condition nums[i] + nums[j] + nums[k] < target.
#
# Have you met this question in a real interview?
# Example
# Given nums = [-2,0,1,3], target = 2, return 2.
#
# Explanation:
# Because there are two triplets which sums are less than 2:
# [-2, 0, 1]
# [-2, 0, 3]
# Challenge
# Could you solve it in O(n2) runtime?
#
# Tags
# Two Pointers Google Array

"my solution, not work, no need to consider duplication"


class MySolution:
    """
    @param nums:  an array of n integers
    @param target: a target
    @return: the number of index triplets satisfy the condition nums[i] + nums[j] + nums[k] < target
    """

    def threeSumSmaller(self, nums, target):
        # Write your code here
        "instead of find target, we find those  smaller than target"

        "use sort + two pointers, need to handle duplicates"

        if nums is None or len(nums) <= 2:
            return 0

        "sort, O(n log(n))"
        nums = sorted(nums)

        countt = 0
        count = 0
        for i in range(0, len(nums) - 2):

            p = i + 1
            q = len(nums) - 1

            if i != 0 and nums[i] == nums[i - 1]:
                "need to skip"
                # "add count for i-1 to total"
                # countt += count
                continue

            "reset count for i"
            count = 0
            while q > p:

                if nums[p] + nums[q] >= target - nums[i]:
                    "move right pointer since we are looking for smaller values"
                    q = q - 1
                elif nums[p] + nums[q] < target - nums[i]:
                    count += 1

                    # p = p + 1
                    "only move one pointer in case we miss (i, p, q-1)"
                    "but which one to move"
                    "try both"

                    "actually not all combinations between p, q will satisfy nums[p] + nums[q] + nums[i] < target"

                    "or we should try p, q from the end, p= q-1, every time try move p first, then q"

                    "try to find all sum smaller than target between p and q and start/end with q/p"
                    p1 = p
                    q1 = q
                    while p1 + 1 < q1 and nums[p1 + 1] + nums[q1] < target - nums[i]:
                        "need to remove duplicate"
                        if nums[p1 + 1] != nums[p1]:
                            count += 1
                        p1 = p1 + 1
                    p1 = p
                    q1 = q
                    while p1 + 1 < q1 and nums[p1] + nums[q1 - 1] < target - nums[i]:
                        "this case is almost always true"
                        "remove duplicate when move this pointer"
                        if nums[q1 - 1] != nums[q1]:
                            count += 1
                        q1 = q1 - 1
                    # "we also need to add all pair start with p, ends with p+1 -> q-1"
                    # count += q-1-p
                    "now we are safe to move both pointers"
                    p = p + 1
                    q = q - 1

                    # "handle duplicate"
                    # while (p < q and nums[p] == nums[p-1]):
                    #     count += 1
                    #     p = p + 1

                    # while q > p and nums[q] == nums[q+1]:
                    #     count += 1
                    #     q = q -1

            "add count for i to total"
            countt += count

        return countt


class Solution:
    """
    @param nums:  an array of n integers
    @param target: a target
    @return: the number of index triplets satisfy the condition nums[i] + nums[j] + nums[k] < target
    """

    def threeSumSmaller(self, nums, target):
        # Write your code here
        "instead of find target, we find those  smaller than target"

        "use sort + two pointers, no need to handle duplicates"

        if nums is None or len(nums) <= 2:
            return 0

        "sort, O(n log(n))"
        nums = sorted(nums)

        count = 0

        for i in range(0, len(nums) - 2):

            start = i + 1
            end = len(nums) - 1

            while end > start:

                if nums[start] + nums[end] >= target - nums[i]:
                    "too large, move end pointer"
                    end -= 1
                else:
                    "all ends with start + 1 to end"
                    count += end - start
                    start += 1

        return count


assert Solution().threeSumSmaller([3, 2, -2, 6, 2, -2, 6, -2, -4, 2, 3, 0, 4, 4, 1], 3) == 151
