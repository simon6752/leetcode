# 363. Trapping Rain Water
# Description
# Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.
#
# Trapping Rain Water
#
# Have you met this question in a real interview?
# Example
# Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
#
# Challenge
# O(n) time and O(1) memory
#
# O(n) time and O(n) memory is also acceptable.

# my own solution v1, not work
class Solution:
	"""
	@param heights: a list of integers
	@return: a integer
	"""
	
	def trapRainWater (self, heights):
		# write your code here
		# we need to find all the peak position. the water between two peak position is defined as sum of all heights < min(a[i],a[j])  in [i:j]
		
		
		if heights is None or len (heights) < 3:
			return 0
		
		total = 0
		
		prevdec = -1
		currentdec = -1
		
		for i in range (0, len (heights)):
			
			if (i == 0 and heights [i] > heights [i + 1]) or (
							i != 0 and i != len (heights) - 1 and heights [i] > heights [i - 1] and heights [i] >
				heights [i + 1]) or (i == len (heights) - 1 and heights [i] > heights [i - 1]):
				# i is the peak
				currentdec = i
				
				if prevdec != -1:
					# going back to at most prevdec
					for j in range (i, prevdec - 1, -1):
						if heights [j] < min (heights [prevdec], heights [currentdec]):
							total += min (heights [prevdec], heights [currentdec]) - heights [j]
						
						# update prevdec to currentdec
			prevdec = currentdec
		
		return total
	
assert Solution().trapRainWater([100,0,100]) == 100

# this case not passed since water can overflow between two high low high regions
assert Solution().trapRainWater([100,50,99,50,100,50,99,50,100,50]) == 202

# another idea is: recursively, find 1st high and second high, the water between these two positions are clear. then calculate the rest part recursively, but time complexity of this case is too high

class Solution2:
	"""
	@param heights: a list of integers
	@return: a integer
	"""
	
	def trapRainWater (self, heights):
		# write your code here
		# how much water can be trapped by a bar is decided by highest on the left and highest on the right
		# w[i] = max(min(left highest, right highest) - heights[i] ,0)
		
		if heights is None or len (heights) < 3:
			return 0
		
		left = []
		right = []
		
		lefthigh = heights [0]
		righthigh = heights [-1]
		
		for i in range (0, len (heights)):
			lefthigh = max (lefthigh, heights [i])
			left.append (lefthigh)
		
		for i in range (len (heights) - 1, -1, -1):
			righthigh = max (righthigh, heights [i])
			# need to put in right order
			right.insert (0, righthigh)
		
		total = 0
		
		for i in range (1, len (heights) - 1):
			# 0 and -1 pos cannot hold any water
			total += max (min (left [i], right [i]) - heights [i], 0)
		
		return total
	
	# time O(n), space, O(n)
	
	
	# https://segmentfault.com/a/1190000004594606

# TODO 1. stack

# TODO 2. two pointer v1

	
	
	# TODO 3. two pointer v2
