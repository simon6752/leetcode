# 383. Container With Most Water
# Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai). n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis forms a container, such that the container contains the most water.

# Example
# Given [1,3,2], the max area of the container is 2.


"Naive O(n^2) solution"
class Solution:
    """
    @param heights: a vector of integers
    @return: an integer
    """
    def maxArea(self, heights):
        # write your code here
        "O(n^2) naive solution"
        
        maxw = 0
        
        for i in range(0,len(heights)):
            for j in range(i+1,len(heights)):
                
                maxw = max(maxw, min(heights[i],heights[j])*(j-i))
                
                
        return maxw

# TODO: use two pointers
# highest must be inside final [start,end]. if not, replace start/end with highest, you can get maximum
# so we only need to find out maximum ends with highest and maximum start with highest, add together???

"this solution did not pass all test cases"
class Solution2:
    """
    @param heights: a vector of integers
    @return: an integer
    """
    def maxArea(self, heights):
        # write your code here
        # highest must be inside final [start,end]. if not, replace start/end with highest, you can get maximum
        # so we only need to find out maximum ends with highest and maximum start with highest, add together
        
        maxw = 0
        
        if heights is None or len(heights) <= 1:
            return maxw
        
        highest = heights[0]
        highpos = 0
        
        for i in range(0,len(heights)):
            if heights[i]> highest:
                highpos = i 
                
        
        maxw1 = 0
        maxw1pos = highpos
        "check left side of highpos"
        for i in range(highpos,-1,-1):
            newarea = min(heights[highpos],heights[i])*(highpos - i)
            if newarea > maxw1:
                maxw1 = newarea
                maxw1pos = i 
                
        maxw2 = 0
        maxw2pos = highpos
        "check right side of highpos"
        for j in range(highpos,len(heights)):
            newarea = min(heights[j], heights[highpos])*(j-highpos)
            if newarea > maxw2:
                maxw2 = newarea
                maxw2pos = j 
        
        "keep it in mind that maximum of left + maximum of right not neccessarily maximum of whole range"        
                
        return min(heights[maxw2pos],heights[maxw1pos])*(maxw2pos - maxw1pos)

assert Solution2().maxArea([1,3,2]) == 2

assert Solution2().maxArea([1000, 1001, 999]) == 1998

"for [a,b], if a<b and b is the furthest that >a,  or a>b, and a is furthest >b, then area between [a,b] is maximum that start with a or ends with b in whole range[0,n)"
"since when w[b] > w[a], [a,b](b>a) or [b,a] is always largest for a in this [0,n), we can continue to search for next maximum: a++(w[b]>w[a]) or b--(w[b]<w[a])"


class Solution3:
    """
    @param heights: a vector of integers
    @return: an integer
    """
    def maxArea(self, heights):
        # write your code here
        # highest must be inside final [start,end]. if not, replace start/end with highest, you can get maximum
        # so we only need to find out maximum ends with highest and maximum start with highest, add together
        
        maxw = 0
        
        if heights is None or len(heights) <= 1:
            return maxw
        
        
        "[a,b], w[a] < w[b]. area = (b-a)*w[b]. "
        
        maxw = 0
        
        p = 0
        q = len(heights) -1 
        
        while p<q:
            are = self.area(p,q,heights)
            maxw = max(maxw,are)
            
            if heights[q] > heights[p]:
                p += 1 
            else: 
                q -= 1 
                
        return maxw
        
    
    def area(self,i,j,heights):
        "start i, end j"
        return (j-i)*min(heights[i],heights[j])

# Approach :

# Note 1 : When you consider a1 and aN, then the area is (N-1) * min(a1, aN).
# Note 2 : The base (N-1) is the maximum possible.
# This implies that if there was a better solution possible, it will definitely have the Height greater than min(a1, aN).
#  Base * Height > (N-1) * min(a_1, a_N) 
# We know that, Base min(a1, aN)
# This means that we can discard min(a1, aN) from our set and look to solve this problem again from the start.
# If a1 < aN, then the problem reduces to solving the same thing for a2, aN.
# Else, it reduces to solving the same thing for a1, aN-1

# [1000, 1, 2,3,...,x,1002], no matter x is 1003 or 10001, we can always be safe to remove a[0] = 1000. if x > a[-1], since lower end is fixed by a[0], no way
# if x <a[-1], both length and base are smaller. so safe to remove a[0] = 1000 since there will be no area larger than [0,-1] for [0,x]
