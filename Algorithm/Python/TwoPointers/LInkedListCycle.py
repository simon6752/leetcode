# 102. Linked List Cycle
# Description
# Notes
# Testcase
# Judge
# Discuss
# Given a linked list, determine if it has a cycle in it.
#
#
#
# Have you met this question in a real interview?
# Example
# Given -21->10->4->5, tail connects to node index 1, return true
#
# Challenge
# Follow up:
# Can you solve it without using extra space?
#
# Tags
# Amazon Bloomberg Microsoft Yahoo Two Pointers Linked List


"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""


class Solution:
    """
    @param: head: The first node of linked list.
    @return: True if it has a cycle, or false
    """

    def hasCycle(self, head):
        # write your code here
        "use two pointers, one move 1 step, one move 2 steps"

        if head is None or head.next is None:
            return False

        p = head
        "q is the faster pointer"
        q = head.next

        while p is not None and q is not None:
            if p == q:
                "q met p, there is cycle"
                return True

            q = q.next
            if q is None:
                "reached end, break"
                break
            elif q == p:
                "q met p, there is cycle"
                return True

            "both move one more step"
            q = q.next
            p = p.next

        return False
