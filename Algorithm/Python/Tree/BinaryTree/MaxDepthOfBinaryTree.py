# 97. Maximum Depth of Binary Tree
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a binary tree, find its maximum depth.
#
# The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
#
# Have you met this question in a real interview?
# Example
# Given a binary tree as follow:
#
#   1
#  / \
# 2   3
#    / \
#   4   5
# The maximum depth is 3.

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """

    def maxDepth(self, root):
        # write your code here

        "recursion is easy"

        if root is None:
            return 0

        return 1 + max(self.maxDepth(root.left), self.maxDepth(root.right))