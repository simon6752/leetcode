# 72. Construct Binary Tree from Inorder and Postorder Traversal
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given inorder and postorder traversal of a tree, construct the binary tree.
#
#  Notice
# You may assume that duplicates do not exist in the tree.
#
# Have you met this question in a real interview?
# Example
# Given inorder [1,2,3] and postorder [1,3,2], return a tree:
#
#   2
#  / \
# 1   3
# Tags
# Binary Tree Microsoft


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode
from Algorithm.Python.Tree.BinaryTree.DFSTraverse import *


class Solution:
    """
    @param inorder: A list of integers that inorder traversal of a tree
    @param postorder: A list of integers that postorder traversal of a tree
    @return: Root of a tree
    """
    def buildTree(self, inorder, postorder):
        # write your code here
        # in in order, we have left -> parent -> right,  in post order, we have left -> right -> parent. so if we have two adjacent nodes in in-order, they can only be left -> parent or parent -> right. if 1st case, in post order, the order will be the same. if 2nd case, the order will also be opposite. change post order list to a hashtable {key:value}, key is node value, value is node position in post order

        if inorder is None or postorder is None or len(inorder) == 0 or len(postorder) == 0:
            return None

        postorderdict = {}

        for i in range(0 ,len(postorder)):
            postorderdict[str(postorder[i])] = i

        node = BinaryTreeNode(inorder[0])
        root = None
        "check whether this may be the root"
        if inorder[0] == postorder[-1]:
            root = node

        prevnode = node
        for i in range(1 ,len(inorder)):
            node = BinaryTreeNode(inorder[i])
            "check whether this may be the root"
            if inorder[i] == postorder[-1]:
                root = node

            'we need to connect all the nodes'
            if postorderdict[str(inorder[i])] > postorderdict[str(inorder[ i -1])]:
                "current node is behind previous node in postorder, which means this is a left -> parent"
                node.left = prevnode
            else:
                "parent -> right"
                prevnode.right = node

            prevnode = node

        "need to return the root node, 1st in preorder and last in post order is the root!"
        return root
" this is wrong sinced we failed to connect all nodes when doing iteration"

Solution().buildTree([2,1,3],[2,3,1])


class Solution2:
    """
    @param inorder: A list of integers that inorder traversal of a tree
    @param postorder: A list of integers that postorder traversal of a tree
    @return: Root of a tree
    """

    def buildTree(self, inorder, postorder):
        # write your code here
        "in in order, we got [ left child subtree in-order, root, right child subtree in-order]. in post order, we got [left child subtree post order, right child subtree post-order, root]. so we can do this recursively as long as we found the start and end of each sbutree region "

        if inorder is None or postorder is None or len(inorder) == 0 or len(postorder) == 0:
            return None

        root = BinaryTreeNode(postorder[-1])
        if len(inorder) == 1:
            return root

        "first, find the element in in-order, divide  left and right into subtree. use number of left, right subtree and divide post order list too. do recursion"

        inorderdict = {}
        rootpos = 0
        for i in range(0, len(inorder)):
            if inorder[i] == postorder[-1]:
                rootpos = i

        root.left = self.buildTree(inorder[:rootpos], postorder[:rootpos])
        root.right = self.buildTree(inorder[rootpos + 1:], postorder[rootpos:-1])

        "need to return the root node, 1st in postorder and last in post order is the root!"

        return root

inordertree = [1,2]
postordertree = [2,1]
print("inorder tree: " + str(inordertree))
print("postorder tree: " + str(postordertree))
result = Solution2().buildTree(inordertree,postordertree)
print(result)
result2 = []
inorderRecursive(result,result2)
print(result2)
assert inordertree ==  result2
result2 = []
postorderRecursive(result, result2)
print(result2)
assert postordertree == result2

inordertree = [1,2,3]
postordertree = [2,1,3]
print("inorder tree: " + str(inordertree))
print("postorder tree: " + str(postordertree))
result = Solution2().buildTree(inordertree,postordertree)
print(result)
result2 = []
inorderRecursive(result,result2)
print(result2)
assert inordertree ==  result2
result2 = []
postorderRecursive(result, result2)
print(result2)
assert postordertree == result2


inordertree = [2,1,3]
postordertree = [2,3,1]
print("inorder tree: " + str(inordertree))
print("postorder tree: " + str(postordertree))
result = Solution2().buildTree(inordertree,postordertree)
print(result)
result2 = []
inorderRecursive(result,result2)
print(result2)
assert inordertree ==  result2
result2 = []
postorderRecursive(result, result2)
print(result2)
assert postordertree == result2
