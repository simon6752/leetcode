# 453. Flatten Binary Tree to Linked List
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Flatten a binary tree to a fake "linked list" in pre-order traversal.
#
# Here we use the right pointer in TreeNode as the next pointer in ListNode.
#
#  Notice
# Don't forget to mark the left child of each node to null. Or you will get Time Limit Exceeded or Memory Limit Exceeded.
#
# Have you met this question in a real interview?
# Example
#               1
#                \
#      1          2
#     / \          \
#    2   5    =>    3
#   / \   \          \
#  3   4   6          4
#                      \
#                       5
#                        \
#                         6
# Challenge
# Do it in-place without any extra memory.

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: a TreeNode, the root of the binary tree
    @return: nothing
    """

    def flatten(self, root):
        # write your code here
        "do it in a post-order traversal"

        if root is None:
            return

        "flatten root.left and root.right recursively and merge all the node in right side"
        self.flatten(root.left)
        self.flatten(root.right)

        "left and right not empty, merge it"
        temp = root.right
        root.right = root.left
        root.left = None
        temp2 = root
        while temp2.right is not None:
            temp2 = temp2.right
        temp2.right = temp
        return
