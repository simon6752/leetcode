# 70. Binary Tree Level Order Traversal II
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).
#
# Have you met this question in a real interview?
# Example
# Given binary tree {3,9,20,#,#,15,7},
#
#     3
#    / \
#   9  20
#     /  \
#    15   7
#
#
# return its bottom-up level order traversal as:
#
# [
#   [15,7],
#   [9,20],
#   [3]
# ]
# Tags
# Queue Binary Tree Binary Tree Traversal Breadth First Search

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: A tree
    @return: buttom-up level order a list of lists of integer
    """

    def levelOrderBottom(self, root):
        # write your code here
        "use 1 queue, record level, when append to result, append to head instead of end"

        if root is None:
            return []

        result = []
        level = 1

        queue = []

        node = root

        queue.append([level, root])

        while len(queue) > 0:
            element = queue.pop(0)
            level = element[0]
            node = element[1]

            "add node value to level list in result"

            if level > len(result):
                "first time insert for this level"
                result.insert(0, [])

            result[len(result) - level].append(node.val)

            if node.left is not None:
                queue.append([level + 1, node.left])

            if node.right is not None:
                queue.append([level + 1, node.right])

        return result


class Solution2:
    """
    @param root: A tree
    @return: buttom-up level order a list of lists of integer
    """

    def levelOrderBottom(self, root):
        # write your code here
        "use recursion, record level, when append to result, append to head instead of end"

        result = []
        self.levelOrderBottomHelper(root, 1, result)
        return result

    def levelOrderBottomHelper(self, root, level, result):

        if root is None:
            return

        if level > len(result):
            "first time for this level"
            result.insert(0, [])

        result[-level].append(root.val)

        if root.left is not None:
            self.levelOrderBottomHelper(root.left, level + 1, result)

        if root.right is not None:
            self.levelOrderBottomHelper(root.right, level + 1, result)
