# 73. Construct Binary Tree from Preorder and Inorder Traversal
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given preorder and inorder traversal of a tree, construct the binary tree.
#
#  Notice
# You may assume that duplicates do not exist in the tree.
#
# Have you met this question in a real interview?
# Example
# Given in-order [1,2,3] and pre-order [2,1,3], return a tree:
#
#   2
#  / \
# 1   3
# Tags
# Binary Tree Bloomberg

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode
from Algorithm.Python.Tree.BinaryTree.DFSTraverse import *


class Solution:
    """
    @param inorder: A list of integers that inorder traversal of a tree
    @param postorder: A list of integers that postorder traversal of a tree
    @return: Root of a tree
    """

    def buildTree(self, inorder, preorder):
        # write your code here
        "inorder [left subtree, root, right subtree], preorder[root, left subtree, right subtree]"

        if inorder is None or len(inorder) == 0 or preorder is None or len(preorder) == 0:
            return None

        root = BinaryTreeNode(preorder[0])

        for i in range(0, len(inorder)):
            if inorder[i] == preorder[0]:
                "root"
                break

        rootpos = i
        "recursion"
        root.left = self.buildTree(inorder[:rootpos], preorder[1:rootpos + 1])

        root.right = self.buildTree(inorder[rootpos+1:], preorder[rootpos + 1:])

        return root


inordertree = [1,2]
preordertree = [2,1]
print("inorder tree: " + str(inordertree))
print("preorder tree: " + str(preordertree))
result = Solution().buildTree(inordertree,preordertree)
print(result)
result2 = []
inorderRecursive(result,result2)
print(result2)
assert inordertree ==  result2
result2 = []
preorderRecursive(result, result2)
print(result2)
assert preordertree == result2

inordertree = [1,2,3]
preordertree = [2,1,3]
print("inorder tree: " + str(inordertree))
print("preorder tree: " + str(preordertree))
result = Solution().buildTree(inordertree,preordertree)
print(result)
result2 = []
inorderRecursive(result,result2)
print(result2)
assert inordertree ==  result2
result2 = []
preorderRecursive(result, result2)
print(result2)
assert preordertree == result2


inordertree = [2,1,3]
preordertree = [2,3,1]
print("inorder tree: " + str(inordertree))
print("preorder tree: " + str(preordertree))
result = Solution().buildTree(inordertree,preordertree)
print(result)
result2 = []
inorderRecursive(result,result2)
print(result2)
assert inordertree ==  result2
result2 = []
preorderRecursive(result, result2)
print(result2)
assert preordertree == result2


inordertree = [9,3,15,20,7]
preordertree = [3,9,20,15,7]
print("inorder tree: " + str(inordertree))
print("preorder tree: " + str(preordertree))
result = Solution().buildTree(inordertree,preordertree)
print(result)
result2 = []
inorderRecursive(result,result2)
print(result2)
assert inordertree ==  result2
result2 = []
preorderRecursive(result, result2)
print(result2)
assert preordertree == result2

#        1
#      2
#   3

inordertree = [3, 2, 1]
preordertree = [1, 2, 3]
print("inorder tree: " + str(inordertree))
print("preorder tree: " + str(preordertree))
result = Solution().buildTree(inordertree, preordertree)
print(result)
result2 = []
inorderRecursive(result, result2)
print(result2)
assert inordertree == result2
result2 = []
preorderRecursive(result, result2)
print(result2)
assert preordertree == result2

inordertree = [6, 5, 4, 3, 2, 1]
preordertree = [1, 2, 3, 4, 5, 6]
print("inorder tree: " + str(inordertree))
print("preorder tree: " + str(preordertree))
result = Solution().buildTree(inordertree, preordertree)
print(result)
result2 = []
inorderRecursive(result, result2)
print(result2)
assert inordertree == result2
result2 = []
preorderRecursive(result, result2)
print(result2)
assert preordertree == result2
