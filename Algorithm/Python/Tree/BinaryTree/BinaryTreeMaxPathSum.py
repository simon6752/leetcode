# 94. Binary Tree Maximum Path Sum
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a binary tree, find the maximum path sum.
#
# The path may start and end at any node in the tree.
#
# Have you met this question in a real interview?
# Example
# Given the below binary tree:
#
#   1
#  / \
# 2   3
# return 6.
#
# Tags
# Recursion Dynamic Programming Divide and Conquer Baidu Microsoft

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.value = val
        self.left, self.right = None, None
"""
import sys

from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode


class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """

    def maxPathSum(self, root):
        # write your code here

        "use recursion, record current sum, currentmax"
        "use divide and conquer, use return value"

        if root is None:
            return 0

        result = self.maxPathSumHelper(root)
        return result.maxpath

    def maxPathSumHelper(self, root):

        if root is None:
            "if root is None, set path to min of int so it will never be selected"
            return ReturnType(-sys.maxsize - 1, -sys.maxsize - 1)

        leftresult = self.maxPathSumHelper(root.left)
        rightresult = self.maxPathSumHelper(root.right)

        singlepath = root.value + max(max(0, leftresult.singlepath), max(0, rightresult.singlepath))
        "it is either maxpath of left, maxpath of right, or cross the root(may or may not choose left or right or both single path)"
        maxpath = max(max(leftresult.maxpath, rightresult.maxpath),
                      root.value + max(0, leftresult.singlepath) + max(0, rightresult.singlepath))

        return ReturnType(singlepath, maxpath)


class ReturnType:

    def __init__(self, singlepath, maxpath):
        "single path is the maximum value start from root"
        self.singlepath = singlepath
        "maxpath is the maxinum value in current tree, it may or may not cross the root"
        self.maxpath = maxpath


tree = BinaryTreeNode(1)
tree.right = BinaryTreeNode(2)
tree.right.left = BinaryTreeNode(3)

assert Solution().maxPathSum(tree) == 6

tree = BinaryTreeNode(-1)
assert Solution().maxPathSum(tree) == -1
