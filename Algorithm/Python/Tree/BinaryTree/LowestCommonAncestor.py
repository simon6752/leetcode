# 88. Lowest Common Ancestor
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given the root and two nodes in a Binary Tree. Find the lowest common ancestor(LCA) of the two nodes.
#
# The lowest common ancestor is the node with largest depth which is the ancestor of both nodes.
#
#  Notice
# Assume two nodes are exist in tree.
#
# Have you met this question in a real interview?
# Example
# For the following binary tree:
#
#   4
#  / \
# 3   7
#    / \
#   5   6
# LCA(3, 5) = 4
#
# LCA(5, 6) = 7
#
# LCA(6, 7) = 7
#
# Tags
# LintCode Copyright Binary Tree LinkedIn Facebook

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param: root: The root of the binary search tree.
    @param: A: A TreeNode in a Binary.
    @param: B: A TreeNode in a Binary.
    @return: Return the least common ancestor(LCA) of the two nodes.
    """

    def lowestCommonAncestor(self, root, A, B):
        # write your code here
        "LCA is node that when first time the two nodes are split into two subtrees"

        "naive solution: find A, get path to A, find B, get path to B, compare path"

        pathA = self.findPath(root, A, [])
        pathB = self.findPath(root, B, [])

        node = root
        for i in range(0, min(len(pathA), len(pathB))):
            if pathA[i] == pathB[i]:
                node = pathA[i]
            else:
                break

        return node

    def findPath(self, root, A, path):

        if root is None:
            return []

        path.append(root)

        if root.val == A.val:
            "find it"
            return path
        "go to left or right"

        path1 = None
        if root.left is not None:
            path1 = self.findPath(root.left, A, list(path))

        path2 = None
        if root.right is not None:
            path2 = self.findPath(root.right, A, list(path))

        if path1 is not None:
            return path1
        if path2 is not None:
            return path2

# Solution 2: use divide and conquer
# the LCA is the first node that split A and B into its two subtrees
# LAC must exist, it is either current node if it is A or B, or it exists in its left or right subtree

class Solution2:
    def lowestCommonAncestor(self, root, A, B):

        if root is None or root == A or root == B:
            "we reached null, or we reached A or B"
            return root

        "then LCA must be in its two subtrees"

        node1 = self.lowestCommonAncestor(root.left, A,B)
        node2 = self.lowestCommonAncestor(root.right, A, B)

        if ( node1 is not None and node2 is not None):
            "A B are in two seperate subtrees, LCA is current node"

            return root

        if node1 is not None and node2 is None:
            "A and B in root.left, LCA is node1"
            return node1

        if node2 is not None and node1 is None:
            "A and B in root.right, LCA is node 2"
            return node2
        "not found"
        return None


