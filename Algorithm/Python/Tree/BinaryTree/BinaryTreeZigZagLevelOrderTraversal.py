# 71. Binary Tree Zigzag Level Order Traversal
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).
#
# Have you met this question in a real interview?
# Example
# Given binary tree {3,9,20,#,#,15,7},
#
#     3
#    / \
#   9  20
#     /  \
#    15   7
#
#
# return its zigzag level order traversal as:
#
# [
#   [3],
#   [20,9],
#   [15,7]
# ]
# Tags
# Queue Binary Tree LinkedIn Binary Tree Traversal Breadth First Search Microsoft Bloomberg


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: A Tree
    @return: A list of lists of integer include the zigzag level order traversal of its nodes' values.
    """

    def zigzagLevelOrder(self, root):
        # write your code here

        result = []

        self.zigzagLevelOrderHelper(root, 1, result)

        return result

    def zigzagLevelOrderHelper(self, root, level, result):

        if root is None:
            return

        if level > len(result):
            "first time for this level"
            result.append([])

        if level % 2 != 0:
            "it is odd"
            result[level - 1].append(root.val)
        else:
            result[level - 1].insert(0, root.val)

        if root.left is not None:
            self.zigzagLevelOrderHelper(root.left, level + 1, result)

        if root.right is not None:
            self.zigzagLevelOrderHelper(root.right, level + 1, result)
