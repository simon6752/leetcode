#632. Binary Tree Maximum Node
#
# Find the maximum node in a binary tree, return the node.
#
# Have you met this question in a real interview?
# Example
# Given a binary tree:
#
#      1
#    /   \
#  -5     2
#  / \   /  \
# 0   3 -4  -5
# return the node with value 3.
#
# Tags
# Binary Tree

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param: root: the root of tree
    @return: the max node
    """

    def maxNode(self, root):
        # write your code here

        "do it recursively, traverse it"

        if root is None:
            return None

        node = root
        maxNode = node

        if node.left is not None:
            maxleftNode = self.maxNode(node.left)
            if maxleftNode is not None and maxleftNode.val > maxNode.val:
                maxNode = maxleftNode

        if node.right is not None:
            maxrightNode = self.maxNode(node.right)
            if maxrightNode is not None and maxrightNode.val > maxNode.val:
                maxNode = maxrightNode

        return maxNode