# use pre-order visit


def doSomethingWithTreeNode(tree, result):
    # print(str(tree.value) + ' -> ')
    result.append(tree.value)


"recursive"


def preorderRecursive(tree, result):
    if tree is None:
        return

    "print it"

    doSomethingWithTreeNode(tree, result)

    if tree.left is not None:
        preorderRecursive(tree.left, result)

    if tree.right is not None:
        preorderRecursive(tree.right, result)

    return


def preorderIterative(tree, result):
    "use stack"

    stack = []
    node = tree

    stack.append(node)

    while len(stack) > 0:

        "do something"
        doSomethingWithTreeNode(node, result)

        "1. start with right child"
        if node.right is not None:
            "push to end of list"
            stack.append(node.right)

        "2. then left child"
        if node.left is not None:
            "push to end of list"
            stack.append(node.left)

        "3. pop the last one out"
        node = stack.pop()


def inorderRecursive(tree, result):
    if tree == None:
        return

    if tree.left is not None:
        inorderRecursive(tree.left, result)

    doSomethingWithTreeNode(tree, result)

    if tree.right is not None:
        inorderRecursive(tree.right, result)


def inorderIterative(tree, result):
    "inorder iteration gives the sorted order of a binary search tree"
    stack = []

    if tree is None:
        return

    node = tree
    while node is not None or len(stack) > 0:
        "node is the head of node to be visited, stack save nodes that are passed but not visited"
        "will only stop when node is none and len(stack) is zero, which means there is no node to push in or no node to pop from"

        "1. go to all way down to the left leaf null"

        while node is not None:
            stack.append(node)
            node = node.left

        "2. pop from stack"

        node = stack.pop()
        "3. do something with left"
        doSomethingWithTreeNode(node, result)

        "4. go to right child"

        node = node.right


def postorderRecursive(tree, result):
    if tree is None:
        return

    if (tree.left is not None):
        postorderRecursive(tree.left, result)

    if (tree.right is not None):
        postorderRecursive(tree.right, result)

    "do something"

    doSomethingWithTreeNode(tree, result)


def postorderIterative(tree, result):
    "we use a previous node to check whether we are up/down from left or right"

    if tree is None:
        return

    node = tree
    stack = []

    stack.append(node)
    prev = None

    while len(stack) > 0:
        "peak element from last of stack"
        node = stack[-1]
        if prev == None or prev.left == node or prev.right == node:
            "1.  first time, or going down, then we keep going down, first push left child and leave, or we push right child when there is no left child( the first not none element in post order: left child -> right child -> parent), or leaf node, do something"
            if node.left is not None:
                stack.append(node.left)
            elif node.right is not None:
                stack.append(node.right)
            else:
                "we reached leaf node, do it"
                "we can do something now"
                doSomethingWithTreeNode(node, result)
                stack.pop()

            "if come down to the leaf node(left child is None and right child is None), "
        elif node.left == prev:
            "2. we moved up from left child, continue to right"
            if node.right is not None:
                stack.append(node.right)
        else:
            "3. moved up from right child, we can do something now"
            doSomethingWithTreeNode(node, result)
            stack.pop()

        prev = node


"TODO: level order traverse, use one Queue"


def levelOrderIterativeOneQueue(tree, result):
    if tree is None:
        return []

    node = tree

    queue = []
    queue.append(node)

    while len(queue) > 0:
        "1. get first from list, which is head of queue"

        node = queue.pop(0)
        "2. do something with it"
        doSomethingWithTreeNode(node, result)

        "3. push left child and right to queue"
        if node.left is not None:
            queue.append(node.left)
        if node.right is not None:
            queue.append(node.right)

def levelOrderIterativeOneQueue2(tree):
    result = []
    if tree is None:
        return result

    queue = []
    node = tree
    queue.append(node)

    while len(queue)> 0:
        node = queue[0]
        "do something for all elements in current queue, all from the same level"
        level = []
        length = len(queue)
        for i in range(0,length):
            node = queue.pop(0)
            level.append(node.value)
            "add left child and right child to queue"
            if node.left is not None:
                queue.append(node.left)
            if node.right is not None:
                queue.append(node.right)

        result.append(level)

    return result



"TODO level order traverse, use two queues"

" level order traverse, recursive"

def levelorderrecursive(tree, result):
    # result = []

    return levelorderrecursiveHelper(tree, 1, result)


def levelorderrecursiveHelper(tree, level, result):
    "preorder recursion"
    if tree is None:
        return

    if level > len(result):
        "means reached new level"
        result.append([])
    "add current node value to specific level of result"
    # doSomethingWithTreeNode(tree, result[level - 1])
    result[level - 1].append(tree.value)

    "when we go to child node, level also increase by 1"
    levelorderrecursiveHelper(tree.left, level + 1, result)
    levelorderrecursiveHelper(tree.right, level + 1, result)


"TODO level order traver, bottom up"
