# 1106. Maximum Binary Tree
# Given an integer array with no duplicates. A maximum tree building on this array is defined as follow:

# The root is the maximum number in the array.
# The left subtree is the maximum tree constructed from left part subarray divided by the maximum number.
# The right subtree is the maximum tree constructed from right part subarray divided by the maximum number.
# Construct the maximum tree by the given array and output the root node of this tree.

# Example
# Input: [3,2,1,6,0,5]
# Output: return the tree root node representing the following tree:

#       6
#     /   \
#    3     5
#     \    / 
#      2  0   
#        \
#         1

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""

class Solution:
    """
    @param nums: an array
    @return: the maximum tree
    """
    def constructMaximumBinaryTree(self, nums):
        # Write your code here
        "do it recursively"
        
        if nums is None or len(nums) == 0:
            return None 
            
        maxpos = 0 
        maxval = nums[0]
        for i in range(0,len(nums)):
            if nums[i] > maxval:
                maxpos = i 
                maxval = nums[i]
                
        node = TreeNode(maxval)
        
        node.left = self.constructMaximumBinaryTree(nums[:maxpos])
        node.right = self.constructMaximumBinaryTree(nums[maxpos+1:])
        
        return node 
        