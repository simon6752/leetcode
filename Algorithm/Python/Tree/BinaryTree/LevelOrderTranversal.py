# 69. Binary Tree Level Order Traversal

# Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
#
# Have you met this question in a real interview?
# Example
# Given binary tree {3,9,20,#,#,15,7},
#
#     3
#    / \
#   9  20
#     /  \
#    15   7
#
#
# return its level order traversal as:
#
# [
#   [3],
#   [9,20],
#   [15,7]
# ]
# Challenge
# Challenge 1: Using only 1 queue to implement it.
#
# Challenge 2: Use DFS algorithm to do it.


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: A Tree
    @return: Level order a list of lists of integer
    """

    def levelOrder(self, root):
        # write your code here
        "do it iteratively use queue"

        if root is None:
            return []

        "since we need to iterative traverse, node will change. We do not hope to change the object passed in as parameter"
        node = root

        queue = []
        result = []

        queue.append([1, node])

        while len(queue) > 0:

            "dequeue"
            element = queue.pop(0)
            level = element[0]
            node = element[1]

            "do something"
            if len(result) < level:
                result.append([])
            result[level - 1].append(node.val)

            "then add left child and right child to queue"

            if node.left is not None:
                queue.append([level + 1, node.left])

            if node.right is not None:
                queue.append([level + 1, node.right])

        return result