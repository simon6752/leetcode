# class BinarySearchTree:
#
#     def __init__(self,root=None):
#         self.root = root


class BinaryTreeNode:

    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

    def _eq__ (self, root1,root2):

        if root1 is None and root2 is None:
            return True

        if root1 is None or root2 is None:
            return False

        if root1.value != root2.value:
            return False

        if root1.left == root2.left and root1.right == root2.right:
            return True

        return False