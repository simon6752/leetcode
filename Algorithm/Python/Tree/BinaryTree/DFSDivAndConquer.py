

"use divide and conquer"

"divide into sub task, then merge the returned results from sub task"

"traverse use recursion is different: use parameter, follow the traverse and get result, need to update parameter"


def preorderDivAndConquer(tree):

    result = []

    if tree is None:
        return result

    result1 = []
    result2 = []
    if tree.left is not None:
        result1 = preorderDivAndConquer(tree.left)

    if tree.right is not None:
        result2 = preorderDivAndConquer(tree.right)

    result.append(tree.value)
    result.extend(result1)
    result.extend(result2)

    return result

def inorderDivAndConquer(tree):
    result = []
    if tree is None:
        return result

    result1 = []
    result2 = []

    if tree.left is not None:
        result1 = inorderDivAndConquer(tree.left)

    if tree.right is not None:
        result2 = inorderDivAndConquer(tree.right)

    result.extend(result1)
    result.append(tree.value)
    result.extend(result2)

    return result

def postorderDivAndConquer(tree):

    result = []

    if tree is None:
        return result

    result1 = []
    result2 = []

    if tree.left is not None:
        result1 = postorderDivAndConquer(tree.left)

    if tree.right is not None:
        result2 = postorderDivAndConquer(tree.right)

    result.extend(result1)
    result.extend(result2)
    result.append(tree.value)

    return result

