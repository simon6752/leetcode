# Given a binary tree, determine if it is height-balanced.
#
# For this problem, a height-balanced binary tree is defined as:
#
# a binary tree in which the depth of the two subtrees of every node never differ by more than 1.
#
# Example 1:
#
# Given the following tree [3,9,20,null,null,15,7]:
#
#     3
#    / \
#   9  20
#     /  \
#    15   7
# Return true.
#
# Example 2:
#
# Given the following tree [1,2,2,3,3,null,null,4,4]:
#
#        1
#       / \
#      2   2
#     / \
#    3   3
#   / \
#  4   4
# Return false.


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode


class Solution:
    """
    @param root: The root of binary tree.
    @return: True if this Binary tree is Balanced, or false.
    """

    def isBalanced(self, root):
        # write your code here

        "use divide and conquer, return height and isBalanced, recursion"

        return self.isBalancedHelper(root).isBalanced

    def isBalancedHelper(self, root):

        height = 0
        isBalanced = True

        if root is None:
            height = 0
            isBalanced = True
            return resulttype(height, isBalanced)

        result1 = self.isBalancedHelper(root.left)
        isBalanced = isBalanced and result1.isBalanced

        result2 = self.isBalancedHelper(root.right)
        isBalanced = isBalanced and result2.isBalanced

        if result1.height - result2.height > 1 or result1.height - result2.height < -1:
            isBalanced = False

        height = max(result1.height, result2.height) + 1

        return resulttype(height, isBalanced)


class resulttype:

    def __init__(self, height, isBalanced):
        self.height = height
        self.isBalanced = isBalanced


# [1,2,2,3,3,null,null,4,4]

tree = BinaryTreeNode(1)

tree.left = BinaryTreeNode(2)
tree.right = BinaryTreeNode(2)
tree.left.left = BinaryTreeNode(3)
tree.left.right = BinaryTreeNode(3)
tree.left.left.left = BinaryTreeNode(4)
tree.left.left.right = BinaryTreeNode(4)

assert Solution().isBalanced(tree) == False
