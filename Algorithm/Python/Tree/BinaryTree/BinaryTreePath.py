# 480. Binary Tree Paths

# Given a binary tree, return all root-to-leaf paths.
#
# Have you met this question in a real interview?
# Example
# Given the following binary tree:
#
#    1
#  /   \
# 2     3
#  \
#   5
# All root-to-leaf paths are:
#
# [
#   "1->2->5",
#   "1->3"
# ]
# Tags
# Binary Tree Binary Tree Traversal Facebook Google Apple


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: the root of the binary tree
    @return: all root-to-leaf paths
    """

    def binaryTreePaths(self, root):
        # write your code here
        "use parameter, DFS traverse recursion, record path"

        if root is None:
            return []
        result = []
        curPath = []
        self.binaryTreePathsHelper(root, result, curPath)
        return result

    def listToPath(self, path):

        if path is None or len(path) == 0:
            return ""

        elif len(path) == 1:
            return str(path[0])
        else:
            result = str(path[0])
            for i in range(1, len(path)):
                result = result + "->" + str(path[i])
            return result

    def binaryTreePathsHelper(self, root, result, curPath):
        "we do not allow root is None, but have to handle"

        if root is None:
            return
        "update current path to include current node value"
        curPath.append(root.val)

        if root.left is None and root.right is None:
            "reach leaf, add curPath to result"
            result.append(self.listToPath(curPath))

        if root.left is not None:
            self.binaryTreePathsHelper(root.left, result, list(curPath))

        if root.right is not None:
            self.binaryTreePathsHelper(root.right, result, list(curPath))
