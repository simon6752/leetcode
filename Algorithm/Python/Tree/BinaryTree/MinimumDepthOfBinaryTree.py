
# Definition of TreeNode:
# class TreeNode:
#     def __init__(self, val):
#         self.val = val
#         self.left, self.right = None, None
#
import sys

from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode


class Solution:
    """
    @param root: The root of binary tree
    @return: An integer
    """

    def minDepth(self, root):
        # write your code here
        "do a level order iterative solution, return if first reach a leaf"
        "we can do it recursively, but that may cost too much if we can stop early"

        if root is None:
            return 0

        node = root

        queue = []
        minLevel = sys.maxsize

        queue.append([1, node])

        while len(queue) > 0:

            " update current level"
            element = queue.pop()
            level = element[0]
            node = element[1]

            if node.left is None and node.right is None:
                "find a leaf, if it is less than current minimum level, update it"
                if level < minLevel:
                    minLevel = level

            if node.left is not None:
                queue.append([level + 1, node.left])

            if node.right is not None:
                queue.append([level + 1, node.right])

        return minLevel


tree = BinaryTreeNode(0)
# {0,0,0,0,#,#,0,0,#,#,0,0,#,#,0,0,#,#,0,0,#,#,0,0,#,#,0,#,#,0}
#
#                                      0
#                       0                                   0
#                  0        #                     #                      0
#               0     #    #    0           0          #           #                    0
#             0  # #    0 0  # #    0     0   #       #   0     #     #             0


# assert Solution().minDepth(tree) == 8


class Solution2:
    """
    @param root: The root of binary tree
    @return: An integer
    """

    def minDepth(self, root):
        # write your code here
        "do a level order iterative solution, return if first reach a leaf"
        "we can do it recursively, but that may cost too much if we can stop early"
        "four scenario depends on left and right node None or not"

        if root is None:
            return 0

        "if leaf node find"
        if root.left is None and root.right is None:
            return 1

        "if left is None right not None, depends on right to find leaf"
        if root.left is None:
            return 1 + self.minDepth(root.right)

        "if left is not None, right is None, depends on left to find leaf"
        if root.right is None:
            return 1 + self.minDepth(root.left)

        "both left right not None, need to compare which one can find leaf"
        return 1 + min(self.minDepth(root.left), self.minDepth(root.right))