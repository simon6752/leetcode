# 175. Invert Binary Tree
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Invert a binary tree.
#
# Have you met this question in a real interview?
# Example
#   1         1
#  / \       / \
# 2   3  => 3   2
#    /       \
#   4         4
# Challenge
# Do it in recursion is acceptable, can you do it without recursion?


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: a TreeNode, the root of the binary tree
    @return: nothing
    """

    def invertBinaryTree(self, root):
        # write your code here
        "do it iteratively"

        "do a post/pre order traverse iteratively and exchange left and right child at action step"

        if root is None:
            return root

        node = root

        stack = []

        stack.append(node)

        while len(stack) > 0:

            node = stack.pop()

            "do something: exchange left and right"

            temp = node.left
            node.left = node.right
            node.right = temp

            "push right child to stack"

            if node.right is not None:
                stack.append(node.right)

            "push left child to stack"

            if node.left is not None:
                stack.append(node.left)

        return root