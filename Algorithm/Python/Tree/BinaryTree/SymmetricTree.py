# 101. Symmetric Tree
# DescriptionHintsSubmissionsDiscussSolution
# Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
#
# For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
#
#     1
#    / \
#   2   2
#  / \ / \
# 3  4 4  3
# But the following [1,2,2,null,3,null,3] is not:
#     1
#    / \
#   2   2
#    \   \
#    3    3
# Note:
# Bonus points if you could solve it both recursively and iteratively.


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        "check result of level order traverse, result must be symmetric at each level, need to put None in it"

        if root is None:
            return True

        return self.isMirror(root.left, root.right)

    def isMirror(self, tree1, tree2):

        if tree1 is None and tree2 is None:
            return True

        if (tree1 is None and tree2 is not None) or (tree1 is not None and tree2 is None):
            return False

        if tree1.val != tree2.val:
            return False

        return self.isMirror(tree1.left, tree2.right) and self.isMirror(tree1.right, tree2.left)


class Solution2:
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        "do it iteratively this time, use stack, every time pop and push two nodes, push their child in an order that every continous two nodes should have the same value and mirror child nodes"

        if root is None:
            return True

        node = root

        stack = []

        stack.append(root.left)
        stack.append(root.right)

        while len(stack) > 0:

            node1 = stack.pop()
            node2 = stack.pop()

            "one of then is not None, not symmetric"
            if node1 is None and node2 is not None:
                return False
            if node1 is not None and node2 is None:
                return False
            "both None, no idea yet, continue"
            if node1 is None and node2 is None:
                continue
            "value not equal, not symmetric"
            if node1.val != node2.val:
                return False

            "append node in an order that two concequent nodes should be the same value with mirror childs"
            stack.append(node1.left)
            stack.append(node2.right)
            stack.append(node1.right)
            stack.append(node2.left)

        return True
