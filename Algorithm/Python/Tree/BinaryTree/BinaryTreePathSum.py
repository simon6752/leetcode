# 376. Binary Tree Path Sum
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a binary tree, find all paths that sum of the nodes in the path equals to a given number target.
#
# A valid path is from root node to any of the leaf nodes.
#
# Have you met this question in a real interview?
# Example
# Given a binary tree, and target = 5:
#
#      1
#     / \
#    2   4
#   / \
#  2   3
# return
#
# [
#   [1, 2, 2],
#   [1, 4]
# ]
# Tags
# Binary Tree Binary Tree Traversal


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode


class Solution:
    """
    @param: root: the root of binary tree
    @param: target: An integer
    @return: all valid paths
    """

    def binaryTreePathSum(self, root, target):
        # write your code here
        "use recursive traversal, record result, current path, and current sum, target"

        if root is None:
            return []

        result = []
        self.binaryTreePathSumHelper(root, target, result, [], 0)
        return result

    def binaryTreePathSumHelper(self, root, target, result, curPath, curSum):

        "update current sum and current path"
        curSum = curSum + root.value
        curPath.append(root.value)

        if root.left is None and root.right is None:
            "stop at leaf node"
            if curSum == target:
                result.append(curPath)

            return

        if root.left is not None:
            "do not go deeper for None"
            self.binaryTreePathSumHelper(root.left, target, result, list(curPath), curSum)
        if root.right is not None:
            self.binaryTreePathSumHelper(root.right, target, result, list(curPath), curSum)


tree = BinaryTreeNode(1)

tree.left = BinaryTreeNode(2)
tree.right = BinaryTreeNode(4)
tree.left.left = BinaryTreeNode(2)
tree.left.right = BinaryTreeNode(3)

assert Solution().binaryTreePathSum(tree, 5) == [[1, 2, 2], [1, 4]]
