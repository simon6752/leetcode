from Algorithm.Python.Tree.BinarySearchTree.BinarySearchTree import BinarySearchTreeNode
from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode
from Algorithm.Python.Tree.BinaryTree.DFSDivAndConquer import *
from Algorithm.Python.Tree.BinaryTree.DFSTraverse import *

# create a binary search tree with value:
#
#             10
#
#         7          13
#
#      4     9    11    14
#    1   5  8


tree = BinaryTreeNode(10)
tree.left = BinaryTreeNode(7)
tree.right = BinaryTreeNode(13)
tree.left.left = BinaryTreeNode(4)
tree.left.right = BinaryTreeNode(9)
tree.left.left.left = BinaryTreeNode(1)
tree.left.left.right = BinaryTreeNode(5)
tree.left.right.left = BinaryTreeNode(8)
tree.right.left = BinaryTreeNode(11)
tree.right.right = BinaryTreeNode(14)

print("preorder traverse recursively")
result = []
preorderRecursive(tree, result)
print(result)

print("preorder traverse iteratively")

result = []
preorderIterative(tree, result)
print(result)

print("preorder divide and conquer")
print(preorderDivAndConquer(tree))


print("inorder traverse recursively")

result = []
inorderRecursive(tree, result)
print(result)

print("inorder traverse iteratively")
result = []
inorderIterative(tree, result)
print(result)

print("inorder divide and conquer")
print(inorderDivAndConquer(tree))

print("postorder traverse recursively")
result = []
postorderRecursive(tree, result)
print(result)

print("postorder traverse iteratively")
result = []
postorderIterative(tree, result)
print(result)

print("postorder divide and conquer")
print(postorderDivAndConquer(tree))


print("levelorder traverse iteratively")
result = []
levelOrderIterativeOneQueue(tree, result)
print(result)

print("levelorder traverse iteratively")
print(levelOrderIterativeOneQueue2(tree))

print("levelorder traverse recursively")
result = []
levelorderrecursive(tree, result)
print(result)

tree2 = BinarySearchTreeNode(1)
list1 = [8, 7, 3, 5, 2, 11, 15, 12, 10, 4, 14, 9, 6, 13]
for i in list1:
    tree2.insert(tree2, i)

print("inorder traverse recursively, should be ascending order")
result = []
inorderRecursive(tree2, result)
print(result)

print("inorder divide and conquer, should be ascending order")
print(inorderDivAndConquer(tree2))

print("inorder traverse iteratively, should be ascending order")
result = []
inorderIterative(tree2, result)
print(result)
