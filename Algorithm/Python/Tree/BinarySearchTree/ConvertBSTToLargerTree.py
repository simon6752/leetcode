# Given a Binary Search Tree (BST), convert it to a Greater Tree such that every key of the original BST is changed to the original key plus sum of all keys greater than the original key in BST.
#
# Have you met this question in a real interview?
# Example
# Given a binary search Tree `{5,2,13}｀:
#
#               5
#             /   \
#            2     13
# Return the root of new tree
#
#              18
#             /   \
#           20     13
# Tags
# Binary Search Tree Amazon Binary Tree


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: the root of binary tree
    @return: the new root
    """

    def convertBST(self, root):
        # write your code here
        "do a right -> parent -> left in-order traversal, set node.val = node.val + previous sum"

        self.presum = 0

        if root is None:
            return root

        self.convertBSTHelper(root)

        return root

    def convertBSTHelper(self, root):

        if root is None:
            return

        if root.right is not None:
            self.convertBSTHelper(root.right)

        self.presum = self.presum + root.val

        root.val = self.presum

        if root.left is not None:
            self.convertBSTHelper(root.left)
