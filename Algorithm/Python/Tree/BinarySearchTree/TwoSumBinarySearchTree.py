# 689. Two Sum - BST edtion
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a binary search tree and a number n, find two numbers in the tree that sums up to n.
#
#  Notice
# Without any extra space.
#
# Have you met this question in a real interview?
# Example
# Given a binary search tree:
#
#     4
#    / \
#   2   5
#  / \
# 1   3
# and a number n = 3
# return [1, 2] or [2, 1]
#
# Tags
# Binary Search Tree Google


"two sum, in-order traverse. assending and descending order use two pointers.need to stop they cross each other"

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param: : the root of tree
    @param: : the target sum
    @return: two numbers from tree which sum is n
    """

    def twoSum(self, root, n):

        result = []

        if root is None:
            return result

        "find minimum and maximum"

        self.inorder(root.right, n, result)
        return result

    def inorder(self, rootleft, rootright, n, result):

        "we assume rootleft and rootright is not none"

        if rootleft.val + rootright.val < n:

            "need to move left pointer to right  "
            if rootleft.right is not None:
                self.inorder(rootleft.right, rootright, n, result)

        elif rootleft.val + rootright.val == n:
            "find it"
            result.append(rootleft.val)
            result.append(rootright.val)
            return
        else:
            "move right pointer to left"
            if rootright.left is not None:
                self.inorder(rootleft, rootright.right, n, result)
