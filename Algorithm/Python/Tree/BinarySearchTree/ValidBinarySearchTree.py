class Solution:
    """
    @param root: The root of binary tree.
    @return: True if the binary tree is BST, or false
    """

    def isValidBST(self, root):
        # write your code here

        "use recursion"
        "need to check: 1. left and right child are BST 2. max of left child and min of right child"
        if root is None:
            return True

        return self.isValidBSTHelper(root).isBST

    def isValidBSTHelper(self, root):

        isBST = True

        resulttype1 = None
        if root.left is not None:
            resulttype1 = self.isValidBSTHelper(root.left)

        resulttype2 = None
        if root.right is not None:
            resulttype2 = self.isValidBSTHelper(root.right)

        "find out min and max"

        if resulttype1 is not None and resulttype1.max_val >= root.val:
            isBST = False

        if resulttype2 is not None and resulttype2.min_val <= root.val:
            isBST = False

        min_val = root.val
        max_val = root.val
        if resulttype1 is not None:
            isBST = isBST and resulttype1.isBST
            min_val = min(resulttype1.min_val, min_val)
            max_val = max(resulttype1.max_val, max_val)
        if resulttype2 is not None:
            isBST = isBST and resulttype2.isBST
            min_val = min(resulttype2.min_val, min_val)
            max_val = max(resulttype2.max_val, max_val)

        return resulttype(min_val, max_val, isBST)


class resulttype:

    def __init__(self, min_val, max_val, isBST):
        self.min_val = min_val
        self.max_val = max_val
        self.isBST = isBST