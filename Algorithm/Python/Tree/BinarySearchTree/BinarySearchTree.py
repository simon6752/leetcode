# class BinarySearchTree:
#
#     def __init__(self,root=None):
#         self.root = root
from Algorithm.Python.Tree.BinaryTree.BinaryTree import BinaryTreeNode
from Algorithm.Python.Tree.BinaryTree.DFSTraverse import levelorderrecursive


class BinarySearchTreeNode(BinaryTreeNode):

    def insert(self, node, value):

        if node is None:
            node = BinarySearchTreeNode(value)

        else:
            if value > node.value:
                if node.right is None:
                    node.right = BinarySearchTreeNode(value)
                else:
                    self.insert(node.right, value)
            elif value == node.value:
                "we allow left child value equal to parent value"
                newNode = BinarySearchTreeNode(value)
                newNode.left = node.left
                node.left = newNode
            else:
                if node.left is None:
                    node.left = BinarySearchTreeNode(value)
                else:
                    self.insert(node.left, value)

    def __str__(self):

        result = []
        levelorderrecursive(self, result)
        return str(result)
