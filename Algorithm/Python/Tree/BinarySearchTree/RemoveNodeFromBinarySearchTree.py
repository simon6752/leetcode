# TODO 87. Remove Node in Binary Search Tree
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Given a root of Binary Search Tree with unique value for each node. Remove the node with given value. If there is no such a node with given value in the binary search tree, do nothing. You should keep the tree still a binary search tree after removal.
#
# Have you met this question in a real interview?
# Example
# Given binary search tree:
#
#     5
#    / \
#   3   6
#  / \
# 2   4
# Remove 3, you can either return:
#
#     5
#    / \
#   2   6
#    \
#     4
# or
#
#     5
#    / \
#   4   6
#  /
# 2
# Tags
# Binary Search Tree LintCode Copyright


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.value = val
        self.left, self.right = None, None
"""
from Algorithm.Python.Tree.BinarySearchTree.BinarySearchTree import BinarySearchTreeNode


class Solution:
    """
    @param: root: The root of the binary search tree.
    @param: value: Remove the node with given value.
    @return: The root of the binary search tree after removal.
    """

    def removeNode(self, root, value):
        # write your code here
        "first find the value, then either pull up left node or right node"

        if root is None:
            return root

        node = root
        prev = None

        while node is not None:

            if node.value == value:
                break

            prev = node
            if node.value > value:
                node = node.left
            elif node.value < value:
                node = node.right

        if node is None:
            "not found"
            return root

        if prev is None:
            "means we got case {2}, 2, need to return"
            if node.left is None:
                return node.right
            if node.right is None:
                return node.left

        if node.left is None and node.right is None:
            "the value is one a leaf"
            prev.left = None
            prev.right = None
            return root

        if node.left is None:
            if prev.left == node:
                prev.left = node.right
            else:
                prev.right = node.right

            return root

        if node.right is None:
            if prev.left == node:
                prev.left = node.left
            else:
                prev.right = node.left
            return root

        "now both left and right is not None"

        "let left child right most add right child as right child, or let left most child add left child as left child"

        if prev.left == node:
            prev.left = node.left

        if prev.right == node:
            prev.right = node.left

        "put left child right most child's right child as current node's right child"

        temp = node.left

        while temp.right != None:
            temp = temp.right

        temp.right = node.right

        return root


tree = BinarySearchTreeNode(2)
print(tree)
print(Solution().removeNode(tree, 2))

tree = BinarySearchTreeNode(1)
tree.right = BinarySearchTreeNode(2)
print(tree)
print(Solution().removeNode(tree, 1))
