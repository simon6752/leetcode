#  86. Binary Search Tree Iterator
# Description
# Design an iterator over a binary search tree with the following rules:
#
# Elements are visited in ascending order (i.e. an in-order traversal)
# next() and hasNext() queries run in O(1) time in average.
# Have you met this question in a real interview?
# Example
# For the following binary search tree, in-order traversal by using iterator is [1, 6, 10, 11, 12]
#
#    10
#  /    \
# 1      11
#  \       \
#   6       12
# Challenge
# Extra memory usage O(h), h is the height of the tree.
#
# Super Star: Extra memory usage O(1)

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

Example of iterate a tree:
iterator = BSTIterator(root)
while iterator.hasNext():
    node = iterator.next()
    do something for node
"""


# analysis
# since we want to get next in O(1) time, so next must already been saved in some way (heap, stack, queue, ...)
# next() is the element that will be visited next. in in-order visit, it is the root element. so everytime, we save all node to current node that has no left child, in a stack. when next(), we pop() from the stack, then keep pushing nodes in right tree until we find a node without left child


class BSTIterator:
	"""
	@param: root: The root of binary tree.
	"""
	
	def __init__ (self, root):
		# do intialization if necessary
		self.root = root
		self.stack = []
		node = root
		# save all nodes that left child is not None, then we reached most left child
		while node is not None and node.left is not None:
			self.stack.append (node)
			node = node.left
		
		if node is not None and node.left is None:
			self.stack.append (node)
	
	"""
	@return: True if there has next node, or false
	"""
	
	def hasNext (self):
		# write your code here
		# when stack is empty, there is no next
		return len (self.stack) != 0
	
	"""
	@return: return next node
	"""
	
	def next (self):
		# write your code here
		if not self.hasNext ():
			return None
		
		nextNode = self.stack.pop ()
		# before return this next node, we need to add all the way down to most left node in the right child to the stack
		node = nextNode.right
		
		while node is not None and node.left is not None:
			self.stack.append (node)
			node = node.left
		
		if node is not None and node.left is None:
			self.stack.append (node)
		
		return nextNode


root = TreeNode(-1)
iterator = BSTIterator(root)
result = []
while iterator.hasNext():
	nextNode = iterator.next()
	if nextNode is not None:
		print(nextNode.val)
		result.append(nextNode.val)
		
assert result == [-1]

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

Example of iterate a tree:
iterator = BSTIterator(root)
while iterator.hasNext():
    node = iterator.next()
    do something for node
"""


# analysis
# since we want to get next in O(1) time, so next must already been saved in some way (heap, stack, queue, ...)
# next() is the element that will be visited next. in in-order visit, it is the root element. so everytime, we save all node to current node that has no left child, in a stack. when next(), we pop() from the stack, then keep pushing nodes in right tree until we find a node without left child


class BSTIterator2:
	"""
	@param: root: The root of binary tree.
	"""
	
	def __init__ (self, root):
		# do intialization if necessary
		self.root = root
		self.stack = []
		node = root
		# save all nodes that left child is not None, then we reached most left child
		while node is not None:
			self.stack.append (node)
			node = node.left
	
	"""
	@return: True if there has next node, or false
	"""
	
	def hasNext (self):
		# write your code here
		# when stack is empty, there is no next
		return len (self.stack) != 0
	
	"""
	@return: return next node
	"""
	
	def next (self):
		# write your code here
		if not self.hasNext ():
			return None
		
		nextNode = self.stack.pop ()
		# before return this next node, we need to add all the way down to most left node in the right child to the stack
		node = nextNode.right
		
		while node is not None:
			self.stack.append (node)
			node = node.left
		
		return nextNode


