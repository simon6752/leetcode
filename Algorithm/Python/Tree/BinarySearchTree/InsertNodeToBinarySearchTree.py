# """
# Definition of TreeNode:
# class TreeNode:
#     def __init__(self, val):
#         self.val = val
#         self.left, self.right = None, None
# """
#
#
from Algorithm.Python.Tree.BinarySearchTree.BinarySearchTree import BinarySearchTreeNode
from Algorithm.Python.Tree.BinaryTree.DFSTraverse import levelorderrecursive

"recursive solution"


class Solution:
    """
    @param: root: The root of the binary search tree.
    @param: node: insert this node into the binary search tree
    @return: The root of the new binary search tree.
    """

    def insertNode(self, root, node):
        # write your code here

        "need to keep the BST still BST"

        if root is None:
            return node

        if node is None:
            return root

        if node.value < root.value:
            self.left = self.insertNode(root.left, node)
        else:
            self.right = self.insertNode(root.right, node)

        return root


tree = BinarySearchTreeNode(2)
tree.left = BinarySearchTreeNode(1)
result = []
levelorderrecursive(Solution().insertNode(tree, BinarySearchTreeNode(3)), result)
print(result)


class Solution2:

    def insertNode(self, root, node):
        "use iteration"

        "if small, go left, otherwise go right, until find a leaf and insert"

        if root is None or node is None:
            return root

        head = root
        prev = None

        while head is not None:

            prev = head

            if node.value > head.value:
                head = head.right
            else:
                head = head.left

        if node.value > prev.value:
            prev.right = node
        else:
            prev.left = node

        return root
