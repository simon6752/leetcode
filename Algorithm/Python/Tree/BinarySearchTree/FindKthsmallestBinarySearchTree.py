#
from Algorithm.Python.Tree.BinarySearchTree.BinarySearchTree import BinarySearchTreeNode


class Solution:
    def kthSmallest(self, root, k):
        # write your code here
        "do in-order traverse recursion, return kth"

        if root is None:
            return None

        self.count = 0
        self.kthSmallestHelper(root, k)

        return self.result

    def kthSmallestHelper(self, root, k):

        if root is None:
            return

        if root.left is not None:
            self.kthSmallestHelper(root.left, k)

        self.count = self.count + 1
        if self.count == k:
            "find it"
            self.result = root.val
            return

        if root.right is not None and self.count < k:
            self.kthSmallestHelper(root.right, k)


tree = BinarySearchTreeNode(1)
tree.insert(tree, 2)

assert Solution().kthSmallest(tree, 2) == 2
