#  132. Word Search II
# Description
# Given a matrix of lower alphabets and a dictionary. Find all words in the dictionary that can be found in the matrix. A word can start from any position in the matrix and go left/right/up/down to the adjacent position. One character only be used once in one word.
#
# Have you met this question in a real interview?
# Example
# Given matrix:
#
# doaf
# agai
# dcan
# and dictionary:
#
# {"dog", "dad", "dgdg", "can", "again"}
#
# return {"dog", "dad", "can", "again"}
#
#
# dog:
# doaf
# agai
# dcan
# dad:
#
# doaf
# agai
# dcan
# can:
#
# doaf
# agai
# dcan
# again:
#
# doaf
# agai
# dcan
# Challenge
# Using trie to implement your algorithm.


class Solution:
	"""
	@param board: A list of lists of character
	@param words: A list of string
	@return: A list of string
	"""
	
	def wordSearchII (self, board, words):
		# write your code here
		# in word search I, we use dfs backtracking. we have a target to compare there. In this case, we have a dictionary to search whether current string is in it or is a prefix of any string in dictionary. So Trie is best solution
		
		if board is None or len (board) == 0 or len (board [0]) == 0 or words is None or len (words) == 0:
			return []
		
		n = len (board)
		m = len (board [0])
		
		trie = Trie ()
		
		for i in range (0, len (words)):
			# put all words in trie, so we can find wether a prefix is in a trie or not
			trie.insert (words [i])
		
		result = []
		resultset = set ()
		visited = []
		
		for i in range (n):
			visited.append ([])
			for j in range (m):
				visited [i].append (False)
		
		for i in range (n):
			for j in range (m):
				currentStr = ""
				self.dfs (board, trie, resultset, currentStr, visited, i, j)
		
		# convert result set to result list
		for key in resultset:
			result.append (key)
		
		return result
	
	def dfs (self, board, trie, resultset, currentStr, visited, i, j):
		
		# validatin. if i or j cross border, return
		
		n = len (board)
		m = len (board [0])
		if i < 0 or i > n - 1 or j < 0 or j > m - 1:
			return
		
		# even if position(i,j) is valid, check if already visited
		if visited [i] [j]:
			return
		
		# we need to decide whether we found a currentStr in trie, add to result. if currentStr is a prefix in trie, we can continue, otherwise, early stop
		
		currentStr += board [i] [j]
		# currentStr updated to add board[i][j], check
		
		if not trie.startWith (currentStr):
			# current string is not a prefix in trie, early stop
			return
		
		if trie.search (currentStr):
			# use hash set, so no duplicate result will be added
			resultset.add (currentStr)
		
		# continue search in all four directions
		
		visited [i] [j] = True
		# need to follow up, left, lower, right, otherwise might end in long and useless search when there are lots of identity cell letter to visit
		self.dfs (board, trie, resultset, currentStr, visited, i - 1, j)
		self.dfs (board, trie, resultset, currentStr, visited, i, j - 1)
		self.dfs (board, trie, resultset, currentStr, visited, i + 1, j)
		self.dfs (board, trie, resultset, currentStr, visited, i, j + 1)
		
		# reset visited matrix, since current i, j visited is only meaningful as parents in all its neighbors. so need to reset once it is no longer in path
		visited [i] [j] = False


class TrieNode:
	def __init__ (self, value = False):
		# the value of TrieNode mark whether there is a word end to this node
		self.value = value
		self.next = [None] * 26


class Trie:
	def __init__ (self):
		
		self.root = TrieNode ()
	
	# insert
	def insert (self, string):
		# insert string into Trie
		self.insertHelper (self.root, string)
	
	def insertHelper (self, node, string):
		if string is None or len (string) == 0:
			# done, mark a word
			node.value = True
			return
		index = ord (string [0]) - ord ("a")
		if node.next [index] is None:
			# no path here
			node.next [index] = TrieNode ()
		
		self.insertHelper (node.next [index], string [1:])
	
	# search
	def search (self, string):
		# check whether a string is in a Trie
		return self.searchHelper (self.root, string)
	
	def searchHelper (self, node, string):
		if string is None or len (string) == 0:
			# reached the end
			return node.value
		
		index = ord (string [0]) - ord ("a")
		if node.next [index] is None:
			return False
		return self.searchHelper (node.next [index], string [1:])
	
	# begin with: search whether a prefix exist in the Trie(different from find a whole word )
	
	def startWith (self, prefix):
		return self.startWithHelper (self.root, prefix)
	
	def startWithHelper (self, node, prefix):
		
		if prefix is None or len (prefix) == 0:
			# if we can reach this node while prefix length is 0, we are done
			return True
		
		index = ord (prefix [0]) - ord ("a")
		if node.next [index] is None:
			# cannot continue
			return False
		else:
			return self.startWithHelper (node.next [index], prefix [1:])


trie1 = Trie()
trie1.insert("lintcode")
assert trie1.search("lintcode") is True
assert trie1.search("lint") is False
trie1.insert("lint")
assert trie1.search("lint") is True

assert trie1.startWith("lin") is True

assert sorted(Solution().wordSearchII(["doaf","agai","dcan"],["dog","dad","dgdg","can","again"])) == sorted(["again","can","dad","dog"])

# time limit exceeded for this case
assert sorted(Solution().wordSearchII(["aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","aaaaaaaaaaaaaaaaaaaaaaaaaaaaab"],["baaaaaaaaaaaaa","a","aa","aaaa","aaaax","abaaabbaz"])) == sorted(["a","aa","aaaa","baaaaaaaaaaaaa"])

#  add a updated version that no time limit exceeded


class Solution2:
	"""
	@param board: A list of lists of character
	@param words: A list of string
	@return: A list of string
	"""
	
	def wordSearchII (self, board, words):
		# write your code here
		# in word search I, we use dfs backtracking. we have a target to compare there. In this case, we have a dictionary to search whether current string is in it or is a prefix of any string in dictionary. So Trie is best solution
		
		if board is None or len (board) == 0 or len (board [0]) == 0 or words is None or len (words) == 0:
			return []
		
		n = len (board)
		m = len (board [0])
		
		trie = Trie2 ()
		
		for i in range (0, len (words)):
			# put all words in trie, so we can find wether a prefix is in a trie or not
			trie.insert (words [i])
		
		result = []
		resultset = set ()
		visited = []
		
		for i in range (n):
			visited.append ([])
			for j in range (m):
				visited [i].append (False)
		
		for i in range (n):
			for j in range (m):
				currentStr = ""
				self.dfs (board, trie, resultset, currentStr, visited, i, j)
		
		# convert result set to result list
		for key in resultset:
			result.append (key)
		
		return result
	
	def dfs (self, board, trie, resultset, currentStr, visited, i, j):
		
		# validatin. if i or j cross border, return
		
		n = len (board)
		m = len (board [0])
		if i < 0 or i > n - 1 or j < 0 or j > m - 1:
			return
		
		# even if position(i,j) is valid, check if already visited
		if visited [i] [j]:
			return
		
		# we need to decide whether we found a currentStr in trie, add to result. if currentStr is a prefix in trie, we can continue, otherwise, early stop
		
		currentStr += board [i] [j]
		# currentStr updated to add board[i][j], check
		
		if not trie.startWith (currentStr):
			# current string is not a prefix in trie, early stop
			return
		
		if trie.search (currentStr):
			# use hash set, so no duplicate result will be added
			resultset.add (currentStr)
			# delete it from trie after we already found it
			trie.delete (currentStr)
		
		# continue search in all four directions
		
		visited [i] [j] = True
		# need to follow up, left, lower, right, otherwise might end in long and useless search when there are lots of identity cell letter to visit
		self.dfs (board, trie, resultset, currentStr, visited, i - 1, j)
		self.dfs (board, trie, resultset, currentStr, visited, i, j - 1)
		self.dfs (board, trie, resultset, currentStr, visited, i + 1, j)
		self.dfs (board, trie, resultset, currentStr, visited, i, j + 1)
		
		# reset visited matrix, since current i, j visited is only meaningful as parents in all its neighbors. so need to reset once it is no longer in path
		visited [i] [j] = False


class TrieNode2:
	def __init__ (self, value = False):
		# the value of TrieNode mark whether there is a word end to this node
		self.value = value
		self.next = [None] * 26


class Trie2:
	def __init__ (self):
		
		self.root = TrieNode2()
	
	# insert
	def insert (self, string):
		# insert string into Trie
		self.insertHelper (self.root, string)
	
	def insertHelper (self, node, string):
		if string is None or len (string) == 0:
			# done, mark a word
			node.value = True
			return
		index = ord (string [0]) - ord ("a")
		if node.next [index] is None:
			# no path here
			node.next [index] = TrieNode2 ()
		
		self.insertHelper (node.next [index], string [1:])
	
	# search
	def search (self, string):
		# check whether a string is in a Trie
		return self.searchHelper (self.root, string)
	
	def searchHelper (self, node, string):
		if string is None or len (string) == 0:
			# reached the end
			return node.value
		
		index = ord (string [0]) - ord ("a")
		if node.next [index] is None:
			return False
		return self.searchHelper (node.next [index], string [1:])
	
	# begin with: search whether a prefix exist in the Trie(different from find a whole word )
	
	def startWith (self, prefix):
		return self.startWithHelper (self.root, prefix)
	
	def startWithHelper (self, node, prefix):
		
		if prefix is None or len (prefix) == 0:
			# if we can reach this node while prefix length is 0, we are done
			return True
		
		index = ord (prefix [0]) - ord ("a")
		if node.next [index] is None:
			# cannot continue
			return False
		else:
			return self.startWithHelper (node.next [index], prefix [1:])
	
	# delete: delete a word from the trie to reduce redundant search. for example, we have word "aaaa", board contains lots of "a". if we delete "aaaa" from trie after we find 1st one in board, it will saves us time
	
	# first find the node and set value to False. if that node still contains non-none child, fine. if all children are none, need to delete it from the trie. delete that node from trie might make its parent also no non-none child. we need to recursively delete until we reach a node with value or non-none child
	# to delete parent, we instead return None for this delete function. this will help parent to use this value for assignment, then do a check and decide what to return.
	
	def delete (self, string):
		
		self.deleteHelper (self.root, string)
	
	def deleteHelper (self, node, string):
		
		if string is None or len (string) == 0:
			# find it
			if node.value is True:
				node.value = False
			
			for i in range (26):
				if node.next [i] is not None:
					return node
				# all child is None, return None
			return None
		
		# continue search and delete
		
		index = ord (string [0]) - ord ("a")
		if node.next [index] is not None:
			# continue search
			node.next [index] = self.deleteHelper (node.next [index], string [1:])
			# need to return original node or None, depends
			# check current node value and all child. if False and all None, return None, otherwise return node
			for i in range (26):
				if node.next [i] is not None:
					return node
				# all None child, now check value
			if node.value is False:
				return None
			else:
				return node
		else:
			# not found
			return None

