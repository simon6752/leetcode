class TrieNode:
    def __init__(self, value=None):
        self.value = value
        "26 links to other nodes"
        self.next = [None] * 26


class  Trie:
    def __init__(self, strings):
        # strings is a list of strings, like ["abc","sea","shell","see"]
        # so we want to put into trie key value pairs: "abc":0, "sea":1,"shell":2,"see":3

        self.root = TrieNode()
        self.length = 0
        "put all string into this root"

        for i in range(0, len(strings)):
            self.put(self.root, strings[i], i)

    def put(self, node, key, val, k=0):
        "put key-value pair into the table, remove key if value is null"
        "put kth value of val into node"
        if node is None:
            node = TrieNode()

        if val is None:
            self.delete(node, key)

        if key is None or len(key) == 0 or len(key) == k:
            return

        if node.next[ord(key[k]) - ord('a')] is None:
            "we need to put it into it"
            if k == len(key) - 1:
                newnode = TrieNode(val)
                self.length += 1
            else:
                newnode = TrieNode()
            node.next[ord(key[k]) - ord('a')] = newnode

        "move on to next"
        self.put(node.next[ord(key[k]) - ord('a')], key, val, k + 1)

    def get(self, node, key, k=0):
        "value for key, None if not found"
        if node is None or key is None or k >= len(key):
            return None

        if node.next[ord(key[k]) - ord("a")] is not None:
            if k == len(key) - 1:
                "reached last character"
                return node.next[ord(key[k]) - ord("a")].value
            return self.get(node.next[ord(key[k]) - ord("a")], key, k + 1)
        else:
            "reached end of trie, but not end of key"
            return None

    def delete(self, node, key, k=0):
        "remove key and its value"
        if node is None or key is None:
            return

        if node.next[ord(key[k]) - ord("a")] is not None:
            "not reached end yet"
            if k == len(key) - 1:
                "reached end of key, found it"
                node.next[ord(key[k]) - ord("a")] = None
                self.length -= 1
            else:
                self.delete(node.next[ord(key[k]) - ord("a")], key, k + 1)

    def contains(self, node, key):
        "check whether key is in the trie"
        return self.get(node, key) is not None

    def keysWithPrefix(self, node, s):
        "return all keys that have prefix s"
        "first, find s, then return keys for this node"

    def longestPrefixOf(self, node, s):
        "TODO return the longest key that is a prefix of s"

    def keysThatMatch(self, node, s):
        "return all the keys that match s, . means any char"

    def size(self):
        "return size of key-value pairs"
        return self.length


    def keys(self, node):
        "return all keys in the trie"

    def isEmpty(self, node):
        "is the table empty"
        return self.size() == 0


strings = ["abc", "sea", "shell", "see"]
trie = Trie(strings)
print(trie.size())
trie.get(trie.root, "abc")  # should return "abc" position in the strings
assert trie.get(trie.root, "abc") == 0
assert trie.get(trie.root, "sea") == 1
assert trie.get(trie.root, "see") == 3
assert trie.get(trie.root, "abd") is None
trie.delete(trie.root, "abc")
assert trie.get(trie.root, "abc") is None
assert trie.size() == 3
trie.put(trie.root, "abd", 4)
assert trie.get(trie.root, "abd") == 4
