# 442. Implement Trie (Prefix Tree)
# Implement a trie with insert, search, and startsWith methods.

# Example
# insert("lintcode")
# search("code")
# >>> false
# startsWith("lint")
# >>> true
# startsWith("linterror")
# >>> false
# insert("linterror")
# search("lintcode)
# >>> true
# startsWith("linterror")
# >>> true


class Trie:
    
    
    def __init__(self):
        # do intialization if necessary
        self.root = Node()
        

    """
    @param: word: a word
    @return: nothing
    """
    def insert(self, word):
        # write your code here
        "start with root, insert into trie"
        self.insertHelper(self.root,word)
        
        
    def insertHelper(self,node,word,k=0):
        
        if node.next[ord(word[k]) - ord("a")] is None:
            node.next[ord(word[k]) - ord("a")] = Node()
        nextnode =node.next[ord(word[k]) - ord("a")]

        if k == len(word)-1:
            "already reach word end, done"
            nextnode.val = True
            return

        self.insertHelper(nextnode,word,k+1)

    """
    @param: word: A string
    @return: if the word is in the trie.
    """
    def search(self, word):
        # write your code here
        return self.searchHelper(self.root,word)
    
    def searchHelper(self,node,word,k=0):
        if k == len(word) :
            "find it"
            return node.val
        
        if node.next[ord(word[k])-ord("a")] is None:
            "cannot go on, not there"
            return False
        
        return self.searchHelper(node.next[ord(word[k])-ord("a")],word,k+1)

    """
    @param: prefix: A string
    @return: if there is any word in the trie that starts with the given prefix.
    """
    def startsWith(self, prefix):
        # write your code here
        return self.startsWithHelper(self.root,prefix,0)  
    
    def startsWithHelper(self,node, prefix,k=0):
        
        if node is None:
            return False
        
        if k < len(prefix):

            "we need to keep moving"
            if node.next[ord(prefix[k]) -ord("a")] is not None:
                if k == len(prefix)-1 :
                    "reached end of prefix"
                    return True
                else:
                    "we can continue move"
                    return self.startsWithHelper(node.next[ord(prefix[k]) -ord("a")],prefix,k+1)
            else:
                "cannot move"
                return False
   

class Node:
        
    def __init__(self,val = False):
        self.next = [None] * 26
        "on each node, we save a value list. the value with key that is the prefix of this node"
        "node value record whether it is a word to this node from root"
        self.val = val


trie= Trie()
trie.insert("lintcode")
assert trie.search("lint") is False
assert trie.startsWith("lint") is True

trie = Trie()
trie.insert("a")
trie.insert("b")
trie.insert("c")
assert trie.startsWith("a") is True
assert trie.search("b") is True
trie.insert("b")
trie.insert("b")
assert trie.search("b") is True