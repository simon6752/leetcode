# 248. Count of Smaller Number
# Give you an integer array (index from 0 to n-1, where n is the size of this array, value from 0 to 10000) and an query list. For each query, give you an integer, return the number of element in the array that are smaller than the given integer.
#
# Example
# For array [1,2,7,8,5], and queries [1,8,5], return [0,4,2]
#
# Challenge
# Could you use three ways to do it.
#
# Just loop
# Sort and binary search
# Build Segment Tree and Search.


class Solution:
    """
    @param A: An integer array
    @param queries: The query list
    @return: The number of element in the array that are smaller that the given integer
    """

    def countOfSmallerNumber(self, A, queries):
        # write your code here
        "build a segment tree[0,10000]. node contain value of count that are between start and end. set it to 0 when build it"

        "then use sgement tree modify to update count of each node, time is O(nlog(10000))"

        root = MySTN(0, 10000, 0).build(-1, 10000)

        for i in range(0, len(A)):
            "there might be duplicates in A, so we need to increase the count"
            root.modify(A[i], root.query(A[i], A[i]) + 1)

        result = []

        for i in range(0, len(queries)):
            "we are looking for smaller than target count"
            result.append(root.query(0, queries[i] - 1))

        return result


class MySTN:
    def __init__(self, start, end, count):
        self.start = start
        self.end = end
        self.count = count
        self.left = None
        self.right = None

    def build(self, start, end):
        "build a segment tree with node value 0"

        if start == end:
            return MySTN(start, end, 0)

        mid = (start + end) // 2
        left = self.build(start, mid)
        right = self.build(mid + 1, end)
        root = MySTN(start, end, 0)
        root.left = left
        root.right = right
        return root

    def query(self, start, end):
        "return the count of specific segment"
        if self.start == start and self.end == end:
            return self.count

        mid = (self.start + self.end) // 2
        if end <= mid:
            "in left side"
            return self.left.query(start, end)
        elif start > mid:
            "in right side"
            return self.right.query(start, end)
        else:
            "partial left and partial right"
            return self.left.query(start, mid) + self.right.query(mid + 1, end)

    def modify(self, index, value):
        "modify specific index to specific value. need to update all the nodes along the way that may be affected"

        if self.start == index and self.end == index:
            self.count = value
            return

        mid = (self.start + self.end) // 2
        if index <= mid:
            "in left side"
            self.left.modify(index, value)
        elif index > mid:
            "in right side"
            self.right.modify(index, value)

        "need to update count of current node after lefr/child is updated"
        self.count = self.left.count + self.right.count


assert Solution().countOfSmallerNumber([1, 2, 3, 4, 5, 6], [1, 2, 3, 4]) == [0, 1, 2, 3]
