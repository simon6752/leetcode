# 202. Segment Tree Query
# For an integer array (index from 0 to n-1, where n is the size of this array), in the corresponding SegmentTree, each node stores an extra attribute max to denote the maximum number in the interval of the array (index from start to end).
import sys

# Design a query method with three parameters root, start and end, find the maximum number in the interval [start, end] by the given root of segment tree.

# Example
# For array [1, 4, 2, 3], the corresponding Segment Tree is:

#                   [0, 3, max=4]
#                  /             \
#           [0,1,max=4]        [2,3,max=3]
#           /         \        /         \
#    [0,0,max=1] [1,1,max=4] [2,2,max=2], [3,3,max=3]
# query(root, 1, 1), return 4

# query(root, 1, 2), return 4

# query(root, 2, 3), return 3

# query(root, 0, 2), return 4

class SegmentTreeNode:
    def __init__(self, start, end, max):
        self.start, self.end, self.max = start, end, max
        self.left, self.right = None, None

"""
Definition of SegmentTreeNode:
class SegmentTreeNode:
    def __init__(self, start, end, max):
        self.start, self.end, self.max = start, end, max
        self.left, self.right = None, None
"""

class Solution:
    """
    @param root: The root of segment tree.
    @param start: start value.
    @param end: end value.
    @return: The maximum number in the interval [start, end]
    """
    def query(self, root, start, end):
        # write your code here
        
        "once we find the right segment, return max value of this node"
        
        if start > end:
            "input invalid"
            return -sys.maxsize
        
        if root.start <= start and root.end >= end:
            "find it"
            return root.max
        else:
            "need to decide whether to go left or right"
            "left is [root.start, (root.start + root.end)//2], right is [(root.start + root.end)//2+1, root.end] "
            "3 cases:  1. end in left, 2. start in right 3. start in left, end in right"
            mid = (root.start + root.end)//2
            if end <= mid:
                "find in left child"
                return self.query(root.left,start,end)
            elif start > mid:
                "find in right child"
                return self.query(root.right,start,end)
            else:
                "need to find max of two segments in left and right child"
                return max(self.query(root.left,start,mid),self.query(root.right,mid+1,end))