# 205. Interval Minimum Number
# Given an integer array (index from 0 to n-1, where n is the size of this array), and an query list. Each query has two integers [start, end]. For each query, calculate the minimum number between index start and end in the given array, return the result list.
#
# Example
# For array [1,2,7,8,5], and queries [(1,2),(0,4),(2,4)], return [2,1,5]
#
# Challenge
# O(logN) time for each query

"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""


class Solution:
    """
    @param A: An integer array
    @param queries: An query list
    @return: The result list
    """

    def intervalMinNumber(self, A, queries):
        # write your code here
        "max, min, sum,interval => segement tree"

        result = []
        if A is None:
            return result
        tree = MySegmentTree.build(0, len(A) - 1, A)
        for i in range(0, len(queries)):
            result.append(MySegmentTree.query(queries[i][0], queries[i][1], tree))

        return result


class MySegmentTree:
    def __init__(self, start, end, minval):
        self.start = start
        self.end = end
        self.minval = minval
        self.left = None
        self.right = None

    @staticmethod
    def build(start, end, A):

        if start == end:
            return MySegmentTree(start, end, A[start])

        mid = (start + end) // 2

        left = MySegmentTree.build(start, mid, A)
        right = MySegmentTree.build(mid + 1, end, A)

        node = MySegmentTree(start, end, min(left.minval, right.minval))

        node.left = left
        node.right = right
        return node

    @staticmethod
    def query(start, end, node):

        "we need to handle what is start < node.start and end > node.end"
        if start <= node.start and end >= node.end:
            return node.minval

        "this should be the start and end of node to get mid of node because only mid of node.start and node.end decides which way to go"
        mid = (node.start + node.end) // 2

        if end <= mid:
            "in left side"
            return MySegmentTree.query(start, end, node.left)
        elif start > mid:
            "in right side"
            return MySegmentTree.query(start, end, node.right)
        else:
            "partial left and partial right"
            return min(MySegmentTree.query(start, mid, node.left), MySegmentTree.query(mid + 1, end, node.right))


assert Solution().intervalMinNumber([1, 2, 7, 8, 5], [(1, 2), (0, 4), (2, 4)]) == [2, 1, 5]
