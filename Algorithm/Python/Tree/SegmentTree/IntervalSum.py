# 206. Interval Sum
# Given an integer array (index from 0 to n-1, where n is the size of this array), and an query list. Each query has two integers [start, end]. For each query, calculate the sum number between index start and end in the given array, return the result list.

# Example
# For array [1,2,7,8,5], and queries [(0,4),(1,2),(2,4)], return [23,9,20]

# Challenge
# O(logN) time for each query

class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""

class Solution:
    """
    @param A: An integer list
    @param queries: An query list
    @return: The result list
    """
    def intervalSum(self, A, queries):
        
        # write your code here
        "store sum from 0 to i"
        
        b = []
        sum = 0
        
        for i in range(0,len(A)):
            sum += A[i]
            b.append(sum)
            
        result = []
        
        for i in range(0,len(queries)):
            result.append(b[queries[i].end]- b[queries[i].start]+ A[queries[i].start])
        
        return result

list1 = []
list1.append(Interval(0,4))
list1.append(Interval(1,2))
list1.append(Interval(2,4))

assert Solution().intervalSum([1,2,7,8,5], list1) == [23,9,20]

"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""


class Solution2:
    """
    @param A: An integer list
    @param queries: An query list
    @return: The result list
    """

    def intervalSum(self, A, queries):
        # write your code here

        "use segment tree build and query this time instead of use list "

        tree = MySegmentTreeNode.build(0, len(A) - 1, A)

        result = []

        for i in range(0, len(queries)):
            result.append(MySegmentTreeNode.query(queries[i].start, queries[i].end, tree))

        return result


class MySegmentTreeNode:

    def __init__(self, start, end, left, right, sum):

        self.start = start
        self.end = end
        self.left = left
        self.right = right
        self.sum = sum

    @staticmethod
    def build(start, end, A):

        "will build a segment tree use start, end"

        if start > end or A is None:
            return None

        if start == end:
            return MySegmentTreeNode(start, end, None, None, A[start])

        mid = (start + end) // 2
        left = MySegmentTreeNode.build(start, mid, A)
        right = MySegmentTreeNode.build(mid + 1, end, A)

        root = MySegmentTreeNode(start, end, left, right, left.sum + right.sum)

        return root

    @staticmethod
    def query(start, end, tree):
        "query a interval [start,end] and return its sum"

        if start == tree.start and end == tree.end:
            return tree.sum

        mid = (tree.start + tree.end) // 2

        if end <= mid:
            "in left"
            return MySegmentTreeNode.query(start, end, tree.left)
        elif start > mid:
            "in right"
            return MySegmentTreeNode.query(start, end, tree.right)
        else:
            return MySegmentTreeNode.query(start, mid, tree.left) + MySegmentTreeNode.query(mid + 1, end, tree.right)


list1 = []
list1.append(Interval(0, 4))
list1.append(Interval(1, 2))
list1.append(Interval(2, 4))

assert Solution2().intervalSum([1, 2, 7, 8, 5], list1) == [23, 9, 20]
