# 247. Segment Tree Query II
# For an array, we can build a SegmentTree for it, each node stores an extra attribute count to denote the number of elements in the the array which value is between interval start and end. (The array may not fully filled by elements)

# Design a query method with three parameters root, start and end, find the number of elements in the in array's interval [start, end] by the given root of value SegmentTree.

# Example
# For array [0, 2, 3], the corresponding value Segment Tree is:

#                      [0, 3, count=3]
#                      /             \
#           [0,1,count=1]             [2,3,count=2]
#           /         \               /            \
#    [0,0,count=1] [1,1,count=0] [2,2,count=1], [3,3,count=1]
# query(1, 1), return 0

# query(1, 2), return 1

# query(2, 3), return 2

# query(0, 2), return 2

class SegmentTreeNode:
    def __init__(self, start, end, count):
        self.start, self.end, self.count = start, end, count
        self.left, self.right = None, None
        
"""
Definition of SegmentTreeNode:
class SegmentTreeNode:
    def __init__(self, start, end, count):
        self.start, self.end, self.count = start, end, count
        self.left, self.right = None, None
"""


class Solution:
    """
    @param: root: The root of segment tree.
    @param: start: start value.
    @param: end: end value.
    @return: The count number in the interval [start, end]
    """
    def query(self, root, start, end):
        # write your code here
        
        "divide and conquer"
        
        if root is None or start > end:
            return 0
        
        if start <= root.start and end >= root.end:
            "find it"
            return root.count
            
        else:
            "3 cases: 1. all in left, 2. all in right, 3. partial left and right"
            mid = (root.start + root.end)//2
            if end <= mid:
                "all in left"
                return self.query(root.left,start,end)
            elif start > mid:
                "all in right"
                return self.query(root.right,start,end)
            else:
                "partial left and right, sum them"
                return self.query(root.left,start,mid)+ self.query(root.right,mid+1,end)