# 03. Segment Tree Modify
# For a Maximum Segment Tree, which each node has an extra value max to store the maximum value in this node's interval.

# Implement a modify function with three parameter root, index and value to change the node's value with [start, end] = [index, index] to the new given value. Make sure after this change, every node in segment tree still has the max attribute with the correct value.

# Example
# For segment tree:

#                       [1, 4, max=3]
#                     /                \
#         [1, 2, max=2]                [3, 4, max=3]
#        /              \             /             \
# [1, 1, max=2], [2, 2, max=1], [3, 3, max=0], [4, 4, max=3]
# if call modify(root, 2, 4), we can get:

#                       [1, 4, max=4]
#                     /                \
#         [1, 2, max=4]                [3, 4, max=3]
#        /              \             /             \
# [1, 1, max=2], [2, 2, max=4], [3, 3, max=0], [4, 4, max=3]
# or call modify(root, 4, 0), we can get:

#                       [1, 4, max=2]
#                     /                \
#         [1, 2, max=2]                [3, 4, max=0]
#        /              \             /             \
# [1, 1, max=2], [2, 2, max=1], [3, 3, max=0], [4, 4, max=0]
# Challenge
# Do it in O(h) time, h is the height of the segment tree.

from Algorithm.Python.Tree.SegmentTree.SegmentTreeBuildII import Solution

class SegmentTreeNode:
    def __init__(self, start, end, max):
        self.start, self.end, self.max = start, end, max
        self.left, self.right = None, None

"""
Definition of SegmentTreeNode:
class SegmentTreeNode:
    def __init__(self, start, end, max):
        self.start, self.end, self.max = start, end, max
        self.left, self.right = None, None
"""

class Solution2:
    """
    @param root: The root of segment tree.
    @param index: index.
    @param value: value
    @return: nothing
    """
    def modify(self, root, index, value):
        # write your code here

        "when we going down, record all the node in a list. after modify, we need to recalculate max of each one in the list from last one to first one"
        parentlist = []
        self.modifyHelper(root, index, value, parentlist)
        "from last to first, update its max"
        for i in range(len(parentlist) - 1, -1, -1):
            parentlist[i].max = max(parentlist[i].left.max, parentlist[i].right.max)

        return root

    def modifyHelper(self, root, index, value, parentlist):

        if index < root.start or index > root.end:
            return

        if index == root.start and index == root.end:
            "find it"
            root.max = value
            return

        mid = (root.start + root.end) // 2
        parentlist.append(root)
        if index <= mid:
            "in the left"
            self.modifyHelper(root.left, index, value, parentlist)
        else:
            "in the right"
            self.modifyHelper(root.right, index, value, parentlist)

tree = Solution().build([-1,2,1,0,3])
assert Solution2().modify(tree,2,4)

"""
Definition of SegmentTreeNode:
class SegmentTreeNode:
    def __init__(self, start, end, max):
        self.start, self.end, self.max = start, end, max
        self.left, self.right = None, None
"""


class Solution3:
    """
    @param root: The root of segment tree.
    @param index: index.
    @param value: value
    @return: nothing
    """

    def modify(self, root, index, value):
        # write your code here

        "do not use extra space. after we find and omdify the node(either left or right child), wethen  modify the node itself"

        if root.start == index and root.end == index:
            root.max = value
            return

        if index < root.start or index > root.end:
            "not in this region"
            return

        mid = (root.start + root.end) // 2

        if index <= mid:
            "in left side"
            self.modify(root.left, index, value)
        elif index > mid:
            "in right side"
            self.modify(root.right, index, value)

        "then do not forget to update current node after lefr/right child are updated"
        "if we come to this step, index must be in this region"
        root.max = max(root.left.max, root.right.max)
