# 1292. Odd Even Linked List
# Given a singly linked list, group all odd nodes together followed by the even nodes. Please note here we are talking about the node number and not the value in the nodes.
#
# Example
# Example:
# Given 1->2->3->4->5->NULL,
# return 1->3->5->2->4->NULL.
#
# Notice
# The relative order inside both the even and odd groups should remain as it was in the input.
# The first node is considered odd, the second node even and so on ...



"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""


class Solution:
	"""
	@param head: a singly linked list
	@return: Modified linked list
	"""
	
	def oddEvenList (self, head):
		# write your code here
		
		if head is None or head.next is None or head.next.next is None:
			return head
		
		p1 = head
		p2 = head.next
		
		oddhead = head
		evenhead = head.next
		
		# now
		# two list, odd list, p.next = p.next.next , even list, p.next = p.next.next
		
		while p2 is not None and p2.next is not None:
			p1.next = p2.next
			p2.next = p2.next.next
			p1 = p1.next
			p2 = p2.next
		
		# the p2 maybe None or p2.next maybe None
		
		# find last odd and point its next to head of even
		p1.next = evenhead
		
		return head