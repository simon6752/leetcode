# 380. Intersection of Two Linked Lists
# Write a program to find the node at which the intersection of two singly linked lists begins.
#
# Example
# The following two linked lists:
#
# A:          a1 → a2
# ↘
# c1 → c2 → c3
# ↗
# B:     b1 → b2 → b3
# begin to intersect at node c1.
#
# Challenge
# Your code should preferably run in O(n) time and use only O(1) memory.


"""
Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None
"""


class Solution:
    """
    @param: headA: the first list
    @param: headB: the second list
    @return: a ListNode
    """
    def getIntersectionNode(self, headA, headB):
        # write your code here
        "naive solution: two pointers iterate A & B,  use hash table to record visited nodes, 1st node in the other hash table is the intersection"
        "optimize: when iterator reach end, connect to head of B. then become detect cycle II in liked list"
        
        if headA is None or headB is None:
            return None
        
        p1 = headA
        p2 = headA # fast pointer 
        
        while True:
            
            if p2 is None:
                p2 = headB
            elif p2.next is None:
                p2.next = headB
            
            if p1 is None:
                "p1 must reached end of headB"
                p1 = headB
            p1 = p1.next 
            p2 = p2.next.next
            
            if p1 == p2:
                "found the 1st node that fast meet slow "
                break 
        
        "now we need to use p3 to start from begining"
        
        p3 = headA 
        
        while p3 != p2:
            p3 = p3.next 
            if p2 is None:
                p2 = headB
            if p2.next is None:
                p2.next = headB
            p2 = p2.next 
            
        return p3 
        