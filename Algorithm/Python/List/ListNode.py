class ListNode:

    def __init__(self, val, next=None):
        self.val = val
        self.next = next

    def getListNodeListFromArray(valList):
        "initialize a Linked List use a list: [1,2,3] to 1->2->3"
        head = ListNode(0)
        result = head

        for i in range(0, len(valList)):
            node = ListNode(valList[i])
            head.next = node
            head = head.next

        return result.next

    def equal(self, node):
        if self.val == node.val:
            if self.next != None and node.next != None and self.next.equal(node.next):
                return True
            if self.next == None and node.next == None:
                return True

        return False

    def stringToList(s):
        " change string 3760->2881->7595->3904->5069->4421->8560->8879->8488->5040->5792->56->1007->2270->3403->6062->null to list"

        if s is None or s == "null":
            return None

        a = s.split("->")

        head = ListNode(int(a[0]))
        cur = head
        for i in range(1, len(a)):
            if a[i] != "null":
                cur.next = ListNode((int)(a[i]))
                cur = cur.next
            else:
                cur.next = None

        return head



# initialize a ListNode from list

list1 = [1, 2, 3]
ListNode.getListNodeListFromArray(list1)

a = ListNode(1)
b = ListNode(2)
c = ListNode(1)
assert c.equal(a)
a.next = b
c.next = b
assert c.equal(a)
