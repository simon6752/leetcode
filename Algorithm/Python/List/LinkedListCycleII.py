# 103. Linked List Cycle II
# Given a linked list, return the node where the cycle begins.
#
# If there is no cycle, return null.
#
# Example
# Given -21->10->4->5, tail connects to node index 1，return 10
#
# Challenge
# Follow up:
#
# Can you solve it without using extra space?
#



"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""

# my version v1, not pass all test cases, but idea is right
from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param: head: The first node of linked list.
    @return: The node where the cycle begins. if there is no cycle, return null
    """
    def detectCycle(self, head):
        # write your code here

        # head -> (a nodes) -> 1st circle node ->(b nodes) -> p1 -> (c nodes) -> null -> 1st node ->(b nodes) -> p2 meet p1.
        # so we have a + b + c + b = 2(a + b ) => a = c
        # so when p2 meet p1, use p3 from head. when p1 meet 1st node, p3 reach 1st circle node

        # wrong ! faster pointer may have made several rounds before reach slower pointer!
        # a + n(b+c) + b = 2(a + b )
        # a -b - c  + (n-1)(b+c) + b = 2 (a + b -b - c   )
        # so we need counter to record how many steps we take for p1 and p2 meet ?
        # we need to know a, we can record counter m, which is (a+b)
        # we can also record when p1 and p2 meet for a second time ?
        #  total length is a+b + c = len(total list)
        # let p1 and p2 meet again: this time both p1 and p2 are in the cycle.  they took (b+c) steps to meet again
        # once we have a+b, b+c, we can figure out n and a
        # when p1 and p2 1st time meet, record the steps needed for them to second time reach this node. then we know that b + c
        # start from beginning and let p1 and p2 meet again and count how many times p2 reached 1st time met position, then we know n!!!
        # with value a+b, b+c, n, we know a and c, b, done
        "solution: use 3 pointers"

        if head is None:
            return None

            # dummy = ListNode(0)
        # dummy.next = head


        p1 = head
        p2 = head.next # faster pointer
        m1 = 0 # will record 1st time p1 meet p2, p1 moves a + b
        m2 = 1 # will record 2nd time p1 meet p2, p1 moves b +c, which is the length of loop

        while p1 is not None and p2 is not None and p1 != p2:

            if p2.next is None:
                "reached end, not cycle"
                return None
            else:
                "move one step"
                p2 = p2.next

            if p1.next is None:
                return None
            else:
                "move one step"
                p1 = p1.next
                m1 += 1

            if p2.next is None:
                return None
            elif p2 == p1:
                "reached p1, cycle"
                break
            else:
                "p2 moves one more step"
                p2 = p2.next

            "in each iteration, p2 moves 2 steps"

        if p1 != p2:
            return None

        "p1 == p2 at this step"
        p4 = p2 # record this meet step

        while p2.next != p4:
            p2 = p2.next
            m2 += 1

        "start from begining, count how many times p2 meet p4 when p1 1st time meet p2"


        p1 = head
        p2 = head.next

        n = 0 # first time p2 met p4 not count, but last time p2 met p4 is not count
        # but p2 runs 1 step more, so last time p2 meet p4 is counted
        while p1 is not None and p2 is not None and (p1 != p2  or (p1 == p2 and p1 ==head)):

            if p2 == p4:
                n += 1

            if p2.next is None:
                "reached end, not cycle"
                return None
            else:
                "move one step"
                p2 = p2.next

            if p1.next is None:
                return None
            else:
                "move one step"
                p1 = p1.next

            if p2 == p4:
                n += 1
            if p2.next is None:
                return None
            elif p2 == p1:
                "reached p1, cycle"
                break
            else:
                "p2 moves one more step"
                p2 = p2.next

            "in each iteration, p2 moves 2 steps"
        if n > 0:
            n -= 1

        "now we have m1 = a + b, m2 = b+c, a + n*(b+c) + b = 2*(a+b)"
        a = 2 * m1 - n * m2
        print(m1)
        print(m2)
        print(n)

        p3 = head
        for i in range(0,a):
            p3 = p3.next

        "p3 is the 1st node of cycle"
        return p3

# -21->10->17->8->4->26->5->35->33->-7->-16->27->-12->6->29->-12->5->9->20->14->14->2->13->-24->21->23->-21->5
# tail connects to node index 24

liststring = "-21->10->17->8->4->26->5->35->33->-7->-16->27->-12->6->29->-12->5->9->20->14->14->2->13->-24->21->23->-21->5"

a = ListNode.stringToList(liststring)
node2 =  a 

for i in range(0,24):
    node2 = node2.next 

node3 = node2 

for i in range(0,3):
    node2 = node2.next 

node2.next = node3 
#  a = 24, b = 0, c = 4, n = 6
assert Solution().detectCycle(a) == node3



# solution V2:

#   head -> a nodes -> 1st in cycle ->  b nodes -> p1 meets p2 -> c nodes -> end of list -> 1st in cycle
# a  + b + n(b +c ) =  2* (a+b) => a  = - b + n*(b+c)  =>  a = c +  (n-1)*(b + c) n>= 1
# when p3 start from head, move a , p2 start from (b)(use 1st node in cycle as zero), move a = c+ (n-1) *(b + c), stop at n*(b+c), which is exactly 1st node in element
#  so p3 will first time meet p2



"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""


class Solution2:
    """
    @param: head: The first node of linked list.
    @return: The node where the cycle begins. if there is no cycle, return null
    """

    def detectCycle(self, head):
        # write your code here
        #   head -> a nodes -> 1st in cycle ->  b nodes -> p1 meets p2 -> c nodes -> end of list -> 1st in cycle
        # a  + b + n(b +c ) =  2* (a+b) => a  = - b + n*(b+c)  =>  a = c +  (n-1)*(b + c) n>= 1
        # when p3 start from head, move a , p2 start from (b)(use 1st node in cycle as zero), move a = c+ (n-1) *(b + c), stop at n*(b+c), which is exactly 1st node in element
        #  so p3 will first time meet p2

        if head is None or head.next is None:
            return None

        if head.next == head:
            return head

        p1 = head
        p2 = head

        while (p1 != p2 or (p1 == p2 and p1 == head)):
            if p2 is None or p2.next is None:
                "no cycle "
                return None

            p2 = p2.next.next
            p1 = p1.next

        "now p1 meet p2 "
        p3 = head

        while p3 != p2:
            p3 = p3.next
            p2 = p2.next

        return p3



# -21->10->17->8->4->26->5->35->33->-7->-16->27->-12->6->29->-12->5->9->20->14->14->2->13->-24->21->23->-21->5
# tail connects to node index 24

liststring = "-21->10->17->8->4->26->5->35->33->-7->-16->27->-12->6->29->-12->5->9->20->14->14->2->13->-24->21->23->-21->5"

a = ListNode.stringToList(liststring)
node2 =  a

for i in range(0,24):
    node2 = node2.next

node3 = node2

for i in range(0,3):
    node2 = node2.next

node2.next = node3
#  a = 24, b = 0, c = 4, n = 6
assert Solution2().detectCycle(a) == node3

"""
ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution3:
    """
    @param head: The first node of the linked list.
    @return: the node where the cycle begins.
                If there is no cycle, return null
    """
    def detectCycle(self, head):
        # write your code here
        if head == None or head.next == None:
            return None
        slow = fast = head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
            if fast == slow:
                break
        if slow == fast:
            slow = head
            while slow != fast:
                slow = slow.next
                fast = fast.next
            return slow
        return None
        