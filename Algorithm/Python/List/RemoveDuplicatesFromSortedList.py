# 112. Remove Duplicates from Sorted List
# Given a sorted linked list, delete all duplicates such that each element appear only once.

# Example
# Given 1->1->2, return 1->2.
# Given 1->1->2->3->3, return 1->2->3.

from Algorithm.Python.List.ListNode import ListNode

# solution 1 is wrong if we do not continue to keep current position
class Solution1:
    """
    @param head: head is the head of the linked list
    @return: head of linked list
    """
    def deleteDuplicates(self, head):
        # write your code here
        # single pointer 
            
        p = head 
        
        while p is not None:
            
            if p.next != None and p.val == p.next.val:
                p.next = p.next.next 
                # if we want to keep on current position, need to continue here 
                continue 
                
            # move 
            p = p.next 
            
        return head 


# Input 
# -14->-14->-13->-13->-13->-13->-12->-12->-11->-10->-9->-9->-9->-9->-9->-9->-9->-9->-8->-8->-8->-7->-7->-7->-4->-4->-4->-4->-2->-1->-1->-1->-1->-1->-1->0->0->1->1->2->2->2->2->2->3->3->4->5->5->5->5->5->5->6->6->6->7->7->7->7->7->8->8->9->9->10->10->10->10->11->11->12->13->14->14->14->15->15->15->16->17->17->17->17->17->17->18->18->19->19->19->20->20->20->20->20->21->22->22->23->23->23->23->24->25->25->25->25->25->null

# Expected
# -14->-13->-12->-11->-10->-9->-8->-7->-4->-2->-1->0->1->2->3->4->5->6->7->8->9->10->11->12->13->14->15->16->17->18->19->20->21->22->23->24->25->null

input = ListNode.stringToList("-14->-14->-13->-13->-13->-13->-12->-12->-11->-10->-9->-9->-9->-9->-9->-9->-9->-9->-8->-8->-8->-7->-7->-7->-4->-4->-4->-4->-2->-1->-1->-1->-1->-1->-1->0->0->1->1->2->2->2->2->2->3->3->4->5->5->5->5->5->5->6->6->6->7->7->7->7->7->8->8->9->9->10->10->10->10->11->11->12->13->14->14->14->15->15->15->16->17->17->17->17->17->17->18->18->19->19->19->20->20->20->20->20->21->22->22->23->23->23->23->24->25->25->25->25->25")
expected = ListNode.stringToList("-14->-13->-12->-11->-10->-9->-8->-7->-4->-2->-1->0->1->2->3->4->5->6->7->8->9->10->11->12->13->14->15->16->17->18->19->20->21->22->23->24->25")
 
assert Solution1().deleteDuplicates(input) == expected

"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""

class Solution:
    """
    @param head: head is the head of the linked list
    @return: head of linked list
    """
    def deleteDuplicates(self, head):
        # write your code here
        # single pointer 
            
        p = head 
        
        while p is not None:
            
            while p.next != None and p.val == p.next.val:
                p.next = p.next.next 
                
            p = p.next 
            
        return head 