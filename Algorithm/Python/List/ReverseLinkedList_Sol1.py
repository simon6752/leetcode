# Reverse a linked list.
#
# Have you met this question in a real interview?
# Example
# For linked list 1->2->3, the reversed linked list is 3->2->1
#
# Challenge
# Reverse it in-place and in one-pass


"""
Definition of ListNode

class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param head: n
    @return: The new head of reversed linked list.
    """

    def reverse(self, head):
        # write your code here
        "use 3 pointers iteration"
        "use tail, p, q, close to each other, move next one step at a time together, tail -> p -> q => tail <- p <- q, then move all 3 pointers forward for 1 step"

        "  1 (tail) -> 2(p) -> 3(q) -> 4  =>  1(tail) <- 2(p) <- 3(q)  4 => 1 <- 2(tail) <- 3(p)   4(q)"

        if head == None or head.next == None:
            return head

        "special case: two elements"
        if head.next.next == None:
            temp = head
            head.next.next = head
            head = head.next
            temp.next = None
            return head

        "at least two elments"

        tail = head
        p = tail.next
        q = p.next

        temp = None
        "record the head before tail"
        previoushead = None
        while q != None:
            temp = q.next
            q.next = p
            p.next = tail
            tail.next = previoushead

            "move one step"
            previoushead = tail
            tail = p
            p = q
            q = temp

        return p


#  test cases

list1 = [1, 2, 3]
reverselist1 = list1[::-1]

assert ListNode.getListNodeListFromArray(reverselist1).equal(Solution().reverse(ListNode.getListNodeListFromArray(list1)))
