# Facebook intervew 8/3 New York office

# TODO 99. Reorder List
# Description
# Given a singly linked list L: L0 → L1 → … → Ln-1 → Ln
#
# reorder it to: L0 → Ln → L1 → Ln-1 → L2 → Ln-2 → …
#
#
# Example
# Given 1->2->3->4->null, reorder it to 1->4->2->3->null.
#
# Challenge
# Can you do this in-place without altering the nodes' values?