# # We have a new version of the editor! Click to try it!
# 104. Merge K Sorted Lists
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Merge k sorted linked lists and return it as one sorted list.
#
# Analyze and describe its complexity.
#
# Have you met this question in a real interview?
# Example
# Given lists:
#
# [
#   2->4->null,
#   null,
#   -1->null
# ],
# return -1->2->4->null.
#
# Tags
# Facebook Priority Queue Divide and Conquer Heap Uber Google Twitter LinkedIn Airbnb Linked List Microsoft Amazon IXL


"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
from queue import PriorityQueue

from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param lists: a list of ListNode
    @return: The head of one sorted list.
    """
    def mergeKLists(self, lists):
        # write your code here
        "1. merge 2 sorted list, then one by one. that is O(2*k*n), this will exceed time limit 2. use k pointers, we need to find minimum among them and move ahead, use priority queue"

        if lists is None or len(lists) <= 0:
            return None

        plist = list(lists)

        q = PriorityQueue()

        for i in range(0, len(lists)):
            if plist[i] is not None:
                "q.put((priority, value)) into priority queue"
                q.put(( plist[i].val, i))

        dummy = ListNode(0)
        head = dummy

        while not q.empty():
            "return turple (priority,value) in ascending order"
            i = q.get()[1]
            head.next = plist[i]
            head = head.next
            plist[i] = plist[i].next
            if plist[i] is not None:
                q.put((plist[i].val,i))

        return dummy.next

list1 = [
  ListNode.getListNodeListFromArray([2, 4]),
  None,
  ListNode(-1)
]

assert Solution().mergeKLists(list1).equal(ListNode.getListNodeListFromArray([-1,2,4]))

list1 = [
  ListNode.getListNodeListFromArray([2]),
  None,
  ListNode(-1)
]

assert Solution().mergeKLists(list1).equal(ListNode.getListNodeListFromArray([-1,2]))

