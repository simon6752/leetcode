# reverse linked list II
#
# 36. Reverse Linked List II
#  Description
#  Notes
#  Testcase
#  Judge
# Discuss
# Reverse a linked list from position m to n.
#
#  Notice
# Given m, n satisfy the following condition: 1 ≤ m ≤ n ≤ length of list.
#
# Have you met this question in a real interview?
# Example
# Given 1->2->3->4->5->NULL, m = 2 and n = 4, return 1->4->3->2->5->NULL.
#
# Challenge
# Reverse it in-place and in one-pass
#
# Tags
# Linked List

"""
Definition of ListNode
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param head: ListNode head is the head of the linked list
    @param m: An integer
    @param n: An integer
    @return: The head of the reversed ListNode
    """

    def reverseBetween(self, head, m, n):
        # write your code here

        "two pointers"

        if head is None or head.next is None or m == n:
            return head

        "1. naive solution, find position m, n, use reverse linked list 1 to reverse m->n, then add head and end region"
        "2. use 3 pointers, from start to end, reverse the linked list"

        count = 0

        p1 = None
        p2 = head
        p3 = head.next

        " reverse node from m -> n to m <- n, then m-1 point to n, m point to n+1"

        while count < m - 1:
            p1 = p2
            p2 = p3
            p3 = p3.next
            count += 1
        "after this, m-1 step moved, p1 point to m-1, p2 point to m, p3 point to m + 1 "

        mhead = p1
        mNode = p2

        while count < n - 1:
            count += 1
            temp = p3.next
            p2.next = p1
            p3.next = p2
            "move all 3 pointers one more step"
            p1 = p2
            p2 = p3
            p3 = temp

        "n-1 step moved, p1 point to n-1, p2 point to n, p3 point to n+1"
        "now we need to point to right position: m-1 to n, m point to n+1"
        mNode.next = p3
        if mhead is None:
            "m == 1, we need to return n since n will be the new head"
            head = p2
        else:
            "else we point our m-1 node's next to n"
            mhead.next = p2

        return head


head = ListNode.stringToList(
    "3760->2881->7595->3904->5069->4421->8560->8879->8488->5040->5792->56->1007->2270->3403->6062->null")

assert Solution().reverseBetween(head, 2, 7).equal(ListNode.stringToList(
    "3760->8560->4421->5069->3904->7595->2881->8879->8488->5040->5792->56->1007->2270->3403->6062->null"))

head = ListNode.stringToList(
    "2074->2258->4517->9186->9335->9534->1213->924->5816->5812->8919->8282->553->7969->5394->475->5871->1972->2327->2157->9931->4285->1071->120->6308->5151->5939->2313->1681->9681->7716->4521->4713->3063->3034->768->1421->9507->1590->6079->5520->427->8412->961->2210->8479->9092->1863->1705->6752->1399->4188->7290->1177->6682->8979->3602->6909->8326->9361->null")

assert Solution().reverseBetween(head, 1, 12).equal(ListNode.stringToList(
    "8282->8919->5812->5816->924->1213->9534->9335->9186->4517->2258->2074->553->7969->5394->475->5871->1972->2327->2157->9931->4285->1071->120->6308->5151->5939->2313->1681->9681->7716->4521->4713->3063->3034->768->1421->9507->1590->6079->5520->427->8412->961->2210->8479->9092->1863->1705->6752->1399->4188->7290->1177->6682->8979->3602->6909->8326->9361->null"))
