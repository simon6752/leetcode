
# 105. Copy List with Random Pointer
# A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.
#
# Return a deep copy of the list.
#
# Challenge
# Could you solve it with O(1) space?
#
# -1->null, [null]
#


class RandomListNode:
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None

"""
Definition for singly-linked list with a random pointer.
class RandomListNode:
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None
"""


class Solution:
    # @param head: A RandomListNode
    # @return: A RandomListNode
    def copyRandomList(self, head):
        # write your code here
        "use hash table to save ready visted nodes"

        "save the mapping relation between old and new node"
        nodesdict = {}

        dummynew = RandomListNode(0)
        dummynew.next = self.copyRandomListHelper(head,nodesdict)

        self.assignRandom(head,nodesdict,dummynew.next)

        return dummynew.next

    def assignRandom(self,root,nodesdict,newnode):

        if root is None:
            return

        if root.random is not None and root.random in nodesdict.keys():
            "we can assume that old root's random must be in the nodesdict"
            newnode.random = nodesdict[root.random]

        self.assignRandom(root.next,nodesdict,newnode.next)

    "recursive"

    def copyRandomListHelper(self,root,nodesdict):

        if root is None:
            return None


        if root in nodesdict.keys():
            newnode = nodesdict(root)
        else:
            newnode = RandomListNode(root.label)
            "put new node into nodesdict"
            nodesdict[root] = newnode

        if root.next is not None and root.next in nodesdict.keys():
            newnode.next = nodesdict(root.next)
        else:
            newnode.next = self.copyRandomListHelper(root.next,nodesdict)



        return newnode


    "do it without recursion,use while loop"


"""
Definition for singly-linked list with a random pointer.
class RandomListNode:
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None
"""



class Solution2:
    # @param head: A RandomListNode
    # @return: A RandomListNode
    def copyRandomList(self, head):
        # write your code here
        "do it use while loop, no recursion"

        if head is None:
            return None

        "save old:new mapping relation"
        nodesdict = {}

        old = head
        dummy = RandomListNode(0)
        pre = dummy

        while old is not None:

            if old not in nodesdict.keys():
                pre.next = RandomListNode(old.label)
                nodesdict[old] = pre.next
            else:
                pre.next = nodesdict[old]

            "update random"

            if old.random is not None:
                if old.random in nodesdict.keys():
                    pre.next.random= nodesdict[old.random]
                else:
                    pre.next.random = RandomListNode(old.random.label)
                    "update nodesdict"
                    nodesdict[old.random] = pre.next.random

            "move forward"
            old = old.next
            pre = pre.next

        return dummy.next





    # challenge: do it in O(1) space
    "use next to save copy: 1->1' ->2 -> 2'... so the old-new mapping is saved"