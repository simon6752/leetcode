# Reverse a linked list.
#
# Have you met this question in a real interview?
# Example
# For linked list 1->2->3, the reversed linked list is 3->2->1
#
# Challenge
# Reverse it in-place and in one-pass


"""
Definition of ListNode

class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
from Algorithm.Python.List.ListNode import ListNode


class Solution:
    """
    @param head: n
    @return: The new head of reversed linked list.
    """

    def reverse(self, head):
        # write your code here
        "use recursion"
        "1-2->3...->n  => n->...3->2->1  or 1<-2<-3...<-n"
        "1->2->3...->n => 1->null, n->...->3->2->null  => n->...->3->2->1->null"

        if head == None or head.next == None:
            return head

        curNode = head
        next = curNode.next
        "set curNode.next to None, so it is removed from current list"
        curNode.next = None
        newhead = self.reverse(next)

        "set node2 next to node1, attach node1 to tail of reverse of rest list"
        next.next = curNode

        return newhead


#  test cases

list1 = [1, 2, 3]
reverselist1 = list1[::-1]

assert ListNode.getListNodeListFromArray(reverselist1).equal(Solution().reverse(ListNode.getListNodeListFromArray(list1)))
