# Reverse a linked list.
#
# Have you met this question in a real interview?
# Example
# For linked list 1->2->3, the reversed linked list is 3->2->1
#
# Challenge
# Reverse it in-place and in one-pass


"""
Definition of ListNode

class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""

from Algorithm.Python.List.ListNode import ListNode

class Solution:
    """
    @param head: n
    @return: The new head of reversed linked list.
    """

    def reverse(self, head):
        # write your code here
        "idea: use dummy node, two lists and 3 pointers"
        "two pointer point to head of two list: old and new, remove a node from old and insert to head of new list: 1->2->3  =>    3->2->1"

        "speical case"
        if head == None or head.next == None:
            return head

        dummy = ListNode(0)

        "insert a dummy node before head"
        dummy.next = head

        "we can start from dummy node"

        start = dummy

        curNode = start.next
        temp = None

        "result list is waiting for insert. It is None at the beginning"
        result = None

        while curNode != None:
            "record current node's next"
            temp = curNode.next
            "remove current node out of current list"
            start.next = curNode.next
            "current node point to result list"
            curNode.next = result
            "result list moves, to include current node"
            result = curNode
            "current node point to its previous next node to start next iteration"
            curNode = temp

        return result


#  test cases

list1 = [1, 2, 3]
reverselist1 = list1[::-1]

assert ListNode.getListNodeListFromArray(reverselist1).equal(Solution().reverse(ListNode.getListNodeListFromArray(list1)))
