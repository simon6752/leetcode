# TODO  223. Palindrome Linked List
# Description
# Implement a function to check if a linked list is a palindrome.
#
# Have you met this question in a real interview?
# Example
# Given 1->2->1, return true
#
# Challenge
# Could you do it in O(n) time and O(1) space?