# 178. Graph Valid Tree
# Given n nodes labeled from 0 to n - 1 and a list of undirected edges (each edge is a pair of nodes), write a function to check whether these edges make up a valid tree.
#
# Example
# Given n = 5 and edges = [[0, 1], [0, 2], [0, 3], [1, 4]], return true.
#
# Given n = 5 and edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]], return false.


"version 1, not work for multi-graph"
"for multi-graph, we will not tree it as valid tree"
#  for a valid tree, 1. edges length is exactly n-1, no more(loop),no less(multi graph) 2. start from any node, should be able to traverse all nodes, no more(loop), no less(multi-graph)
class Solution:
    """
    @param n: An integer
    @param edges: a list of undirected edges
    @return: true if it's a valid tree, or false
    """
    def validTree(self, n, edges):
        # write your code here
        "use BFS to traverse, if no loop, then it is a tree. use hashset to record visited nodes"

        "first, we build neighbors for each node, then BFS, and detect loop"

        "if there is loop, node will point to a visited non-parent node"

        "edge cases"
        if n == 0 or n == 1:
            return True

        if edges is None or len(edges) != n-1 :
            return False

        nodesdict = {}


        for i in range(0 ,n):
            nodesdict[i] = set()

        level = 0

        for i in range(0 ,len(edges)):

            if edges[i][1] not in nodesdict[edges[i][0]]:
                nodesdict[edges[i][0]].add(edges[i][1])

            if edges[i][0] not in nodesdict[edges[i][1]]:
                nodesdict[edges[i][1]].add(edges[i][0])

        loopExist = False

        visitednodes = {}
        nodeparent = {}
        "visitednodes saves key:value, key is node label, value is level"

        queue = []
        "put node 0 into queue, 0 as root. 0's parent is itself"
        queue.append(0)
        nodeparent[0] = 0
        "initialize root 0's parent(0) level to -1"
        visitednodes[0 ]= -1

        while len(queue) != 0:
            node = queue.pop(-1)
            visitednodes[node] = visitednodes[nodeparent[node]]  + 1

            for neighbor in nodesdict[node]:
                if neighbor not in visitednodes:
                    queue.append(neighbor)

                else:
                    if (visitednodes[neighbor] < visitednodes[node] ) and nodeparent[node] != neighbor:
                        "we found a neighbor that is already visited, with smaller level, and  is not node's parent, loop exists "
                        loopExist = True
                        break

                nodeparent[neighbor] = node

        "1st, no loop exist, 2nd, start from any node(0 in our case), we should be able to traverse the whole graph"
        return (not loopExist) and len(visitednodes) == n


assert Solution().validTree(5,[[0, 1], [0, 2], [0, 3], [1, 4]]) is True
assert Solution().validTree(5, [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]) is False

# Input
# 10
# [[0,1],[5,6],[6,7],[9,0],[3,7],[4,8],[1,8],[5,2],[5,3]]
# Output
# true
# Expected
# false

assert Solution().validTree(10,[[0,1],[5,6],[6,7],[9,0],[3,7],[4,8],[1,8],[5,2],[5,3]]) is False
assert Solution().validTree(1,[[0,0],[0,0],[0,0]]) is False
assert Solution().validTree(2,[[0,0],[0,0],[0,0],[0,1]]) is False
