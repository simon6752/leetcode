# 127. Topological Sorting
# Description
# Given an directed graph, a topological order of the graph nodes is defined as follow:
#
# For each directed edge A -> B in graph, A must before B in the order list.
# The first node in the order can be any node in the graph with no nodes direct to it.
# Find any topological order for the given graph.
#
# You can assume that there is at least one topological order in the graph.
#
# Have you met this question in a real interview?
# Clarification
# Learn more about representation of graphs
#
# Example
# For graph as follow:
#
# picture
#
# The topological order can be:
#
# [0, 1, 2, 3, 4, 5]
# [0, 2, 3, 1, 5, 4]
# ...
# Challenge
# Can you do it in both BFS and DFS?


"""
Definition for a Directed graph node
class DirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []
"""

"DFS"
"""
Definition for a Directed graph node
class DirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []
"""


"""
Definition for a Directed graph node
class DirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []
"""


class Solution:
    

    
    """
    @param: graph: A list of Directed graph node
    @return: Any topological order for the given graph.
    """
    def topSort(self, graph):
        # write your code here
        
        if graph is None:
            return [] 
            
        "use hash table to save permanent done nodes and temporarily visited nodes "
        permanent = {}
        temporary = {}
        topsort = []
        
        " use DFS "
        
        for i in range(0,len(graph)):
            if graph[i] not in permanent :
                self.dfs(graph[i],permanent,temporary,topsort)
            
        return topsort 
        
    def dfs(self,graph, permanent, temporary, topsort):
        
        if graph in permanent:
            return 
        
        if graph in temporary and temporary[graph] == 1:
            "met visited parent node, not a valid DAG"
            return 
        
        temporary[graph] = 1
        for node in graph.neighbors:
            self.dfs(node,permanent,temporary,topsort)
        
        temporary[graph] = 0 
        permanent[graph] = 1
        "insert to head of topsort "
        topsort.insert(0,graph)
            
        
                
"Try BFS "      