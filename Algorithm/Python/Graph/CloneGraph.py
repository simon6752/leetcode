"""
Definition for a undirected graph node
class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []
"""

"my version, fail"


class Solution:
    """
    @param: node: A undirected graph node
    @return: A undirected graph node
    """

    def cloneGraph(self, node):
        # write your code here
        "do this recursively or iteratively(use queue). use hashtable to record already visited node"

        "do it recursively first"
        if node is None:
            return None

        nodedict = {}
        resultnode = None
        resultnode = self.CloneGraphHelper(node, nodedict, resultnode)
        return resultnode

    def CloneGraphHelper(self, node, nodedict, resultnode):

        "recursively clone. when to stop? when all of its neighbors are in the dict"
        if node.label not in nodedict:
            newnode = UndirectedGraphNode(node.label)
            if len(nodedict) == 0:
                resultnode = newnode
            nodedict[newnode.label] = newnode

        "check if all neighbors of node neighbors are in nodedict"

        allin = True

        for i in range(0, len(node.neighbors)):
            if not node.neighbors[i] in nodedict.keys():
                allin = False

        if allin:
            return resultnode

        for i in range(0, len(node.neighbors)):
            if not node.neighbors[i].label in nodedict.keys():
                newnode = self.CloneGraphHelper(node.neighbors[i], nodedict, resultnode)
            resultnode.neighbors.append(nodedict[node.neighbors[i].label])

        return resultnode


class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []


head = UndirectedGraphNode(-1)

head2 = Solution().cloneGraph(head)

head = UndirectedGraphNode(-1)
node1 = UndirectedGraphNode(1)
node1.neighbors.append(node1)
head.neighbors.append(node1)

head2 = Solution().cloneGraph(head)
print(head2)

"""
Definition for a undirected graph node
class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []
"""

"version 2, BFS, pass all test cases"


class Solution2:
    """
    @param: node: A undirected graph node
    @return: A undirected graph node
    """

    def cloneGraph(self, node):
        # write your code here
        "we can assume it is single connected graph, not multigraph. so start from one node, use BFS, should be able to get all nodes"

        "use queue and hashset. BFS to get all nodes first. then generate neighbors one by one"

        if node is None:
            return None

        nodes = self.getNodes(node)
        oldnodesdict = {}
        newnodesdict = {}
        newnodes = []
        for i in range(0, len(nodes)):
            newnode = UndirectedGraphNode(nodes[i].label)
            oldnodesdict[nodes[i].label] = nodes[i]
            newnodes.append(newnode)
            newnodesdict[nodes[i].label] = newnode

        "now use the neighors relation to reproduce all neighors"

        for i in range(0, len(nodes)):

            for j in range(0, len(nodes[i].neighbors)):
                newnodesdict[nodes[i].label].neighbors.append(newnodesdict[nodes[i].neighbors[j].label])

        return newnodesdict[node.label]

    def getNodes(self, root):
        "return all nodes of a single connected graph"

        if root is None:
            return []

        nodes = []
        queue = []
        nodesdict = {}

        queue.append(root)

        while len(queue) != 0:
            "pop, get 1st element"
            node = queue.pop(-1)
            if node.label not in nodesdict.keys():
                nodesdict[node.label] = node
                nodes.append(node)

            for i in range(0, len(node.neighbors)):
                if node.neighbors[i].label not in nodesdict.keys():
                    queue.append(node.neighbors[i])

        "use BFS, we get all old nodes"
        return nodes


"""
Definition for a undirected graph node
class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []
"""

"DFS recursive"
"cannot pass self loop cases because check node in visitednodes, not node.label. now fixed"


class Solution2:
    """
    @param: node: A undirected graph node
    @return: A undirected graph node
    """

    def cloneGraph(self, node):
        # write your code here
        "use DFS recursion to clone it. for each node, we try to clone itself, then clone each of its neighbors. we need to maintain a dictionary to save cloned nodes"

        if node is None:
            return None

        if node.label in self.visitednodes.keys():
            "alread there, return existing cloned node"
            return self.visitednodes[node.label]
        else:
            "clone this node and put in dictionary"
            newnode = UndirectedGraphNode(node.label)
            self.visitednodes[node.label] = newnode

            "try node's neighbor one by one, DFS clone it and add to neighbor of new node"
            for neighbor in node.neighbors:
                newneighbor = self.cloneGraph(neighbor)
                newnode.neighbors.append(newneighbor)

            return newnode

    def __init__(self):
        self.visitednodes = {}


root = UndirectedGraphNode(0)
root.neighbors.append(root)
root.neighbors.append(root)
result = Solution2().cloneGraph(root)
print(result)

"""
Definition for a undirected graph node
class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []
"""

"solution 3, pass all test cases"


class Solution3:
    """
    @param: node: A undirected graph node
    @return: A undirected graph node
    """

    def cloneGraph(self, node):
        # write your code here
        "use DFS recursion to clone it. for each node, we try to clone itself, then clone each of its neighbors. we need to maintain a dictionary to save cloned nodes"

        if node is None:
            return None

        if node.label in self.visitednodes.keys():
            "alread there, return existing cloned node"
            return self.visitednodes[node.label]
        else:
            "clone this node and put in dictionary"
            newnode = UndirectedGraphNode(node.label)
            self.visitednodes[node.label] = newnode

            "try node's neighbor one by one, DFS clone it and add to neighbor of new node"
            for neighbor in node.neighbors:
                "neighbor maybe itself(self loop), double check!"
                if neighbor.label != node.label:
                    newneighbor = self.cloneGraph(neighbor)
                    newnode.neighbors.append(newneighbor)
                else:
                    "otherwise append edge to itself"
                    newnode.neighbors.append(newnode)

            return newnode

    def __init__(self):
        self.visitednodes = {}


class Solution4:
    # @param node, a undirected graph node
    # @return a undirected graph node
    def __init__(self):
        self.dict = {}

    def cloneGraph(self, node):
        if node == None:
            return None
        if node.label in self.dict:
            return self.dict[node.label]
        root = UndirectedGraphNode(node.label)
        self.dict[node.label] = root
        for item in node.neighbors:
            root.neighbors.append(self.cloneGraph(item))
        return root
