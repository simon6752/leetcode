
import java.util.*;
// for a valid tree, 1. edges length is exactly n-1, no more(loop),no less(multi graph) 2. start from any node, should be able to traverse all nodes, no more(loop), no less(multi-graph)
// version 1: BFS
public class GraphValidTree {
    /**
     * @param n     an integer
     * @param edges a list of undirected edges
     * @return true if it's a valid tree, or false
     */
    public boolean validTree(int n, int[][] edges) {
        if (n == 0) {
            return false;
        }

        if (edges.length != n - 1) {
            return false;
        }

        Map<Integer, Set<Integer>> graph = initializeGraph(n, edges);

        // bfs
        Queue<Integer> queue = new LinkedList<>();
        Set<Integer> hash = new HashSet<>();

        queue.offer(0);
        hash.add(0);
        while (!queue.isEmpty()) {
            int node = queue.poll();
            for (Integer neighbor : graph.get(node)) {
                if (hash.contains(neighbor)) {
//                    "no more(loop)"
                    continue;
                }
                hash.add(neighbor);
                queue.offer(neighbor);
            }
        }
//          no less
        return (hash.size() == n);
    }


    private Map<Integer, Set<Integer>> initializeGraph(int n, int[][] edges) {
        Map<Integer, Set<Integer>> graph = new HashMap<>();
        for (int i = 0; i < n; i++) {
            graph.put(i, new HashSet<Integer>());
        }

        for (int i = 0; i < edges.length; i++) {
            int u = edges[i][0];
            int v = edges[i][1];
            graph.get(u).add(v);
            graph.get(v).add(u);
        }

        return graph;
    }

    public static void main(String[] args){
        GraphValidTree solution = new GraphValidTree();
        int[][] edges = {{0,1},{5,6},{6,7},{9,0},{3,7},{4,8},{1,8},{5,2},{5,3}};
        assert solution.validTree(10,edges) == false  ;

         int[][] edges2 = {{0,0},{0,0},{0,0}};
         assert solution.validTree(1,edges2) == false ;

         int[][] edges3 = {{0,0},{0,0},{0,0},{0,1}};
         assert solution.validTree(2,edges3) == false;

         assert solution.validTree(4,edges3) == false;

    }

}

