# 615. Course Schedule
# There are a total of n courses you have to take, labeled from 0 to n - 1.

# Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

# Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?

# Example
# Given n = 2, prerequisites = [[1,0]]
# Return true

# Given n = 2, prerequisites = [[1,0],[0,1]]
# Return false

class GraphNode:
    
    def __init__(self, label):
        self.label = label 
        self.neighbors = []
        "calculate incoming degree"
        self.indegree = 0


# solution version 1, pass all test cases, but cost too much time
class Solution:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: true if can finish all courses or false
    """
    def canFinish(self, numCourses, prerequisites):
        # write your code here
        # first step, build graph 
        
        # "check cycle "
        if numCourses is None or numCourses == 0:
            return True
        
        nodes = self.buildGraph(numCourses,prerequisites)
        
        return not self.detectCycle(nodes)
        
    def detectCycle(self,nodes):
        # return whether there is cycle 
        
        onpath = {}
        visited = {}
        
        "use dfs "
        
        for node in nodes:
            if node not in visited.keys():
                if self.dfs(node, onpath, visited )  :
                    return True
        
        return False 
        
    def dfs(self,graph,onpath,visited):
        
        # visit each of its node, if find direction to a node that is on its path, there is cycle 
        # if graph in visited.keys() :
        #     return False
        
        if graph in onpath.keys() and onpath[graph] == 1:
            # current node is on the path to its parent, there is cycle
            return True 

        if graph in visited.keys() :
             return False
        
        onpath[graph] = 1 
        visited[graph] = 1
        for node in graph.neighbors:

            "visit "
            if self.dfs(node,onpath, visited):
                return True

        # after visit, move the node from the path to 0 
        onpath[graph ]= 0

        return False 
        
    def buildGraph(self,numCourses,prerequisites):
        
        if numCourses == 0:
            return None 
            
        graph = GraphNode(0)
        
        nodes = []
        for i in range(0,numCourses):
            nodes.append(GraphNode(i))
            
        for i in range(0,len(prerequisites)):
            for j in range(1,len(prerequisites[i])):
                "add neighbors for each node"
                nodes[prerequisites[i][0]].neighbors.append(nodes[prerequisites[i][j]])
                
        return nodes 

# class GraphNode:
    
#     def __init__(self, label):
#         self.label = label 
#         self.neighbors = []

# 2
# [[1,0],[0,1]]

assert Solution().canFinish(2,[[1,0],[0,1]]) is False 



# version 2, can work but too much memory used 
class Solution2:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: true if can finish all courses or false
    """
    def canFinish(self, numCourses, prerequisites):
        # write your code here
        # first step, build graph 
        
        # "check cycle "
        # for cycle detection for directed graph, if there is a pointer point to itself or any node on path to itself, there is cycle. otherwise, no cycle 
        
        if numCourses is None or numCourses == 0:
            return True
        
        nodes = self.buildGraph(numCourses,prerequisites)
        
        return not self.detectCycle(nodes)
        
    def detectCycle(self,nodes):
        # return whether there is cycle 
        
        onpath = {}
        visited = {}
        
        "use dfs "
        
        for node in nodes:
            if node not in visited.keys():
                if self.dfs(node, onpath, visited )  :
                    return True
        
        return False 
        
    def dfs(self,graph,onpath,visited):
        
        # visit each of its node, if find direction to a node that is on its path, there is cycle 
            
        onpath[graph] = 1 
        visited[graph] = 1
        
        for node in graph.neighbors:
     
            if not node in visited.keys() :
                "not visited"
                "otherwise, visit it and dfs "
                if self.dfs(node,onpath, visited):
                    return True
            
            if node in onpath.keys() and onpath[node] == 1:
                # current node is on the path to its parent, there is cycle
                return True 

        # after visit, move the node from the path to 0 
        onpath[graph ]= 0

        return False 
        
    def buildGraph(self,numCourses,prerequisites):
        
        if numCourses == 0:
            return None 
            
        graph = GraphNode(0)
        
        nodes = []
        for i in range(0,numCourses):
            nodes.append(GraphNode(i))
            
        for i in range(0,len(prerequisites)):
            for j in range(1,len(prerequisites[i])):
                "add neighbors for each node"
                nodes[prerequisites[i][0]].neighbors.append(nodes[prerequisites[i][j]])
                nodes[prerequisites[i][j]].indegree += 1 
                
        return nodes 



assert Solution2().canFinish(2,[[1,0],[0,1]]) is False 

class Solution3:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: true if can finish all courses or false
    """
    def canFinish(self, numCourses, prerequisites):
        # write your code here
        # first step, build graph 
        
        # "check cycle "
        # for cycle detection for directed graph, if there is a pointer point to itself or any node on path to itself, there is cycle. otherwise, no cycle 
        
        if numCourses is None or numCourses == 0:
            return True

        # return whether there is cycle 
        
        onpath = {}
        visited = {}
        neighbors = {}
        for i in range(0,numCourses):
            neighbors[i] = []
        
        for i in range(0,len(prerequisites)):
            # need to change prerequisites to neighbor list 
            for j in range(1,len(prerequisites[i])):
                neighbors[prerequisites[i][j]].append(prerequisites[i][0])
        
        "use dfs "
        
        for i in range(0,numCourses):
            if i not in visited.keys():
                if self.dfs(i, onpath, visited,neighbors )  :
                    return False
        
        return True 
        
    def dfs(self,graph,onpath,visited,condition):
        
        # visit each of its node, if find direction to a node that is on its path, there is cycle 
            
        onpath[graph] = 1 
        visited[graph] = 1
        
        if graph in condition.keys():
            for node in condition[graph]:
         
                if not node in visited.keys() :
                    "not visited"
                    "otherwise, visit it and dfs "
                    if self.dfs(node,onpath, visited,condition):
                        return True
                
                if node in onpath.keys() and onpath[node] == 1:
                    # current node is on the path to its parent, there is cycle
                    return True 

        # after visit, move the node from the path to 0 
        onpath[graph]= 0

        return False 


assert Solution3().canFinish(2,[[1,0],[0,1]]) is False 

# memory limit exceeded
class Solution4:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: true if can finish all courses or false
    """
    def canFinish(self, numCourses, prerequisites):
        # write your code here
        # first step, build graph or save it 
        # topological sort = cycle detect + reverse post stack of DFS 
        
        # "check cycle "
        # for cycle detection for directed graph, if there is a pointer point to itself or any node on path to itself, there is cycle. otherwise, no cycle 
        
        if numCourses is None or numCourses == 0 :
            return True 
        
        neighbors = {}
        for i in range(0,numCourses):
            neighbors[i] = []
        
        for i in range(0,len(prerequisites)):
            # need to change prerequisites to neighbor list 
            for j in range(1,len(prerequisites[i])):
                neighbors[prerequisites[i][j]].append(prerequisites[i][0])
            
        # do dfs and topological sort 
        visited = {}
        onpath = {}
        # topological sort save in result, post order dfs stack 
        result = []
        
        for i in range(0,numCourses):
            
            if i not in visited.keys():
                # if not visited, do dfs 
                if self.dfs(i, numCourses,neighbors,visited, onpath):
                    return False  
                    
        return True 
        
    def dfs(self,node, numCourses,neighbors,visited, onpath):
        # if there is cycle, return True 
        if node in onpath.keys() and onpath[node] == 1 :
            # met a node that in the path to current node, there is cycle 
            return True 
            
        if node in visited.keys():
            # visited, return 
            return False 
        
        onpath[node] = 1 
        visited[node] = 1 
        if node in neighbors.keys():
            # node has neighbors
            for neighbor in neighbors[node]:
                
                if self.dfs(neighbor,numCourses,neighbors,visited,onpath):
                    # if there is cycle when doging DFS, return True 
                    return True 
        
        # remove node from onpath 
        onpath[node] = 0 
        
        # no cycle 
        return False 
        
        
  # improve it: change way to detect cycle. Every time we find a node with 0 indgree, remove all its edges. repeat this until we cannot continue 
  # if there is no cycle, we should removed all nodes.    use BFS  

class Solution5:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: true if can finish all courses or false
    """
    def canFinish(self, numCourses, prerequisites):
        # write your code here
        # detect cycle, no topological sort yet 
        
        if numCourses is None or numCourses == 0 :
            return True 
        
        # choose a node with 0 indgree, remove all its edges, repeat until done 
        
        # build graph, need indgree for each node 
        
        indgrees = [0]  * numCourses 
        # since input prerequisites may contain duplicate edges, we need to use hash set to calculate indgrees 
        
        neighbors = []
        incoming = []
        
        for i in range(0,numCourses):
            # keep all out edge in hashset 
            neighbors.append(set())
            incoming.append(set())
            
        for i in range(0,len(prerequisites)):
            neighbors[prerequisites[i][1]].add(prerequisites[i][0])
            # update indgrees
            incoming[prerequisites[i][0]].add(prerequisites[i][1])
        
        for i in range(0,numCourses):
            indgrees[i] = len(incoming[i])
            
        queue = []
        # need a queue to keep all 0 indgree nodes during update 
        for i in range(0,len(indgrees)):
            if indgrees[i] == 0 :
                queue.append(i)
        
        nodesremoved = 0 
        while len(queue) != 0 :
            
            # remove all edges from 0 indgree nodes, unpdate queue if there is new node with zero degree 
            # count all nodes removed 
            node = queue.pop(0)
            nodesremoved += 1 
            
            for neighbor in neighbors[node]:
                # for each of its neighbor, remove edge and update indgree 
                indgrees[neighbor] -= 1
                if indgrees[neighbor] == 0 :
                    queue.append(neighbor)
                    
        # check if removed all nodes 
        return nodesremoved == numCourses
            
            

assert Solution5().canFinish(10,[[5,8],[3,5],[1,9],[4,5],[0,2],[1,9],[7,8],[4,9]]) is True 