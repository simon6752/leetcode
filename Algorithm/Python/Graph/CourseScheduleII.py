# 616. Course Schedule II
# There are a total of n courses you have to take, labeled from 0 to n - 1.
# Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

# Given the total number of courses and a list of prerequisite pairs, return the ordering of courses you should take to finish all courses.

# There may be multiple correct orders, you just need to return one of them. If it is impossible to finish all courses, return an empty array.

# Example
# Given n = 2, prerequisites = [[1,0]]
# Return [0,1]

# Given n = 4, prerequisites = [1,0],[2,0],[3,1],[3,2]]
# Return [0,1,2,3] or [0,2,1,3]

class Solution:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: the course order
    """
    def findOrder(self, numCourses, prerequisites):
        # write your code here
        # first step, build graph or save it 
        # topological sort = cycle detect + reverse post stack of DFS 
        
        # "check cycle "
        # for cycle detection for directed graph, if there is a pointer point to itself or any node on path to itself, there is cycle. otherwise, no cycle 
        
        if numCourses is None or numCourses == 0 :
            return [] 
        
        neighbors = {}
        
        for i in range(0,numCourses):
            neighbors[i] = []
        
        for i in range(0,len(prerequisites)):
            # need to change prerequisites to neighbor list 
            for j in range(1,len(prerequisites[i])):
                neighbors[prerequisites[i][j]].append(prerequisites[i][0])
            
        # do dfs and topological sort 
        visited = {}
        onpath = {}
        # topological sort save in result, post order dfs stack 
        result = []
        
        for i in range(0,numCourses):
            
            if i not in visited.keys():
                # if not visited, do dfs 
                if self.dfs(i, numCourses,neighbors,visited, onpath,result):
                    return []
                    
        return result 
        
    def dfs(self,node, numCourses,neighbors,visited, onpath,result):
        # if there is cycle, return True 
        
        if node in onpath.keys() and onpath[node] == 1 :
            # met a node that in the path to current node, there is cycle 
            return True 
            
        if node in visited.keys():
            # visited, return 
            return False 
            
        onpath[node] = 1 
        visited[node] = 1 
        if node in neighbors.keys():
            # node has neighbors
            for neighbor in neighbors[node]:
                
                if self.dfs(neighbor,numCourses,neighbors,visited,onpath,result):
                    # if there is cycle when doging DFS, return True 
                    return True 
        
        # remove node from onpath 
        onpath[node] = 0 
        
        # optional, insert into head of result
        result.insert(0,node)
        
        # no cycle 
        return False 

    # since the previous solution created two dimensional array and costed too much memory, we are going to use list of hashset to represent graph to save meory

class Solution2:
    """
    @param: numCourses: a total of n courses
    @param: prerequisites: a list of prerequisite pairs
    @return: the course order
    """
    def findOrder(self, numCourses, prerequisites):
        # write your code here
        # first step, build graph or save it 
        # topological sort = cycle detect + reverse post stack of DFS 
        # graph respresentation: 1. adjacent list 2. adjacent matrix, 3. edges 2 costs too much memory and we are going to use 1  

        # "check cycle "
        # for cycle detection for directed graph, if there is a pointer point to itself or any node on path to itself, there is cycle. otherwise, no cycle

        # build graph

        # this time we are going to use BFS. start from zero indegree nodes, then do BFS. every time, remove the edge and decrease the degree. when done, check if number of edges removed is the same as total edges and all nodes visited.

        incoming = []
        outgoing = []
        indegree = []
        visited = []
        # save topological sort result
        result = []

        for i in range (0, numCourses):
            incoming.append (set ())
            outgoing.append (set ())
            indegree.append (0)
            visited.append (0)

        for i in range (0, len (prerequisites)):
            incoming [prerequisites [i] [0]].add (prerequisites [i] [1])
            outgoing [prerequisites [i] [1]].add (prerequisites [i] [0])

        # there maybe duplicated input edges, so indegree must be calculated from length of incoming

        for i in range (0, numCourses):
            indegree [i] = len (incoming [i])
    
            # use BFS, non recursive

        queue = []
        for i in range (0, numCourses):
            if indegree [i] == 0:
                queue.append (i)

        while len (queue) != 0:
            # while queue is not empty
            # pop from 0 index
            node = queue.pop (0)
            if visited [node] == 1:
                # already visited, cycle
                return []
    
            result.append (node)
            # update indegree, check and update queue
            for neighbor in outgoing [node]:
                # for each neighbor
                indegree [neighbor] -= 1
                if indegree [neighbor] == 0:
                    queue.append (neighbor)

        if len (result) != numCourses:
            # if not all nodes are added to result, there is cycle that each node in the cycle has a non-zero indegree
            return []

        return result
                
assert Solution2().findOrder(10,[[5,8],[3,5],[1,9],[4,5],[0,2],[1,9],[7,8],[4,9]]) ==     [2,6,8,9,0,5,7,1,3,4]
        
    